# README #

This document contains information about how to run PmiRExAt and its dependencies

### What is this repository for? ###

* PmiRExAt, a new online web resource, provides the most comprehensive comparative view of plant miRNA expression in multiple tissues and developmental stages
* 1.0 Beta version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Developed by Abhijeet Panwar
* Code review
* Other guidelines
