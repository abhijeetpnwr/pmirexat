
package com.pmirexat.nabi.api.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class was generated by Apache CXF 3.0.2
 * Mon Dec 15 14:29:56 IST 2014
 * Generated source version: 3.0.2
 */

@XmlRootElement(name = "searchMaizeDbbySequenceResponse", namespace = "http://api.nabi.pmirexat.com/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchMaizeDbbySequenceResponse", namespace = "http://api.nabi.pmirexat.com/")

public class SearchMaizeDbbySequenceResponse {

    @XmlElement(name = "return")
    private java.lang.Integer _return;

    public java.lang.Integer getReturn() {
        return this._return;
    }

    public void setReturn(java.lang.Integer new_return)  {
        this._return = new_return;
    }

}

