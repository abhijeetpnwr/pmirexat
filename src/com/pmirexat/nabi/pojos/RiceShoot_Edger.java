package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="riceshoot_edger")
public class RiceShoot_Edger implements EdgeRProp 
{

	public String getMir_ids() {
		return mir_ids;
	}
	public void setMir_ids(String mir_ids) {
		this.mir_ids = mir_ids;
	}
	public float getLogFC() {
		return logFC;
	}
	public void setLogFC(float logFC) {
		this.logFC = logFC;
	}
	public float getLogCPM() {
		return logCPM;
	}
	public void setLogCPM(float logCPM) {
		this.logCPM = logCPM;
	}
	public float getPValue() {
		return PValue;
	}
	public void setPValue(float pValue) {
		PValue = pValue;
	}
	public float getFDR() {
		return FDR;
	}
	public void setFDR(float fDR) {
		FDR = fDR;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int primarykey;
	
	String mir_ids;
	float logFC,	logCPM	,PValue	,FDR;	
}
