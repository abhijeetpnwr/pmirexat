package com.pmirexat.nabi.pojos;

public interface EdgeRProp 
{

	public String getMir_ids();
	
	
	public float getLogFC();

	
	public float getLogCPM();

	
	public float getPValue();
	
	
	public float getFDR();
	

}
