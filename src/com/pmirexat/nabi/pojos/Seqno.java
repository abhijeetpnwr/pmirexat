package com.pmirexat.nabi.pojos;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="seqno")
public class Seqno
{
    public double getSequence() {
		return sequence;
	}

	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int primkey;
	
   public int getPrimkey() {
	return primkey;
}
   

public void setPrimkey(int primkey) {
	this.primkey = primkey;
}
public String getDatasetname() {
	return datasetname;
}
public void setDatasetname(String datasetname) {
	this.datasetname = datasetname;
}

private String datasetname;
   private double sequence;

public void setSequence(double numericCellValue) 
{
	// TODO Auto-generated method stub
	
	this.sequence = numericCellValue;
	
}


}
