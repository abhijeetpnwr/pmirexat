package com.pmirexat.nabi.pojos;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="passwordreset")
public class passwordreset 
{
    private String emailid;
    private Date requesttime;
    private String resetkeykey;
	
    public Date getRequesttime() {
		return requesttime;
	}
	public void setRequesttime(Date requesttime) {
		this.requesttime = requesttime;
	}
	public String getResetkeykey() {
		return resetkeykey;
	}
	public void setResetkeykey(String resetkeykey) {
		this.resetkeykey = resetkeykey;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	public int getPrimkey() 
	{
		return primkey;
	}
	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}

	public passwordreset(String emailid, Date requesttime, String resetkeykey) {
		super();
		this.emailid = emailid;
		this.requesttime = requesttime;
		this.resetkeykey = resetkeykey;
	}
	@Override
	public String toString() {
		return "passwordreset [emailid=" + emailid + ", requesttime=" + requesttime
				+ ", resetkeykey=" + resetkeykey + "]";
	}
	public passwordreset() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getresetkeykey() {
		return resetkeykey;
	}
	public void setresetkeykey(String resetkeykey) {
		this.resetkeykey = resetkeykey;
	}    
}
