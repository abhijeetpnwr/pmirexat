package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="salttable")
public class Saltpojo 
{
	
	public Saltpojo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Saltpojo(String emailid, String verificationkey, String salt) {
		super();
		this.emailid = emailid;
		this.verificationkey = verificationkey;
		this.salt = salt;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
   public String getVerificationkey() {
		return verificationkey;
	}

	public void setVerificationkey(String verificationkey) {
		this.verificationkey = verificationkey;
	}

	
	
private String emailid;

   private String verificationkey;
   public int getPrimkey() {
	return primkey;
}
public void setPrimkey(int primkey) {
	this.primkey = primkey;
}
private String salt;
   
   
public String getEmailid() {
	return emailid;
}

public void setEmailid(String emailid) {
	this.emailid = emailid;
}
public String getSalt() {
	return salt;
}
public void setSalt(String salt) {
	this.salt = salt;
}
@Override
public String toString() {
	return "Saltpojo [primkey=" + primkey + ", emailid=" + emailid + ", salt="
			+ salt + "]";
}


   
}
