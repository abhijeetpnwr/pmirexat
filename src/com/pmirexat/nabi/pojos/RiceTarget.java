package com.pmirexat.nabi.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RiceTarget
{
	
	 @Override
	public String toString() {
		return "RiceTarget [miRNA_Acc=" + miRNA_Acc + ", Target_Acc="
				+ Target_Acc + ", miRNA_aligned_fragment="
				+ miRNA_aligned_fragment + ", Target_aligned_fragment="
				+ Target_aligned_fragment + ", Inhibition=" + Inhibition
				+ ", Target_Desc=" + Target_Desc + ", Expectation="
				+ Expectation + ", miRNA_start=" + miRNA_start + ", miRNA_end="
				+ miRNA_end + ", Target_start=" + Target_start
				+ ", Target_end=" + Target_end + ", Multiplicity="
				+ Multiplicity + ", UPE=" + UPE + "]";
	}
	@Id	
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int primkey;
	
	public int getPrimkey() {
		return primkey;
	}
	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}
	public String getMiRNA_Acc() {
		return miRNA_Acc;
	}
	public void setMiRNA_Acc(String miRNA_Acc) {
		this.miRNA_Acc = miRNA_Acc;
	}
	public String getTarget_Acc() {
		return Target_Acc;
	}
	public void setTarget_Acc(String target_Acc) {
		Target_Acc = target_Acc;
	}
	public String getMiRNA_aligned_fragment() {
		return miRNA_aligned_fragment;
	}
	public void setMiRNA_aligned_fragment(String miRNA_aligned_fragment) {
		this.miRNA_aligned_fragment = miRNA_aligned_fragment;
	}
	public String getTarget_aligned_fragment() {
		return Target_aligned_fragment;
	}
	public void setTarget_aligned_fragment(String target_aligned_fragment) {
		Target_aligned_fragment = target_aligned_fragment;
	}
	public String getInhibition() {
		return Inhibition;
	}
	public void setInhibition(String inhibition) {
		Inhibition = inhibition;
	}
	public String getTarget_Desc() {
		return Target_Desc;
	}
	public void setTarget_Desc(String target_Desc) {
		Target_Desc = target_Desc;
	}
	public int getExpectation() {
		return Expectation;
	}
	public void setExpectation(int expectation) {
		Expectation = expectation;
	}
	public int getMiRNA_start() {
		return miRNA_start;
	}
	public void setMiRNA_start(int miRNA_start) {
		this.miRNA_start = miRNA_start;
	}
	public int getMiRNA_end() {
		return miRNA_end;
	}
	public void setMiRNA_end(int miRNA_end) {
		this.miRNA_end = miRNA_end;
	}
	public int getTarget_start() {
		return Target_start;
	}
	public void setTarget_start(int target_start) {
		Target_start = target_start;
	}
	public int getTarget_end() {
		return Target_end;
	}
	public void setTarget_end(int target_end) {
		Target_end = target_end;
	}
	public int getMultiplicity() {
		return Multiplicity;
	}
	public void setMultiplicity(int multiplicity) {
		Multiplicity = multiplicity;
	}
	public float getUPE() {
		return UPE;
	}
	public void setUPE(float uPE) {
		UPE = uPE;
	}
	String miRNA_Acc,Target_Acc,miRNA_aligned_fragment,Target_aligned_fragment,Inhibition;	
	
	@Column(columnDefinition="varchar(2000)")
	String Target_Desc;
	  int Expectation,miRNA_start,miRNA_end,Target_start,Target_end,Multiplicity;
	  float UPE;
}
