package com.pmirexat.nabi.pojos;

public class ArabDiffPojo {

	
	public ArabDiffPojo(String dataset1, String dataset2, String miRNA_ID,
			float logFC, float logCPM, float pValue, float fDR) {
		super();
		Dataset1 = dataset1;
		Dataset2 = dataset2;
		this.miRNA_ID = miRNA_ID;
		this.logFC = logFC;
		this.logCPM = logCPM;
		PValue = pValue;
		FDR = fDR;
	}
	String Dataset1,Dataset2,miRNA_ID;
	float logFC,logCPM,	PValue,	FDR;
	public String getDataset1() {
		return Dataset1;
	}
	public void setDataset1(String dataset1) {
		Dataset1 = dataset1;
	}
	public String getDataset2() {
		return Dataset2;
	}
	public void setDataset2(String dataset2) {
		Dataset2 = dataset2;
	}
	public String getMiRNA_ID() {
		return miRNA_ID;
	}
	public void setMiRNA_ID(String miRNA_ID) {
		this.miRNA_ID = miRNA_ID;
	}
	public float getLogFC() {
		return logFC;
	}
	public void setLogFC(float logFC) {
		this.logFC = logFC;
	}
	public float getLogCPM() {
		return logCPM;
	}
	public void setLogCPM(float logCPM) {
		this.logCPM = logCPM;
	}
	public float getPValue() {
		return PValue;
	}
	public void setPValue(float pValue) {
		PValue = pValue;
	}
	public float getFDR() {
		return FDR;
	}
	public void setFDR(float fDR) {
		FDR = fDR;
	}
}
