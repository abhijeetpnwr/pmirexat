package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@javax.persistence.Table(name="arabdb")
public class ArabPojo
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	private String mir_ids,sequence;
 
   int Arabidopsis_SRR1039931_Seed,Arabidopsis_SRR1039932_Seed,Arabidopsis_SRR1039933_Seed,Arabidopsis_SRR1039934_Seed,Arabidopsis_SRR1039935_Seed,Arabidopsis_SRR1039936_Seed,Arabidopsis_SRR1039937_Seed,Arabidopsis_SRR1039938_Seed,Arabidopsis_SRR1039939_Seed,Arabidopsis_SRR1039940_Seed,Arabidopsis_SRR1039941_Seed,Arabidopsis_SRR1039930_Seed6DAP,Arabidopsis_SRR072957_2xX2xDAP6seeds,Arabidopsis_SRR072958_4xX4xDAP6seeds,Arabidopsis_SRR072959_2xX4xDAP6seeds,Arabidopsis_SRR072960_4xX2xDAP6seeds,Arabidopsis_SRR1575168_seedling,Arabidopsis_SRR1575169_seedling,Arabidopsis_SRR1049769_seedling,Arabidopsis_SRR1049771_seedling,Arabidopsis_SRR1049773_seedling,Arabidopsis_SRR1049775_seedling,Arabidopsis_SRR1049777_seedling,Arabidopsis_SRR1049779_seedling,Arabidopsis_SRR1049781_seedling,Arabidopsis_SRR1049783_seedling,Arabidopsis_SRR1049784_seedling,Arabidopsis_SRR1049785_seedling,Arabidopsis_SRR1049786_seedling,Arabidopsis_SRR1049787_seedling,Arabidopsis_SRR1049788_seedling,Arabidopsis_SRR1163718_Seedling,Arabidopsis_SRR1164378_Seedling,Arabidopsis_SRR189803_seedling,Arabidopsis_SRR189807_seedling,Arabidopsis_SRR189811_seedling,Arabidopsis_SRR349699_Seedling,Arabidopsis_SRR349700_Seedling,Arabidopsis_SRR390740_Seedling,Arabidopsis_SRR390741_Seedling,Arabidopsis_SRR392134_Seedling,Arabidopsis_SRR392135_Seedling,Arabidopsis_SRR392136_Seedling,Arabidopsis_SRR392137_Seedling,Arabidopsis_SRR392138_Seedling,Arabidopsis_SRR392139_Seedling,Arabidopsis_SRR392140_Seedling,Arabidopsis_SRR392141_Seedling,Arabidopsis_SRR392142_Seedling,Arabidopsis_SRR392143_Seedling,Arabidopsis_SRR392144_Seedling,Arabidopsis_SRR392145_Seedling,Arabidopsis_SRR441525_seedlings,Arabidopsis_SRR441526_seedlings,Arabidopsis_SRR441527_seedlings,Arabidopsis_SRR441528_seedlings,Arabidopsis_SRR441529_seedlings,Arabidopsis_SRR441530_seedlings,Arabidopsis_SRR441531_seedlings,Arabidopsis_SRR441532_seedlings,Arabidopsis_SRR441533_seedlings,Arabidopsis_SRR441534_seedlings,Arabidopsis_SRR441535_seedlings,Arabidopsis_SRR441536_seedlings,Arabidopsis_SRR441537_seedlings,Arabidopsis_SRR441538_seedlings,Arabidopsis_SRR441539_seedlings,Arabidopsis_SRR441540_seedlings,Arabidopsis_SRR441541_seedlings,Arabidopsis_SRR441542_seedlings,Arabidopsis_SRR441543_seedlings,Arabidopsis_SRR441544_seedlings,Arabidopsis_SRR441545_seedlings,Arabidopsis_SRR441546_seedlings,Arabidopsis_SRR441547_seedlings,Arabidopsis_SRR441548_seedlings,Arabidopsis_SRR441549_seedlings,Arabidopsis_SRR441550_seedlings,Arabidopsis_SRR441551_seedlings,Arabidopsis_SRR441552_seedlings,Arabidopsis_SRR441553_seedlings,Arabidopsis_SRR441554_seedlings,Arabidopsis_SRR441555_seedlings,Arabidopsis_SRR441556_seedlings,Arabidopsis_SRR441557_seedlings,Arabidopsis_SRR441558_seedlings,Arabidopsis_SRR441559_seedlings,Arabidopsis_SRR445204_seedlings,Arabidopsis_SRR445205_seedlings,Arabidopsis_SRR445206_seedlings,Arabidopsis_SRR445207_seedlings,Arabidopsis_SRR445208_seedlings,Arabidopsis_SRR445209_seedlings,Arabidopsis_SRR445210_seedlings,Arabidopsis_SRR445211_seedlings,Arabidopsis_SRR445212_seedlings,Arabidopsis_SRR445213_seedlings,Arabidopsis_SRR445214_seedlings,Arabidopsis_SRR445215_seedlings,Arabidopsis_SRR445216_seedlings,Arabidopsis_SRR445217_seedlings,Arabidopsis_SRR445218_seedlings,Arabidopsis_SRR924263_Seedling,Arabidopsis_SRR924264_Seedling,Arabidopsis_SRR924265_Seedling,Arabidopsis_SRR924266_Seedling,Arabidopsis_SRR1055106_seedlings,Arabidopsis_SRR1055107_seedlings,Arabidopsis_SRR189802_root,Arabidopsis_SRR189806_root,Arabidopsis_SRR189810_root,Arabidopsis_SRR189813_root,Arabidopsis_SRR218085_Root,Arabidopsis_SRR218086_Root,Arabidopsis_SRR218087_Root,Arabidopsis_SRR218088_Root,Arabidopsis_SRR218089_Root,Arabidopsis_SRR218090_Root,Arabidopsis_SRR218092_Root,Arabidopsis_SRR218094_Root,Arabidopsis_SRR218095_Root,Arabidopsis_SRR218096_Root,Arabidopsis_SRR218097_Root,Arabidopsis_SRR218098_Root,Arabidopsis_SRR218099_Root,Arabidopsis_SRR218100_Root,Arabidopsis_SRR218101_Root,Arabidopsis_SRR218102_Root,Arabidopsis_SRR339951_Root,Arabidopsis_SRR493082_root,Arabidopsis_SRR493083_root,Arabidopsis_SRR522916_root,Arabidopsis_SRR922228_Root,Arabidopsis_SRR922229_Root,Arabidopsis_SRR922230_Root,Arabidopsis_SRR922231_Root,Arabidopsis_SRR029634_controlroot_control_1,Arabidopsis_SRR029635_root_lowoxygen_1,Arabidopsis_SRR032112_Root__Pi_sRNASeq,Arabidopsis_SRR032113_Root_Pi_sRNASeq,Arabidopsis_SRR037675_5WAGroot,Arabidopsis_SRR037676_5WAGroot,Arabidopsis_SRR037677_5WAGroot,Arabidopsis_SRR037678_5WAGroot,Arabidopsis_SRR037679_5WAGroot,Arabidopsis_SRR037680_5WAGroot,Arabidopsis_SRR037663_5WAGroot,Arabidopsis_SRR037664_5WAGroot,Arabidopsis_SRR037665_5WAGroot,Arabidopsis_SRR037666_5WAGroot,Arabidopsis_SRR037667_5WAGroot,Arabidopsis_SRR037668_5WAGroot,Arabidopsis_SRR037669_5WAGroot,Arabidopsis_SRR037670_5WAGroot,Arabidopsis_SRR037653_5WAGroot,Arabidopsis_SRR037654_5WAGroot,Arabidopsis_SRR037655_5WAGroot,Arabidopsis_SRR037656_5WAGroot,Arabidopsis_SRR037657_5WAGroot,Arabidopsis_SRR037658_5WAGroot,Arabidopsis_SRR037659_5WAGroot,Arabidopsis_SRR037660_5WAGroot,Arabidopsis_SRR372699_Seedling_Shoot,Arabidopsis_SRR372700_Seedling_Shoot,Arabidopsis_SRR372701_Seedling_Shoot,Arabidopsis_SRR493084_shoot,Arabidopsis_SRR493085_shoot,Arabidopsis_SRR922224_Shoot,Arabidopsis_SRR922225_Shoot,Arabidopsis_SRR922226_Shoot,Arabidopsis_SRR922227_Shoot,Arabidopsis_SRR032114_Shoot__Pi_sRNASeq,Arabidopsis_SRR032115_Shoot_Pi_sRNASeq,Arabidopsis_SRR037661_5WAGshoot,Arabidopsis_SRR037662_5WAGshoot,Arabidopsis_SRR037671_5WAGshoot,Arabidopsis_SRR037672_5WAGshoot,Arabidopsis_SRR037673_5WAGshoot,Arabidopsis_SRR037674_5WAGshoot,Arabidopsis_SRR037681_5WAGshoot,Arabidopsis_SRR037682_5WAGshoot,Arabidopsis_SRR037683_4WAGshoot,Arabidopsis_SRR037684_4WAGshoot,Arabidopsis_SRR037685_4WAGshoot,Arabidopsis_SRR037686_4WAGshoot,Arabidopsis_SRR924283_Seedling_leaf,Arabidopsis_SRR924284_Seedling_leaf,Arabidopsis_SRR1171794_leaf,Arabidopsis_SRR1171795_leaf,Arabidopsis_SRR1171796_leaf,Arabidopsis_SRR1171797_leaf,Arabidopsis_SRR1171802_leaf,Arabidopsis_SRR1171803_leaf,Arabidopsis_SRR1171804_leaf,Arabidopsis_SRR1171805_leaf,Arabidopsis_SRR1171806_leaf,Arabidopsis_SRR1171807_leaf,Arabidopsis_SRR189801_leaf,Arabidopsis_SRR189805_leaf,Arabidopsis_SRR189809_leaf,Arabidopsis_SRR1561610_leaves_mockInfection,Arabidopsis_SRR058435_leaves,Arabidopsis_SRR058436_leaves,Arabidopsis_SRR536975_leaf,Arabidopsis_SRR536976_leaf,Arabidopsis_SRR1561611_leavesvirusInfection,Arabidopsis_SRR1561612_leavesmockInfection,Arabidopsis_SRR1561613_leavesvirusInfection,Arabidopsis_SRR1023825_Wildtype3weekleaftissue,Arabidopsis_SRR1023828_Wildtype3weekleaftissue,Arabidopsis_SRR1023829_Wildtype3weekleaftissue,Arabidopsis_SRR1023830_suvh2suvh93weekleaftissue,Arabidopsis_SRR036640_H2Oleaves,Arabidopsis_SRR036641_flg22leaves,Arabidopsis_SRR402370_rosette_leaves,Arabidopsis_SRR402371_rosette_leaves,Arabidopsis_SRR452274_rossette_leaves,Arabidopsis_SRR452275_rossette_leaves,Arabidopsis_SRR452276_rossette_leaves,Arabidopsis_SRR452277_rossette_leaves,Arabidopsis_SRR452278_rossette_leaves,Arabidopsis_SRR452279_rossette_leaves,Arabidopsis_SRR786985_rosette_leaves,Arabidopsis_SRR786986_rosette_leaves,Arabidopsis_SRR786987_rosette_leaves,Arabidopsis_SRR786988_rosette_leaves,Arabidopsis_SRR072961_2xX2xrosetteleaves,Arabidopsis_SRR072962_4xX4xrosetteleaves,Arabidopsis_SRR072963_2xX4xrosetteleaves,Arabidopsis_SRR072964_4xX2xrosetteleaves,Arabidopsis_SRR515915_whole_rosette,Arabidopsis_SRR515916_whole_rosette,Arabidopsis_SRR515917_whole_rosette,Arabidopsis_SRR515918_whole_rosette,Arabidopsis_SRR515919_whole_rosette,Arabidopsis_SRR515920_whole_rosette,Arabidopsis_SRR515921_whole_rosette,Arabidopsis_SRR925853_rosette_stage,Arabidopsis_SRR925854_rosette_stage,Arabidopsis_SRR925855_rosette_stage,Arabidopsis_SRR925856_rosette_stage,Arabidopsis_SRR925858_rosette_stage,Arabidopsis_SRR925860_rosette_stage,Arabidopsis_SRR925862_rosette_stage,Arabidopsis_SRR925863_rosette_stage,Arabidopsis_SRR925864_rosette_stage,Arabidopsis_SRR950397_aerial,Arabidopsis_SRR950398_aerial,Arabidopsis_SRR950399_aerial,Arabidopsis_SRR950400_aerial,Arabidopsis_SRR1042171_inflorescence,Arabidopsis_SRR1042172_inflorescence,Arabidopsis_SRR1042173_inflorescence,Arabidopsis_SRR1042174_inflorescence,Arabidopsis_SRR1042175_inflorescence,Arabidopsis_SRR1042176_inflorescence,Arabidopsis_SRR1042177_inflorescence,Arabidopsis_SRR1042178_inflorescence,Arabidopsis_SRR1042179_inflorescence,Arabidopsis_SRR443163_immature_inflorescence,Arabidopsis_SRR443164_immature_inflorescence,Arabidopsis_SRR443165_immature_inflorescence,Arabidopsis_SRR443166_immature_inflorescence,Arabidopsis_SRR443167_immature_inflorescence,Arabidopsis_SRR443168_immature_inflorescence,Arabidopsis_SRR443169_immature_inflorescence,Arabidopsis_SRR443170_immature_inflorescence,Arabidopsis_SRR443171_immature_inflorescence,Arabidopsis_SRR443172_immature_inflorescence,Arabidopsis_SRR443173_immature_inflorescence,Arabidopsis_SRR443174_immature_inflorescence,Arabidopsis_SRR443175_immature_inflorescence,Arabidopsis_SRR896250_inflorescence,Arabidopsis_SRR896251_inflorescence,Arabidopsis_SRR896252_inflorescence,Arabidopsis_SRR896253_inflorescence,Arabidopsis_SRR896254_inflorescence,Arabidopsis_SRR896255_inflorescence,Arabidopsis_SRR014255_InflorescenceHomozygousRep1,Arabidopsis_SRR014256_InflorescenceHomozygousRep2,Arabidopsis_SRR014257_InflorescenceHomozygousRep3,Arabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1,Arabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2,Arabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3,Arabidopsis_SRR1567670_Inflorescence,Arabidopsis_SRR1567671_Inflorescence,Arabidopsis_SRR036391_InflorescenceIWR1_type,Arabidopsis_SRR036392_Inflorescencewildtype,Arabidopsis_SRR167709_immature_flowers,Arabidopsis_SRR167710_immature_flowers,Arabidopsis_SRR167711_immature_flowers,Arabidopsis_SRR1163504_Flower,Arabidopsis_SRR1163720_Flower,Arabidopsis_SRR1164382_Flower,Arabidopsis_SRR1164433_Flower,Arabidopsis_SRR1164434_Flower,Arabidopsis_SRR189800_flower,Arabidopsis_SRR189804_flower,Arabidopsis_SRR189808_flower,Arabidopsis_SRR189812_flower,Arabidopsis_SRR546141_flower,Arabidopsis_SRR546142_flower,Arabidopsis_SRR546143_flower,Arabidopsis_SRR924267_flower,Arabidopsis_SRR924268_flower,Arabidopsis_SRR924269_flower,Arabidopsis_SRR924270_flower,Arabidopsis_SRR505135_Flowerbud,Arabidopsis_SRR505136_Flowerbud,Arabidopsis_SRR505137_Flowerbud,Arabidopsis_SRR505138_Flowerbud,Arabidopsis_SRR825579_Flowerbud,Arabidopsis_SRR825580_Flowerbud,Arabidopsis_SRR825581_Flowerbud,Arabidopsis_SRR825582_Flowerbud,Arabidopsis_SRR825583_Flowerbud,Arabidopsis_SRR825584_Flowerbud,Arabidopsis_SRR863540_flowerbud,Arabidopsis_SRR863541_flowerbud,Arabidopsis_SRR863548_flowerbud,Arabidopsis_SRR863549_flowerbud,Arabidopsis_SRR1265998_unopened_flower_buds,Arabidopsis_SRR1265999_unopened_flower_buds,Arabidopsis_SRR1266000_unopened_flower_buds,Arabidopsis_SRR1266001_unopened_flower_buds,Arabidopsis_SRR1266003_unopened_flower_buds,Arabidopsis_SRR1266004_unopened_flower_buds,Arabidopsis_SRR1266005_unopened_flower_buds,Arabidopsis_SRR1266006_unopened_flower_buds,Arabidopsis_SRR1266007_unopened_flower_buds,Arabidopsis_SRR1266008_unopened_flower_buds,Arabidopsis_SRR1266009_unopened_flower_buds,Arabidopsis_SRR1266010_unopened_flower_buds,Arabidopsis_SRR1266011_unopened_flower_buds,Arabidopsis_SRR1266012_unopened_flower_buds,Arabidopsis_SRR1266013_unopened_flower_buds,Arabidopsis_SRR1266014_unopened_flower_buds,Arabidopsis_SRR1266015_unopened_flower_buds,Arabidopsis_SRR1266016_unopened_flower_buds,Arabidopsis_SRR1266017_unopened_flower_buds,Arabidopsis_SRR493033_floral,Arabidopsis_SRR493034_floral,Arabidopsis_SRR493035_floral,Arabidopsis_SRR520363_floral,Arabidopsis_SRR520364_floral,Arabidopsis_SRR520365_floral,Arabidopsis_SRR520366_floral,Arabidopsis_SRR942026_floral,Arabidopsis_SRR942027_floral,Arabidopsis_SRR942028_floral,Arabidopsis_SRR942029_floral,Arabidopsis_SRR942030_floral,Arabidopsis_SRR942031_floral,Arabidopsis_SRR942032_floral,Arabidopsis_SRR942033_floral,Arabidopsis_SRR058437_flowersHA_AGO1,Arabidopsis_SRR058438_flowersHA_AGO1,Arabidopsis_SRR064982_wild_typeflower,Arabidopsis_SRR064983_rdr6flower,Arabidopsis_SRR1023813_Wildtypefloraltissue,Arabidopsis_SRR1023814_Wildtypefloraltissue,Arabidopsis_SRR1023815_suvh2suvh9floraltissue,Arabidopsis_SRR1023816_Wildtypefloraltissue,Arabidopsis_SRR1023817_Wildtypefloraltissue,Arabidopsis_SRR1023818_suvh2suvh9floraltissue,Arabidopsis_SRR019670_floral_Col_0_rep1,Arabidopsis_SRR019671_floral_Col_0_rep2,Arabidopsis_SRR019672_floral_Col_0_rep3,Arabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0,Arabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7,Arabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2,Arabidopsis_SRR1567672_Pollen,Arabidopsis_SRR387834_Pollen,Arabidopsis_SRR387835_Pollen,Arabidopsis_SRR387836_Pollen,Arabidopsis_SRR387837_Pollen,Arabidopsis_SRR1171798_silique,Arabidopsis_SRR1171799_silique,Arabidopsis_SRR1171800_silique,Arabidopsis_SRR1171801_silique,Arabidopsis_SRR788205_Wild_Columbia,Arabidopsis_SRR788206_Wild_Columbia,Arabidopsis_SRR788207_nrpd1_4_Columbia,Arabidopsis_SRR788208_nrpe1_12_Columbia,Arabidopsis_SRR788209_shh1_1_Columbia,Arabidopsis_SRR788210_drm2_2_Columbia,Arabidopsis_SRR788211_Wild_Columbia,Arabidopsis_SRR788212_shh1_1_Columbia,Arabidopsis_SRR788213_Y140A_point_mutant_Columbia,Arabidopsis_SRR788214_F162A_point_mutant_Columbia,Arabidopsis_SRR788215_D141A_point_mutant_Columbia,Arabidopsis_SRR788216_Y212A_point_mutant_Columbia,Arabidopsis_SRR788217_wild_Columbia,Arabidopsis_SRR013343_Columbia_0wildtype_leaf,Arabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf,Arabidopsis_SRR013345_met1_3nullmutantLeaf,Arabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf,Arabidopsis_SRR1567673_Sperm;

   private float Seed_foldchange,Seedling_foldchange,Root_foldchange,Shoot_foldchange,Leaf_foldchange,Inflorescence_foldchange,Flower_foldchange,Pollen_foldchange,Silique_foldchange,Sperm_foldchange,Shannon_Entropy;
  
   public int getArabidopsis_SRR032113_Root_Pi_sRNASeq() {
	return Arabidopsis_SRR032113_Root_Pi_sRNASeq;
}
public void setArabidopsis_SRR032113_Root_Pi_sRNASeq(
		int arabidopsis_SRR032113_Root_Pi_sRNASeq) {
	Arabidopsis_SRR032113_Root_Pi_sRNASeq = arabidopsis_SRR032113_Root_Pi_sRNASeq;
}
public int getArabidopsis_SRR037675_5WAGroot() {
	return Arabidopsis_SRR037675_5WAGroot;
}
public float getRoot_foldchange() {
	return Root_foldchange;
}
public void setRoot_foldchange(float root_foldchange) {
	Root_foldchange = root_foldchange;
}
public float getShoot_foldchange() {
	return Shoot_foldchange;
}
public void setShoot_foldchange(float shoot_foldchange) {
	Shoot_foldchange = shoot_foldchange;
}
public float getLeaf_foldchange() {
	return Leaf_foldchange;
}
public void setLeaf_foldchange(float leaf_foldchange) {
	Leaf_foldchange = leaf_foldchange;
}
public float getInflorescence_foldchange() {
	return Inflorescence_foldchange;
}
public void setInflorescence_foldchange(float inflorescence_foldchange) {
	Inflorescence_foldchange = inflorescence_foldchange;
}
public float getSilique_foldchange() {
	return Silique_foldchange;
}
public void setSilique_foldchange(float silique_foldchange) {
	Silique_foldchange = silique_foldchange;
}
public void setArabidopsis_SRR037675_5WAGroot(int arabidopsis_SRR037675_5WAGroot) {
	Arabidopsis_SRR037675_5WAGroot = arabidopsis_SRR037675_5WAGroot;
}
public int getArabidopsis_SRR037676_5WAGroot() {
	return Arabidopsis_SRR037676_5WAGroot;
}
public void setArabidopsis_SRR037676_5WAGroot(int arabidopsis_SRR037676_5WAGroot) {
	Arabidopsis_SRR037676_5WAGroot = arabidopsis_SRR037676_5WAGroot;
}
public int getArabidopsis_SRR037677_5WAGroot() {
	return Arabidopsis_SRR037677_5WAGroot;
}
public void setArabidopsis_SRR037677_5WAGroot(int arabidopsis_SRR037677_5WAGroot) {
	Arabidopsis_SRR037677_5WAGroot = arabidopsis_SRR037677_5WAGroot;
}
public int getArabidopsis_SRR037678_5WAGroot() {
	return Arabidopsis_SRR037678_5WAGroot;
}
public void setArabidopsis_SRR037678_5WAGroot(int arabidopsis_SRR037678_5WAGroot) {
	Arabidopsis_SRR037678_5WAGroot = arabidopsis_SRR037678_5WAGroot;
}
public int getArabidopsis_SRR037679_5WAGroot() {
	return Arabidopsis_SRR037679_5WAGroot;
}
public void setArabidopsis_SRR037679_5WAGroot(int arabidopsis_SRR037679_5WAGroot) {
	Arabidopsis_SRR037679_5WAGroot = arabidopsis_SRR037679_5WAGroot;
}
public int getArabidopsis_SRR037680_5WAGroot() {
	return Arabidopsis_SRR037680_5WAGroot;
}
public void setArabidopsis_SRR037680_5WAGroot(int arabidopsis_SRR037680_5WAGroot) {
	Arabidopsis_SRR037680_5WAGroot = arabidopsis_SRR037680_5WAGroot;
}
public int getArabidopsis_SRR037663_5WAGroot() {
	return Arabidopsis_SRR037663_5WAGroot;
}
public void setArabidopsis_SRR037663_5WAGroot(int arabidopsis_SRR037663_5WAGroot) {
	Arabidopsis_SRR037663_5WAGroot = arabidopsis_SRR037663_5WAGroot;
}
public int getArabidopsis_SRR037664_5WAGroot() {
	return Arabidopsis_SRR037664_5WAGroot;
}
public void setArabidopsis_SRR037664_5WAGroot(int arabidopsis_SRR037664_5WAGroot) {
	Arabidopsis_SRR037664_5WAGroot = arabidopsis_SRR037664_5WAGroot;
}
public int getArabidopsis_SRR037665_5WAGroot() {
	return Arabidopsis_SRR037665_5WAGroot;
}
public void setArabidopsis_SRR037665_5WAGroot(int arabidopsis_SRR037665_5WAGroot) {
	Arabidopsis_SRR037665_5WAGroot = arabidopsis_SRR037665_5WAGroot;
}
public int getArabidopsis_SRR037666_5WAGroot() {
	return Arabidopsis_SRR037666_5WAGroot;
}
public void setArabidopsis_SRR037666_5WAGroot(int arabidopsis_SRR037666_5WAGroot) {
	Arabidopsis_SRR037666_5WAGroot = arabidopsis_SRR037666_5WAGroot;
}
public int getArabidopsis_SRR037667_5WAGroot() {
	return Arabidopsis_SRR037667_5WAGroot;
}
public void setArabidopsis_SRR037667_5WAGroot(int arabidopsis_SRR037667_5WAGroot) {
	Arabidopsis_SRR037667_5WAGroot = arabidopsis_SRR037667_5WAGroot;
}
public int getArabidopsis_SRR037668_5WAGroot() {
	return Arabidopsis_SRR037668_5WAGroot;
}
public void setArabidopsis_SRR037668_5WAGroot(int arabidopsis_SRR037668_5WAGroot) {
	Arabidopsis_SRR037668_5WAGroot = arabidopsis_SRR037668_5WAGroot;
}
public int getArabidopsis_SRR037669_5WAGroot() {
	return Arabidopsis_SRR037669_5WAGroot;
}
public void setArabidopsis_SRR037669_5WAGroot(int arabidopsis_SRR037669_5WAGroot) {
	Arabidopsis_SRR037669_5WAGroot = arabidopsis_SRR037669_5WAGroot;
}
public int getArabidopsis_SRR037670_5WAGroot() {
	return Arabidopsis_SRR037670_5WAGroot;
}
public void setArabidopsis_SRR037670_5WAGroot(int arabidopsis_SRR037670_5WAGroot) {
	Arabidopsis_SRR037670_5WAGroot = arabidopsis_SRR037670_5WAGroot;
}
public int getArabidopsis_SRR037653_5WAGroot() {
	return Arabidopsis_SRR037653_5WAGroot;
}
public void setArabidopsis_SRR037653_5WAGroot(int arabidopsis_SRR037653_5WAGroot) {
	Arabidopsis_SRR037653_5WAGroot = arabidopsis_SRR037653_5WAGroot;
}
public int getArabidopsis_SRR037654_5WAGroot() {
	return Arabidopsis_SRR037654_5WAGroot;
}
public void setArabidopsis_SRR037654_5WAGroot(int arabidopsis_SRR037654_5WAGroot) {
	Arabidopsis_SRR037654_5WAGroot = arabidopsis_SRR037654_5WAGroot;
}
public int getArabidopsis_SRR037655_5WAGroot() {
	return Arabidopsis_SRR037655_5WAGroot;
}
public void setArabidopsis_SRR037655_5WAGroot(int arabidopsis_SRR037655_5WAGroot) {
	Arabidopsis_SRR037655_5WAGroot = arabidopsis_SRR037655_5WAGroot;
}
public int getArabidopsis_SRR037656_5WAGroot() {
	return Arabidopsis_SRR037656_5WAGroot;
}
public void setArabidopsis_SRR037656_5WAGroot(int arabidopsis_SRR037656_5WAGroot) {
	Arabidopsis_SRR037656_5WAGroot = arabidopsis_SRR037656_5WAGroot;
}
public int getArabidopsis_SRR037657_5WAGroot() {
	return Arabidopsis_SRR037657_5WAGroot;
}
public void setArabidopsis_SRR037657_5WAGroot(int arabidopsis_SRR037657_5WAGroot) {
	Arabidopsis_SRR037657_5WAGroot = arabidopsis_SRR037657_5WAGroot;
}
public int getArabidopsis_SRR037658_5WAGroot() {
	return Arabidopsis_SRR037658_5WAGroot;
}
public void setArabidopsis_SRR037658_5WAGroot(int arabidopsis_SRR037658_5WAGroot) {
	Arabidopsis_SRR037658_5WAGroot = arabidopsis_SRR037658_5WAGroot;
}
public int getArabidopsis_SRR037659_5WAGroot() {
	return Arabidopsis_SRR037659_5WAGroot;
}
public void setArabidopsis_SRR037659_5WAGroot(int arabidopsis_SRR037659_5WAGroot) {
	Arabidopsis_SRR037659_5WAGroot = arabidopsis_SRR037659_5WAGroot;
}
public int getArabidopsis_SRR037660_5WAGroot() {
	return Arabidopsis_SRR037660_5WAGroot;
}
public void setArabidopsis_SRR037660_5WAGroot(int arabidopsis_SRR037660_5WAGroot) {
	Arabidopsis_SRR037660_5WAGroot = arabidopsis_SRR037660_5WAGroot;
}
public int getArabidopsis_SRR372699_Seedling_Shoot() {
	return Arabidopsis_SRR372699_Seedling_Shoot;
}
public void setArabidopsis_SRR372699_Seedling_Shoot(
		int arabidopsis_SRR372699_Seedling_Shoot) {
	Arabidopsis_SRR372699_Seedling_Shoot = arabidopsis_SRR372699_Seedling_Shoot;
}
public int getArabidopsis_SRR372700_Seedling_Shoot() {
	return Arabidopsis_SRR372700_Seedling_Shoot;
}
public void setArabidopsis_SRR372700_Seedling_Shoot(
		int arabidopsis_SRR372700_Seedling_Shoot) {
	Arabidopsis_SRR372700_Seedling_Shoot = arabidopsis_SRR372700_Seedling_Shoot;
}
public int getArabidopsis_SRR372701_Seedling_Shoot() {
	return Arabidopsis_SRR372701_Seedling_Shoot;
}
public void setArabidopsis_SRR372701_Seedling_Shoot(
		int arabidopsis_SRR372701_Seedling_Shoot) {
	Arabidopsis_SRR372701_Seedling_Shoot = arabidopsis_SRR372701_Seedling_Shoot;
}
public int getArabidopsis_SRR493084_shoot() {
	return Arabidopsis_SRR493084_shoot;
}
public void setArabidopsis_SRR493084_shoot(int arabidopsis_SRR493084_shoot) {
	Arabidopsis_SRR493084_shoot = arabidopsis_SRR493084_shoot;
}
public int getArabidopsis_SRR493085_shoot() {
	return Arabidopsis_SRR493085_shoot;
}
public void setArabidopsis_SRR493085_shoot(int arabidopsis_SRR493085_shoot) {
	Arabidopsis_SRR493085_shoot = arabidopsis_SRR493085_shoot;
}
public int getArabidopsis_SRR922224_Shoot() {
	return Arabidopsis_SRR922224_Shoot;
}
public void setArabidopsis_SRR922224_Shoot(int arabidopsis_SRR922224_Shoot) {
	Arabidopsis_SRR922224_Shoot = arabidopsis_SRR922224_Shoot;
}
public int getArabidopsis_SRR922225_Shoot() {
	return Arabidopsis_SRR922225_Shoot;
}
public void setArabidopsis_SRR922225_Shoot(int arabidopsis_SRR922225_Shoot) {
	Arabidopsis_SRR922225_Shoot = arabidopsis_SRR922225_Shoot;
}
public int getArabidopsis_SRR922226_Shoot() {
	return Arabidopsis_SRR922226_Shoot;
}
public void setArabidopsis_SRR922226_Shoot(int arabidopsis_SRR922226_Shoot) {
	Arabidopsis_SRR922226_Shoot = arabidopsis_SRR922226_Shoot;
}
public int getArabidopsis_SRR922227_Shoot() {
	return Arabidopsis_SRR922227_Shoot;
}
public void setArabidopsis_SRR922227_Shoot(int arabidopsis_SRR922227_Shoot) {
	Arabidopsis_SRR922227_Shoot = arabidopsis_SRR922227_Shoot;
}
public int getArabidopsis_SRR032114_Shoot__Pi_sRNASeq() {
	return Arabidopsis_SRR032114_Shoot__Pi_sRNASeq;
}
public void setArabidopsis_SRR032114_Shoot__Pi_sRNASeq(
		int arabidopsis_SRR032114_Shoot__Pi_sRNASeq) {
	Arabidopsis_SRR032114_Shoot__Pi_sRNASeq = arabidopsis_SRR032114_Shoot__Pi_sRNASeq;
}
public int getArabidopsis_SRR032115_Shoot_Pi_sRNASeq() {
	return Arabidopsis_SRR032115_Shoot_Pi_sRNASeq;
}
public void setArabidopsis_SRR032115_Shoot_Pi_sRNASeq(
		int arabidopsis_SRR032115_Shoot_Pi_sRNASeq) {
	Arabidopsis_SRR032115_Shoot_Pi_sRNASeq = arabidopsis_SRR032115_Shoot_Pi_sRNASeq;
}
public int getArabidopsis_SRR037661_5WAGshoot() {
	return Arabidopsis_SRR037661_5WAGshoot;
}
public void setArabidopsis_SRR037661_5WAGshoot(
		int arabidopsis_SRR037661_5WAGshoot) {
	Arabidopsis_SRR037661_5WAGshoot = arabidopsis_SRR037661_5WAGshoot;
}
public int getArabidopsis_SRR037662_5WAGshoot() {
	return Arabidopsis_SRR037662_5WAGshoot;
}
public void setArabidopsis_SRR037662_5WAGshoot(
		int arabidopsis_SRR037662_5WAGshoot) {
	Arabidopsis_SRR037662_5WAGshoot = arabidopsis_SRR037662_5WAGshoot;
}
public int getArabidopsis_SRR037671_5WAGshoot() {
	return Arabidopsis_SRR037671_5WAGshoot;
}
public void setArabidopsis_SRR037671_5WAGshoot(
		int arabidopsis_SRR037671_5WAGshoot) {
	Arabidopsis_SRR037671_5WAGshoot = arabidopsis_SRR037671_5WAGshoot;
}
public int getArabidopsis_SRR037672_5WAGshoot() {
	return Arabidopsis_SRR037672_5WAGshoot;
}
public void setArabidopsis_SRR037672_5WAGshoot(
		int arabidopsis_SRR037672_5WAGshoot) {
	Arabidopsis_SRR037672_5WAGshoot = arabidopsis_SRR037672_5WAGshoot;
}
public int getArabidopsis_SRR037673_5WAGshoot() {
	return Arabidopsis_SRR037673_5WAGshoot;
}
public void setArabidopsis_SRR037673_5WAGshoot(
		int arabidopsis_SRR037673_5WAGshoot) {
	Arabidopsis_SRR037673_5WAGshoot = arabidopsis_SRR037673_5WAGshoot;
}
public int getArabidopsis_SRR037674_5WAGshoot() {
	return Arabidopsis_SRR037674_5WAGshoot;
}
public void setArabidopsis_SRR037674_5WAGshoot(
		int arabidopsis_SRR037674_5WAGshoot) {
	Arabidopsis_SRR037674_5WAGshoot = arabidopsis_SRR037674_5WAGshoot;
}
public int getArabidopsis_SRR037681_5WAGshoot() {
	return Arabidopsis_SRR037681_5WAGshoot;
}
public void setArabidopsis_SRR037681_5WAGshoot(
		int arabidopsis_SRR037681_5WAGshoot) {
	Arabidopsis_SRR037681_5WAGshoot = arabidopsis_SRR037681_5WAGshoot;
}
public int getArabidopsis_SRR037682_5WAGshoot() {
	return Arabidopsis_SRR037682_5WAGshoot;
}
public void setArabidopsis_SRR037682_5WAGshoot(
		int arabidopsis_SRR037682_5WAGshoot) {
	Arabidopsis_SRR037682_5WAGshoot = arabidopsis_SRR037682_5WAGshoot;
}
public int getArabidopsis_SRR037683_4WAGshoot() {
	return Arabidopsis_SRR037683_4WAGshoot;
}
public void setArabidopsis_SRR037683_4WAGshoot(
		int arabidopsis_SRR037683_4WAGshoot) {
	Arabidopsis_SRR037683_4WAGshoot = arabidopsis_SRR037683_4WAGshoot;
}
public int getArabidopsis_SRR037684_4WAGshoot() {
	return Arabidopsis_SRR037684_4WAGshoot;
}
public void setArabidopsis_SRR037684_4WAGshoot(
		int arabidopsis_SRR037684_4WAGshoot) {
	Arabidopsis_SRR037684_4WAGshoot = arabidopsis_SRR037684_4WAGshoot;
}
public int getArabidopsis_SRR037685_4WAGshoot() {
	return Arabidopsis_SRR037685_4WAGshoot;
}
public void setArabidopsis_SRR037685_4WAGshoot(
		int arabidopsis_SRR037685_4WAGshoot) {
	Arabidopsis_SRR037685_4WAGshoot = arabidopsis_SRR037685_4WAGshoot;
}
public int getArabidopsis_SRR037686_4WAGshoot() {
	return Arabidopsis_SRR037686_4WAGshoot;
}
public void setArabidopsis_SRR037686_4WAGshoot(
		int arabidopsis_SRR037686_4WAGshoot) {
	Arabidopsis_SRR037686_4WAGshoot = arabidopsis_SRR037686_4WAGshoot;
}
public int getArabidopsis_SRR924283_Seedling_leaf() {
	return Arabidopsis_SRR924283_Seedling_leaf;
}
public void setArabidopsis_SRR924283_Seedling_leaf(
		int arabidopsis_SRR924283_Seedling_leaf) {
	Arabidopsis_SRR924283_Seedling_leaf = arabidopsis_SRR924283_Seedling_leaf;
}
public int getArabidopsis_SRR924284_Seedling_leaf() {
	return Arabidopsis_SRR924284_Seedling_leaf;
}
public void setArabidopsis_SRR924284_Seedling_leaf(
		int arabidopsis_SRR924284_Seedling_leaf) {
	Arabidopsis_SRR924284_Seedling_leaf = arabidopsis_SRR924284_Seedling_leaf;
}
public int getArabidopsis_SRR1171794_leaf() {
	return Arabidopsis_SRR1171794_leaf;
}
public void setArabidopsis_SRR1171794_leaf(int arabidopsis_SRR1171794_leaf) {
	Arabidopsis_SRR1171794_leaf = arabidopsis_SRR1171794_leaf;
}
public int getArabidopsis_SRR1171795_leaf() {
	return Arabidopsis_SRR1171795_leaf;
}
public void setArabidopsis_SRR1171795_leaf(int arabidopsis_SRR1171795_leaf) {
	Arabidopsis_SRR1171795_leaf = arabidopsis_SRR1171795_leaf;
}
public int getArabidopsis_SRR1171796_leaf() {
	return Arabidopsis_SRR1171796_leaf;
}
public void setArabidopsis_SRR1171796_leaf(int arabidopsis_SRR1171796_leaf) {
	Arabidopsis_SRR1171796_leaf = arabidopsis_SRR1171796_leaf;
}
public int getArabidopsis_SRR1171797_leaf() {
	return Arabidopsis_SRR1171797_leaf;
}
public void setArabidopsis_SRR1171797_leaf(int arabidopsis_SRR1171797_leaf) {
	Arabidopsis_SRR1171797_leaf = arabidopsis_SRR1171797_leaf;
}
public int getArabidopsis_SRR1171802_leaf() {
	return Arabidopsis_SRR1171802_leaf;
}
public void setArabidopsis_SRR1171802_leaf(int arabidopsis_SRR1171802_leaf) {
	Arabidopsis_SRR1171802_leaf = arabidopsis_SRR1171802_leaf;
}
public int getArabidopsis_SRR1171803_leaf() {
	return Arabidopsis_SRR1171803_leaf;
}
public void setArabidopsis_SRR1171803_leaf(int arabidopsis_SRR1171803_leaf) {
	Arabidopsis_SRR1171803_leaf = arabidopsis_SRR1171803_leaf;
}
public int getArabidopsis_SRR1171804_leaf() {
	return Arabidopsis_SRR1171804_leaf;
}
public void setArabidopsis_SRR1171804_leaf(int arabidopsis_SRR1171804_leaf) {
	Arabidopsis_SRR1171804_leaf = arabidopsis_SRR1171804_leaf;
}
public int getArabidopsis_SRR1171805_leaf() {
	return Arabidopsis_SRR1171805_leaf;
}
public void setArabidopsis_SRR1171805_leaf(int arabidopsis_SRR1171805_leaf) {
	Arabidopsis_SRR1171805_leaf = arabidopsis_SRR1171805_leaf;
}
public int getArabidopsis_SRR1171806_leaf() {
	return Arabidopsis_SRR1171806_leaf;
}
public void setArabidopsis_SRR1171806_leaf(int arabidopsis_SRR1171806_leaf) {
	Arabidopsis_SRR1171806_leaf = arabidopsis_SRR1171806_leaf;
}
public int getArabidopsis_SRR1171807_leaf() {
	return Arabidopsis_SRR1171807_leaf;
}
public void setArabidopsis_SRR1171807_leaf(int arabidopsis_SRR1171807_leaf) {
	Arabidopsis_SRR1171807_leaf = arabidopsis_SRR1171807_leaf;
}
public int getArabidopsis_SRR189801_leaf() {
	return Arabidopsis_SRR189801_leaf;
}
public void setArabidopsis_SRR189801_leaf(int arabidopsis_SRR189801_leaf) {
	Arabidopsis_SRR189801_leaf = arabidopsis_SRR189801_leaf;
}
public int getArabidopsis_SRR189805_leaf() {
	return Arabidopsis_SRR189805_leaf;
}
public void setArabidopsis_SRR189805_leaf(int arabidopsis_SRR189805_leaf) {
	Arabidopsis_SRR189805_leaf = arabidopsis_SRR189805_leaf;
}
public int getArabidopsis_SRR189809_leaf() {
	return Arabidopsis_SRR189809_leaf;
}
public void setArabidopsis_SRR189809_leaf(int arabidopsis_SRR189809_leaf) {
	Arabidopsis_SRR189809_leaf = arabidopsis_SRR189809_leaf;
}
public int getArabidopsis_SRR1561610_leaves_mockInfection() {
	return Arabidopsis_SRR1561610_leaves_mockInfection;
}
public void setArabidopsis_SRR1561610_leaves_mockInfection(
		int arabidopsis_SRR1561610_leaves_mockInfection) {
	Arabidopsis_SRR1561610_leaves_mockInfection = arabidopsis_SRR1561610_leaves_mockInfection;
}
public int getArabidopsis_SRR058435_leaves() {
	return Arabidopsis_SRR058435_leaves;
}
public void setArabidopsis_SRR058435_leaves(int arabidopsis_SRR058435_leaves) {
	Arabidopsis_SRR058435_leaves = arabidopsis_SRR058435_leaves;
}
public int getArabidopsis_SRR058436_leaves() {
	return Arabidopsis_SRR058436_leaves;
}
public void setArabidopsis_SRR058436_leaves(int arabidopsis_SRR058436_leaves) {
	Arabidopsis_SRR058436_leaves = arabidopsis_SRR058436_leaves;
}

public int getArabidopsis_SRR536975_leaf() {
	return Arabidopsis_SRR536975_leaf;
}
public void setArabidopsis_SRR536975_leaf(int arabidopsis_SRR536975_leaf) {
	Arabidopsis_SRR536975_leaf = arabidopsis_SRR536975_leaf;
}
public int getArabidopsis_SRR536976_leaf() {
	return Arabidopsis_SRR536976_leaf;
}
public void setArabidopsis_SRR536976_leaf(int arabidopsis_SRR536976_leaf) {
	Arabidopsis_SRR536976_leaf = arabidopsis_SRR536976_leaf;
}
public int getArabidopsis_SRR1561611_leavesvirusInfection() {
	return Arabidopsis_SRR1561611_leavesvirusInfection;
}
public void setArabidopsis_SRR1561611_leavesvirusInfection(
		int arabidopsis_SRR1561611_leavesvirusInfection) {
	Arabidopsis_SRR1561611_leavesvirusInfection = arabidopsis_SRR1561611_leavesvirusInfection;
}
public int getArabidopsis_SRR1561612_leavesmockInfection() {
	return Arabidopsis_SRR1561612_leavesmockInfection;
}
public void setArabidopsis_SRR1561612_leavesmockInfection(
		int arabidopsis_SRR1561612_leavesmockInfection) {
	Arabidopsis_SRR1561612_leavesmockInfection = arabidopsis_SRR1561612_leavesmockInfection;
}
public int getArabidopsis_SRR1561613_leavesvirusInfection() {
	return Arabidopsis_SRR1561613_leavesvirusInfection;
}
public void setArabidopsis_SRR1561613_leavesvirusInfection(
		int arabidopsis_SRR1561613_leavesvirusInfection) {
	Arabidopsis_SRR1561613_leavesvirusInfection = arabidopsis_SRR1561613_leavesvirusInfection;
}
public int getArabidopsis_SRR1023825_Wildtype3weekleaftissue() {
	return Arabidopsis_SRR1023825_Wildtype3weekleaftissue;
}
public void setArabidopsis_SRR1023825_Wildtype3weekleaftissue(
		int arabidopsis_SRR1023825_Wildtype3weekleaftissue) {
	Arabidopsis_SRR1023825_Wildtype3weekleaftissue = arabidopsis_SRR1023825_Wildtype3weekleaftissue;
}
public int getArabidopsis_SRR1023828_Wildtype3weekleaftissue() {
	return Arabidopsis_SRR1023828_Wildtype3weekleaftissue;
}
public void setArabidopsis_SRR1023828_Wildtype3weekleaftissue(
		int arabidopsis_SRR1023828_Wildtype3weekleaftissue) {
	Arabidopsis_SRR1023828_Wildtype3weekleaftissue = arabidopsis_SRR1023828_Wildtype3weekleaftissue;
}
public int getArabidopsis_SRR1023829_Wildtype3weekleaftissue() {
	return Arabidopsis_SRR1023829_Wildtype3weekleaftissue;
}
public void setArabidopsis_SRR1023829_Wildtype3weekleaftissue(
		int arabidopsis_SRR1023829_Wildtype3weekleaftissue) {
	Arabidopsis_SRR1023829_Wildtype3weekleaftissue = arabidopsis_SRR1023829_Wildtype3weekleaftissue;
}
public int getArabidopsis_SRR1023830_suvh2suvh93weekleaftissue() {
	return Arabidopsis_SRR1023830_suvh2suvh93weekleaftissue;
}
public void setArabidopsis_SRR1023830_suvh2suvh93weekleaftissue(
		int arabidopsis_SRR1023830_suvh2suvh93weekleaftissue) {
	Arabidopsis_SRR1023830_suvh2suvh93weekleaftissue = arabidopsis_SRR1023830_suvh2suvh93weekleaftissue;
}
public int getArabidopsis_SRR036640_H2Oleaves() {
	return Arabidopsis_SRR036640_H2Oleaves;
}
public void setArabidopsis_SRR036640_H2Oleaves(
		int arabidopsis_SRR036640_H2Oleaves) {
	Arabidopsis_SRR036640_H2Oleaves = arabidopsis_SRR036640_H2Oleaves;
}
public int getArabidopsis_SRR036641_flg22leaves() {
	return Arabidopsis_SRR036641_flg22leaves;
}
public void setArabidopsis_SRR036641_flg22leaves(
		int arabidopsis_SRR036641_flg22leaves) {
	Arabidopsis_SRR036641_flg22leaves = arabidopsis_SRR036641_flg22leaves;
}
public int getArabidopsis_SRR402370_rosette_leaves() {
	return Arabidopsis_SRR402370_rosette_leaves;
}
public void setArabidopsis_SRR402370_rosette_leaves(
		int arabidopsis_SRR402370_rosette_leaves) {
	Arabidopsis_SRR402370_rosette_leaves = arabidopsis_SRR402370_rosette_leaves;
}
public int getArabidopsis_SRR402371_rosette_leaves() {
	return Arabidopsis_SRR402371_rosette_leaves;
}
public void setArabidopsis_SRR402371_rosette_leaves(
		int arabidopsis_SRR402371_rosette_leaves) {
	Arabidopsis_SRR402371_rosette_leaves = arabidopsis_SRR402371_rosette_leaves;
}
public int getArabidopsis_SRR452274_rossette_leaves() {
	return Arabidopsis_SRR452274_rossette_leaves;
}
public void setArabidopsis_SRR452274_rossette_leaves(
		int arabidopsis_SRR452274_rossette_leaves) {
	Arabidopsis_SRR452274_rossette_leaves = arabidopsis_SRR452274_rossette_leaves;
}
public int getArabidopsis_SRR452275_rossette_leaves() {
	return Arabidopsis_SRR452275_rossette_leaves;
}
public void setArabidopsis_SRR452275_rossette_leaves(
		int arabidopsis_SRR452275_rossette_leaves) {
	Arabidopsis_SRR452275_rossette_leaves = arabidopsis_SRR452275_rossette_leaves;
}
public int getArabidopsis_SRR452276_rossette_leaves() {
	return Arabidopsis_SRR452276_rossette_leaves;
}
public void setArabidopsis_SRR452276_rossette_leaves(
		int arabidopsis_SRR452276_rossette_leaves) {
	Arabidopsis_SRR452276_rossette_leaves = arabidopsis_SRR452276_rossette_leaves;
}
public int getArabidopsis_SRR452277_rossette_leaves() {
	return Arabidopsis_SRR452277_rossette_leaves;
}
public void setArabidopsis_SRR452277_rossette_leaves(
		int arabidopsis_SRR452277_rossette_leaves) {
	Arabidopsis_SRR452277_rossette_leaves = arabidopsis_SRR452277_rossette_leaves;
}
public int getArabidopsis_SRR452278_rossette_leaves() {
	return Arabidopsis_SRR452278_rossette_leaves;
}
public void setArabidopsis_SRR452278_rossette_leaves(
		int arabidopsis_SRR452278_rossette_leaves) {
	Arabidopsis_SRR452278_rossette_leaves = arabidopsis_SRR452278_rossette_leaves;
}
public int getArabidopsis_SRR452279_rossette_leaves() {
	return Arabidopsis_SRR452279_rossette_leaves;
}
public void setArabidopsis_SRR452279_rossette_leaves(
		int arabidopsis_SRR452279_rossette_leaves) {
	Arabidopsis_SRR452279_rossette_leaves = arabidopsis_SRR452279_rossette_leaves;
}
public int getArabidopsis_SRR786985_rosette_leaves() {
	return Arabidopsis_SRR786985_rosette_leaves;
}
public void setArabidopsis_SRR786985_rosette_leaves(
		int arabidopsis_SRR786985_rosette_leaves) {
	Arabidopsis_SRR786985_rosette_leaves = arabidopsis_SRR786985_rosette_leaves;
}
public int getArabidopsis_SRR786986_rosette_leaves() {
	return Arabidopsis_SRR786986_rosette_leaves;
}
public void setArabidopsis_SRR786986_rosette_leaves(
		int arabidopsis_SRR786986_rosette_leaves) {
	Arabidopsis_SRR786986_rosette_leaves = arabidopsis_SRR786986_rosette_leaves;
}
public int getArabidopsis_SRR786987_rosette_leaves() {
	return Arabidopsis_SRR786987_rosette_leaves;
}
public void setArabidopsis_SRR786987_rosette_leaves(
		int arabidopsis_SRR786987_rosette_leaves) {
	Arabidopsis_SRR786987_rosette_leaves = arabidopsis_SRR786987_rosette_leaves;
}
public int getArabidopsis_SRR786988_rosette_leaves() {
	return Arabidopsis_SRR786988_rosette_leaves;
}
public void setArabidopsis_SRR786988_rosette_leaves(
		int arabidopsis_SRR786988_rosette_leaves) {
	Arabidopsis_SRR786988_rosette_leaves = arabidopsis_SRR786988_rosette_leaves;
}
public int getArabidopsis_SRR072961_2xX2xrosetteleaves() {
	return Arabidopsis_SRR072961_2xX2xrosetteleaves;
}
public void setArabidopsis_SRR072961_2xX2xrosetteleaves(
		int arabidopsis_SRR072961_2xX2xrosetteleaves) {
	Arabidopsis_SRR072961_2xX2xrosetteleaves = arabidopsis_SRR072961_2xX2xrosetteleaves;
}
public int getArabidopsis_SRR072962_4xX4xrosetteleaves() {
	return Arabidopsis_SRR072962_4xX4xrosetteleaves;
}
public void setArabidopsis_SRR072962_4xX4xrosetteleaves(
		int arabidopsis_SRR072962_4xX4xrosetteleaves) {
	Arabidopsis_SRR072962_4xX4xrosetteleaves = arabidopsis_SRR072962_4xX4xrosetteleaves;
}
public int getArabidopsis_SRR072963_2xX4xrosetteleaves() {
	return Arabidopsis_SRR072963_2xX4xrosetteleaves;
}
public void setArabidopsis_SRR072963_2xX4xrosetteleaves(
		int arabidopsis_SRR072963_2xX4xrosetteleaves) {
	Arabidopsis_SRR072963_2xX4xrosetteleaves = arabidopsis_SRR072963_2xX4xrosetteleaves;
}
public int getArabidopsis_SRR072964_4xX2xrosetteleaves() {
	return Arabidopsis_SRR072964_4xX2xrosetteleaves;
}
public void setArabidopsis_SRR072964_4xX2xrosetteleaves(
		int arabidopsis_SRR072964_4xX2xrosetteleaves) {
	Arabidopsis_SRR072964_4xX2xrosetteleaves = arabidopsis_SRR072964_4xX2xrosetteleaves;
}
public int getArabidopsis_SRR515915_whole_rosette() {
	return Arabidopsis_SRR515915_whole_rosette;
}
public void setArabidopsis_SRR515915_whole_rosette(
		int arabidopsis_SRR515915_whole_rosette) {
	Arabidopsis_SRR515915_whole_rosette = arabidopsis_SRR515915_whole_rosette;
}
public int getArabidopsis_SRR515916_whole_rosette() {
	return Arabidopsis_SRR515916_whole_rosette;
}
public void setArabidopsis_SRR515916_whole_rosette(
		int arabidopsis_SRR515916_whole_rosette) {
	Arabidopsis_SRR515916_whole_rosette = arabidopsis_SRR515916_whole_rosette;
}
public int getArabidopsis_SRR515917_whole_rosette() {
	return Arabidopsis_SRR515917_whole_rosette;
}
public void setArabidopsis_SRR515917_whole_rosette(
		int arabidopsis_SRR515917_whole_rosette) {
	Arabidopsis_SRR515917_whole_rosette = arabidopsis_SRR515917_whole_rosette;
}
public int getArabidopsis_SRR515918_whole_rosette() {
	return Arabidopsis_SRR515918_whole_rosette;
}
public void setArabidopsis_SRR515918_whole_rosette(
		int arabidopsis_SRR515918_whole_rosette) {
	Arabidopsis_SRR515918_whole_rosette = arabidopsis_SRR515918_whole_rosette;
}
public int getArabidopsis_SRR515919_whole_rosette() {
	return Arabidopsis_SRR515919_whole_rosette;
}
public void setArabidopsis_SRR515919_whole_rosette(
		int arabidopsis_SRR515919_whole_rosette) {
	Arabidopsis_SRR515919_whole_rosette = arabidopsis_SRR515919_whole_rosette;
}
public int getArabidopsis_SRR515920_whole_rosette() {
	return Arabidopsis_SRR515920_whole_rosette;
}
public void setArabidopsis_SRR515920_whole_rosette(
		int arabidopsis_SRR515920_whole_rosette) {
	Arabidopsis_SRR515920_whole_rosette = arabidopsis_SRR515920_whole_rosette;
}
public int getArabidopsis_SRR515921_whole_rosette() {
	return Arabidopsis_SRR515921_whole_rosette;
}
public void setArabidopsis_SRR515921_whole_rosette(
		int arabidopsis_SRR515921_whole_rosette) {
	Arabidopsis_SRR515921_whole_rosette = arabidopsis_SRR515921_whole_rosette;
}
public int getArabidopsis_SRR925853_rosette_stage() {
	return Arabidopsis_SRR925853_rosette_stage;
}
public void setArabidopsis_SRR925853_rosette_stage(
		int arabidopsis_SRR925853_rosette_stage) {
	Arabidopsis_SRR925853_rosette_stage = arabidopsis_SRR925853_rosette_stage;
}
public int getArabidopsis_SRR925854_rosette_stage() {
	return Arabidopsis_SRR925854_rosette_stage;
}
public void setArabidopsis_SRR925854_rosette_stage(
		int arabidopsis_SRR925854_rosette_stage) {
	Arabidopsis_SRR925854_rosette_stage = arabidopsis_SRR925854_rosette_stage;
}
public int getArabidopsis_SRR925855_rosette_stage() {
	return Arabidopsis_SRR925855_rosette_stage;
}
public void setArabidopsis_SRR925855_rosette_stage(
		int arabidopsis_SRR925855_rosette_stage) {
	Arabidopsis_SRR925855_rosette_stage = arabidopsis_SRR925855_rosette_stage;
}
public int getArabidopsis_SRR925856_rosette_stage() {
	return Arabidopsis_SRR925856_rosette_stage;
}
public void setArabidopsis_SRR925856_rosette_stage(
		int arabidopsis_SRR925856_rosette_stage) {
	Arabidopsis_SRR925856_rosette_stage = arabidopsis_SRR925856_rosette_stage;
}
public int getArabidopsis_SRR925858_rosette_stage() {
	return Arabidopsis_SRR925858_rosette_stage;
}
public void setArabidopsis_SRR925858_rosette_stage(
		int arabidopsis_SRR925858_rosette_stage) {
	Arabidopsis_SRR925858_rosette_stage = arabidopsis_SRR925858_rosette_stage;
}
public int getArabidopsis_SRR925860_rosette_stage() {
	return Arabidopsis_SRR925860_rosette_stage;
}
public void setArabidopsis_SRR925860_rosette_stage(
		int arabidopsis_SRR925860_rosette_stage) {
	Arabidopsis_SRR925860_rosette_stage = arabidopsis_SRR925860_rosette_stage;
}
public int getArabidopsis_SRR925862_rosette_stage() {
	return Arabidopsis_SRR925862_rosette_stage;
}
public void setArabidopsis_SRR925862_rosette_stage(
		int arabidopsis_SRR925862_rosette_stage) {
	Arabidopsis_SRR925862_rosette_stage = arabidopsis_SRR925862_rosette_stage;
}
public int getArabidopsis_SRR925863_rosette_stage() {
	return Arabidopsis_SRR925863_rosette_stage;
}
public void setArabidopsis_SRR925863_rosette_stage(
		int arabidopsis_SRR925863_rosette_stage) {
	Arabidopsis_SRR925863_rosette_stage = arabidopsis_SRR925863_rosette_stage;
}
public int getArabidopsis_SRR925864_rosette_stage() {
	return Arabidopsis_SRR925864_rosette_stage;
}
public void setArabidopsis_SRR925864_rosette_stage(
		int arabidopsis_SRR925864_rosette_stage) {
	Arabidopsis_SRR925864_rosette_stage = arabidopsis_SRR925864_rosette_stage;
}
public int getArabidopsis_SRR950397_aerial() {
	return Arabidopsis_SRR950397_aerial;
}
public void setArabidopsis_SRR950397_aerial(int arabidopsis_SRR950397_aerial) {
	Arabidopsis_SRR950397_aerial = arabidopsis_SRR950397_aerial;
}
public int getArabidopsis_SRR950398_aerial() {
	return Arabidopsis_SRR950398_aerial;
}
public void setArabidopsis_SRR950398_aerial(int arabidopsis_SRR950398_aerial) {
	Arabidopsis_SRR950398_aerial = arabidopsis_SRR950398_aerial;
}
public int getArabidopsis_SRR950399_aerial() {
	return Arabidopsis_SRR950399_aerial;
}
public void setArabidopsis_SRR950399_aerial(int arabidopsis_SRR950399_aerial) {
	Arabidopsis_SRR950399_aerial = arabidopsis_SRR950399_aerial;
}
public int getArabidopsis_SRR950400_aerial() {
	return Arabidopsis_SRR950400_aerial;
}
public void setArabidopsis_SRR950400_aerial(int arabidopsis_SRR950400_aerial) {
	Arabidopsis_SRR950400_aerial = arabidopsis_SRR950400_aerial;
}
public int getArabidopsis_SRR1042171_inflorescence() {
	return Arabidopsis_SRR1042171_inflorescence;
}
public void setArabidopsis_SRR1042171_inflorescence(
		int arabidopsis_SRR1042171_inflorescence) {
	Arabidopsis_SRR1042171_inflorescence = arabidopsis_SRR1042171_inflorescence;
}
public int getArabidopsis_SRR1042172_inflorescence() {
	return Arabidopsis_SRR1042172_inflorescence;
}
public void setArabidopsis_SRR1042172_inflorescence(
		int arabidopsis_SRR1042172_inflorescence) {
	Arabidopsis_SRR1042172_inflorescence = arabidopsis_SRR1042172_inflorescence;
}
public int getArabidopsis_SRR1042173_inflorescence() {
	return Arabidopsis_SRR1042173_inflorescence;
}
public void setArabidopsis_SRR1042173_inflorescence(
		int arabidopsis_SRR1042173_inflorescence) {
	Arabidopsis_SRR1042173_inflorescence = arabidopsis_SRR1042173_inflorescence;
}
public int getArabidopsis_SRR1042174_inflorescence() {
	return Arabidopsis_SRR1042174_inflorescence;
}
public void setArabidopsis_SRR1042174_inflorescence(
		int arabidopsis_SRR1042174_inflorescence) {
	Arabidopsis_SRR1042174_inflorescence = arabidopsis_SRR1042174_inflorescence;
}
public int getArabidopsis_SRR1042175_inflorescence() {
	return Arabidopsis_SRR1042175_inflorescence;
}
public void setArabidopsis_SRR1042175_inflorescence(
		int arabidopsis_SRR1042175_inflorescence) {
	Arabidopsis_SRR1042175_inflorescence = arabidopsis_SRR1042175_inflorescence;
}
public int getArabidopsis_SRR1042176_inflorescence() {
	return Arabidopsis_SRR1042176_inflorescence;
}
public void setArabidopsis_SRR1042176_inflorescence(
		int arabidopsis_SRR1042176_inflorescence) {
	Arabidopsis_SRR1042176_inflorescence = arabidopsis_SRR1042176_inflorescence;
}
public int getArabidopsis_SRR1042177_inflorescence() {
	return Arabidopsis_SRR1042177_inflorescence;
}
public void setArabidopsis_SRR1042177_inflorescence(
		int arabidopsis_SRR1042177_inflorescence) {
	Arabidopsis_SRR1042177_inflorescence = arabidopsis_SRR1042177_inflorescence;
}
public int getArabidopsis_SRR1042178_inflorescence() {
	return Arabidopsis_SRR1042178_inflorescence;
}
public void setArabidopsis_SRR1042178_inflorescence(
		int arabidopsis_SRR1042178_inflorescence) {
	Arabidopsis_SRR1042178_inflorescence = arabidopsis_SRR1042178_inflorescence;
}
public int getArabidopsis_SRR1042179_inflorescence() {
	return Arabidopsis_SRR1042179_inflorescence;
}
public void setArabidopsis_SRR1042179_inflorescence(
		int arabidopsis_SRR1042179_inflorescence) {
	Arabidopsis_SRR1042179_inflorescence = arabidopsis_SRR1042179_inflorescence;
}
public int getArabidopsis_SRR443163_immature_inflorescence() {
	return Arabidopsis_SRR443163_immature_inflorescence;
}
public void setArabidopsis_SRR443163_immature_inflorescence(
		int arabidopsis_SRR443163_immature_inflorescence) {
	Arabidopsis_SRR443163_immature_inflorescence = arabidopsis_SRR443163_immature_inflorescence;
}
public int getArabidopsis_SRR443164_immature_inflorescence() {
	return Arabidopsis_SRR443164_immature_inflorescence;
}
public void setArabidopsis_SRR443164_immature_inflorescence(
		int arabidopsis_SRR443164_immature_inflorescence) {
	Arabidopsis_SRR443164_immature_inflorescence = arabidopsis_SRR443164_immature_inflorescence;
}
public int getArabidopsis_SRR443165_immature_inflorescence() {
	return Arabidopsis_SRR443165_immature_inflorescence;
}
public void setArabidopsis_SRR443165_immature_inflorescence(
		int arabidopsis_SRR443165_immature_inflorescence) {
	Arabidopsis_SRR443165_immature_inflorescence = arabidopsis_SRR443165_immature_inflorescence;
}
public int getArabidopsis_SRR443166_immature_inflorescence() {
	return Arabidopsis_SRR443166_immature_inflorescence;
}
public void setArabidopsis_SRR443166_immature_inflorescence(
		int arabidopsis_SRR443166_immature_inflorescence) {
	Arabidopsis_SRR443166_immature_inflorescence = arabidopsis_SRR443166_immature_inflorescence;
}
public int getArabidopsis_SRR443167_immature_inflorescence() {
	return Arabidopsis_SRR443167_immature_inflorescence;
}
public void setArabidopsis_SRR443167_immature_inflorescence(
		int arabidopsis_SRR443167_immature_inflorescence) {
	Arabidopsis_SRR443167_immature_inflorescence = arabidopsis_SRR443167_immature_inflorescence;
}
public int getArabidopsis_SRR443168_immature_inflorescence() {
	return Arabidopsis_SRR443168_immature_inflorescence;
}
public void setArabidopsis_SRR443168_immature_inflorescence(
		int arabidopsis_SRR443168_immature_inflorescence) {
	Arabidopsis_SRR443168_immature_inflorescence = arabidopsis_SRR443168_immature_inflorescence;
}
public int getArabidopsis_SRR443169_immature_inflorescence() {
	return Arabidopsis_SRR443169_immature_inflorescence;
}
public void setArabidopsis_SRR443169_immature_inflorescence(
		int arabidopsis_SRR443169_immature_inflorescence) {
	Arabidopsis_SRR443169_immature_inflorescence = arabidopsis_SRR443169_immature_inflorescence;
}
public int getArabidopsis_SRR443170_immature_inflorescence() {
	return Arabidopsis_SRR443170_immature_inflorescence;
}
public void setArabidopsis_SRR443170_immature_inflorescence(
		int arabidopsis_SRR443170_immature_inflorescence) {
	Arabidopsis_SRR443170_immature_inflorescence = arabidopsis_SRR443170_immature_inflorescence;
}
public int getArabidopsis_SRR443171_immature_inflorescence() {
	return Arabidopsis_SRR443171_immature_inflorescence;
}
public void setArabidopsis_SRR443171_immature_inflorescence(
		int arabidopsis_SRR443171_immature_inflorescence) {
	Arabidopsis_SRR443171_immature_inflorescence = arabidopsis_SRR443171_immature_inflorescence;
}
public int getArabidopsis_SRR443172_immature_inflorescence() {
	return Arabidopsis_SRR443172_immature_inflorescence;
}
public void setArabidopsis_SRR443172_immature_inflorescence(
		int arabidopsis_SRR443172_immature_inflorescence) {
	Arabidopsis_SRR443172_immature_inflorescence = arabidopsis_SRR443172_immature_inflorescence;
}
public int getArabidopsis_SRR443173_immature_inflorescence() {
	return Arabidopsis_SRR443173_immature_inflorescence;
}
public void setArabidopsis_SRR443173_immature_inflorescence(
		int arabidopsis_SRR443173_immature_inflorescence) {
	Arabidopsis_SRR443173_immature_inflorescence = arabidopsis_SRR443173_immature_inflorescence;
}
public int getArabidopsis_SRR443174_immature_inflorescence() {
	return Arabidopsis_SRR443174_immature_inflorescence;
}
public void setArabidopsis_SRR443174_immature_inflorescence(
		int arabidopsis_SRR443174_immature_inflorescence) {
	Arabidopsis_SRR443174_immature_inflorescence = arabidopsis_SRR443174_immature_inflorescence;
}
public int getArabidopsis_SRR443175_immature_inflorescence() {
	return Arabidopsis_SRR443175_immature_inflorescence;
}
public void setArabidopsis_SRR443175_immature_inflorescence(
		int arabidopsis_SRR443175_immature_inflorescence) {
	Arabidopsis_SRR443175_immature_inflorescence = arabidopsis_SRR443175_immature_inflorescence;
}
public int getArabidopsis_SRR896250_inflorescence() {
	return Arabidopsis_SRR896250_inflorescence;
}
public void setArabidopsis_SRR896250_inflorescence(
		int arabidopsis_SRR896250_inflorescence) {
	Arabidopsis_SRR896250_inflorescence = arabidopsis_SRR896250_inflorescence;
}
public int getArabidopsis_SRR896251_inflorescence() {
	return Arabidopsis_SRR896251_inflorescence;
}
public void setArabidopsis_SRR896251_inflorescence(
		int arabidopsis_SRR896251_inflorescence) {
	Arabidopsis_SRR896251_inflorescence = arabidopsis_SRR896251_inflorescence;
}
public int getArabidopsis_SRR896252_inflorescence() {
	return Arabidopsis_SRR896252_inflorescence;
}
public void setArabidopsis_SRR896252_inflorescence(
		int arabidopsis_SRR896252_inflorescence) {
	Arabidopsis_SRR896252_inflorescence = arabidopsis_SRR896252_inflorescence;
}
public int getArabidopsis_SRR896253_inflorescence() {
	return Arabidopsis_SRR896253_inflorescence;
}
public void setArabidopsis_SRR896253_inflorescence(
		int arabidopsis_SRR896253_inflorescence) {
	Arabidopsis_SRR896253_inflorescence = arabidopsis_SRR896253_inflorescence;
}
public int getArabidopsis_SRR896254_inflorescence() {
	return Arabidopsis_SRR896254_inflorescence;
}
public void setArabidopsis_SRR896254_inflorescence(
		int arabidopsis_SRR896254_inflorescence) {
	Arabidopsis_SRR896254_inflorescence = arabidopsis_SRR896254_inflorescence;
}
public int getArabidopsis_SRR896255_inflorescence() {
	return Arabidopsis_SRR896255_inflorescence;
}
public void setArabidopsis_SRR896255_inflorescence(
		int arabidopsis_SRR896255_inflorescence) {
	Arabidopsis_SRR896255_inflorescence = arabidopsis_SRR896255_inflorescence;
}
public int getArabidopsis_SRR014255_InflorescenceHomozygousRep1() {
	return Arabidopsis_SRR014255_InflorescenceHomozygousRep1;
}
public void setArabidopsis_SRR014255_InflorescenceHomozygousRep1(
		int arabidopsis_SRR014255_InflorescenceHomozygousRep1) {
	Arabidopsis_SRR014255_InflorescenceHomozygousRep1 = arabidopsis_SRR014255_InflorescenceHomozygousRep1;
}
public int getArabidopsis_SRR014256_InflorescenceHomozygousRep2() {
	return Arabidopsis_SRR014256_InflorescenceHomozygousRep2;
}
public void setArabidopsis_SRR014256_InflorescenceHomozygousRep2(
		int arabidopsis_SRR014256_InflorescenceHomozygousRep2) {
	Arabidopsis_SRR014256_InflorescenceHomozygousRep2 = arabidopsis_SRR014256_InflorescenceHomozygousRep2;
}
public int getArabidopsis_SRR014257_InflorescenceHomozygousRep3() {
	return Arabidopsis_SRR014257_InflorescenceHomozygousRep3;
}
public void setArabidopsis_SRR014257_InflorescenceHomozygousRep3(
		int arabidopsis_SRR014257_InflorescenceHomozygousRep3) {
	Arabidopsis_SRR014257_InflorescenceHomozygousRep3 = arabidopsis_SRR014257_InflorescenceHomozygousRep3;
}
public int getArabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1() {
	return Arabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1;
}
public void setArabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1(
		int arabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1) {
	Arabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1 = arabidopsis_SRR014258_inflorescencewildtypeCol_0Rep1;
}
public int getArabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2() {
	return Arabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2;
}
public void setArabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2(
		int arabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2) {
	Arabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2 = arabidopsis_SRR014259_inflorescencewildtypeCol_0Rep2;
}
public int getArabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3() {
	return Arabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3;
}
public void setArabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3(
		int arabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3) {
	Arabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3 = arabidopsis_SRR014260_inflorescencewildtypeCol_0Rep3;
}
public int getArabidopsis_SRR1567670_Inflorescence() {
	return Arabidopsis_SRR1567670_Inflorescence;
}
public void setArabidopsis_SRR1567670_Inflorescence(
		int arabidopsis_SRR1567670_Inflorescence) {
	Arabidopsis_SRR1567670_Inflorescence = arabidopsis_SRR1567670_Inflorescence;
}
public int getArabidopsis_SRR1567671_Inflorescence() {
	return Arabidopsis_SRR1567671_Inflorescence;
}
public void setArabidopsis_SRR1567671_Inflorescence(
		int arabidopsis_SRR1567671_Inflorescence) {
	Arabidopsis_SRR1567671_Inflorescence = arabidopsis_SRR1567671_Inflorescence;
}
public int getArabidopsis_SRR036391_InflorescenceIWR1_type() {
	return Arabidopsis_SRR036391_InflorescenceIWR1_type;
}
public void setArabidopsis_SRR036391_InflorescenceIWR1_type(
		int arabidopsis_SRR036391_InflorescenceIWR1_type) {
	Arabidopsis_SRR036391_InflorescenceIWR1_type = arabidopsis_SRR036391_InflorescenceIWR1_type;
}
public int getArabidopsis_SRR036392_Inflorescencewildtype() {
	return Arabidopsis_SRR036392_Inflorescencewildtype;
}
public void setArabidopsis_SRR036392_Inflorescencewildtype(
		int arabidopsis_SRR036392_Inflorescencewildtype) {
	Arabidopsis_SRR036392_Inflorescencewildtype = arabidopsis_SRR036392_Inflorescencewildtype;
}
public int getArabidopsis_SRR167709_immature_flowers() {
	return Arabidopsis_SRR167709_immature_flowers;
}
public void setArabidopsis_SRR167709_immature_flowers(
		int arabidopsis_SRR167709_immature_flowers) {
	Arabidopsis_SRR167709_immature_flowers = arabidopsis_SRR167709_immature_flowers;
}
public int getArabidopsis_SRR167710_immature_flowers() {
	return Arabidopsis_SRR167710_immature_flowers;
}
public void setArabidopsis_SRR167710_immature_flowers(
		int arabidopsis_SRR167710_immature_flowers) {
	Arabidopsis_SRR167710_immature_flowers = arabidopsis_SRR167710_immature_flowers;
}
public int getArabidopsis_SRR167711_immature_flowers() {
	return Arabidopsis_SRR167711_immature_flowers;
}
public void setArabidopsis_SRR167711_immature_flowers(
		int arabidopsis_SRR167711_immature_flowers) {
	Arabidopsis_SRR167711_immature_flowers = arabidopsis_SRR167711_immature_flowers;
}
public int getArabidopsis_SRR1163504_Flower() {
	return Arabidopsis_SRR1163504_Flower;
}
public void setArabidopsis_SRR1163504_Flower(int arabidopsis_SRR1163504_Flower) {
	Arabidopsis_SRR1163504_Flower = arabidopsis_SRR1163504_Flower;
}
public int getArabidopsis_SRR1163720_Flower() {
	return Arabidopsis_SRR1163720_Flower;
}
public void setArabidopsis_SRR1163720_Flower(int arabidopsis_SRR1163720_Flower) {
	Arabidopsis_SRR1163720_Flower = arabidopsis_SRR1163720_Flower;
}
public int getArabidopsis_SRR1164382_Flower() {
	return Arabidopsis_SRR1164382_Flower;
}
public void setArabidopsis_SRR1164382_Flower(int arabidopsis_SRR1164382_Flower) {
	Arabidopsis_SRR1164382_Flower = arabidopsis_SRR1164382_Flower;
}
public int getArabidopsis_SRR1164433_Flower() {
	return Arabidopsis_SRR1164433_Flower;
}
public void setArabidopsis_SRR1164433_Flower(int arabidopsis_SRR1164433_Flower) {
	Arabidopsis_SRR1164433_Flower = arabidopsis_SRR1164433_Flower;
}
public int getArabidopsis_SRR1164434_Flower() {
	return Arabidopsis_SRR1164434_Flower;
}
public void setArabidopsis_SRR1164434_Flower(int arabidopsis_SRR1164434_Flower) {
	Arabidopsis_SRR1164434_Flower = arabidopsis_SRR1164434_Flower;
}
public int getArabidopsis_SRR189800_flower() {
	return Arabidopsis_SRR189800_flower;
}
public void setArabidopsis_SRR189800_flower(int arabidopsis_SRR189800_flower) {
	Arabidopsis_SRR189800_flower = arabidopsis_SRR189800_flower;
}
public int getArabidopsis_SRR189804_flower() {
	return Arabidopsis_SRR189804_flower;
}
public void setArabidopsis_SRR189804_flower(int arabidopsis_SRR189804_flower) {
	Arabidopsis_SRR189804_flower = arabidopsis_SRR189804_flower;
}
public int getArabidopsis_SRR189808_flower() {
	return Arabidopsis_SRR189808_flower;
}
public void setArabidopsis_SRR189808_flower(int arabidopsis_SRR189808_flower) {
	Arabidopsis_SRR189808_flower = arabidopsis_SRR189808_flower;
}
public int getArabidopsis_SRR189812_flower() {
	return Arabidopsis_SRR189812_flower;
}
public void setArabidopsis_SRR189812_flower(int arabidopsis_SRR189812_flower) {
	Arabidopsis_SRR189812_flower = arabidopsis_SRR189812_flower;
}
public int getArabidopsis_SRR546141_flower() {
	return Arabidopsis_SRR546141_flower;
}
public void setArabidopsis_SRR546141_flower(int arabidopsis_SRR546141_flower) {
	Arabidopsis_SRR546141_flower = arabidopsis_SRR546141_flower;
}
public int getArabidopsis_SRR546142_flower() {
	return Arabidopsis_SRR546142_flower;
}
public void setArabidopsis_SRR546142_flower(int arabidopsis_SRR546142_flower) {
	Arabidopsis_SRR546142_flower = arabidopsis_SRR546142_flower;
}
public int getArabidopsis_SRR546143_flower() {
	return Arabidopsis_SRR546143_flower;
}
public void setArabidopsis_SRR546143_flower(int arabidopsis_SRR546143_flower) {
	Arabidopsis_SRR546143_flower = arabidopsis_SRR546143_flower;
}
public int getArabidopsis_SRR924267_flower() {
	return Arabidopsis_SRR924267_flower;
}
public void setArabidopsis_SRR924267_flower(int arabidopsis_SRR924267_flower) {
	Arabidopsis_SRR924267_flower = arabidopsis_SRR924267_flower;
}
public int getArabidopsis_SRR924268_flower() {
	return Arabidopsis_SRR924268_flower;
}
public void setArabidopsis_SRR924268_flower(int arabidopsis_SRR924268_flower) {
	Arabidopsis_SRR924268_flower = arabidopsis_SRR924268_flower;
}
public int getArabidopsis_SRR924269_flower() {
	return Arabidopsis_SRR924269_flower;
}
public void setArabidopsis_SRR924269_flower(int arabidopsis_SRR924269_flower) {
	Arabidopsis_SRR924269_flower = arabidopsis_SRR924269_flower;
}
public int getArabidopsis_SRR924270_flower() {
	return Arabidopsis_SRR924270_flower;
}
public void setArabidopsis_SRR924270_flower(int arabidopsis_SRR924270_flower) {
	Arabidopsis_SRR924270_flower = arabidopsis_SRR924270_flower;
}
public int getArabidopsis_SRR505135_Flowerbud() {
	return Arabidopsis_SRR505135_Flowerbud;
}
public void setArabidopsis_SRR505135_Flowerbud(
		int arabidopsis_SRR505135_Flowerbud) {
	Arabidopsis_SRR505135_Flowerbud = arabidopsis_SRR505135_Flowerbud;
}
public int getArabidopsis_SRR505136_Flowerbud() {
	return Arabidopsis_SRR505136_Flowerbud;
}
public void setArabidopsis_SRR505136_Flowerbud(
		int arabidopsis_SRR505136_Flowerbud) {
	Arabidopsis_SRR505136_Flowerbud = arabidopsis_SRR505136_Flowerbud;
}
public int getArabidopsis_SRR505137_Flowerbud() {
	return Arabidopsis_SRR505137_Flowerbud;
}
public void setArabidopsis_SRR505137_Flowerbud(
		int arabidopsis_SRR505137_Flowerbud) {
	Arabidopsis_SRR505137_Flowerbud = arabidopsis_SRR505137_Flowerbud;
}
public int getArabidopsis_SRR505138_Flowerbud() {
	return Arabidopsis_SRR505138_Flowerbud;
}
public void setArabidopsis_SRR505138_Flowerbud(
		int arabidopsis_SRR505138_Flowerbud) {
	Arabidopsis_SRR505138_Flowerbud = arabidopsis_SRR505138_Flowerbud;
}
public int getArabidopsis_SRR825579_Flowerbud() {
	return Arabidopsis_SRR825579_Flowerbud;
}
public void setArabidopsis_SRR825579_Flowerbud(
		int arabidopsis_SRR825579_Flowerbud) {
	Arabidopsis_SRR825579_Flowerbud = arabidopsis_SRR825579_Flowerbud;
}
public int getArabidopsis_SRR825580_Flowerbud() {
	return Arabidopsis_SRR825580_Flowerbud;
}
public void setArabidopsis_SRR825580_Flowerbud(
		int arabidopsis_SRR825580_Flowerbud) {
	Arabidopsis_SRR825580_Flowerbud = arabidopsis_SRR825580_Flowerbud;
}
public int getArabidopsis_SRR825581_Flowerbud() {
	return Arabidopsis_SRR825581_Flowerbud;
}
public void setArabidopsis_SRR825581_Flowerbud(
		int arabidopsis_SRR825581_Flowerbud) {
	Arabidopsis_SRR825581_Flowerbud = arabidopsis_SRR825581_Flowerbud;
}
public int getArabidopsis_SRR825582_Flowerbud() {
	return Arabidopsis_SRR825582_Flowerbud;
}
public void setArabidopsis_SRR825582_Flowerbud(
		int arabidopsis_SRR825582_Flowerbud) {
	Arabidopsis_SRR825582_Flowerbud = arabidopsis_SRR825582_Flowerbud;
}
public int getArabidopsis_SRR825583_Flowerbud() {
	return Arabidopsis_SRR825583_Flowerbud;
}
public void setArabidopsis_SRR825583_Flowerbud(
		int arabidopsis_SRR825583_Flowerbud) {
	Arabidopsis_SRR825583_Flowerbud = arabidopsis_SRR825583_Flowerbud;
}
public int getArabidopsis_SRR825584_Flowerbud() {
	return Arabidopsis_SRR825584_Flowerbud;
}
public void setArabidopsis_SRR825584_Flowerbud(
		int arabidopsis_SRR825584_Flowerbud) {
	Arabidopsis_SRR825584_Flowerbud = arabidopsis_SRR825584_Flowerbud;
}
public int getArabidopsis_SRR863540_flowerbud() {
	return Arabidopsis_SRR863540_flowerbud;
}
public void setArabidopsis_SRR863540_flowerbud(
		int arabidopsis_SRR863540_flowerbud) {
	Arabidopsis_SRR863540_flowerbud = arabidopsis_SRR863540_flowerbud;
}
public int getArabidopsis_SRR863541_flowerbud() {
	return Arabidopsis_SRR863541_flowerbud;
}
public void setArabidopsis_SRR863541_flowerbud(
		int arabidopsis_SRR863541_flowerbud) {
	Arabidopsis_SRR863541_flowerbud = arabidopsis_SRR863541_flowerbud;
}
public int getArabidopsis_SRR863548_flowerbud() {
	return Arabidopsis_SRR863548_flowerbud;
}
public void setArabidopsis_SRR863548_flowerbud(
		int arabidopsis_SRR863548_flowerbud) {
	Arabidopsis_SRR863548_flowerbud = arabidopsis_SRR863548_flowerbud;
}
public int getArabidopsis_SRR863549_flowerbud() {
	return Arabidopsis_SRR863549_flowerbud;
}
public void setArabidopsis_SRR863549_flowerbud(
		int arabidopsis_SRR863549_flowerbud) {
	Arabidopsis_SRR863549_flowerbud = arabidopsis_SRR863549_flowerbud;
}
public int getArabidopsis_SRR1265998_unopened_flower_buds() {
	return Arabidopsis_SRR1265998_unopened_flower_buds;
}
public void setArabidopsis_SRR1265998_unopened_flower_buds(
		int arabidopsis_SRR1265998_unopened_flower_buds) {
	Arabidopsis_SRR1265998_unopened_flower_buds = arabidopsis_SRR1265998_unopened_flower_buds;
}
public int getArabidopsis_SRR1265999_unopened_flower_buds() {
	return Arabidopsis_SRR1265999_unopened_flower_buds;
}
public void setArabidopsis_SRR1265999_unopened_flower_buds(
		int arabidopsis_SRR1265999_unopened_flower_buds) {
	Arabidopsis_SRR1265999_unopened_flower_buds = arabidopsis_SRR1265999_unopened_flower_buds;
}
public int getArabidopsis_SRR1266000_unopened_flower_buds() {
	return Arabidopsis_SRR1266000_unopened_flower_buds;
}
public void setArabidopsis_SRR1266000_unopened_flower_buds(
		int arabidopsis_SRR1266000_unopened_flower_buds) {
	Arabidopsis_SRR1266000_unopened_flower_buds = arabidopsis_SRR1266000_unopened_flower_buds;
}
public int getArabidopsis_SRR1266001_unopened_flower_buds() {
	return Arabidopsis_SRR1266001_unopened_flower_buds;
}
public void setArabidopsis_SRR1266001_unopened_flower_buds(
		int arabidopsis_SRR1266001_unopened_flower_buds) {
	Arabidopsis_SRR1266001_unopened_flower_buds = arabidopsis_SRR1266001_unopened_flower_buds;
}
public int getArabidopsis_SRR1266003_unopened_flower_buds() {
	return Arabidopsis_SRR1266003_unopened_flower_buds;
}
public void setArabidopsis_SRR1266003_unopened_flower_buds(
		int arabidopsis_SRR1266003_unopened_flower_buds) {
	Arabidopsis_SRR1266003_unopened_flower_buds = arabidopsis_SRR1266003_unopened_flower_buds;
}
public int getArabidopsis_SRR1266004_unopened_flower_buds() {
	return Arabidopsis_SRR1266004_unopened_flower_buds;
}
public void setArabidopsis_SRR1266004_unopened_flower_buds(
		int arabidopsis_SRR1266004_unopened_flower_buds) {
	Arabidopsis_SRR1266004_unopened_flower_buds = arabidopsis_SRR1266004_unopened_flower_buds;
}
public int getArabidopsis_SRR1266005_unopened_flower_buds() {
	return Arabidopsis_SRR1266005_unopened_flower_buds;
}
public void setArabidopsis_SRR1266005_unopened_flower_buds(
		int arabidopsis_SRR1266005_unopened_flower_buds) {
	Arabidopsis_SRR1266005_unopened_flower_buds = arabidopsis_SRR1266005_unopened_flower_buds;
}
public int getArabidopsis_SRR1266006_unopened_flower_buds() {
	return Arabidopsis_SRR1266006_unopened_flower_buds;
}
public void setArabidopsis_SRR1266006_unopened_flower_buds(
		int arabidopsis_SRR1266006_unopened_flower_buds) {
	Arabidopsis_SRR1266006_unopened_flower_buds = arabidopsis_SRR1266006_unopened_flower_buds;
}
public int getArabidopsis_SRR1266007_unopened_flower_buds() {
	return Arabidopsis_SRR1266007_unopened_flower_buds;
}
public void setArabidopsis_SRR1266007_unopened_flower_buds(
		int arabidopsis_SRR1266007_unopened_flower_buds) {
	Arabidopsis_SRR1266007_unopened_flower_buds = arabidopsis_SRR1266007_unopened_flower_buds;
}
public int getArabidopsis_SRR1266008_unopened_flower_buds() {
	return Arabidopsis_SRR1266008_unopened_flower_buds;
}
public void setArabidopsis_SRR1266008_unopened_flower_buds(
		int arabidopsis_SRR1266008_unopened_flower_buds) {
	Arabidopsis_SRR1266008_unopened_flower_buds = arabidopsis_SRR1266008_unopened_flower_buds;
}
public int getArabidopsis_SRR1266009_unopened_flower_buds() {
	return Arabidopsis_SRR1266009_unopened_flower_buds;
}
public void setArabidopsis_SRR1266009_unopened_flower_buds(
		int arabidopsis_SRR1266009_unopened_flower_buds) {
	Arabidopsis_SRR1266009_unopened_flower_buds = arabidopsis_SRR1266009_unopened_flower_buds;
}
public int getArabidopsis_SRR1266010_unopened_flower_buds() {
	return Arabidopsis_SRR1266010_unopened_flower_buds;
}
public void setArabidopsis_SRR1266010_unopened_flower_buds(
		int arabidopsis_SRR1266010_unopened_flower_buds) {
	Arabidopsis_SRR1266010_unopened_flower_buds = arabidopsis_SRR1266010_unopened_flower_buds;
}
public int getArabidopsis_SRR1266011_unopened_flower_buds() {
	return Arabidopsis_SRR1266011_unopened_flower_buds;
}
public void setArabidopsis_SRR1266011_unopened_flower_buds(
		int arabidopsis_SRR1266011_unopened_flower_buds) {
	Arabidopsis_SRR1266011_unopened_flower_buds = arabidopsis_SRR1266011_unopened_flower_buds;
}
public int getArabidopsis_SRR1266012_unopened_flower_buds() {
	return Arabidopsis_SRR1266012_unopened_flower_buds;
}
public void setArabidopsis_SRR1266012_unopened_flower_buds(
		int arabidopsis_SRR1266012_unopened_flower_buds) {
	Arabidopsis_SRR1266012_unopened_flower_buds = arabidopsis_SRR1266012_unopened_flower_buds;
}
public int getArabidopsis_SRR1266013_unopened_flower_buds() {
	return Arabidopsis_SRR1266013_unopened_flower_buds;
}
public void setArabidopsis_SRR1266013_unopened_flower_buds(
		int arabidopsis_SRR1266013_unopened_flower_buds) {
	Arabidopsis_SRR1266013_unopened_flower_buds = arabidopsis_SRR1266013_unopened_flower_buds;
}
public int getArabidopsis_SRR1266014_unopened_flower_buds() {
	return Arabidopsis_SRR1266014_unopened_flower_buds;
}
public void setArabidopsis_SRR1266014_unopened_flower_buds(
		int arabidopsis_SRR1266014_unopened_flower_buds) {
	Arabidopsis_SRR1266014_unopened_flower_buds = arabidopsis_SRR1266014_unopened_flower_buds;
}
public int getArabidopsis_SRR1266015_unopened_flower_buds() {
	return Arabidopsis_SRR1266015_unopened_flower_buds;
}
public void setArabidopsis_SRR1266015_unopened_flower_buds(
		int arabidopsis_SRR1266015_unopened_flower_buds) {
	Arabidopsis_SRR1266015_unopened_flower_buds = arabidopsis_SRR1266015_unopened_flower_buds;
}
public int getArabidopsis_SRR1266016_unopened_flower_buds() {
	return Arabidopsis_SRR1266016_unopened_flower_buds;
}
public void setArabidopsis_SRR1266016_unopened_flower_buds(
		int arabidopsis_SRR1266016_unopened_flower_buds) {
	Arabidopsis_SRR1266016_unopened_flower_buds = arabidopsis_SRR1266016_unopened_flower_buds;
}
public int getArabidopsis_SRR1266017_unopened_flower_buds() {
	return Arabidopsis_SRR1266017_unopened_flower_buds;
}
public void setArabidopsis_SRR1266017_unopened_flower_buds(
		int arabidopsis_SRR1266017_unopened_flower_buds) {
	Arabidopsis_SRR1266017_unopened_flower_buds = arabidopsis_SRR1266017_unopened_flower_buds;
}
public int getArabidopsis_SRR493033_floral() {
	return Arabidopsis_SRR493033_floral;
}
public void setArabidopsis_SRR493033_floral(int arabidopsis_SRR493033_floral) {
	Arabidopsis_SRR493033_floral = arabidopsis_SRR493033_floral;
}
public int getArabidopsis_SRR493034_floral() {
	return Arabidopsis_SRR493034_floral;
}
public void setArabidopsis_SRR493034_floral(int arabidopsis_SRR493034_floral) {
	Arabidopsis_SRR493034_floral = arabidopsis_SRR493034_floral;
}
public int getArabidopsis_SRR493035_floral() {
	return Arabidopsis_SRR493035_floral;
}
public void setArabidopsis_SRR493035_floral(int arabidopsis_SRR493035_floral) {
	Arabidopsis_SRR493035_floral = arabidopsis_SRR493035_floral;
}
public int getArabidopsis_SRR520363_floral() {
	return Arabidopsis_SRR520363_floral;
}
public void setArabidopsis_SRR520363_floral(int arabidopsis_SRR520363_floral) {
	Arabidopsis_SRR520363_floral = arabidopsis_SRR520363_floral;
}
public int getArabidopsis_SRR520364_floral() {
	return Arabidopsis_SRR520364_floral;
}
public void setArabidopsis_SRR520364_floral(int arabidopsis_SRR520364_floral) {
	Arabidopsis_SRR520364_floral = arabidopsis_SRR520364_floral;
}
public int getArabidopsis_SRR520365_floral() {
	return Arabidopsis_SRR520365_floral;
}
public void setArabidopsis_SRR520365_floral(int arabidopsis_SRR520365_floral) {
	Arabidopsis_SRR520365_floral = arabidopsis_SRR520365_floral;
}
public int getArabidopsis_SRR520366_floral() {
	return Arabidopsis_SRR520366_floral;
}
public void setArabidopsis_SRR520366_floral(int arabidopsis_SRR520366_floral) {
	Arabidopsis_SRR520366_floral = arabidopsis_SRR520366_floral;
}
public int getArabidopsis_SRR942026_floral() {
	return Arabidopsis_SRR942026_floral;
}
public void setArabidopsis_SRR942026_floral(int arabidopsis_SRR942026_floral) {
	Arabidopsis_SRR942026_floral = arabidopsis_SRR942026_floral;
}
public int getArabidopsis_SRR942027_floral() {
	return Arabidopsis_SRR942027_floral;
}
public void setArabidopsis_SRR942027_floral(int arabidopsis_SRR942027_floral) {
	Arabidopsis_SRR942027_floral = arabidopsis_SRR942027_floral;
}
public int getArabidopsis_SRR942028_floral() {
	return Arabidopsis_SRR942028_floral;
}
public void setArabidopsis_SRR942028_floral(int arabidopsis_SRR942028_floral) {
	Arabidopsis_SRR942028_floral = arabidopsis_SRR942028_floral;
}
public int getArabidopsis_SRR942029_floral() {
	return Arabidopsis_SRR942029_floral;
}
public void setArabidopsis_SRR942029_floral(int arabidopsis_SRR942029_floral) {
	Arabidopsis_SRR942029_floral = arabidopsis_SRR942029_floral;
}
public int getArabidopsis_SRR942030_floral() {
	return Arabidopsis_SRR942030_floral;
}
public void setArabidopsis_SRR942030_floral(int arabidopsis_SRR942030_floral) {
	Arabidopsis_SRR942030_floral = arabidopsis_SRR942030_floral;
}
public int getArabidopsis_SRR942031_floral() {
	return Arabidopsis_SRR942031_floral;
}
public void setArabidopsis_SRR942031_floral(int arabidopsis_SRR942031_floral) {
	Arabidopsis_SRR942031_floral = arabidopsis_SRR942031_floral;
}
public int getArabidopsis_SRR942032_floral() {
	return Arabidopsis_SRR942032_floral;
}
public void setArabidopsis_SRR942032_floral(int arabidopsis_SRR942032_floral) {
	Arabidopsis_SRR942032_floral = arabidopsis_SRR942032_floral;
}
public int getArabidopsis_SRR942033_floral() {
	return Arabidopsis_SRR942033_floral;
}
public void setArabidopsis_SRR942033_floral(int arabidopsis_SRR942033_floral) {
	Arabidopsis_SRR942033_floral = arabidopsis_SRR942033_floral;
}
public int getArabidopsis_SRR058437_flowersHA_AGO1() {
	return Arabidopsis_SRR058437_flowersHA_AGO1;
}
public void setArabidopsis_SRR058437_flowersHA_AGO1(
		int arabidopsis_SRR058437_flowersHA_AGO1) {
	Arabidopsis_SRR058437_flowersHA_AGO1 = arabidopsis_SRR058437_flowersHA_AGO1;
}
public int getArabidopsis_SRR058438_flowersHA_AGO1() {
	return Arabidopsis_SRR058438_flowersHA_AGO1;
}
public void setArabidopsis_SRR058438_flowersHA_AGO1(
		int arabidopsis_SRR058438_flowersHA_AGO1) {
	Arabidopsis_SRR058438_flowersHA_AGO1 = arabidopsis_SRR058438_flowersHA_AGO1;
}
public int getArabidopsis_SRR064982_wild_typeflower() {
	return Arabidopsis_SRR064982_wild_typeflower;
}
public void setArabidopsis_SRR064982_wild_typeflower(
		int arabidopsis_SRR064982_wild_typeflower) {
	Arabidopsis_SRR064982_wild_typeflower = arabidopsis_SRR064982_wild_typeflower;
}
public int getArabidopsis_SRR064983_rdr6flower() {
	return Arabidopsis_SRR064983_rdr6flower;
}
public void setArabidopsis_SRR064983_rdr6flower(
		int arabidopsis_SRR064983_rdr6flower) {
	Arabidopsis_SRR064983_rdr6flower = arabidopsis_SRR064983_rdr6flower;
}
public int getArabidopsis_SRR1023813_Wildtypefloraltissue() {
	return Arabidopsis_SRR1023813_Wildtypefloraltissue;
}
public void setArabidopsis_SRR1023813_Wildtypefloraltissue(
		int arabidopsis_SRR1023813_Wildtypefloraltissue) {
	Arabidopsis_SRR1023813_Wildtypefloraltissue = arabidopsis_SRR1023813_Wildtypefloraltissue;
}
public int getArabidopsis_SRR1023814_Wildtypefloraltissue() {
	return Arabidopsis_SRR1023814_Wildtypefloraltissue;
}
public void setArabidopsis_SRR1023814_Wildtypefloraltissue(
		int arabidopsis_SRR1023814_Wildtypefloraltissue) {
	Arabidopsis_SRR1023814_Wildtypefloraltissue = arabidopsis_SRR1023814_Wildtypefloraltissue;
}
public int getArabidopsis_SRR1023815_suvh2suvh9floraltissue() {
	return Arabidopsis_SRR1023815_suvh2suvh9floraltissue;
}
public void setArabidopsis_SRR1023815_suvh2suvh9floraltissue(
		int arabidopsis_SRR1023815_suvh2suvh9floraltissue) {
	Arabidopsis_SRR1023815_suvh2suvh9floraltissue = arabidopsis_SRR1023815_suvh2suvh9floraltissue;
}
public int getArabidopsis_SRR1023816_Wildtypefloraltissue() {
	return Arabidopsis_SRR1023816_Wildtypefloraltissue;
}
public void setArabidopsis_SRR1023816_Wildtypefloraltissue(
		int arabidopsis_SRR1023816_Wildtypefloraltissue) {
	Arabidopsis_SRR1023816_Wildtypefloraltissue = arabidopsis_SRR1023816_Wildtypefloraltissue;
}
public int getArabidopsis_SRR1023817_Wildtypefloraltissue() {
	return Arabidopsis_SRR1023817_Wildtypefloraltissue;
}
public void setArabidopsis_SRR1023817_Wildtypefloraltissue(
		int arabidopsis_SRR1023817_Wildtypefloraltissue) {
	Arabidopsis_SRR1023817_Wildtypefloraltissue = arabidopsis_SRR1023817_Wildtypefloraltissue;
}
public int getArabidopsis_SRR1023818_suvh2suvh9floraltissue() {
	return Arabidopsis_SRR1023818_suvh2suvh9floraltissue;
}
public void setArabidopsis_SRR1023818_suvh2suvh9floraltissue(
		int arabidopsis_SRR1023818_suvh2suvh9floraltissue) {
	Arabidopsis_SRR1023818_suvh2suvh9floraltissue = arabidopsis_SRR1023818_suvh2suvh9floraltissue;
}
public int getArabidopsis_SRR019670_floral_Col_0_rep1() {
	return Arabidopsis_SRR019670_floral_Col_0_rep1;
}
public void setArabidopsis_SRR019670_floral_Col_0_rep1(
		int arabidopsis_SRR019670_floral_Col_0_rep1) {
	Arabidopsis_SRR019670_floral_Col_0_rep1 = arabidopsis_SRR019670_floral_Col_0_rep1;
}
public int getArabidopsis_SRR019671_floral_Col_0_rep2() {
	return Arabidopsis_SRR019671_floral_Col_0_rep2;
}
public void setArabidopsis_SRR019671_floral_Col_0_rep2(
		int arabidopsis_SRR019671_floral_Col_0_rep2) {
	Arabidopsis_SRR019671_floral_Col_0_rep2 = arabidopsis_SRR019671_floral_Col_0_rep2;
}
public int getArabidopsis_SRR019672_floral_Col_0_rep3() {
	return Arabidopsis_SRR019672_floral_Col_0_rep3;
}
public void setArabidopsis_SRR019672_floral_Col_0_rep3(
		int arabidopsis_SRR019672_floral_Col_0_rep3) {
	Arabidopsis_SRR019672_floral_Col_0_rep3 = arabidopsis_SRR019672_floral_Col_0_rep3;
}
public int getArabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0() {
	return Arabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0;
}
public void setArabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0(
		int arabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0) {
	Arabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0 = arabidopsis_SRR019674_floralwildtypewhole_aerial_Col_0;
}
public int getArabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7() {
	return Arabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7;
}
public void setArabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7(
		int arabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7) {
	Arabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7 = arabidopsis_SRR019675_floralhomozydcl1_7whole_aerial_dcl1_7;
}
public int getArabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2() {
	return Arabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2;
}
public void setArabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2(
		int arabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2) {
	Arabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2 = arabidopsis_SRR019676_floralhomozydcl2_1whole_aerial_2;
}
public int getArabidopsis_SRR1567672_Pollen() {
	return Arabidopsis_SRR1567672_Pollen;
}
public void setArabidopsis_SRR1567672_Pollen(int arabidopsis_SRR1567672_Pollen) {
	Arabidopsis_SRR1567672_Pollen = arabidopsis_SRR1567672_Pollen;
}
public int getArabidopsis_SRR387834_Pollen() {
	return Arabidopsis_SRR387834_Pollen;
}
public void setArabidopsis_SRR387834_Pollen(int arabidopsis_SRR387834_Pollen) {
	Arabidopsis_SRR387834_Pollen = arabidopsis_SRR387834_Pollen;
}
public int getArabidopsis_SRR387835_Pollen() {
	return Arabidopsis_SRR387835_Pollen;
}
public void setArabidopsis_SRR387835_Pollen(int arabidopsis_SRR387835_Pollen) {
	Arabidopsis_SRR387835_Pollen = arabidopsis_SRR387835_Pollen;
}
public int getArabidopsis_SRR387836_Pollen() {
	return Arabidopsis_SRR387836_Pollen;
}
public void setArabidopsis_SRR387836_Pollen(int arabidopsis_SRR387836_Pollen) {
	Arabidopsis_SRR387836_Pollen = arabidopsis_SRR387836_Pollen;
}
public int getArabidopsis_SRR387837_Pollen() {
	return Arabidopsis_SRR387837_Pollen;
}
public void setArabidopsis_SRR387837_Pollen(int arabidopsis_SRR387837_Pollen) {
	Arabidopsis_SRR387837_Pollen = arabidopsis_SRR387837_Pollen;
}
public int getArabidopsis_SRR1171798_silique() {
	return Arabidopsis_SRR1171798_silique;
}
public void setArabidopsis_SRR1171798_silique(int arabidopsis_SRR1171798_silique) {
	Arabidopsis_SRR1171798_silique = arabidopsis_SRR1171798_silique;
}
public int getArabidopsis_SRR1171799_silique() {
	return Arabidopsis_SRR1171799_silique;
}
public void setArabidopsis_SRR1171799_silique(int arabidopsis_SRR1171799_silique) {
	Arabidopsis_SRR1171799_silique = arabidopsis_SRR1171799_silique;
}
public int getArabidopsis_SRR1171800_silique() {
	return Arabidopsis_SRR1171800_silique;
}
public void setArabidopsis_SRR1171800_silique(int arabidopsis_SRR1171800_silique) {
	Arabidopsis_SRR1171800_silique = arabidopsis_SRR1171800_silique;
}
public int getArabidopsis_SRR1171801_silique() {
	return Arabidopsis_SRR1171801_silique;
}
public void setArabidopsis_SRR1171801_silique(int arabidopsis_SRR1171801_silique) {
	Arabidopsis_SRR1171801_silique = arabidopsis_SRR1171801_silique;
}
public int getArabidopsis_SRR788205_Wild_Columbia() {
	return Arabidopsis_SRR788205_Wild_Columbia;
}
public void setArabidopsis_SRR788205_Wild_Columbia(
		int arabidopsis_SRR788205_Wild_Columbia) {
	Arabidopsis_SRR788205_Wild_Columbia = arabidopsis_SRR788205_Wild_Columbia;
}
public int getArabidopsis_SRR788206_Wild_Columbia() {
	return Arabidopsis_SRR788206_Wild_Columbia;
}
public void setArabidopsis_SRR788206_Wild_Columbia(
		int arabidopsis_SRR788206_Wild_Columbia) {
	Arabidopsis_SRR788206_Wild_Columbia = arabidopsis_SRR788206_Wild_Columbia;
}
public int getArabidopsis_SRR788207_nrpd1_4_Columbia() {
	return Arabidopsis_SRR788207_nrpd1_4_Columbia;
}
public void setArabidopsis_SRR788207_nrpd1_4_Columbia(
		int arabidopsis_SRR788207_nrpd1_4_Columbia) {
	Arabidopsis_SRR788207_nrpd1_4_Columbia = arabidopsis_SRR788207_nrpd1_4_Columbia;
}
public int getArabidopsis_SRR788208_nrpe1_12_Columbia() {
	return Arabidopsis_SRR788208_nrpe1_12_Columbia;
}
public void setArabidopsis_SRR788208_nrpe1_12_Columbia(
		int arabidopsis_SRR788208_nrpe1_12_Columbia) {
	Arabidopsis_SRR788208_nrpe1_12_Columbia = arabidopsis_SRR788208_nrpe1_12_Columbia;
}
public int getArabidopsis_SRR788209_shh1_1_Columbia() {
	return Arabidopsis_SRR788209_shh1_1_Columbia;
}
public void setArabidopsis_SRR788209_shh1_1_Columbia(
		int arabidopsis_SRR788209_shh1_1_Columbia) {
	Arabidopsis_SRR788209_shh1_1_Columbia = arabidopsis_SRR788209_shh1_1_Columbia;
}
public int getArabidopsis_SRR788210_drm2_2_Columbia() {
	return Arabidopsis_SRR788210_drm2_2_Columbia;
}
public void setArabidopsis_SRR788210_drm2_2_Columbia(
		int arabidopsis_SRR788210_drm2_2_Columbia) {
	Arabidopsis_SRR788210_drm2_2_Columbia = arabidopsis_SRR788210_drm2_2_Columbia;
}
public int getArabidopsis_SRR788211_Wild_Columbia() {
	return Arabidopsis_SRR788211_Wild_Columbia;
}
public void setArabidopsis_SRR788211_Wild_Columbia(
		int arabidopsis_SRR788211_Wild_Columbia) {
	Arabidopsis_SRR788211_Wild_Columbia = arabidopsis_SRR788211_Wild_Columbia;
}
public int getArabidopsis_SRR788212_shh1_1_Columbia() {
	return Arabidopsis_SRR788212_shh1_1_Columbia;
}
public void setArabidopsis_SRR788212_shh1_1_Columbia(
		int arabidopsis_SRR788212_shh1_1_Columbia) {
	Arabidopsis_SRR788212_shh1_1_Columbia = arabidopsis_SRR788212_shh1_1_Columbia;
}
public int getArabidopsis_SRR788213_Y140A_point_mutant_Columbia() {
	return Arabidopsis_SRR788213_Y140A_point_mutant_Columbia;
}
public void setArabidopsis_SRR788213_Y140A_point_mutant_Columbia(
		int arabidopsis_SRR788213_Y140A_point_mutant_Columbia) {
	Arabidopsis_SRR788213_Y140A_point_mutant_Columbia = arabidopsis_SRR788213_Y140A_point_mutant_Columbia;
}
public int getArabidopsis_SRR788214_F162A_point_mutant_Columbia() {
	return Arabidopsis_SRR788214_F162A_point_mutant_Columbia;
}
public void setArabidopsis_SRR788214_F162A_point_mutant_Columbia(
		int arabidopsis_SRR788214_F162A_point_mutant_Columbia) {
	Arabidopsis_SRR788214_F162A_point_mutant_Columbia = arabidopsis_SRR788214_F162A_point_mutant_Columbia;
}
public int getArabidopsis_SRR788215_D141A_point_mutant_Columbia() {
	return Arabidopsis_SRR788215_D141A_point_mutant_Columbia;
}
public void setArabidopsis_SRR788215_D141A_point_mutant_Columbia(
		int arabidopsis_SRR788215_D141A_point_mutant_Columbia) {
	Arabidopsis_SRR788215_D141A_point_mutant_Columbia = arabidopsis_SRR788215_D141A_point_mutant_Columbia;
}
public int getArabidopsis_SRR788216_Y212A_point_mutant_Columbia() {
	return Arabidopsis_SRR788216_Y212A_point_mutant_Columbia;
}
public void setArabidopsis_SRR788216_Y212A_point_mutant_Columbia(
		int arabidopsis_SRR788216_Y212A_point_mutant_Columbia) {
	Arabidopsis_SRR788216_Y212A_point_mutant_Columbia = arabidopsis_SRR788216_Y212A_point_mutant_Columbia;
}
public int getArabidopsis_SRR788217_wild_Columbia() {
	return Arabidopsis_SRR788217_wild_Columbia;
}
public void setArabidopsis_SRR788217_wild_Columbia(
		int arabidopsis_SRR788217_wild_Columbia) {
	Arabidopsis_SRR788217_wild_Columbia = arabidopsis_SRR788217_wild_Columbia;
}
public int getArabidopsis_SRR013343_Columbia_0wildtype_leaf() {
	return Arabidopsis_SRR013343_Columbia_0wildtype_leaf;
}
public void setArabidopsis_SRR013343_Columbia_0wildtype_leaf(
		int arabidopsis_SRR013343_Columbia_0wildtype_leaf) {
	Arabidopsis_SRR013343_Columbia_0wildtype_leaf = arabidopsis_SRR013343_Columbia_0wildtype_leaf;
}
public int getArabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf() {
	return Arabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf;
}
public void setArabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf(
		int arabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf) {
	Arabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf = arabidopsis_SRR013344_drm1_2drm2_2cmt3_11nullmutantleaf;
}
public int getArabidopsis_SRR013345_met1_3nullmutantLeaf() {
	return Arabidopsis_SRR013345_met1_3nullmutantLeaf;
}
public void setArabidopsis_SRR013345_met1_3nullmutantLeaf(
		int arabidopsis_SRR013345_met1_3nullmutantLeaf) {
	Arabidopsis_SRR013345_met1_3nullmutantLeaf = arabidopsis_SRR013345_met1_3nullmutantLeaf;
}
public int getArabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf() {
	return Arabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf;
}
public void setArabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf(
		int arabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf) {
	Arabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf = arabidopsis_SRR013346_ros1_3dml2_1dml3_1nullmutant_leaf;
}
public int getArabidopsis_SRR1567673_Sperm() {
	return Arabidopsis_SRR1567673_Sperm;
}
public void setArabidopsis_SRR1567673_Sperm(int arabidopsis_SRR1567673_Sperm) {
	Arabidopsis_SRR1567673_Sperm = arabidopsis_SRR1567673_Sperm;
}
String sourcedb,accessurl;
   
public String getMir_ids() {
	return mir_ids;
}
public void setMir_ids(String mir_ids) {
	this.mir_ids = mir_ids;
}
public String getSequence() {
	return sequence;
}
public void setSequence(String sequence) {
	this.sequence = sequence;
}
public int getPrimkey() {
	return primkey;
}
public void setPrimkey(int primkey) {
	this.primkey = primkey;
}
public int getArabidopsis_SRR1039931_Seed() {
	return Arabidopsis_SRR1039931_Seed;
}
public void setArabidopsis_SRR1039931_Seed(int arabidopsis_SRR1039931_Seed) {
	Arabidopsis_SRR1039931_Seed = arabidopsis_SRR1039931_Seed;
}
public int getArabidopsis_SRR1039932_Seed() {
	return Arabidopsis_SRR1039932_Seed;
}
public void setArabidopsis_SRR1039932_Seed(int arabidopsis_SRR1039932_Seed) {
	Arabidopsis_SRR1039932_Seed = arabidopsis_SRR1039932_Seed;
}
public int getArabidopsis_SRR1039933_Seed() {
	return Arabidopsis_SRR1039933_Seed;
}
public void setArabidopsis_SRR1039933_Seed(int arabidopsis_SRR1039933_Seed) {
	Arabidopsis_SRR1039933_Seed = arabidopsis_SRR1039933_Seed;
}
public int getArabidopsis_SRR1039934_Seed() {
	return Arabidopsis_SRR1039934_Seed;
}
public void setArabidopsis_SRR1039934_Seed(int arabidopsis_SRR1039934_Seed) {
	Arabidopsis_SRR1039934_Seed = arabidopsis_SRR1039934_Seed;
}
public int getArabidopsis_SRR1039935_Seed() {
	return Arabidopsis_SRR1039935_Seed;
}
public void setArabidopsis_SRR1039935_Seed(int arabidopsis_SRR1039935_Seed) {
	Arabidopsis_SRR1039935_Seed = arabidopsis_SRR1039935_Seed;
}
public int getArabidopsis_SRR1039936_Seed() {
	return Arabidopsis_SRR1039936_Seed;
}
public void setArabidopsis_SRR1039936_Seed(int arabidopsis_SRR1039936_Seed) {
	Arabidopsis_SRR1039936_Seed = arabidopsis_SRR1039936_Seed;
}
public int getArabidopsis_SRR1039937_Seed() {
	return Arabidopsis_SRR1039937_Seed;
}
public void setArabidopsis_SRR1039937_Seed(int arabidopsis_SRR1039937_Seed) {
	Arabidopsis_SRR1039937_Seed = arabidopsis_SRR1039937_Seed;
}
public int getArabidopsis_SRR1039938_Seed() {
	return Arabidopsis_SRR1039938_Seed;
}
public void setArabidopsis_SRR1039938_Seed(int arabidopsis_SRR1039938_Seed) {
	Arabidopsis_SRR1039938_Seed = arabidopsis_SRR1039938_Seed;
}
public int getArabidopsis_SRR1039939_Seed() {
	return Arabidopsis_SRR1039939_Seed;
}
public void setArabidopsis_SRR1039939_Seed(int arabidopsis_SRR1039939_Seed) {
	Arabidopsis_SRR1039939_Seed = arabidopsis_SRR1039939_Seed;
}
public int getArabidopsis_SRR1039940_Seed() {
	return Arabidopsis_SRR1039940_Seed;
}
public void setArabidopsis_SRR1039940_Seed(int arabidopsis_SRR1039940_Seed) {
	Arabidopsis_SRR1039940_Seed = arabidopsis_SRR1039940_Seed;
}
public int getArabidopsis_SRR1039941_Seed() {
	return Arabidopsis_SRR1039941_Seed;
}
public void setArabidopsis_SRR1039941_Seed(int arabidopsis_SRR1039941_Seed) {
	Arabidopsis_SRR1039941_Seed = arabidopsis_SRR1039941_Seed;
}
public int getArabidopsis_SRR1039930_Seed6DAP() {
	return Arabidopsis_SRR1039930_Seed6DAP;
}
public void setArabidopsis_SRR1039930_Seed6DAP(
		int arabidopsis_SRR1039930_Seed6DAP) {
	Arabidopsis_SRR1039930_Seed6DAP = arabidopsis_SRR1039930_Seed6DAP;
}
public int getArabidopsis_SRR072957_2xX2xDAP6seeds() {
	return Arabidopsis_SRR072957_2xX2xDAP6seeds;
}
public void setArabidopsis_SRR072957_2xX2xDAP6seeds(
		int arabidopsis_SRR072957_2xX2xDAP6seeds) {
	Arabidopsis_SRR072957_2xX2xDAP6seeds = arabidopsis_SRR072957_2xX2xDAP6seeds;
}
public int getArabidopsis_SRR072958_4xX4xDAP6seeds() {
	return Arabidopsis_SRR072958_4xX4xDAP6seeds;
}
public void setArabidopsis_SRR072958_4xX4xDAP6seeds(
		int arabidopsis_SRR072958_4xX4xDAP6seeds) {
	Arabidopsis_SRR072958_4xX4xDAP6seeds = arabidopsis_SRR072958_4xX4xDAP6seeds;
}
public int getArabidopsis_SRR072959_2xX4xDAP6seeds() {
	return Arabidopsis_SRR072959_2xX4xDAP6seeds;
}
public void setArabidopsis_SRR072959_2xX4xDAP6seeds(
		int arabidopsis_SRR072959_2xX4xDAP6seeds) {
	Arabidopsis_SRR072959_2xX4xDAP6seeds = arabidopsis_SRR072959_2xX4xDAP6seeds;
}
public int getArabidopsis_SRR072960_4xX2xDAP6seeds() {
	return Arabidopsis_SRR072960_4xX2xDAP6seeds;
}
public void setArabidopsis_SRR072960_4xX2xDAP6seeds(
		int arabidopsis_SRR072960_4xX2xDAP6seeds) {
	Arabidopsis_SRR072960_4xX2xDAP6seeds = arabidopsis_SRR072960_4xX2xDAP6seeds;
}
public int getArabidopsis_SRR1575168_seedling() {
	return Arabidopsis_SRR1575168_seedling;
}
public void setArabidopsis_SRR1575168_seedling(
		int arabidopsis_SRR1575168_seedling) {
	Arabidopsis_SRR1575168_seedling = arabidopsis_SRR1575168_seedling;
}
public int getArabidopsis_SRR1575169_seedling() {
	return Arabidopsis_SRR1575169_seedling;
}
public void setArabidopsis_SRR1575169_seedling(
		int arabidopsis_SRR1575169_seedling) {
	Arabidopsis_SRR1575169_seedling = arabidopsis_SRR1575169_seedling;
}
public int getArabidopsis_SRR1049769_seedling() {
	return Arabidopsis_SRR1049769_seedling;
}
public void setArabidopsis_SRR1049769_seedling(
		int arabidopsis_SRR1049769_seedling) {
	Arabidopsis_SRR1049769_seedling = arabidopsis_SRR1049769_seedling;
}
public int getArabidopsis_SRR1049771_seedling() {
	return Arabidopsis_SRR1049771_seedling;
}
public void setArabidopsis_SRR1049771_seedling(
		int arabidopsis_SRR1049771_seedling) {
	Arabidopsis_SRR1049771_seedling = arabidopsis_SRR1049771_seedling;
}
public int getArabidopsis_SRR1049773_seedling() {
	return Arabidopsis_SRR1049773_seedling;
}
public void setArabidopsis_SRR1049773_seedling(
		int arabidopsis_SRR1049773_seedling) {
	Arabidopsis_SRR1049773_seedling = arabidopsis_SRR1049773_seedling;
}
public int getArabidopsis_SRR1049775_seedling() {
	return Arabidopsis_SRR1049775_seedling;
}
public void setArabidopsis_SRR1049775_seedling(
		int arabidopsis_SRR1049775_seedling) {
	Arabidopsis_SRR1049775_seedling = arabidopsis_SRR1049775_seedling;
}
public int getArabidopsis_SRR1049777_seedling() {
	return Arabidopsis_SRR1049777_seedling;
}
public void setArabidopsis_SRR1049777_seedling(
		int arabidopsis_SRR1049777_seedling) {
	Arabidopsis_SRR1049777_seedling = arabidopsis_SRR1049777_seedling;
}
public int getArabidopsis_SRR1049779_seedling() {
	return Arabidopsis_SRR1049779_seedling;
}
public void setArabidopsis_SRR1049779_seedling(
		int arabidopsis_SRR1049779_seedling) {
	Arabidopsis_SRR1049779_seedling = arabidopsis_SRR1049779_seedling;
}
public int getArabidopsis_SRR1049781_seedling() {
	return Arabidopsis_SRR1049781_seedling;
}
public void setArabidopsis_SRR1049781_seedling(
		int arabidopsis_SRR1049781_seedling) {
	Arabidopsis_SRR1049781_seedling = arabidopsis_SRR1049781_seedling;
}
public int getArabidopsis_SRR1049783_seedling() {
	return Arabidopsis_SRR1049783_seedling;
}
public void setArabidopsis_SRR1049783_seedling(
		int arabidopsis_SRR1049783_seedling) {
	Arabidopsis_SRR1049783_seedling = arabidopsis_SRR1049783_seedling;
}
public int getArabidopsis_SRR1049784_seedling() {
	return Arabidopsis_SRR1049784_seedling;
}
public void setArabidopsis_SRR1049784_seedling(
		int arabidopsis_SRR1049784_seedling) {
	Arabidopsis_SRR1049784_seedling = arabidopsis_SRR1049784_seedling;
}
public int getArabidopsis_SRR1049785_seedling() {
	return Arabidopsis_SRR1049785_seedling;
}
public void setArabidopsis_SRR1049785_seedling(
		int arabidopsis_SRR1049785_seedling) {
	Arabidopsis_SRR1049785_seedling = arabidopsis_SRR1049785_seedling;
}
public int getArabidopsis_SRR1049786_seedling() {
	return Arabidopsis_SRR1049786_seedling;
}
public void setArabidopsis_SRR1049786_seedling(
		int arabidopsis_SRR1049786_seedling) {
	Arabidopsis_SRR1049786_seedling = arabidopsis_SRR1049786_seedling;
}
public int getArabidopsis_SRR1049787_seedling() {
	return Arabidopsis_SRR1049787_seedling;
}
public void setArabidopsis_SRR1049787_seedling(
		int arabidopsis_SRR1049787_seedling) {
	Arabidopsis_SRR1049787_seedling = arabidopsis_SRR1049787_seedling;
}
public int getArabidopsis_SRR1049788_seedling() {
	return Arabidopsis_SRR1049788_seedling;
}
public void setArabidopsis_SRR1049788_seedling(
		int arabidopsis_SRR1049788_seedling) {
	Arabidopsis_SRR1049788_seedling = arabidopsis_SRR1049788_seedling;
}
public int getArabidopsis_SRR1163718_Seedling() {
	return Arabidopsis_SRR1163718_Seedling;
}
public void setArabidopsis_SRR1163718_Seedling(
		int arabidopsis_SRR1163718_Seedling) {
	Arabidopsis_SRR1163718_Seedling = arabidopsis_SRR1163718_Seedling;
}
public int getArabidopsis_SRR1164378_Seedling() {
	return Arabidopsis_SRR1164378_Seedling;
}
public void setArabidopsis_SRR1164378_Seedling(
		int arabidopsis_SRR1164378_Seedling) {
	Arabidopsis_SRR1164378_Seedling = arabidopsis_SRR1164378_Seedling;
}
public int getArabidopsis_SRR189803_seedling() {
	return Arabidopsis_SRR189803_seedling;
}
public void setArabidopsis_SRR189803_seedling(int arabidopsis_SRR189803_seedling) {
	Arabidopsis_SRR189803_seedling = arabidopsis_SRR189803_seedling;
}
public int getArabidopsis_SRR189807_seedling() {
	return Arabidopsis_SRR189807_seedling;
}
public void setArabidopsis_SRR189807_seedling(int arabidopsis_SRR189807_seedling) {
	Arabidopsis_SRR189807_seedling = arabidopsis_SRR189807_seedling;
}
public int getArabidopsis_SRR189811_seedling() {
	return Arabidopsis_SRR189811_seedling;
}
public void setArabidopsis_SRR189811_seedling(int arabidopsis_SRR189811_seedling) {
	Arabidopsis_SRR189811_seedling = arabidopsis_SRR189811_seedling;
}
public int getArabidopsis_SRR349699_Seedling() {
	return Arabidopsis_SRR349699_Seedling;
}
public void setArabidopsis_SRR349699_Seedling(int arabidopsis_SRR349699_Seedling) {
	Arabidopsis_SRR349699_Seedling = arabidopsis_SRR349699_Seedling;
}
public int getArabidopsis_SRR349700_Seedling() {
	return Arabidopsis_SRR349700_Seedling;
}
public void setArabidopsis_SRR349700_Seedling(int arabidopsis_SRR349700_Seedling) {
	Arabidopsis_SRR349700_Seedling = arabidopsis_SRR349700_Seedling;
}
public int getArabidopsis_SRR390740_Seedling() {
	return Arabidopsis_SRR390740_Seedling;
}
public void setArabidopsis_SRR390740_Seedling(int arabidopsis_SRR390740_Seedling) {
	Arabidopsis_SRR390740_Seedling = arabidopsis_SRR390740_Seedling;
}
public int getArabidopsis_SRR390741_Seedling() {
	return Arabidopsis_SRR390741_Seedling;
}
public void setArabidopsis_SRR390741_Seedling(int arabidopsis_SRR390741_Seedling) {
	Arabidopsis_SRR390741_Seedling = arabidopsis_SRR390741_Seedling;
}
public int getArabidopsis_SRR392134_Seedling() {
	return Arabidopsis_SRR392134_Seedling;
}
public void setArabidopsis_SRR392134_Seedling(int arabidopsis_SRR392134_Seedling) {
	Arabidopsis_SRR392134_Seedling = arabidopsis_SRR392134_Seedling;
}
public int getArabidopsis_SRR392135_Seedling() {
	return Arabidopsis_SRR392135_Seedling;
}
public void setArabidopsis_SRR392135_Seedling(int arabidopsis_SRR392135_Seedling) {
	Arabidopsis_SRR392135_Seedling = arabidopsis_SRR392135_Seedling;
}
public int getArabidopsis_SRR392136_Seedling() {
	return Arabidopsis_SRR392136_Seedling;
}
public void setArabidopsis_SRR392136_Seedling(int arabidopsis_SRR392136_Seedling) {
	Arabidopsis_SRR392136_Seedling = arabidopsis_SRR392136_Seedling;
}
public int getArabidopsis_SRR392137_Seedling() {
	return Arabidopsis_SRR392137_Seedling;
}
public void setArabidopsis_SRR392137_Seedling(int arabidopsis_SRR392137_Seedling) {
	Arabidopsis_SRR392137_Seedling = arabidopsis_SRR392137_Seedling;
}
public int getArabidopsis_SRR392138_Seedling() {
	return Arabidopsis_SRR392138_Seedling;
}
public void setArabidopsis_SRR392138_Seedling(int arabidopsis_SRR392138_Seedling) {
	Arabidopsis_SRR392138_Seedling = arabidopsis_SRR392138_Seedling;
}
public int getArabidopsis_SRR392139_Seedling() {
	return Arabidopsis_SRR392139_Seedling;
}
public void setArabidopsis_SRR392139_Seedling(int arabidopsis_SRR392139_Seedling) {
	Arabidopsis_SRR392139_Seedling = arabidopsis_SRR392139_Seedling;
}
public int getArabidopsis_SRR392140_Seedling() {
	return Arabidopsis_SRR392140_Seedling;
}
public void setArabidopsis_SRR392140_Seedling(int arabidopsis_SRR392140_Seedling) {
	Arabidopsis_SRR392140_Seedling = arabidopsis_SRR392140_Seedling;
}
public int getArabidopsis_SRR392141_Seedling() {
	return Arabidopsis_SRR392141_Seedling;
}
public void setArabidopsis_SRR392141_Seedling(int arabidopsis_SRR392141_Seedling) {
	Arabidopsis_SRR392141_Seedling = arabidopsis_SRR392141_Seedling;
}
public int getArabidopsis_SRR392142_Seedling() {
	return Arabidopsis_SRR392142_Seedling;
}
public void setArabidopsis_SRR392142_Seedling(int arabidopsis_SRR392142_Seedling) {
	Arabidopsis_SRR392142_Seedling = arabidopsis_SRR392142_Seedling;
}
public int getArabidopsis_SRR392143_Seedling() {
	return Arabidopsis_SRR392143_Seedling;
}
public void setArabidopsis_SRR392143_Seedling(int arabidopsis_SRR392143_Seedling) {
	Arabidopsis_SRR392143_Seedling = arabidopsis_SRR392143_Seedling;
}
public int getArabidopsis_SRR392144_Seedling() {
	return Arabidopsis_SRR392144_Seedling;
}
public void setArabidopsis_SRR392144_Seedling(int arabidopsis_SRR392144_Seedling) {
	Arabidopsis_SRR392144_Seedling = arabidopsis_SRR392144_Seedling;
}
public int getArabidopsis_SRR392145_Seedling() {
	return Arabidopsis_SRR392145_Seedling;
}
public void setArabidopsis_SRR392145_Seedling(int arabidopsis_SRR392145_Seedling) {
	Arabidopsis_SRR392145_Seedling = arabidopsis_SRR392145_Seedling;
}
public int getArabidopsis_SRR441525_seedlings() {
	return Arabidopsis_SRR441525_seedlings;
}
public void setArabidopsis_SRR441525_seedlings(
		int arabidopsis_SRR441525_seedlings) {
	Arabidopsis_SRR441525_seedlings = arabidopsis_SRR441525_seedlings;
}
public int getArabidopsis_SRR441526_seedlings() {
	return Arabidopsis_SRR441526_seedlings;
}
public void setArabidopsis_SRR441526_seedlings(
		int arabidopsis_SRR441526_seedlings) {
	Arabidopsis_SRR441526_seedlings = arabidopsis_SRR441526_seedlings;
}
public int getArabidopsis_SRR441527_seedlings() {
	return Arabidopsis_SRR441527_seedlings;
}
public void setArabidopsis_SRR441527_seedlings(
		int arabidopsis_SRR441527_seedlings) {
	Arabidopsis_SRR441527_seedlings = arabidopsis_SRR441527_seedlings;
}
public int getArabidopsis_SRR441528_seedlings() {
	return Arabidopsis_SRR441528_seedlings;
}
public void setArabidopsis_SRR441528_seedlings(
		int arabidopsis_SRR441528_seedlings) {
	Arabidopsis_SRR441528_seedlings = arabidopsis_SRR441528_seedlings;
}
public int getArabidopsis_SRR441529_seedlings() {
	return Arabidopsis_SRR441529_seedlings;
}
public void setArabidopsis_SRR441529_seedlings(
		int arabidopsis_SRR441529_seedlings) {
	Arabidopsis_SRR441529_seedlings = arabidopsis_SRR441529_seedlings;
}
public int getArabidopsis_SRR441530_seedlings() {
	return Arabidopsis_SRR441530_seedlings;
}
public void setArabidopsis_SRR441530_seedlings(
		int arabidopsis_SRR441530_seedlings) {
	Arabidopsis_SRR441530_seedlings = arabidopsis_SRR441530_seedlings;
}
public int getArabidopsis_SRR441531_seedlings() {
	return Arabidopsis_SRR441531_seedlings;
}
public void setArabidopsis_SRR441531_seedlings(
		int arabidopsis_SRR441531_seedlings) {
	Arabidopsis_SRR441531_seedlings = arabidopsis_SRR441531_seedlings;
}
public int getArabidopsis_SRR441532_seedlings() {
	return Arabidopsis_SRR441532_seedlings;
}
public void setArabidopsis_SRR441532_seedlings(
		int arabidopsis_SRR441532_seedlings) {
	Arabidopsis_SRR441532_seedlings = arabidopsis_SRR441532_seedlings;
}
public int getArabidopsis_SRR441533_seedlings() {
	return Arabidopsis_SRR441533_seedlings;
}
public void setArabidopsis_SRR441533_seedlings(
		int arabidopsis_SRR441533_seedlings) {
	Arabidopsis_SRR441533_seedlings = arabidopsis_SRR441533_seedlings;
}
public int getArabidopsis_SRR441534_seedlings() {
	return Arabidopsis_SRR441534_seedlings;
}
public void setArabidopsis_SRR441534_seedlings(
		int arabidopsis_SRR441534_seedlings) {
	Arabidopsis_SRR441534_seedlings = arabidopsis_SRR441534_seedlings;
}
public int getArabidopsis_SRR441535_seedlings() {
	return Arabidopsis_SRR441535_seedlings;
}
public void setArabidopsis_SRR441535_seedlings(
		int arabidopsis_SRR441535_seedlings) {
	Arabidopsis_SRR441535_seedlings = arabidopsis_SRR441535_seedlings;
}
public int getArabidopsis_SRR441536_seedlings() {
	return Arabidopsis_SRR441536_seedlings;
}
public void setArabidopsis_SRR441536_seedlings(
		int arabidopsis_SRR441536_seedlings) {
	Arabidopsis_SRR441536_seedlings = arabidopsis_SRR441536_seedlings;
}
public int getArabidopsis_SRR441537_seedlings() {
	return Arabidopsis_SRR441537_seedlings;
}
public void setArabidopsis_SRR441537_seedlings(
		int arabidopsis_SRR441537_seedlings) {
	Arabidopsis_SRR441537_seedlings = arabidopsis_SRR441537_seedlings;
}
public int getArabidopsis_SRR441538_seedlings() {
	return Arabidopsis_SRR441538_seedlings;
}
public void setArabidopsis_SRR441538_seedlings(
		int arabidopsis_SRR441538_seedlings) {
	Arabidopsis_SRR441538_seedlings = arabidopsis_SRR441538_seedlings;
}
public int getArabidopsis_SRR441539_seedlings() {
	return Arabidopsis_SRR441539_seedlings;
}
public void setArabidopsis_SRR441539_seedlings(
		int arabidopsis_SRR441539_seedlings) {
	Arabidopsis_SRR441539_seedlings = arabidopsis_SRR441539_seedlings;
}
public int getArabidopsis_SRR441540_seedlings() {
	return Arabidopsis_SRR441540_seedlings;
}
public void setArabidopsis_SRR441540_seedlings(
		int arabidopsis_SRR441540_seedlings) {
	Arabidopsis_SRR441540_seedlings = arabidopsis_SRR441540_seedlings;
}
public int getArabidopsis_SRR441541_seedlings() {
	return Arabidopsis_SRR441541_seedlings;
}
public void setArabidopsis_SRR441541_seedlings(
		int arabidopsis_SRR441541_seedlings) {
	Arabidopsis_SRR441541_seedlings = arabidopsis_SRR441541_seedlings;
}
public int getArabidopsis_SRR441542_seedlings() {
	return Arabidopsis_SRR441542_seedlings;
}
public void setArabidopsis_SRR441542_seedlings(
		int arabidopsis_SRR441542_seedlings) {
	Arabidopsis_SRR441542_seedlings = arabidopsis_SRR441542_seedlings;
}
public int getArabidopsis_SRR441543_seedlings() {
	return Arabidopsis_SRR441543_seedlings;
}
public void setArabidopsis_SRR441543_seedlings(
		int arabidopsis_SRR441543_seedlings) {
	Arabidopsis_SRR441543_seedlings = arabidopsis_SRR441543_seedlings;
}
public int getArabidopsis_SRR441544_seedlings() {
	return Arabidopsis_SRR441544_seedlings;
}
public void setArabidopsis_SRR441544_seedlings(
		int arabidopsis_SRR441544_seedlings) {
	Arabidopsis_SRR441544_seedlings = arabidopsis_SRR441544_seedlings;
}
public int getArabidopsis_SRR441545_seedlings() {
	return Arabidopsis_SRR441545_seedlings;
}
public void setArabidopsis_SRR441545_seedlings(
		int arabidopsis_SRR441545_seedlings) {
	Arabidopsis_SRR441545_seedlings = arabidopsis_SRR441545_seedlings;
}
public int getArabidopsis_SRR441546_seedlings() {
	return Arabidopsis_SRR441546_seedlings;
}
public void setArabidopsis_SRR441546_seedlings(
		int arabidopsis_SRR441546_seedlings) {
	Arabidopsis_SRR441546_seedlings = arabidopsis_SRR441546_seedlings;
}
public int getArabidopsis_SRR441547_seedlings() {
	return Arabidopsis_SRR441547_seedlings;
}
public void setArabidopsis_SRR441547_seedlings(
		int arabidopsis_SRR441547_seedlings) {
	Arabidopsis_SRR441547_seedlings = arabidopsis_SRR441547_seedlings;
}
public int getArabidopsis_SRR441548_seedlings() {
	return Arabidopsis_SRR441548_seedlings;
}
public void setArabidopsis_SRR441548_seedlings(
		int arabidopsis_SRR441548_seedlings) {
	Arabidopsis_SRR441548_seedlings = arabidopsis_SRR441548_seedlings;
}
public int getArabidopsis_SRR441549_seedlings() {
	return Arabidopsis_SRR441549_seedlings;
}
public void setArabidopsis_SRR441549_seedlings(
		int arabidopsis_SRR441549_seedlings) {
	Arabidopsis_SRR441549_seedlings = arabidopsis_SRR441549_seedlings;
}
public int getArabidopsis_SRR441550_seedlings() {
	return Arabidopsis_SRR441550_seedlings;
}
public void setArabidopsis_SRR441550_seedlings(
		int arabidopsis_SRR441550_seedlings) {
	Arabidopsis_SRR441550_seedlings = arabidopsis_SRR441550_seedlings;
}
public int getArabidopsis_SRR441551_seedlings() {
	return Arabidopsis_SRR441551_seedlings;
}
public void setArabidopsis_SRR441551_seedlings(
		int arabidopsis_SRR441551_seedlings) {
	Arabidopsis_SRR441551_seedlings = arabidopsis_SRR441551_seedlings;
}
public int getArabidopsis_SRR441552_seedlings() {
	return Arabidopsis_SRR441552_seedlings;
}
public void setArabidopsis_SRR441552_seedlings(
		int arabidopsis_SRR441552_seedlings) {
	Arabidopsis_SRR441552_seedlings = arabidopsis_SRR441552_seedlings;
}
public int getArabidopsis_SRR441553_seedlings() {
	return Arabidopsis_SRR441553_seedlings;
}
public void setArabidopsis_SRR441553_seedlings(
		int arabidopsis_SRR441553_seedlings) {
	Arabidopsis_SRR441553_seedlings = arabidopsis_SRR441553_seedlings;
}
public int getArabidopsis_SRR441554_seedlings() {
	return Arabidopsis_SRR441554_seedlings;
}
public void setArabidopsis_SRR441554_seedlings(
		int arabidopsis_SRR441554_seedlings) {
	Arabidopsis_SRR441554_seedlings = arabidopsis_SRR441554_seedlings;
}
public int getArabidopsis_SRR441555_seedlings() {
	return Arabidopsis_SRR441555_seedlings;
}
public void setArabidopsis_SRR441555_seedlings(
		int arabidopsis_SRR441555_seedlings) {
	Arabidopsis_SRR441555_seedlings = arabidopsis_SRR441555_seedlings;
}
public int getArabidopsis_SRR441556_seedlings() {
	return Arabidopsis_SRR441556_seedlings;
}
public void setArabidopsis_SRR441556_seedlings(
		int arabidopsis_SRR441556_seedlings) {
	Arabidopsis_SRR441556_seedlings = arabidopsis_SRR441556_seedlings;
}
public int getArabidopsis_SRR441557_seedlings() {
	return Arabidopsis_SRR441557_seedlings;
}
public void setArabidopsis_SRR441557_seedlings(
		int arabidopsis_SRR441557_seedlings) {
	Arabidopsis_SRR441557_seedlings = arabidopsis_SRR441557_seedlings;
}
public int getArabidopsis_SRR441558_seedlings() {
	return Arabidopsis_SRR441558_seedlings;
}
public void setArabidopsis_SRR441558_seedlings(
		int arabidopsis_SRR441558_seedlings) {
	Arabidopsis_SRR441558_seedlings = arabidopsis_SRR441558_seedlings;
}
public int getArabidopsis_SRR441559_seedlings() {
	return Arabidopsis_SRR441559_seedlings;
}
public void setArabidopsis_SRR441559_seedlings(
		int arabidopsis_SRR441559_seedlings) {
	Arabidopsis_SRR441559_seedlings = arabidopsis_SRR441559_seedlings;
}
public int getArabidopsis_SRR445204_seedlings() {
	return Arabidopsis_SRR445204_seedlings;
}
public void setArabidopsis_SRR445204_seedlings(
		int arabidopsis_SRR445204_seedlings) {
	Arabidopsis_SRR445204_seedlings = arabidopsis_SRR445204_seedlings;
}
public int getArabidopsis_SRR445205_seedlings() {
	return Arabidopsis_SRR445205_seedlings;
}
public void setArabidopsis_SRR445205_seedlings(
		int arabidopsis_SRR445205_seedlings) {
	Arabidopsis_SRR445205_seedlings = arabidopsis_SRR445205_seedlings;
}
public int getArabidopsis_SRR445206_seedlings() {
	return Arabidopsis_SRR445206_seedlings;
}
public void setArabidopsis_SRR445206_seedlings(
		int arabidopsis_SRR445206_seedlings) {
	Arabidopsis_SRR445206_seedlings = arabidopsis_SRR445206_seedlings;
}
public int getArabidopsis_SRR445207_seedlings() {
	return Arabidopsis_SRR445207_seedlings;
}
public void setArabidopsis_SRR445207_seedlings(
		int arabidopsis_SRR445207_seedlings) {
	Arabidopsis_SRR445207_seedlings = arabidopsis_SRR445207_seedlings;
}
public int getArabidopsis_SRR445208_seedlings() {
	return Arabidopsis_SRR445208_seedlings;
}
public void setArabidopsis_SRR445208_seedlings(
		int arabidopsis_SRR445208_seedlings) {
	Arabidopsis_SRR445208_seedlings = arabidopsis_SRR445208_seedlings;
}
public int getArabidopsis_SRR445209_seedlings() {
	return Arabidopsis_SRR445209_seedlings;
}
public void setArabidopsis_SRR445209_seedlings(
		int arabidopsis_SRR445209_seedlings) {
	Arabidopsis_SRR445209_seedlings = arabidopsis_SRR445209_seedlings;
}
public int getArabidopsis_SRR445210_seedlings() {
	return Arabidopsis_SRR445210_seedlings;
}
public void setArabidopsis_SRR445210_seedlings(
		int arabidopsis_SRR445210_seedlings) {
	Arabidopsis_SRR445210_seedlings = arabidopsis_SRR445210_seedlings;
}
public int getArabidopsis_SRR445211_seedlings() {
	return Arabidopsis_SRR445211_seedlings;
}
public void setArabidopsis_SRR445211_seedlings(
		int arabidopsis_SRR445211_seedlings) {
	Arabidopsis_SRR445211_seedlings = arabidopsis_SRR445211_seedlings;
}
public int getArabidopsis_SRR445212_seedlings() {
	return Arabidopsis_SRR445212_seedlings;
}
public void setArabidopsis_SRR445212_seedlings(
		int arabidopsis_SRR445212_seedlings) {
	Arabidopsis_SRR445212_seedlings = arabidopsis_SRR445212_seedlings;
}
public int getArabidopsis_SRR445213_seedlings() {
	return Arabidopsis_SRR445213_seedlings;
}
public void setArabidopsis_SRR445213_seedlings(
		int arabidopsis_SRR445213_seedlings) {
	Arabidopsis_SRR445213_seedlings = arabidopsis_SRR445213_seedlings;
}
public int getArabidopsis_SRR445214_seedlings() {
	return Arabidopsis_SRR445214_seedlings;
}
public void setArabidopsis_SRR445214_seedlings(
		int arabidopsis_SRR445214_seedlings) {
	Arabidopsis_SRR445214_seedlings = arabidopsis_SRR445214_seedlings;
}
public int getArabidopsis_SRR445215_seedlings() {
	return Arabidopsis_SRR445215_seedlings;
}
public void setArabidopsis_SRR445215_seedlings(
		int arabidopsis_SRR445215_seedlings) {
	Arabidopsis_SRR445215_seedlings = arabidopsis_SRR445215_seedlings;
}
public int getArabidopsis_SRR445216_seedlings() {
	return Arabidopsis_SRR445216_seedlings;
}
public void setArabidopsis_SRR445216_seedlings(
		int arabidopsis_SRR445216_seedlings) {
	Arabidopsis_SRR445216_seedlings = arabidopsis_SRR445216_seedlings;
}
public int getArabidopsis_SRR445217_seedlings() {
	return Arabidopsis_SRR445217_seedlings;
}
public void setArabidopsis_SRR445217_seedlings(
		int arabidopsis_SRR445217_seedlings) {
	Arabidopsis_SRR445217_seedlings = arabidopsis_SRR445217_seedlings;
}
public int getArabidopsis_SRR445218_seedlings() {
	return Arabidopsis_SRR445218_seedlings;
}
public void setArabidopsis_SRR445218_seedlings(
		int arabidopsis_SRR445218_seedlings) {
	Arabidopsis_SRR445218_seedlings = arabidopsis_SRR445218_seedlings;
}
public int getArabidopsis_SRR924263_Seedling() {
	return Arabidopsis_SRR924263_Seedling;
}
public void setArabidopsis_SRR924263_Seedling(int arabidopsis_SRR924263_Seedling) {
	Arabidopsis_SRR924263_Seedling = arabidopsis_SRR924263_Seedling;
}
public int getArabidopsis_SRR924264_Seedling() {
	return Arabidopsis_SRR924264_Seedling;
}
public void setArabidopsis_SRR924264_Seedling(int arabidopsis_SRR924264_Seedling) {
	Arabidopsis_SRR924264_Seedling = arabidopsis_SRR924264_Seedling;
}
public int getArabidopsis_SRR924265_Seedling() {
	return Arabidopsis_SRR924265_Seedling;
}
public void setArabidopsis_SRR924265_Seedling(int arabidopsis_SRR924265_Seedling) {
	Arabidopsis_SRR924265_Seedling = arabidopsis_SRR924265_Seedling;
}
public int getArabidopsis_SRR924266_Seedling() {
	return Arabidopsis_SRR924266_Seedling;
}
public void setArabidopsis_SRR924266_Seedling(int arabidopsis_SRR924266_Seedling) {
	Arabidopsis_SRR924266_Seedling = arabidopsis_SRR924266_Seedling;
}
public int getArabidopsis_SRR1055106_seedlings() {
	return Arabidopsis_SRR1055106_seedlings;
}
public void setArabidopsis_SRR1055106_seedlings(
		int arabidopsis_SRR1055106_seedlings) {
	Arabidopsis_SRR1055106_seedlings = arabidopsis_SRR1055106_seedlings;
}
public int getArabidopsis_SRR1055107_seedlings() {
	return Arabidopsis_SRR1055107_seedlings;
}
public void setArabidopsis_SRR1055107_seedlings(
		int arabidopsis_SRR1055107_seedlings) {
	Arabidopsis_SRR1055107_seedlings = arabidopsis_SRR1055107_seedlings;
}
public int getArabidopsis_SRR189802_root() {
	return Arabidopsis_SRR189802_root;
}
public void setArabidopsis_SRR189802_root(int arabidopsis_SRR189802_root) {
	Arabidopsis_SRR189802_root = arabidopsis_SRR189802_root;
}
public int getArabidopsis_SRR189806_root() {
	return Arabidopsis_SRR189806_root;
}
public void setArabidopsis_SRR189806_root(int arabidopsis_SRR189806_root) {
	Arabidopsis_SRR189806_root = arabidopsis_SRR189806_root;
}
public int getArabidopsis_SRR189810_root() {
	return Arabidopsis_SRR189810_root;
}
public void setArabidopsis_SRR189810_root(int arabidopsis_SRR189810_root) {
	Arabidopsis_SRR189810_root = arabidopsis_SRR189810_root;
}
public int getArabidopsis_SRR189813_root() {
	return Arabidopsis_SRR189813_root;
}
public void setArabidopsis_SRR189813_root(int arabidopsis_SRR189813_root) {
	Arabidopsis_SRR189813_root = arabidopsis_SRR189813_root;
}
public int getArabidopsis_SRR218085_Root() {
	return Arabidopsis_SRR218085_Root;
}
public void setArabidopsis_SRR218085_Root(int arabidopsis_SRR218085_Root) {
	Arabidopsis_SRR218085_Root = arabidopsis_SRR218085_Root;
}
public int getArabidopsis_SRR218086_Root() {
	return Arabidopsis_SRR218086_Root;
}
public void setArabidopsis_SRR218086_Root(int arabidopsis_SRR218086_Root) {
	Arabidopsis_SRR218086_Root = arabidopsis_SRR218086_Root;
}
public int getArabidopsis_SRR218087_Root() {
	return Arabidopsis_SRR218087_Root;
}
public void setArabidopsis_SRR218087_Root(int arabidopsis_SRR218087_Root) {
	Arabidopsis_SRR218087_Root = arabidopsis_SRR218087_Root;
}
public int getArabidopsis_SRR218088_Root() {
	return Arabidopsis_SRR218088_Root;
}
public void setArabidopsis_SRR218088_Root(int arabidopsis_SRR218088_Root) {
	Arabidopsis_SRR218088_Root = arabidopsis_SRR218088_Root;
}
public int getArabidopsis_SRR218089_Root() {
	return Arabidopsis_SRR218089_Root;
}
public void setArabidopsis_SRR218089_Root(int arabidopsis_SRR218089_Root) {
	Arabidopsis_SRR218089_Root = arabidopsis_SRR218089_Root;
}
public int getArabidopsis_SRR218090_Root() {
	return Arabidopsis_SRR218090_Root;
}
public void setArabidopsis_SRR218090_Root(int arabidopsis_SRR218090_Root) {
	Arabidopsis_SRR218090_Root = arabidopsis_SRR218090_Root;
}
public int getArabidopsis_SRR218092_Root() {
	return Arabidopsis_SRR218092_Root;
}
public void setArabidopsis_SRR218092_Root(int arabidopsis_SRR218092_Root) {
	Arabidopsis_SRR218092_Root = arabidopsis_SRR218092_Root;
}
public int getArabidopsis_SRR218094_Root() {
	return Arabidopsis_SRR218094_Root;
}
public void setArabidopsis_SRR218094_Root(int arabidopsis_SRR218094_Root) {
	Arabidopsis_SRR218094_Root = arabidopsis_SRR218094_Root;
}
public int getArabidopsis_SRR218095_Root() {
	return Arabidopsis_SRR218095_Root;
}
public void setArabidopsis_SRR218095_Root(int arabidopsis_SRR218095_Root) {
	Arabidopsis_SRR218095_Root = arabidopsis_SRR218095_Root;
}
public int getArabidopsis_SRR218096_Root() {
	return Arabidopsis_SRR218096_Root;
}
public void setArabidopsis_SRR218096_Root(int arabidopsis_SRR218096_Root) {
	Arabidopsis_SRR218096_Root = arabidopsis_SRR218096_Root;
}
public int getArabidopsis_SRR218097_Root() {
	return Arabidopsis_SRR218097_Root;
}
public void setArabidopsis_SRR218097_Root(int arabidopsis_SRR218097_Root) {
	Arabidopsis_SRR218097_Root = arabidopsis_SRR218097_Root;
}
public int getArabidopsis_SRR218098_Root() {
	return Arabidopsis_SRR218098_Root;
}
public void setArabidopsis_SRR218098_Root(int arabidopsis_SRR218098_Root) {
	Arabidopsis_SRR218098_Root = arabidopsis_SRR218098_Root;
}
public int getArabidopsis_SRR218099_Root() {
	return Arabidopsis_SRR218099_Root;
}
public void setArabidopsis_SRR218099_Root(int arabidopsis_SRR218099_Root) {
	Arabidopsis_SRR218099_Root = arabidopsis_SRR218099_Root;
}
public int getArabidopsis_SRR218100_Root() {
	return Arabidopsis_SRR218100_Root;
}
public void setArabidopsis_SRR218100_Root(int arabidopsis_SRR218100_Root) {
	Arabidopsis_SRR218100_Root = arabidopsis_SRR218100_Root;
}
public int getArabidopsis_SRR218101_Root() {
	return Arabidopsis_SRR218101_Root;
}
public void setArabidopsis_SRR218101_Root(int arabidopsis_SRR218101_Root) {
	Arabidopsis_SRR218101_Root = arabidopsis_SRR218101_Root;
}
public int getArabidopsis_SRR218102_Root() {
	return Arabidopsis_SRR218102_Root;
}
public void setArabidopsis_SRR218102_Root(int arabidopsis_SRR218102_Root) {
	Arabidopsis_SRR218102_Root = arabidopsis_SRR218102_Root;
}
public int getArabidopsis_SRR339951_Root() {
	return Arabidopsis_SRR339951_Root;
}
public void setArabidopsis_SRR339951_Root(int arabidopsis_SRR339951_Root) {
	Arabidopsis_SRR339951_Root = arabidopsis_SRR339951_Root;
}
public int getArabidopsis_SRR493082_root() {
	return Arabidopsis_SRR493082_root;
}
public void setArabidopsis_SRR493082_root(int arabidopsis_SRR493082_root) {
	Arabidopsis_SRR493082_root = arabidopsis_SRR493082_root;
}
public int getArabidopsis_SRR493083_root() {
	return Arabidopsis_SRR493083_root;
}
public void setArabidopsis_SRR493083_root(int arabidopsis_SRR493083_root) {
	Arabidopsis_SRR493083_root = arabidopsis_SRR493083_root;
}
public int getArabidopsis_SRR522916_root() {
	return Arabidopsis_SRR522916_root;
}
public void setArabidopsis_SRR522916_root(int arabidopsis_SRR522916_root) {
	Arabidopsis_SRR522916_root = arabidopsis_SRR522916_root;
}
public int getArabidopsis_SRR922228_Root() {
	return Arabidopsis_SRR922228_Root;
}
public void setArabidopsis_SRR922228_Root(int arabidopsis_SRR922228_Root) {
	Arabidopsis_SRR922228_Root = arabidopsis_SRR922228_Root;
}
public int getArabidopsis_SRR922229_Root() {
	return Arabidopsis_SRR922229_Root;
}
public void setArabidopsis_SRR922229_Root(int arabidopsis_SRR922229_Root) {
	Arabidopsis_SRR922229_Root = arabidopsis_SRR922229_Root;
}
public int getArabidopsis_SRR922230_Root() {
	return Arabidopsis_SRR922230_Root;
}
public void setArabidopsis_SRR922230_Root(int arabidopsis_SRR922230_Root) {
	Arabidopsis_SRR922230_Root = arabidopsis_SRR922230_Root;
}
public int getArabidopsis_SRR922231_Root() {
	return Arabidopsis_SRR922231_Root;
}
public void setArabidopsis_SRR922231_Root(int arabidopsis_SRR922231_Root) {
	Arabidopsis_SRR922231_Root = arabidopsis_SRR922231_Root;
}
public int getArabidopsis_SRR029634_controlroot_control_1() {
	return Arabidopsis_SRR029634_controlroot_control_1;
}
public void setArabidopsis_SRR029634_controlroot_control_1(
		int arabidopsis_SRR029634_controlroot_control_1) {
	Arabidopsis_SRR029634_controlroot_control_1 = arabidopsis_SRR029634_controlroot_control_1;
}
public int getArabidopsis_SRR029635_root_lowoxygen_1() {
	return Arabidopsis_SRR029635_root_lowoxygen_1;
}
public void setArabidopsis_SRR029635_root_lowoxygen_1(
		int arabidopsis_SRR029635_root_lowoxygen_1) {
	Arabidopsis_SRR029635_root_lowoxygen_1 = arabidopsis_SRR029635_root_lowoxygen_1;
}
public int getArabidopsis_SRR032112_Root__Pi_sRNASeq() {
	return Arabidopsis_SRR032112_Root__Pi_sRNASeq;
}
public void setArabidopsis_SRR032112_Root__Pi_sRNASeq(
		int arabidopsis_SRR032112_Root__Pi_sRNASeq) {
	Arabidopsis_SRR032112_Root__Pi_sRNASeq = arabidopsis_SRR032112_Root__Pi_sRNASeq;
}

public String getSourcedb() {
	return sourcedb;
}
public void setSourcedb(String sourcedb) {
	this.sourcedb = sourcedb;
}
public String getAccessurl() {
	return accessurl;
}
public void setAccessurl(String accessurl) {
	this.accessurl = accessurl;
}

public float getFlower_foldchange() {
	return Flower_foldchange;
}
public void setFlower_foldchange(float flower_foldchange) {
	Flower_foldchange = flower_foldchange;
}
public float getPollen_foldchange() {
	return Pollen_foldchange;
}
public void setPollen_foldchange(float pollen_foldchange) {
	Pollen_foldchange = pollen_foldchange;
}
public float getSeed_foldchange() {
	return Seed_foldchange;
}
public void setSeed_foldchange(float seed_foldchange) {
	Seed_foldchange = seed_foldchange;
}
public float getSeedling_foldchange() {
	return Seedling_foldchange;
}
public void setSeedling_foldchange(float seedling_foldchange) {
	Seedling_foldchange = seedling_foldchange;
}
public float getSperm_foldchange() {
	return Sperm_foldchange;
}
public void setSperm_foldchange(float sperm_foldchange) {
	Sperm_foldchange = sperm_foldchange;
}
public float getShannon_Entropy() {
	return Shannon_Entropy;
}
public void setShannon_Entropy(float shannon_Entropy) {
	Shannon_Entropy = shannon_Entropy;
}

}
