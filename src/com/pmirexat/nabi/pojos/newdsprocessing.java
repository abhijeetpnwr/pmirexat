package com.pmirexat.nabi.pojos;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="newdsprocessing")
public class newdsprocessing 
{
	public String getSpeciesearchedagainst() {
		return speciesearchedagainst;
	}

	public newdsprocessing() 
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public void setSpeciesearchedagainst(String speciesearchedagainst) {
		this.speciesearchedagainst = speciesearchedagainst;
	}


	@Override
	public String toString() {
		return "newdsprocessing [realname=" + realname + ", newfilename="
				+ newfilename + ", status=" + status + ", speciesearchedagainst="
				+ speciesearchedagainst + ", owneremail=" + owneremail
				+ ", enterytime=" + enterytime + ", completiontime="
				+ completiontime + "]";
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getnewfilename() {
		return newfilename;
	}

	public void setnewfilename(String newfilename) {
		this.newfilename = newfilename;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEnterytime() {
		return enterytime;
	}

	public void setEnterytime(Date enterytime) {
		this.enterytime = enterytime;
	}

	public Date getCompletiontime() {
		return completiontime;
	}

	public void setCompletiontime(Date completiontime) {
		this.completiontime = completiontime;
	}
	public newdsprocessing(String realname, String newfilename, String status,
			String speciesearchedagainst, String owneremail, Date enterytime,
			Date completiontime) 
	{
		super();
		this.realname = realname;
		this.newfilename = newfilename;
		this.status = status;
		this.speciesearchedagainst = speciesearchedagainst;
		this.owneremail = owneremail;
		this.enterytime = enterytime;
		this.completiontime = completiontime;
	}

	public String getOwneremail() {
		return owneremail;
	}

	public void setOwneremail(String owneremail) {
		this.owneremail = owneremail;
	}
	private String realname,newfilename,status,speciesearchedagainst,owneremail;
	
	private Date enterytime,completiontime;
}
