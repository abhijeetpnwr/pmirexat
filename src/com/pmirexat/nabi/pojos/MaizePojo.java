package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="maizedb")
public class MaizePojo
{

	public float getShannon_Entropy() {
		return Shannon_Entropy;
	}
	public void setShannon_Entropy(float shannon_Entropy) {
		Shannon_Entropy = shannon_Entropy;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	private float Shannon_Entropy,root_foldchange,shoot_foldchange,leaf_foldchange,ear_foldchange,Anther_foldchange,Tassel_foldchange,Pollen_foldchange,Silk_foldchange,DOC5_foldchange;

	private String mir_ids,sequence,sourcedb,accessurl;
	private int Maize_SRR899546_wholeplant_Control,Maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae,Maize_SRR218319_roottipB73,Maize_SRR768485_15DaySeedlingroottipOptimalnitrate,Maize_SRR768487_15DaySeedlingroottipLownitrate,Maize_SRR488770_seedlingShootapexB73_11Day,Maize_SRR488771_seedlingShootapexMo17_11Day,Maize_SRR488772_seedlingShootapexB73xMo17_11Day,Maize_SRR488773_seedlingShootapexMo17xB73_11Day,Maize_SRR488774_seedlingShootapexB73_14Day,Maize_SRR488775_seedlingshootapexMo17_14Day,Maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant,Maize_SRR924284_SeedlingleafB73_hen1_1mutant,Maize_SRR034096_SeedlingleafB73,Maize_SRR768489_15DaySeedlingleafLownitrate,Maize_SRR768492_15DaySeedlingleafOptimalnitrate,Maize_SRR488776_developingearB73HighN,Maize_SRR488777_developingearMo17HighN,Maize_SRR488778_developingearB73xMo17HighN,Maize_SRR488779_developingearB73LowN,Maize_SRR488780_developingearMo17LowN,Maize_SRR488781_developingearB73xMo17LowN,Maize_SRR317192_anthers1MM,Maize_SRR317193_anthers1_5MM,Maize_SRR317194_anthers2MM,Maize_SRR032087_ear,Maize_SRR032088_pollen,Maize_SRR032089_RootB73,Maize_SRR032090_seedlingB73,Maize_SRR032091_tassels,Maize_SRR034097_Femaleinflorescenceears_PoMS,Maize_SRR034098_Maleinflorescencetassels_PrMS,Maize_SRR768248_maturepollen,Maize_SRR768249_invitrogerminatedpollens,Maize_SRR768250_maturesilks,Maize_SRR768251_pollinatedsilks,Maize_SRR408793_unfertilizedouterear,Maize_GSM958935_13run_5DOC_B73_smRNA_rep1,Maize_SRR521124_5DOC_B73_smRNA_rep2,Maize_SRR521125_5DOC_B73_smRNA_rep3,Maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1,Maize_SRR521139_5DOC_Mo17_smRNA_rep2,Maize_SRR521140_5DOC_Mo17_smRNA_rep3;
	

	public int getPrimkey() {
		return primkey;
	}
	public float getRoot_foldchange() {
		return root_foldchange;
	}
	public void setRoot_foldchange(float root_foldchange) {
		this.root_foldchange = root_foldchange;
	}
	public float getShoot_foldchange() {
		return shoot_foldchange;
	}
	public void setShoot_foldchange(float shoot_foldchange) {
		this.shoot_foldchange = shoot_foldchange;
	}
	public float getLeaf_foldchange() {
		return leaf_foldchange;
	}
	public void setLeaf_foldchange(float leaf_foldchange) {
		this.leaf_foldchange = leaf_foldchange;
	}
	public float getEar_foldchange() {
		return ear_foldchange;
	}
	public void setEar_foldchange(float ear_foldchange) {
		this.ear_foldchange = ear_foldchange;
	}
	public float getAnther_foldchange() {
		return Anther_foldchange;
	}
	public void setAnther_foldchange(float anther_foldchange) {
		Anther_foldchange = anther_foldchange;
	}
	public float getTassel_foldchange() {
		return Tassel_foldchange;
	}
	public void setTassel_foldchange(float tassel_foldchange) {
		Tassel_foldchange = tassel_foldchange;
	}
	public float getPollen_foldchange() {
		return Pollen_foldchange;
	}
	public void setPollen_foldchange(float pollen_foldchange) {
		Pollen_foldchange = pollen_foldchange;
	}
	public float getSilk_foldchange() {
		return Silk_foldchange;
	}
	public void setSilk_foldchange(float silk_foldchange) {
		Silk_foldchange = silk_foldchange;
	}
	public float getDOC5_foldchange() {
		return DOC5_foldchange;
	}
	public void setDOC5_foldchange(float dOC5_foldchange) {
		DOC5_foldchange = dOC5_foldchange;
	}
	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}
	
	public String getMir_ids() {
		return mir_ids;
	}
	public void setMir_ids(String mir_ids) {
		this.mir_ids = mir_ids;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getSourcedb() {
		return sourcedb;
	}
	public void setSourcedb(String sourcedb) {
		this.sourcedb = sourcedb;
	}
	public String getAccessurl() {
		return accessurl;
	}
	public void setAccessurl(String accessurl) {
		this.accessurl = accessurl;
	}
	public int getMaize_SRR899546_wholeplant_Control() {
		return Maize_SRR899546_wholeplant_Control;
	}
	public void setMaize_SRR899546_wholeplant_Control(
			int maize_SRR899546_wholeplant_Control) {
		Maize_SRR899546_wholeplant_Control = maize_SRR899546_wholeplant_Control;
	}
	public int getMaize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae() {
		return Maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae;
	}
	public void setMaize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae(
			int maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae) {
		Maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae = maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae;
	}
	public int getMaize_SRR218319_roottipB73() {
		return Maize_SRR218319_roottipB73;
	}
	public void setMaize_SRR218319_roottipB73(int maize_SRR218319_roottipB73) {
		Maize_SRR218319_roottipB73 = maize_SRR218319_roottipB73;
	}
	public int getMaize_SRR768485_15DaySeedlingroottipOptimalnitrate() {
		return Maize_SRR768485_15DaySeedlingroottipOptimalnitrate;
	}
	public void setMaize_SRR768485_15DaySeedlingroottipOptimalnitrate(
			int maize_SRR768485_15DaySeedlingroottipOptimalnitrate) {
		Maize_SRR768485_15DaySeedlingroottipOptimalnitrate = maize_SRR768485_15DaySeedlingroottipOptimalnitrate;
	}
	public int getMaize_SRR768487_15DaySeedlingroottipLownitrate() {
		return Maize_SRR768487_15DaySeedlingroottipLownitrate;
	}
	public void setMaize_SRR768487_15DaySeedlingroottipLownitrate(
			int maize_SRR768487_15DaySeedlingroottipLownitrate) {
		Maize_SRR768487_15DaySeedlingroottipLownitrate = maize_SRR768487_15DaySeedlingroottipLownitrate;
	}
	public int getMaize_SRR488770_seedlingShootapexB73_11Day() {
		return Maize_SRR488770_seedlingShootapexB73_11Day;
	}
	public void setMaize_SRR488770_seedlingShootapexB73_11Day(
			int maize_SRR488770_seedlingShootapexB73_11Day) {
		Maize_SRR488770_seedlingShootapexB73_11Day = maize_SRR488770_seedlingShootapexB73_11Day;
	}
	public int getMaize_SRR488771_seedlingShootapexMo17_11Day() {
		return Maize_SRR488771_seedlingShootapexMo17_11Day;
	}
	public void setMaize_SRR488771_seedlingShootapexMo17_11Day(
			int maize_SRR488771_seedlingShootapexMo17_11Day) {
		Maize_SRR488771_seedlingShootapexMo17_11Day = maize_SRR488771_seedlingShootapexMo17_11Day;
	}
	public int getMaize_SRR488772_seedlingShootapexB73xMo17_11Day() {
		return Maize_SRR488772_seedlingShootapexB73xMo17_11Day;
	}
	public void setMaize_SRR488772_seedlingShootapexB73xMo17_11Day(
			int maize_SRR488772_seedlingShootapexB73xMo17_11Day) {
		Maize_SRR488772_seedlingShootapexB73xMo17_11Day = maize_SRR488772_seedlingShootapexB73xMo17_11Day;
	}
	public int getMaize_SRR488773_seedlingShootapexMo17xB73_11Day() {
		return Maize_SRR488773_seedlingShootapexMo17xB73_11Day;
	}
	public void setMaize_SRR488773_seedlingShootapexMo17xB73_11Day(
			int maize_SRR488773_seedlingShootapexMo17xB73_11Day) {
		Maize_SRR488773_seedlingShootapexMo17xB73_11Day = maize_SRR488773_seedlingShootapexMo17xB73_11Day;
	}
	public int getMaize_SRR488774_seedlingShootapexB73_14Day() {
		return Maize_SRR488774_seedlingShootapexB73_14Day;
	}
	public void setMaize_SRR488774_seedlingShootapexB73_14Day(
			int maize_SRR488774_seedlingShootapexB73_14Day) {
		Maize_SRR488774_seedlingShootapexB73_14Day = maize_SRR488774_seedlingShootapexB73_14Day;
	}
	public int getMaize_SRR488775_seedlingshootapexMo17_14Day() {
		return Maize_SRR488775_seedlingshootapexMo17_14Day;
	}
	public void setMaize_SRR488775_seedlingshootapexMo17_14Day(
			int maize_SRR488775_seedlingshootapexMo17_14Day) {
		Maize_SRR488775_seedlingshootapexMo17_14Day = maize_SRR488775_seedlingshootapexMo17_14Day;
	}
	public int getMaize_SRR924283_SeedlingleafB73xW22_hen1_1mutant() {
		return Maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant;
	}
	public void setMaize_SRR924283_SeedlingleafB73xW22_hen1_1mutant(
			int maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant) {
		Maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant = maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant;
	}
	public int getMaize_SRR924284_SeedlingleafB73_hen1_1mutant() {
		return Maize_SRR924284_SeedlingleafB73_hen1_1mutant;
	}
	public void setMaize_SRR924284_SeedlingleafB73_hen1_1mutant(
			int maize_SRR924284_SeedlingleafB73_hen1_1mutant) {
		Maize_SRR924284_SeedlingleafB73_hen1_1mutant = maize_SRR924284_SeedlingleafB73_hen1_1mutant;
	}
	public int getMaize_SRR034096_SeedlingleafB73() {
		return Maize_SRR034096_SeedlingleafB73;
	}
	public void setMaize_SRR034096_SeedlingleafB73(
			int maize_SRR034096_SeedlingleafB73) {
		Maize_SRR034096_SeedlingleafB73 = maize_SRR034096_SeedlingleafB73;
	}
	public int getMaize_SRR768489_15DaySeedlingleafLownitrate() {
		return Maize_SRR768489_15DaySeedlingleafLownitrate;
	}
	public void setMaize_SRR768489_15DaySeedlingleafLownitrate(
			int maize_SRR768489_15DaySeedlingleafLownitrate) {
		Maize_SRR768489_15DaySeedlingleafLownitrate = maize_SRR768489_15DaySeedlingleafLownitrate;
	}
	public int getMaize_SRR768492_15DaySeedlingleafOptimalnitrate() {
		return Maize_SRR768492_15DaySeedlingleafOptimalnitrate;
	}
	public void setMaize_SRR768492_15DaySeedlingleafOptimalnitrate(
			int maize_SRR768492_15DaySeedlingleafOptimalnitrate) {
		Maize_SRR768492_15DaySeedlingleafOptimalnitrate = maize_SRR768492_15DaySeedlingleafOptimalnitrate;
	}
	public int getMaize_SRR488776_developingearB73HighN() {
		return Maize_SRR488776_developingearB73HighN;
	}
	public void setMaize_SRR488776_developingearB73HighN(
			int maize_SRR488776_developingearB73HighN) {
		Maize_SRR488776_developingearB73HighN = maize_SRR488776_developingearB73HighN;
	}
	public int getMaize_SRR488777_developingearMo17HighN() {
		return Maize_SRR488777_developingearMo17HighN;
	}
	public void setMaize_SRR488777_developingearMo17HighN(
			int maize_SRR488777_developingearMo17HighN) {
		Maize_SRR488777_developingearMo17HighN = maize_SRR488777_developingearMo17HighN;
	}
	public int getMaize_SRR488778_developingearB73xMo17HighN() {
		return Maize_SRR488778_developingearB73xMo17HighN;
	}
	public void setMaize_SRR488778_developingearB73xMo17HighN(
			int maize_SRR488778_developingearB73xMo17HighN) {
		Maize_SRR488778_developingearB73xMo17HighN = maize_SRR488778_developingearB73xMo17HighN;
	}
	public int getMaize_SRR488779_developingearB73LowN() {
		return Maize_SRR488779_developingearB73LowN;
	}
	public void setMaize_SRR488779_developingearB73LowN(
			int maize_SRR488779_developingearB73LowN) {
		Maize_SRR488779_developingearB73LowN = maize_SRR488779_developingearB73LowN;
	}
	public int getMaize_SRR488780_developingearMo17LowN() {
		return Maize_SRR488780_developingearMo17LowN;
	}
	public void setMaize_SRR488780_developingearMo17LowN(
			int maize_SRR488780_developingearMo17LowN) {
		Maize_SRR488780_developingearMo17LowN = maize_SRR488780_developingearMo17LowN;
	}
	public int getMaize_SRR488781_developingearB73xMo17LowN() {
		return Maize_SRR488781_developingearB73xMo17LowN;
	}
	public void setMaize_SRR488781_developingearB73xMo17LowN(
			int maize_SRR488781_developingearB73xMo17LowN) {
		Maize_SRR488781_developingearB73xMo17LowN = maize_SRR488781_developingearB73xMo17LowN;
	}
	public int getMaize_SRR317192_anthers1MM() {
		return Maize_SRR317192_anthers1MM;
	}
	public void setMaize_SRR317192_anthers1MM(int maize_SRR317192_anthers1MM) {
		Maize_SRR317192_anthers1MM = maize_SRR317192_anthers1MM;
	}
	public int getMaize_SRR317193_anthers1_5MM() {
		return Maize_SRR317193_anthers1_5MM;
	}
	public void setMaize_SRR317193_anthers1_5MM(int maize_SRR317193_anthers1_5MM) {
		Maize_SRR317193_anthers1_5MM = maize_SRR317193_anthers1_5MM;
	}
	public int getMaize_SRR317194_anthers2MM() {
		return Maize_SRR317194_anthers2MM;
	}
	public void setMaize_SRR317194_anthers2MM(int maize_SRR317194_anthers2MM) {
		Maize_SRR317194_anthers2MM = maize_SRR317194_anthers2MM;
	}
	public int getMaize_SRR032087_ear() {
		return Maize_SRR032087_ear;
	}
	public void setMaize_SRR032087_ear(int maize_SRR032087_ear) {
		Maize_SRR032087_ear = maize_SRR032087_ear;
	}
	public int getMaize_SRR032088_pollen() {
		return Maize_SRR032088_pollen;
	}
	public void setMaize_SRR032088_pollen(int maize_SRR032088_pollen) {
		Maize_SRR032088_pollen = maize_SRR032088_pollen;
	}
	public int getMaize_SRR032089_RootB73() {
		return Maize_SRR032089_RootB73;
	}
	public void setMaize_SRR032089_RootB73(int maize_SRR032089_RootB73) {
		Maize_SRR032089_RootB73 = maize_SRR032089_RootB73;
	}
	public int getMaize_SRR032090_seedlingB73() {
		return Maize_SRR032090_seedlingB73;
	}
	public void setMaize_SRR032090_seedlingB73(int maize_SRR032090_seedlingB73) {
		Maize_SRR032090_seedlingB73 = maize_SRR032090_seedlingB73;
	}
	public int getMaize_SRR032091_tassels() {
		return Maize_SRR032091_tassels;
	}
	public void setMaize_SRR032091_tassels(int maize_SRR032091_tassels) {
		Maize_SRR032091_tassels = maize_SRR032091_tassels;
	}
	public int getMaize_SRR034097_Femaleinflorescenceears_PoMS() {
		return Maize_SRR034097_Femaleinflorescenceears_PoMS;
	}
	public void setMaize_SRR034097_Femaleinflorescenceears_PoMS(
			int maize_SRR034097_Femaleinflorescenceears_PoMS) {
		Maize_SRR034097_Femaleinflorescenceears_PoMS = maize_SRR034097_Femaleinflorescenceears_PoMS;
	}
	public int getMaize_SRR034098_Maleinflorescencetassels_PrMS() {
		return Maize_SRR034098_Maleinflorescencetassels_PrMS;
	}
	public void setMaize_SRR034098_Maleinflorescencetassels_PrMS(
			int maize_SRR034098_Maleinflorescencetassels_PrMS) {
		Maize_SRR034098_Maleinflorescencetassels_PrMS = maize_SRR034098_Maleinflorescencetassels_PrMS;
	}
	public int getMaize_SRR768248_maturepollen() {
		return Maize_SRR768248_maturepollen;
	}
	public void setMaize_SRR768248_maturepollen(int maize_SRR768248_maturepollen) {
		Maize_SRR768248_maturepollen = maize_SRR768248_maturepollen;
	}
	public int getMaize_SRR768249_invitrogerminatedpollens() {
		return Maize_SRR768249_invitrogerminatedpollens;
	}
	public void setMaize_SRR768249_invitrogerminatedpollens(
			int maize_SRR768249_invitrogerminatedpollens) {
		Maize_SRR768249_invitrogerminatedpollens = maize_SRR768249_invitrogerminatedpollens;
	}
	public int getMaize_SRR768250_maturesilks() {
		return Maize_SRR768250_maturesilks;
	}
	public void setMaize_SRR768250_maturesilks(int maize_SRR768250_maturesilks) {
		Maize_SRR768250_maturesilks = maize_SRR768250_maturesilks;
	}
	public int getMaize_SRR768251_pollinatedsilks() {
		return Maize_SRR768251_pollinatedsilks;
	}
	public void setMaize_SRR768251_pollinatedsilks(
			int maize_SRR768251_pollinatedsilks) {
		Maize_SRR768251_pollinatedsilks = maize_SRR768251_pollinatedsilks;
	}
	public int getMaize_SRR408793_unfertilizedouterear() {
		return Maize_SRR408793_unfertilizedouterear;
	}
	public void setMaize_SRR408793_unfertilizedouterear(
			int maize_SRR408793_unfertilizedouterear) {
		Maize_SRR408793_unfertilizedouterear = maize_SRR408793_unfertilizedouterear;
	}
	public int getMaize_GSM958935_13run_5DOC_B73_smRNA_rep1() {
		return Maize_GSM958935_13run_5DOC_B73_smRNA_rep1;
	}
	public void setMaize_GSM958935_13run_5DOC_B73_smRNA_rep1(
			int maize_GSM958935_13run_5DOC_B73_smRNA_rep1) {
		Maize_GSM958935_13run_5DOC_B73_smRNA_rep1 = maize_GSM958935_13run_5DOC_B73_smRNA_rep1;
	}
	public int getMaize_SRR521124_5DOC_B73_smRNA_rep2() {
		return Maize_SRR521124_5DOC_B73_smRNA_rep2;
	}
	public void setMaize_SRR521124_5DOC_B73_smRNA_rep2(
			int maize_SRR521124_5DOC_B73_smRNA_rep2) {
		Maize_SRR521124_5DOC_B73_smRNA_rep2 = maize_SRR521124_5DOC_B73_smRNA_rep2;
	}
	public int getMaize_SRR521125_5DOC_B73_smRNA_rep3() {
		return Maize_SRR521125_5DOC_B73_smRNA_rep3;
	}
	public void setMaize_SRR521125_5DOC_B73_smRNA_rep3(
			int maize_SRR521125_5DOC_B73_smRNA_rep3) {
		Maize_SRR521125_5DOC_B73_smRNA_rep3 = maize_SRR521125_5DOC_B73_smRNA_rep3;
	}
	public int getMaize_GSM958938_13run_5DOC_Mo17_smRNA_rep1() {
		return Maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1;
	}
	public void setMaize_GSM958938_13run_5DOC_Mo17_smRNA_rep1(
			int maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1) {
		Maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1 = maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1;
	}
	public int getMaize_SRR521139_5DOC_Mo17_smRNA_rep2() {
		return Maize_SRR521139_5DOC_Mo17_smRNA_rep2;
	}
	public void setMaize_SRR521139_5DOC_Mo17_smRNA_rep2(
			int maize_SRR521139_5DOC_Mo17_smRNA_rep2) {
		Maize_SRR521139_5DOC_Mo17_smRNA_rep2 = maize_SRR521139_5DOC_Mo17_smRNA_rep2;
	}
	public int getMaize_SRR521140_5DOC_Mo17_smRNA_rep3() {
		return Maize_SRR521140_5DOC_Mo17_smRNA_rep3;
	}
	public void setMaize_SRR521140_5DOC_Mo17_smRNA_rep3(
			int maize_SRR521140_5DOC_Mo17_smRNA_rep3) {
		Maize_SRR521140_5DOC_Mo17_smRNA_rep3 = maize_SRR521140_5DOC_Mo17_smRNA_rep3;
	}
	

    
	
}
