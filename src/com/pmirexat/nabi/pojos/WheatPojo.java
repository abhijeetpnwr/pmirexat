package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="wheatdb")
public class WheatPojo
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;

	@Override
	public String toString() {
		return "WheatPojo [primkey=" + primkey + ", mir_ids=" + mir_ids
				+ ", sequence=" + sequence + ", sourcedb=" + sourcedb
				+ ", accessurl=" + accessurl
				+ ", Wheat_SRR101427_youngleafControl="
				+ Wheat_SRR101427_youngleafControl
				+ ", Wheat_SRR101428_youngleafinfectionpowderymildew="
				+ Wheat_SRR101428_youngleafinfectionpowderymildew
				+ ", Wheat_SRR101429_youngleafControl="
				+ Wheat_SRR101429_youngleafControl
				+ ", Wheat_SRR101430_youngleafinfectionpowderymildew="
				+ Wheat_SRR101430_youngleafinfectionpowderymildew
				+ ", Wheat_SRR101431_youngleafControl="
				+ Wheat_SRR101431_youngleafControl
				+ ", Wheat_SRR101432_youngleafstress40Cheatstress="
				+ Wheat_SRR101432_youngleafstress40Cheatstress
				+ ", Wheat_SRR057406_FlagleafNAM_RNAi="
				+ Wheat_SRR057406_FlagleafNAM_RNAi
				+ ", Wheat_SRR449364_spiketissuesControl="
				+ Wheat_SRR449364_spiketissuesControl
				+ ", Wheat_SRR449365_spiketissuesColdtreatment="
				+ Wheat_SRR449365_spiketissuesColdtreatment
				+ ", Wheat_SRR449366_spiketissuesColdtreatment="
				+ Wheat_SRR449366_spiketissuesColdtreatment
				+ ", Wheat_SRR449367_spiketissuesColdtreatment="
				+ Wheat_SRR449367_spiketissuesColdtreatment
				+ ", Wheat_SRR449368_spiketissuesControl="
				+ Wheat_SRR449368_spiketissuesControl
				+ ", Wheat_SRR449369_spiketissuesControl="
				+ Wheat_SRR449369_spiketissuesControl
				+ ", Wheat_SRR449370_spiketissuesControl="
				+ Wheat_SRR449370_spiketissuesControl
				+ ", Wheat_SRR800157_Genericsample="
				+ Wheat_SRR800157_Genericsample
				+ ", Wheat_SRR800158_Genericsample="
				+ Wheat_SRR800158_Genericsample
				+ ", Wheat_SRR017199_Genericsample="
				+ Wheat_SRR017199_Genericsample
				+ ", Wheat_SRR203489_wholeplantparentBBAA="
				+ Wheat_SRR203489_wholeplantparentBBAA
				+ ", Wheat_SRR203490_wholeplantparentDD="
				+ Wheat_SRR203490_wholeplantparentDD
				+ ", Wheat_SRR203491_wholeplantHybridBAD="
				+ Wheat_SRR203491_wholeplantHybridBAD
				+ ", Wheat_SRR203492_wholeplantPolyploidBBAADD="
				+ Wheat_SRR203492_wholeplantPolyploidBBAADD
				+ ", LeafS_foldchange=" + LeafS_foldchange
				+ ", SpikeS_foldchange=" + SpikeS_foldchange
				+ ", Shannon_Entropy=" + Shannon_Entropy + "]";
	}

	public int getPrimkey() {
		return primkey;
	}

	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}

	public String getMir_ids() {
		return mir_ids;
	}

	public void setMir_ids(String mir_ids) {
		this.mir_ids = mir_ids;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getSourcedb() {
		return sourcedb;
	}

	public void setSourcedb(String sourcedb) {
		this.sourcedb = sourcedb;
	}

	public String getAccessurl() {
		return accessurl;
	}

	public void setAccessurl(String accessurl) {
		this.accessurl = accessurl;
	}

	public int getWheat_SRR101427_youngleafControl() {
		return Wheat_SRR101427_youngleafControl;
	}

	public void setWheat_SRR101427_youngleafControl(
			int wheat_SRR101427_youngleafControl) {
		Wheat_SRR101427_youngleafControl = wheat_SRR101427_youngleafControl;
	}

	public int getWheat_SRR101428_youngleafinfectionpowderymildew() {
		return Wheat_SRR101428_youngleafinfectionpowderymildew;
	}

	public void setWheat_SRR101428_youngleafinfectionpowderymildew(
			int wheat_SRR101428_youngleafinfectionpowderymildew) {
		Wheat_SRR101428_youngleafinfectionpowderymildew = wheat_SRR101428_youngleafinfectionpowderymildew;
	}

	public int getWheat_SRR101429_youngleafControl() {
		return Wheat_SRR101429_youngleafControl;
	}

	public void setWheat_SRR101429_youngleafControl(
			int wheat_SRR101429_youngleafControl) {
		Wheat_SRR101429_youngleafControl = wheat_SRR101429_youngleafControl;
	}

	public int getWheat_SRR101430_youngleafinfectionpowderymildew() {
		return Wheat_SRR101430_youngleafinfectionpowderymildew;
	}

	public void setWheat_SRR101430_youngleafinfectionpowderymildew(
			int wheat_SRR101430_youngleafinfectionpowderymildew) {
		Wheat_SRR101430_youngleafinfectionpowderymildew = wheat_SRR101430_youngleafinfectionpowderymildew;
	}

	public int getWheat_SRR101431_youngleafControl() {
		return Wheat_SRR101431_youngleafControl;
	}

	public void setWheat_SRR101431_youngleafControl(
			int wheat_SRR101431_youngleafControl) {
		Wheat_SRR101431_youngleafControl = wheat_SRR101431_youngleafControl;
	}

	public int getWheat_SRR101432_youngleafstress40Cheatstress() {
		return Wheat_SRR101432_youngleafstress40Cheatstress;
	}

	public void setWheat_SRR101432_youngleafstress40Cheatstress(
			int wheat_SRR101432_youngleafstress40Cheatstress) {
		Wheat_SRR101432_youngleafstress40Cheatstress = wheat_SRR101432_youngleafstress40Cheatstress;
	}

	public int getWheat_SRR057406_FlagleafNAM_RNAi() {
		return Wheat_SRR057406_FlagleafNAM_RNAi;
	}

	public void setWheat_SRR057406_FlagleafNAM_RNAi(
			int wheat_SRR057406_FlagleafNAM_RNAi) {
		Wheat_SRR057406_FlagleafNAM_RNAi = wheat_SRR057406_FlagleafNAM_RNAi;
	}

	public int getWheat_SRR449364_spiketissuesControl() {
		return Wheat_SRR449364_spiketissuesControl;
	}

	public void setWheat_SRR449364_spiketissuesControl(
			int wheat_SRR449364_spiketissuesControl) {
		Wheat_SRR449364_spiketissuesControl = wheat_SRR449364_spiketissuesControl;
	}

	public int getWheat_SRR449365_spiketissuesColdtreatment() {
		return Wheat_SRR449365_spiketissuesColdtreatment;
	}

	public void setWheat_SRR449365_spiketissuesColdtreatment(
			int wheat_SRR449365_spiketissuesColdtreatment) {
		Wheat_SRR449365_spiketissuesColdtreatment = wheat_SRR449365_spiketissuesColdtreatment;
	}

	public int getWheat_SRR449366_spiketissuesColdtreatment() {
		return Wheat_SRR449366_spiketissuesColdtreatment;
	}

	public void setWheat_SRR449366_spiketissuesColdtreatment(
			int wheat_SRR449366_spiketissuesColdtreatment) {
		Wheat_SRR449366_spiketissuesColdtreatment = wheat_SRR449366_spiketissuesColdtreatment;
	}

	public int getWheat_SRR449367_spiketissuesColdtreatment() {
		return Wheat_SRR449367_spiketissuesColdtreatment;
	}

	public void setWheat_SRR449367_spiketissuesColdtreatment(
			int wheat_SRR449367_spiketissuesColdtreatment) {
		Wheat_SRR449367_spiketissuesColdtreatment = wheat_SRR449367_spiketissuesColdtreatment;
	}

	public int getWheat_SRR449368_spiketissuesControl() {
		return Wheat_SRR449368_spiketissuesControl;
	}

	public void setWheat_SRR449368_spiketissuesControl(
			int wheat_SRR449368_spiketissuesControl) {
		Wheat_SRR449368_spiketissuesControl = wheat_SRR449368_spiketissuesControl;
	}

	public int getWheat_SRR449369_spiketissuesControl() {
		return Wheat_SRR449369_spiketissuesControl;
	}

	public void setWheat_SRR449369_spiketissuesControl(
			int wheat_SRR449369_spiketissuesControl) {
		Wheat_SRR449369_spiketissuesControl = wheat_SRR449369_spiketissuesControl;
	}

	public int getWheat_SRR449370_spiketissuesControl() {
		return Wheat_SRR449370_spiketissuesControl;
	}

	public void setWheat_SRR449370_spiketissuesControl(
			int wheat_SRR449370_spiketissuesControl) {
		Wheat_SRR449370_spiketissuesControl = wheat_SRR449370_spiketissuesControl;
	}

	public int getWheat_SRR800157_Genericsample() {
		return Wheat_SRR800157_Genericsample;
	}

	public void setWheat_SRR800157_Genericsample(int wheat_SRR800157_Genericsample) {
		Wheat_SRR800157_Genericsample = wheat_SRR800157_Genericsample;
	}

	public int getWheat_SRR800158_Genericsample() {
		return Wheat_SRR800158_Genericsample;
	}

	public void setWheat_SRR800158_Genericsample(int wheat_SRR800158_Genericsample) {
		Wheat_SRR800158_Genericsample = wheat_SRR800158_Genericsample;
	}

	public int getWheat_SRR017199_Genericsample() {
		return Wheat_SRR017199_Genericsample;
	}

	public void setWheat_SRR017199_Genericsample(int wheat_SRR017199_Genericsample) {
		Wheat_SRR017199_Genericsample = wheat_SRR017199_Genericsample;
	}

	public int getWheat_SRR203489_wholeplantparentBBAA() {
		return Wheat_SRR203489_wholeplantparentBBAA;
	}

	public void setWheat_SRR203489_wholeplantparentBBAA(
			int wheat_SRR203489_wholeplantparentBBAA) {
		Wheat_SRR203489_wholeplantparentBBAA = wheat_SRR203489_wholeplantparentBBAA;
	}

	public int getWheat_SRR203490_wholeplantparentDD() {
		return Wheat_SRR203490_wholeplantparentDD;
	}

	public void setWheat_SRR203490_wholeplantparentDD(
			int wheat_SRR203490_wholeplantparentDD) {
		Wheat_SRR203490_wholeplantparentDD = wheat_SRR203490_wholeplantparentDD;
	}

	public int getWheat_SRR203491_wholeplantHybridBAD() {
		return Wheat_SRR203491_wholeplantHybridBAD;
	}

	public void setWheat_SRR203491_wholeplantHybridBAD(
			int wheat_SRR203491_wholeplantHybridBAD) {
		Wheat_SRR203491_wholeplantHybridBAD = wheat_SRR203491_wholeplantHybridBAD;
	}

	public int getWheat_SRR203492_wholeplantPolyploidBBAADD() {
		return Wheat_SRR203492_wholeplantPolyploidBBAADD;
	}

	public void setWheat_SRR203492_wholeplantPolyploidBBAADD(
			int wheat_SRR203492_wholeplantPolyploidBBAADD) {
		Wheat_SRR203492_wholeplantPolyploidBBAADD = wheat_SRR203492_wholeplantPolyploidBBAADD;
	}

	public float getLeafS_foldchange() {
		return LeafS_foldchange;
	}

	public void setLeafS_foldchange(float leafS_foldchange) {
		LeafS_foldchange = leafS_foldchange;
	}

	public float getSpikeS_foldchange() {
		return SpikeS_foldchange;
	}

	public void setSpikeS_foldchange(float spikeS_foldchange) {
		SpikeS_foldchange = spikeS_foldchange;
	}

	String mir_ids,sequence,sourcedb,accessurl;
	
	int Wheat_SRR101427_youngleafControl,Wheat_SRR101428_youngleafinfectionpowderymildew,Wheat_SRR101429_youngleafControl,Wheat_SRR101430_youngleafinfectionpowderymildew,Wheat_SRR101431_youngleafControl,Wheat_SRR101432_youngleafstress40Cheatstress,Wheat_SRR057406_FlagleafNAM_RNAi,Wheat_SRR449364_spiketissuesControl,Wheat_SRR449365_spiketissuesColdtreatment,Wheat_SRR449366_spiketissuesColdtreatment,Wheat_SRR449367_spiketissuesColdtreatment,Wheat_SRR449368_spiketissuesControl,Wheat_SRR449369_spiketissuesControl,Wheat_SRR449370_spiketissuesControl,Wheat_SRR800157_Genericsample,Wheat_SRR800158_Genericsample,Wheat_SRR017199_Genericsample,Wheat_SRR203489_wholeplantparentBBAA,Wheat_SRR203490_wholeplantparentDD,Wheat_SRR203491_wholeplantHybridBAD,Wheat_SRR203492_wholeplantPolyploidBBAADD;
	
    float LeafS_foldchange,SpikeS_foldchange,Shannon_Entropy;

	public float getShannon_Entropy() {
		return Shannon_Entropy;
	}

	public void setShannon_Entropy(float shannon_Entropy) {
		Shannon_Entropy = shannon_Entropy;
	}
	
}
