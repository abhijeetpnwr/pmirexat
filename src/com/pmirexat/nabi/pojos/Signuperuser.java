package com.pmirexat.nabi.pojos;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="userinfo")
public class Signuperuser
{
	
	public Signuperuser(String first_name, String last_name, String email,
			String password, String verificationstatus, Date startdate,
			String institutename, String address, String contactno,
			String reasontosignup, String department) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.password = password;
		this.verificationstatus = verificationstatus;
		this.startdate = startdate;
		this.institutename = institutename;
		Address = address;
		this.contactno = contactno;
		this.reasontosignup = reasontosignup;
		this.department = department;
	}

	public Signuperuser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	public int getPrimkey() {
		return primkey;
	}

	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}

	private  String first_name;
	private  String last_name;
	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public String getReasontosignup() {
		return reasontosignup;
	}

	public void setReasontosignup(String reasontosignup) {
		this.reasontosignup = reasontosignup;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	private  String email ;
	private  String password ;
	private String verificationstatus;
	private Date startdate;
	private String institutename,Address,contactno,reasontosignup,department;
	
	public String getInstitutename() {
		return institutename;
	}

	public void setInstitutename(String institutename) {
		this.institutename = institutename;
	}

	public String getVerificationstatus() {
		return verificationstatus;
	}

	public void setVerificationstatus(String verificationstatus) {
		this.verificationstatus = verificationstatus;
	}






	@Override
	public String toString() {
		return "Signuperuser [primkey=" + primkey + ", first_name="
				+ first_name + ", last_name=" + last_name + ", email=" + email
				+ ", password=" + password + ", verificationstatus="
				+ verificationstatus + ", startdate=" + startdate + "]";
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
