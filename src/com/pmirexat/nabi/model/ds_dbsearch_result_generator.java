package com.pmirexat.nabi.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import com.pmirexat.nabi.dao.DbCommands;


public class ds_dbsearch_result_generator
{
	
	public List<List<String>> generatetablle(String tobesearch_rna,String select_datasets,String checkedcategory)
	{
		
		//System.out.println("toser rna "+tobesearch_rna);
		//System.out.println(" Check 2 : toser rna "+select_datasets);
		
		String towrite = "Dataset Name \t"+tobesearch_rna.replace(",","\t");
		
		List<List<String>> dbspecific_result=new DbCommands().Specfic_ds_search0(tobesearch_rna, select_datasets,new class_entityconvert().getclassname(checkedcategory));
		
		
		////System.out.println(" dbspecific result is :"+dbspecific_result);
		StringTokenizer st=new StringTokenizer(select_datasets,",");
		
		List<List<String>> finalresultlist=new ArrayList<List<String>>();				
		
		List<String> datasets=new ArrayList<String>();
		
		while (st.hasMoreElements()) 
		{
			
			////System.out.println("In while");
			 String dataset = (String) st.nextElement();	
			 datasets.add(dataset);
		}
		
		
		////System.out.println("while ended");
		int totaldatasets=datasets.size();
		
	//	//System.out.println("size is"+totaldatasets);
  	  List<String> row_list;
  	  
		 for(int i=0;i<totaldatasets;i++)
	      {
       	   
			 row_list=new ArrayList<String>();
			 
		//	 //System.out.println("value of dataset"+datasets.get(0));
			 
			 row_list.add(datasets.get(i));
       	 
 
       	  
       	    	for (List<String> innerlist : dbspecific_result)
       	    	{	 
       	    		row_list.add((innerlist.get(i)));
		        }
       	  
       	     String tabbedline = StringUtils.join(row_list, "\t");
       	     towrite=towrite+"\n"+tabbedline;
       	  
		    finalresultlist.add(row_list); 
       	  
	      }
		 
		
		 PrintWriter writer;
		 
		 Properties prop = new Properties();
		 InputStream input = null;
		 String path="";
		 
		 String filename ="";
	     try
	     {
			  input =  new FileInputStream("config.properties");
			  prop.load(input);
			  path =  prop.getProperty("path");
			  //System.out.println("Path is :"+path);
		} 
	     catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	  

	     Random randomGenerator = new Random();
	     int rand = randomGenerator.nextInt();

			// get the property value and print it out
			//System.out.println(prop.getProperty("database"));
			
		try {
			
			path = path.replace("\"","");
			
			 filename = path+"temp"+rand+".txt";
			
			//System.out.println("File name is :"+filename);
			
			writer = new PrintWriter(filename, "UTF-8");
			 writer.println(towrite);
			 writer.close();
			 //System.out.println(" File should be written by now");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> tempfilename = new ArrayList<>();

		tempfilename.add(filename);
		
		//Name of temporary created csv file will be required to generate morphesus utility.So added it as last element
		finalresultlist.add(tempfilename); 
		
		return finalresultlist;
	}
}
