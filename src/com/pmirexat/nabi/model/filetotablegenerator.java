package com.pmirexat.nabi.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class filetotablegenerator
{
   public String fietotableconvertor(String filename,String path) throws IOException
   {

	 String filePath = path+filename; //remember to remove in linux --- > .txt
	 
       File downloadFile = new File(filePath);
       FileInputStream inStream = new FileInputStream(downloadFile);
        
       BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
       StringBuilder out = new StringBuilder();
       
       String line;
       
       
       String tablestring = "<table class=\"table  table-striped table-bordered table-hover table-condensed\" id=\"resultable\" style=\"max-height:270px;overflow:auto;width:100%;display:block;margin-top:20px;\">";
       
       while ((line = reader.readLine()) != null) 
       {
       	
       	tablestring =  tablestring +"<tr>";
           out.append(line);
         
            
           String[] splitelem = line.split("\\s+");

           
           for (String string : splitelem)
           {
          
				 tablestring = tablestring+ "<td>"+string.toString()+"</td>";
			}		            
           
           tablestring = tablestring + "</tr>";
       }
       
       tablestring = tablestring + "</table>";  //Prints the string content read from input stream
       
       reader.close();
       
       return tablestring;
	   
   }
}
