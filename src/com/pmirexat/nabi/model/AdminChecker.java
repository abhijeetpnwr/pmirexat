package com.pmirexat.nabi.model;

import java.util.Arrays;

import org.eclipse.jdt.internal.compiler.ast.ThisReference;

//import bsh.This;

public class AdminChecker
{
	private String[] adminids = {"bioinfoman3@gmail.com", "shrikant@nabi.res.in","abhijeetpnwr@gmail.com"};
	
	
	public String[] getAdminids() {
		return adminids;
	}

	public void setAdminids(String[] adminids) {
		this.adminids = adminids;
	}

	public boolean checkadmin(String email)
	{	
		
	    if(Arrays.asList(adminids).contains(email))
	    		{
	    	        System.out.println("This is an admin");
	    			return true;
	    		}
	    System.out.println("Not an admin, return false");
		return false;	
	}
	
}
