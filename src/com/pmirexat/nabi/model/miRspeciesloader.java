package com.pmirexat.nabi.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class miRspeciesloader
{
   public static List getmirspecies()
   {
	   InputStream Excel_istream=Thread.currentThread().getContextClassLoader().getResourceAsStream("74plantSpsnumbercodes.txt"); 
	   
	   BufferedReader in = new BufferedReader(new InputStreamReader(Excel_istream));
	   
	   String line = "";
	   
	   List<String> returnlist = new ArrayList<>();
	   
	   try {
				while((line = in.readLine()) != null)   
						{  
				       		returnlist.add(line);
						}
			} 
	   catch (IOException e)
	   		{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    return returnlist;
   }
}
