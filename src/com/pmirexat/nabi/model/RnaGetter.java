package com.pmirexat.nabi.model;

import java.util.List;

import com.pmirexat.nabi.dao.DbCommands;

public class RnaGetter 
{
     public List<String> getmiRlist(String species)
     { 	 
    
         List<String> miRlist = new DbCommands().get_mature_microrna(new class_entityconvert().getclassname(species));
         
        // System.out.println("To retuen Mir list :"+miRlist);
         return miRlist;       
     }
}
