package com.pmirexat.nabi.model;
import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.Saltpojo;
import com.pmirexat.nabi.pojos.Signuperuser;
import com.pmirexat.nabi.pojos.passwordreset;

public class Passwordresetter
{
   public Boolean checkresetpassword(String email,String key)
   {
	   
	  DbCommands dbobj = new DbCommands();
	  passwordreset pwdreset = dbobj.getpasswordresetobject(email);
	  
	  System.out.println("--- email is  : "+email + " and key is :"+key + "----");
	  
	  String salt = dbobj.getsalttableobject(email).getSalt();
	  
	  System.out.println(" ---- key is ---- "+key);
	  
	  if(pwdreset == null || dbobj.getsalttableobject(email) == null)
	  {
		  System.out.println("Fail condition 1");
		  return false;
	  }
	  
	  Cryptoworker cw = new Cryptoworker();
	  if(cw.Issamepassword(pwdreset.getResetkeykey(), salt+key))
	  {
		 return true;
	  }
	     
	  else
	  {
		  System.out.println("fail 2");
		  return false;
	  }   
   }
   
   public String resetpassword(String email,String password,String key)
   {
		   DbCommands dbobj =  new DbCommands();
		   Signuperuser user_toreset = dbobj.getuser(email);		   
		   user_toreset.setPassword(new Cryptoworker().hashgenrator(password, dbobj.getsalttableobject(email).getSalt()));	  		   
		   dbobj.updateEntry(user_toreset);
		   return "Password reset successfully";
	   
   }
}

		