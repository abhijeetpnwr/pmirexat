package com.pmirexat.nabi.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.dao.HibernateUtils;
import com.pmirexat.nabi.listners.exceldbmapper;
import com.pmirexat.nabi.pojos.ArabPojo;
import com.pmirexat.nabi.pojos.ArabTarget;
import com.pmirexat.nabi.pojos.MaizeDiffPojo;
import com.pmirexat.nabi.pojos.MaizePojo;
import com.pmirexat.nabi.pojos.MaizeTarget;
import com.pmirexat.nabi.pojos.RiceDiffPojo;
import com.pmirexat.nabi.pojos.RicePojo;
import com.pmirexat.nabi.pojos.RiceTarget;
import com.pmirexat.nabi.pojos.Seqno;
import com.pmirexat.nabi.pojos.TablePojo;
import com.pmirexat.nabi.pojos.WheatDiffPojo;
import com.pmirexat.nabi.pojos.WheatLeaf_Edger;
import com.pmirexat.nabi.pojos.WheatPojo;
import com.pmirexat.nabi.pojos.WheatSpike_Edger;
import com.pmirexat.nabi.pojos.WheatTarget;

public class Exceltodbmapper extends Thread
{

	private static FileInputStream Excel_istream;
	private static XSSFWorkbook Hw;
    @SuppressWarnings("rawtypes")
	private static Iterator rows;
	@SuppressWarnings("rawtypes")
	private Iterator cols;
    private static XSSFSheet sheet ;
    private XSSFCell cell;
    private XSSFRow row;
    int count;	 	
    int i=0;
    int colscounter;
    String temp_methodname;
	 
    
    //This function ig give file path and filename of excelfile,Does all basic setup for that.Creates xssf workbook object
	 public void doSetup(String path,String filename)	 
	 {
		 try 
		    {
			 System.out.println("path is"+path);
			 
			    System.out.println("I am in model class dosetup method.Path for excel file is "+path);
			    
			    if(path.equals("inproject"))
			    {
			    	System.out.println(" ---- if called iin do setup ---");
			    	System.out.println("excel file is kept in project resource folder");
			      InputStream Excel_istream1=Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);    	
			    
			      Hw=new XSSFWorkbook(Excel_istream1);
			    }
			 
			    else
			    {
			    	System.out.println(" --- else called in do setup ----");
				       Excel_istream=new FileInputStream(path+filename);
				       Hw=new XSSFWorkbook(Excel_istream); 
			    }

			    sheet=Hw.getSheetAt(0);
				   
				   rows= sheet.rowIterator();			
			}
		    catch (IOException e)
		    {
				e.printStackTrace();
			}
	 }
	 
	// ---------- function ends here --------------- //
	
	 
	 public void targetmatrixmappers(String path,String filename) throws NoSuchMethodException, SecurityException
	 {
		 Object obj = null;
		 
		 
		 //System.out.println(" filename is "+filename);
		 
		 if(filename.equals("wheattarget.xlsx"))
		   {
			// System.out.println("Wheat target object should be created");
	 		 	obj = new WheatTarget();
		   }
		   
		   if(filename.equals("maizetarget.xlsx"))
		   {
			    obj = new MaizeTarget();
		   }
		   
		   if(filename.equals("ricetarget.xlsx"))
		   {
			    obj = new RiceTarget();
		   }
		   
		   if(filename.equals("arabtarget.xlsx"))
		   {
			    obj = new ArabTarget();
		   }
		 
			List<String> setterlist = new ArrayList<String>();
		 //System.out.println("Should do entries in db");
		 
		 int count = 0;
		 Method method = null;
		 doSetup(path,filename);
		 
		 
		 int checkrows = 0;
		 
		 while (rows.hasNext() && checkrows<=10000)
		    {
			 
			  checkrows++;
			 		row= (XSSFRow) rows.next();
			        cols=row.cellIterator();
			 //  System.out.println("Inside row");    
			        while(cols.hasNext())
			        {
			 

			        	//System.out.println("inside column");
			        
			        	cell=(XSSFCell) cols.next();
			        	
			        	//If the first row is being processed,So it will get name for datasets, and will set accordingly values in maps
			        	
			        	if(count ==  0)
			        	{
			                //    System.out.println("first line is being processed so I will use it to map to pojo-------");		    
			                   setterlist.add("set"+cell.getStringCellValue().substring(0, 1).toUpperCase()+cell.getStringCellValue().substring(1,cell.getStringCellValue().length()));
			                       	
			        	}
			        	else
			        	{
			        	 
			        		
			        		//System.out.println("Cols counter is : "+colscounter);
	        				 
	        				String temp_methodname = setterlist.get(colscounter);
	        				
	        				if(temp_methodname.contains("."))
	        				{
	        					temp_methodname = temp_methodname.substring(0,temp_methodname.length()-1);
	        				}

	        				//System.out.println(" ----------------------- Received temp method is : -------------------"+temp_methodname);
			        		
	        				//System.out.println("Cell type is :"+cell.getCellType());
	        				
	        				
	        				
			        		switch (cell.getCellType())
			        		{	
			        	
			        			case HSSFCell.CELL_TYPE_NUMERIC:
			        			{
	             				   
			        				//System.out.println(" ----- Numeric type called  for ---"+temp_methodname);
	             					if(setterlist.get(colscounter).contains("UPE"))
	    			        		{
	    			        			if(filename.equals("wheattarget.xlsx"))
				        				   {
				        			 		 
				        			 		 	method = WheatTarget.class.getMethod(temp_methodname,float.class);
				        				   }
				        				   
				        				   if(filename.equals("maizetarget.xlsx"))
				        				   {
				        					    method = MaizeTarget.class.getMethod(temp_methodname,float.class);
				        				   }
				        				   
				        				   if(filename.equals("ricetarget.xlsx"))
				        				   {
				        					   method = RiceTarget.class.getMethod(temp_methodname,float.class);
				        				   }
				        				   if(filename.equals("arabtarget.xlsx"))
				        				   {
				        					   method = ArabTarget.class.getMethod(temp_methodname,float.class);
				        				   }
				        				   
				        				   
				        				   
				        				   try
											{
												method.invoke(obj, (float) cell.getNumericCellValue());
											} catch (
													IllegalAccessException
													| IllegalArgumentException
													| InvocationTargetException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
	    			        		}
	             					else
	             					{
	             						
	             						//System.out.println(" ----------------------- else called why? ------");
	             						
	             						//System.out.println(" value is"+(int)cell.getNumericCellValue());
	             						if(filename.equals("wheattarget.xlsx"))
				        				   {
				        			 		 
				        			 		 	method = WheatTarget.class.getMethod(temp_methodname,int.class);
				        				   }
				        				   
				        				   if(filename.equals("maizetarget.xlsx"))
				        				   {
				        					    method = MaizeTarget.class.getMethod(temp_methodname,int.class);
				        				   }
				        				   
				        				   if(filename.equals("ricetarget.xlsx"))
				        				   {
				        					   method = RiceTarget.class.getMethod(temp_methodname,int.class);
				        				   }
				        				   
				        				   if(filename.equals("arabtarget.xlsx"))
				        				   {
				        					   method = ArabTarget.class.getMethod(temp_methodname,int.class);
				        				   }
				        				   
				        				   try
											{
												method.invoke(obj, (int) cell.getNumericCellValue());
											} catch (
													IllegalAccessException
													| IllegalArgumentException
													| InvocationTargetException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
				        				   
	             					}
			        			}
			        			break;
			        			
			        			case HSSFCell.CELL_TYPE_STRING:
			        			{
			        				//System.out.println(" ----- method type is  String for----- "+temp_methodname);
			        				if(filename.equals("wheattarget.xlsx"))
			        				   {
			        			 		 
			        					//   System.out.println("Settere to get --- "+temp_methodname);
			        			 		   method = WheatTarget.class.getMethod(temp_methodname,String.class);
			        				   }
			        				   
			        				   if(filename.equals("maizetarget.xlsx"))
			        				   {
			        					    method = MaizeTarget.class.getMethod(temp_methodname,String.class);
			        				   }
			        				   
			        				   if(filename.equals("ricetarget.xlsx"))
			        				   {
			        					   
			        					   method = RiceTarget.class.getMethod(temp_methodname,String.class);
			        				   }
			        				   
			        				   if(filename.equals("arabtarget.xlsx"))
			        				   {
			        					   
			        					   method = ArabTarget.class.getMethod(temp_methodname,String.class);
			        				   }
			        				   
			        				   try
										{
			        					 //System.out.println("VAlue to save : "+cell.getStringCellValue()); 
											method.invoke(obj,cell.getStringCellValue());
										} catch (
												IllegalAccessException
												| IllegalArgumentException
												| InvocationTargetException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}   		
			        			}
			        		}		        		
			        		colscounter++;
			        	}
			        }
			        colscounter = 0;
		//	        System.out.println(" ----- setters are -----"+setterlist.toString());
			        count++;
			        new DbCommands().saveEntry(obj);
		    }
		 System.out.println(  "----- Mapping done --------" );
		 
	 }
	 

	//This function does all  work of mapping excel file to db 
	 
	public HashMap<Integer, String> Readexcel(String path,String filename) throws NoSuchMethodException, SecurityException
	 {
		System.out.println("--- Reached readexcel method ---");
		
		//dosetup , above defined method called
		
		System.out.println(" give excelfile name :"+filename+" and given path is :"+path);
		
		doSetup(path,filename); //opens all streams and does basic setup for displaying information
		   
		 //This will be returned from function
		   HashMap<Integer, String> settermap = new HashMap<Integer, String>();
		   
		   String setter_for= "";
		   
	    
		   Object obj = null ;
		   
		 //While there are rows in excel file,keeps iterating through column values
		    while (rows.hasNext())
		    {	
		    	colscounter=0;
		    	
		    	System.out.println("-----------------------------------------  Outer loop --------------------------------------------------------");
		    	
		 	   if(filename.equals("wheat.xlsx"))
			   {
		 		 	obj = new WheatPojo();
				   setter_for = "wheat";
			   }
			   
			   if(filename.equals("maize.xlsx"))
			   {
				    obj = new MaizePojo();
				   setter_for = "maize";
			   }
			   
			   if(filename.equals("rice.xlsx"))
			   {
				    obj = new RicePojo();
				    setter_for = "rice";
			   }
			   
			   if(filename.equals("arab.xlsx"))
			   {
				    obj = new ArabPojo();
				    setter_for = "arab";
			   }
		    	
		           //Excelpojo object created		         	         
		    	
		    	
		    	i++;
		    	
				//System.out.println("-----------index of row is------"+i);
		   	    
		    	//Get the next row 
		    	row= (XSSFRow) rows.next();
			    //  System.out.println();
		    	  
		    	    //Cols is an iterator , for iteratign columns
			        cols=row.cellIterator();
			        
			        
			        
			        while(cols.hasNext())
			        {
			        	
			        	
			        	
			        	colscounter++;
			        	cell=(XSSFCell) cols.next();
			        	
			        	//If the first row is being processed,So it will get name for datasets, and will set accordingly values in maps
			        	
			        	
			        	
			        	if(i==1)
			        	{
			                //    System.out.println("first line is being processed so I will use it to map to pojo-------");		
			        	        
			                    try 
			        	         {			                    	
			                     	 settermap=new processsetter().checkforsetter(cell.getStringCellValue(),colscounter,settermap,setter_for);			   	 
			        	         }
			        	         
			        	         catch (NoSuchMethodException e)
			        	         {
			        	        	 // TODO Auto-generated catch block
			        	        	 e.printStackTrace();
			        	        }
			        	         
			        	         catch (SecurityException e)
			        	         {
			        	        	 // TODO Auto-generated catch block
									e.printStackTrace();
			        	         }
			                       	
			        	}
			        	
			        	else
			        	{
			        		System.out.println("Column count :"+colscounter+"cell type :"+cell.getCellType());    
			        		
	            		        switch (cell.getCellType())
			             		{
			             			case HSSFCell.CELL_TYPE_NUMERIC:
			             			{
			             				// System.out.print(" "+cell.getNumericCellValue());
			             				
			             				 temp_methodname=settermap.get(colscounter);
			             			   	System.out.println("value is integer : "+temp_methodname+"col count is"+colscounter+"row count is"+i);
			             				
			             			   Method method = null;
			             			   	
			             			   if(filename.equals("wheat.xlsx"))
			            			   {
			             				  if(temp_methodname.contains("foldchange") || temp_methodname.contains("Shannon"))
			             				  {
			             					  System.out.println("Float type method should be called with "+temp_methodname);
			             					 method = WheatPojo.class.getMethod(temp_methodname,float.class);
			             				  }
			             				  else
			             				  {
			             					 method = WheatPojo.class.getMethod(temp_methodname,int.class);
			             				  }
			             			      		  
			             				  
			            			   }
			            			   
			            			   if(filename.equals("maize.xlsx"))
			            			   {
			            				   if(temp_methodname.contains("foldchange") || temp_methodname.contains("Shannon"))
				             				  {
			            					   		method = MaizePojo.class.getMethod(temp_methodname,float.class);
				             				  }
			            				   else
			            				   {
			            					   method = MaizePojo.class.getMethod(temp_methodname,int.class);
			            				   }
			            			   }
			            			   
			            			   if(filename.equals("rice.xlsx"))
			            			   {
			            				   
			            				   if(temp_methodname.contains("foldchange") || temp_methodname.contains("Shannon"))
			            				   {
			            					   method = RicePojo.class.getMethod(temp_methodname,float.class);
			            				   }
			            				
			            				   else
			            				   {
			            					   method = RicePojo.class.getMethod(temp_methodname,int.class);
			            				   }
			            				   
			            				  
			            			   }
			            			   
			            			   
			            			   if(filename.equals("arab.xlsx"))
			            			   {
			             				  if(temp_methodname.contains("foldchange") || temp_methodname.contains("Shannon"))
			             				  {
			             					  System.out.println("Float type method should be called with "+temp_methodname);
			             					 method = ArabPojo.class.getMethod(temp_methodname,float.class);
			             				  }
			             				  else
			             				  {
			             					 method = ArabPojo.class.getMethod(temp_methodname,int.class);
			             				  }
			             			      		  
			             				  
			            			   }
			            			   
		 
			             				 try
			             				 {
			             					// System.out.println("now I will call method");
			             					 
			             					 if(temp_methodname.contains("foldchange") || temp_methodname.contains("Shannon"))  
			             					 {
			             						 float topass = (float) cell.getNumericCellValue();
			             						method.invoke(obj,topass);
			             					 }
			             					
			             					 else
			             					 {
			             					    int topass= (int) cell.getNumericCellValue();
			             					    method.invoke(obj,topass);
			             					 }
			             				//	System.out.println("Method to call"+method.getName()+" with value"+topass);
			             					 
											
									   	} 
			             				 
			             				 catch (IllegalAccessException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IllegalArgumentException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (InvocationTargetException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
			             				
			             				break;
			             			}
			             			
			             			case HSSFCell.CELL_TYPE_STRING:
					        			{
					        				
					        				System.out.println("-----------So this time value is String------"+colscounter);					  				
					        				
					        				
					        				System.out.println("---- chk val--"+settermap.get(colscounter));
					       			
					        				
					        				temp_methodname=settermap.get(colscounter);
					        				
					        				Method method = null;
					        				
					        				if(filename.equals("wheat.xlsx"))
					            			   {
					             				  method = WheatPojo.class.getMethod(temp_methodname,String.class);
					            			   }
					            			   
					            			   if(filename.equals("maize.xlsx"))
					            			   {
					            				   method = MaizePojo.class.getMethod(temp_methodname,String.class);
					            			   }
					            			   
					            			   if(filename.equals("rice.xlsx"))
					            			   {
					            				   method = RicePojo.class.getMethod(temp_methodname,String.class);
					            			   }
			 
					            			   if(filename.equals("arab.xlsx"))
					            			   {
					             				  method = ArabPojo.class.getMethod(temp_methodname,String.class);
					            			   }
					            			   
												try
												{
													method.invoke(obj,cell.getStringCellValue());
												} catch (
														IllegalAccessException
														| IllegalArgumentException
														| InvocationTargetException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}			
					        			}
		
			             			default:
					        		break;
			             		}
			        	}
			        }
			        		        
			        //Saving RowEntry object in table
			         
			        new DbCommands().saveEntry(obj);
	    	}
		  
			return settermap;
	 }

	public void Readexcel2(String excelsheet_path,String filename)
	{
		
		System.out.println("Excel sheet path is :"+excelsheet_path);
		
		System.out.println("filename is :"+filename);
		
		doSetup(excelsheet_path,filename);
		
		int rowcount = 0;
		//row= (XSSFRow) rows.next();
		
		while (rows.hasNext())
	    {	
			
			
			System.out.println("----- row no: ----- "+rowcount);
			rowcount++;
			
			 Seqno record = new  Seqno();
			 
	     	 row= (XSSFRow) rows.next();
			   
		     cols=row.cellIterator();
		
		    
		     int cellcount=1;
		     
             while(cols.hasNext())
             {
        
             	cell=(XSSFCell) cols.next();
             	
                 if(cellcount==1)
                 {  	   
                	  System.out.println("In if:"+cell.getStringCellValue());
                	  record.setDatasetname(cell.getStringCellValue());
                	 
                 }
                 
                 else
                 {
                	if(cellcount == 2)
                	{
                		System.out.print("In 2nd if :"+cell.getNumericCellValue());
                   	   record.setSequence(cell.getNumericCellValue());
                	}
                	 
                	 
                 }          
                 cellcount++;
       
             }            
             
             System.out.println(" --- object ----"+record.getDatasetname()+"-- value --"+record.getSequence());
             
             new DbCommands().savesequence(record);
        }    		        
        //Saving RowEntry object in table  
    }
	
	public void mapwheatexcel(String excelsheet_path,String filename)
	 {
		doSetup(excelsheet_path,filename);
		
		 WheatDiffPojo obj = new WheatDiffPojo();
		
		
		int rowcount = 0;
		
		while (rows.hasNext())
	    {	
			rowcount++;
			
			if(rowcount % 1000 ==0)
			{
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
	     	 row= (XSSFRow) rows.next();
		     cols=row.cellIterator();
		     int cellcount=0;
		     
		     if(rowcount>1)
		     {
             while(cols.hasNext())
             {
             	cell=(XSSFCell) cols.next();	
                 if(cellcount==0)
                 {  	   
                	  obj. setDataset1(cell.getStringCellValue());
                 }
                    
                 if(cellcount==1)
                 {  	   
                	  obj.setDataset2(cell.getStringCellValue());
                 }
                 if(cellcount==2)
                 {  	   
                	  obj.setMiRNA_ID(cell.getStringCellValue());
                 }
                 if(cellcount==3)
                 {  	   
                	  obj.setLogFC((float) cell.getNumericCellValue());
                 }
                 if(cellcount==4)
                 {  	   
                	  obj.setLogCPM((float) cell.getNumericCellValue());
                 }
                 if(cellcount==5)
                 {  	   
                	  obj.setPValue((float) cell.getNumericCellValue());
                 }
                 if(cellcount==6)
                 {  	   
                	  obj.setFDR((float) cell.getNumericCellValue()); 
                 }
                 
                 cellcount++;
             } 
            // System.out.println(" saving :"+obj);
             new DbCommands().saveEntry(obj);
		     }
	    }
        }    
		
		public void mapMaizeexcel(String excelsheet_path,String filename)
		 {
			doSetup(excelsheet_path,filename);
			
			 MaizeDiffPojo obj = new MaizeDiffPojo();
			
			
			int rowcount = 0;
			
			while (rows.hasNext())
		    {	
				rowcount++;
		
				//System.out.println(" --- reaxchde inside -- ");
				
				if(rowcount % 1000 ==0)
				{
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			
		     	 row= (XSSFRow) rows.next();
			     cols=row.cellIterator();
			     int cellcount=0;
			     
			     if(rowcount>1)
			     {
	
			    	// System.out.println(" --- reaxchde if -- ");
			    	 while(cols.hasNext())
		             {
		             	cell=(XSSFCell) cols.next();	
		                 if(cellcount==0)
		                 {  	   
		                	  obj. setDataset1(cell.getStringCellValue());
		                 }
		                    
		                 if(cellcount==1)
		                 {  	   
		                	  obj.setDataset2(cell.getStringCellValue());
		                 }
		                 if(cellcount==2)
		                 {  	   
		                	  obj.setMiRNA_ID(cell.getStringCellValue());
		                 }
		                 if(cellcount==3)
		                 {  	   
		                	  obj.setLogFC((float) cell.getNumericCellValue());
		                 }
		                 if(cellcount==4)
		                 {  	   
		                	  obj.setLogCPM((float) cell.getNumericCellValue());
		                 }
		                 if(cellcount==5)
		                 {  	   
		                	  obj.setPValue((float) cell.getNumericCellValue());
		                 }
		                 if(cellcount==6)
		                 {  	   
		                	  obj.setFDR((float) cell.getNumericCellValue()); 
		                 }
		                 
		                 cellcount++;
		             }            
	             new DbCommands().saveEntry(obj);
		    	 
			     }
		    }
	        }
			
			public void mapRiceexcel(String excelsheet_path,String filename)
			 {
				doSetup(excelsheet_path,filename);
				
				
				 RiceDiffPojo obj = new RiceDiffPojo();
				
				
				int rowcount = 0;
				
				while (rows.hasNext())
			    {	
					//System.out.println(" --In if --");
					rowcount++;
				
					if(rowcount % 1000 ==0)
					{
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				
					
			     	 row= (XSSFRow) rows.next();
				     cols=row.cellIterator();
				     int cellcount=0;
				  
				     if(rowcount>1)
				     {
				     
				    	// System.out.println(" ---- Inside if ----");
				    	 
				    	 while(cols.hasNext())
			             {
			             	cell=(XSSFCell) cols.next();	
			                 if(cellcount==0)
			                 {  	   
			                	  obj. setDataset1(cell.getStringCellValue());
			                 }
			                    
			                 if(cellcount==1)
			                 {  	   
			                	  obj.setDataset2(cell.getStringCellValue());
			                 }
			                 if(cellcount==2)
			                 {  	   
			                	  obj.setMiRNA_ID(cell.getStringCellValue());
			                 }
			                 if(cellcount==3)
			                 {  	   
			                	  obj.setLogFC((float) cell.getNumericCellValue());
			                 }
			                 if(cellcount==4)
			                 {  	   
			                	  obj.setLogCPM((float) cell.getNumericCellValue());
			                 }
			                 if(cellcount==5)
			                 {  	   
			                	  obj.setPValue((float) cell.getNumericCellValue());
			                 }
			                 if(cellcount==6)
			                 {  	   
			                	  obj.setFDR((float) cell.getNumericCellValue()); 
			                 }
			                 
			                 cellcount++;
			             }            
		             new DbCommands().saveEntry(obj);
				     }
		        }
		
      }
			
			public void maptissues(String filepath,String filename)
			{
				doSetup(filepath,filename);
				
				// Emptyr references of all classes 
				
				String tablename = filename.split("\\.")[0];
			
				System.out.println("Tablle is :"+tablename);
				
				int rowcount = 0;
				
				while (rows.hasNext())
			    {	
					
					rowcount++;
					
			     	 row= (XSSFRow) rows.next();
				     cols=row.cellIterator();
				     int cellcount=0;
				  
				     String mir_ids = "";
				     float logFC= 0.0f,logCPM=0.0f,Pvalue=0.0f,FDR=0.0f;
				     
				     if(rowcount>1)
				     {
				     
				    	// System.out.println(" ---- Inside if ----");
				    	 
				    	 while(cols.hasNext())
			             {
			             	cell=(XSSFCell) cols.next();	
			                 if(cellcount==0)
			                 {  	
			                	 
			                	  mir_ids = cell.getStringCellValue();
			                 }
			                    
			                 if(cellcount==1)
			                 {  	   
			                	 if(cell.getCellType()==1)
			                	 {
			                		 logFC = 100001;
			                	 }
			                	 else			                		 
			                	   logFC = (float) cell.getNumericCellValue();
			                 }
			                 if(cellcount==2)
			                 {  	   
			                	 if(cell.getCellType()==1)
			                	 {
			                		 logCPM = 100001;
			                	 }
			                	 else			                		 
			                	   logCPM = (float) cell.getNumericCellValue();
			                 }
			                 if(cellcount==3)
			                 {  	   
			                	 if(cell.getCellType()==1)
			                	 {
			                		 Pvalue = 100001;
			                	 }
			                	 else			                		 
			                	   Pvalue = (float) cell.getNumericCellValue();
			                 }
			                 if(cellcount==4)
			                 {  	   
			                	 if(cell.getCellType()==1)
			                	 {
			                		 FDR = 100001;
			                	 }
			                	 else			                		 
			                	   FDR = (float) cell.getNumericCellValue();
			                 }
			        
			                 cellcount++;
			             }            
		             
				    	 String sql = "INSERT INTO "+ tablename.toLowerCase()+" (mir_ids,logFC,logCPM,PValue,FDR)"+" VALUES ("+ "\""+mir_ids+"\","+logFC+","+logCPM+","+Pvalue+","+FDR+")";
				    	 
				    	 if(mir_ids.length()>0)
				    	 {
				    	 Session ses=new HibernateUtils().getSession();
				    	 ses.beginTransaction();
				    	 SQLQuery query = ses.createSQLQuery(sql);
			 
				    	// System.out.println("query :"+sql);
				    	 
				    	 ses.createSQLQuery(sql).executeUpdate();//has no effect. Query doesn't execute.
				    	 ses.getTransaction().commit();
				    	 ses.close();
	
				    	 }
				     }
		        }
				
				System.out.println(" ---- mMapping done --- ");
			}
	
	}


