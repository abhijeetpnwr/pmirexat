package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;

import com.pmirexat.nabi.api.ApiClass;

public class crossspecies_resultgen 
{

	  Commonsequencegetter com_obj = new Commonsequencegetter();
	  
	  
	  
	  @SuppressWarnings("unchecked")
		List ricegetterlist=(List<String>) new datasetgetter().getdatasets("rice");
	    @SuppressWarnings("unchecked")
		static
		List wheatgetterlist=(List<String>)new datasetgetter().getdatasets("wheat");
	    @SuppressWarnings("unchecked")
		static
		List maizegetterlist=(List<String>)new datasetgetter().getdatasets("maize");
	  
	
		  
		  String wheatid = "";
		  String maizeid = "";
		  String riceid = "";
	
		  
		  
	  
   public  List<List<String>> getcommon(List<String> temp)
   {
	   String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
	   
	   Map<String, String> species_rna = new HashMap<String, String>();
		  
	   for(String species:temp)
	   {
		    species_rna.put(species,"");
	   }
	 	  
   	  List<String> comm_wm = com_obj.find_common(temp);  
   	  
   	  List<String> searched_micro_rnas=new ArrayList<String>();
	  List<String> finalresult = new ArrayList<String>();
   	  List<String> generatedheaders = new ArrayList<String>();
	  generatedheaders.add("Searched Dataset Name");
	  searched_micro_rnas.add("Searched Dataset Name"); 
		  
	  System.out.println("Common are :"+comm_wm);
	  
	  searched_micro_rnas = ListUtils.union(searched_micro_rnas,comm_wm);	  
	  
	  int i =0;
		  
		 for (Iterator iterator = comm_wm.iterator(); iterator.hasNext();)
		 {
			String sequence = (String) iterator.next();
			String header = sequence+"</br>";
						
			for(String species: temp)
			{
				
				String foundid = new ApiClass().findmiRidbysequence(species, sequence);
				
				if(!(foundid.equals("") || foundid.equals("")))
				{
					 if(i == 0)
					    {
						   species_rna.put(species, species_rna.get(species)+foundid);
					   }
					 else
					 {
						 species_rna.put(species, species_rna.get(species)+","+foundid);
					 }
						 
				}
				header = header+foundid+" ";
			}
			
			generatedheaders.add(header);
		 i++; //Counter increased
		 }			
		
		 
		for(String species : temp)
		{
			System.out.println("Rna in "+species+" : "+species_rna.get(species));
			
			System.out.println("Length is :"+species_rna.get(species).split(",").length);
			
			finalresult = ListUtils.union(finalresult, new search_result_generator().searchdb(species_rna.get(species),species, (List<String>) new datasetgetter().getdatasets(species)));
		}
		 
		List<List<String>> toreturn = new ArrayList<List<String>>();
		toreturn.add(generatedheaders); //added table header header
		toreturn.add(finalresult); //added table content
		return toreturn;
   }
}

