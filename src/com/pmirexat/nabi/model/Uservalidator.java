package com.pmirexat.nabi.model;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.Saltpojo;
import com.pmirexat.nabi.pojos.Signuperuser;

public class Uservalidator
{
   public Boolean checkauthenicity(String emailaddress,String password)
   {
	   if(checkemailavailbility(emailaddress))
	   {
		     String saltvalue = new DbCommands().getsalttableobject(emailaddress).getSalt();
		    
		     Signuperuser su = new DbCommands().getuser(emailaddress);
		     String hash = su.getPassword();
		     
		     Cryptoworker cw = new Cryptoworker();
		     
		     System.out.println("//Hash is :"+hash + "---- to match with it :"+saltvalue+password);
		     if(cw.Issamepassword(hash,saltvalue+password) && su.getVerificationstatus().equals("Verified"))
		     {
		    	 return true;
		     }	     
		     else
		     {
		    	 return false;
		     }	     
	   }   
	   else
	   {
		   return false;
	   }
   }
   
   //Return true if email already exists in pmirexat db
   public Boolean checkemailavailbility(String email)
   {   
	    return(new DbCommands().getsalttableobject(email)!= null);
   }
   
   public Boolean verifyaccount(String email,String verificationkeykey)
   {
	  Saltpojo sp =  new DbCommands().getsalttableobject(email);
	  
	  if(sp!=null)
	  {
		  System.out.println("In object verif. verif. key in db :"+sp.getVerificationkey() + " ---- to comapare :"+verificationkeykey);
		  if(sp.getVerificationkey().equals(verificationkeykey))
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	  }
	  
	  else
	  {
		  System.out.println("No. such email is available");
		  return false;
	  }
   }

}
