package com.pmirexat.nabi.model;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.Saltpojo;
import com.pmirexat.nabi.pojos.Signuperuser;
public class SignupExecuter 
{
   public static String signup(Signuperuser signup_acc,String salt) 
   {
	   System.out.println("Sign up to be called for : "+signup_acc);
	   System.out.println("Value for salt is :"+salt);	   
	   /*
	   
	      Code for checking first that whether this id is already exist or not
	   
	      and proceed accordingly
	     
	   */	   
	   Uservalidator userval = new Uservalidator();
	   
	   //If email is not available already in our Database
	   
	   if(!userval.checkemailavailbility(signup_acc.getEmail()))
 	   {
		   String hash = new Cryptoworker().hashgenrator(signup_acc.getPassword(),salt);
		   signup_acc.setPassword(hash);
		   
		   DbCommands dbobj = new DbCommands();
		   
		   dbobj.saveEntry(signup_acc); //Entry done in usertable table
		   
		   String verificationkey = new Cryptoworker().generatesalt();
		   String verificationkey_encrypted = new Cryptoworker().hashgenrator("pmirexat",verificationkey);
		   
		   System.out.println("Code for sending mail should be called");
		   
		   String encodedemail = "";
		   try {
			encodedemail =  URLEncoder.encode(signup_acc.getEmail(),"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		  
		  String encodedkey = "";
		try {
			System.out.println(" --------- Without encoding key is :  ---------- "+verificationkey_encrypted);
			encodedkey = URLEncoder.encode(verificationkey_encrypted,"UTF-8");
			System.out.println(" ----- after encoding the key is : ------- "+encodedkey);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		   new SimpleEmail().sendemail("Please click the link provided: http://pmirexat.nabi.res.in/userverification?email="+signup_acc.getEmail()+"&key="+encodedkey, "pmirexat@gmail.com",signup_acc.getEmail());
		   
		   dbobj.saveEntry(new Saltpojo(signup_acc.getEmail(),verificationkey_encrypted,salt)); //Entry done in salttable
		   
		   return "A verification link has been sent on your email address , Please check your email and click given link to activate your account.";
	   }
	   
	   else
	   {
		   return "This email id is already registered with us. Please sign in to continue";	
	   }
	   
   }
}
