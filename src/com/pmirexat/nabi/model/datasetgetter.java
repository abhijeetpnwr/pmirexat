package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.List;

import com.pmirexat.nabi.dao.DbCommands;
import com.sun.org.apache.bcel.internal.generic.RETURN;

public class datasetgetter
{
    public List<String> getdatasets(String species)
    {
    	
    	//System.out.println("Table desc to get is :"+new class_entityconvert().gettablename(species));
    	
    	List<String> completelist = new DbCommands().getTableDesc(new class_entityconvert().gettablename(species)); 
    	
    	List<String> toreturnlist = new ArrayList<String>(); 
    			
    	//System.out.println("Result is : "+completelist);
    	
    	for (String string : completelist)
    	{
    		//System.out.println("String is :"+string);
    		
		    if(string.toLowerCase().contains(species.toLowerCase()))
		    {
		    	toreturnlist.add(string);
		    }
		}

		return toreturnlist;
		
    }
    
  
}
