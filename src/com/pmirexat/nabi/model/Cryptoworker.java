package com.pmirexat.nabi.model;
import org.jasypt.salt.RandomSaltGenerator;
import org.jasypt.util.password.StrongPasswordEncryptor;

public class Cryptoworker
{
   public String generatesalt()
   {
	    RandomSaltGenerator saltobj = new  RandomSaltGenerator();
	    byte[] saltbytes = saltobj.generateSalt(10);
	    String salt = new String(saltbytes);
	    System.out.println("Value for salt is :"+salt);
	    return salt;	    
   }
	
   public String hashgenrator(String password,String salt)
   {
	   StrongPasswordEncryptor  passwordEncryptor = new StrongPasswordEncryptor();
	   String encryptedPassword = passwordEncryptor.encryptPassword(salt+password);
	   System.out.println("Encrypted password :"+encryptedPassword);   
	   //Rememeber , It was written that If I am using  RandomSaltGenerator  class instead of normal saltgenerator class ,  I need to append undigested salt with the hased result.Bit I did't as was not needed	   
	  
	   System.out.println(" ------- Check in hash genrator function  ------- ");
	   System.out.println("To match with :"+salt+password);
	   System.out.println("Check just after :"+Issamepassword(encryptedPassword, salt+password));
	   
	   return encryptedPassword;   
   }
   
   public Boolean Issamepassword(String encryptedpassword,String passwordwithsalt)
   {
	   StrongPasswordEncryptor  passwordEncryptor = new StrongPasswordEncryptor();  
	  
	   if(passwordEncryptor.checkPassword(passwordwithsalt,encryptedpassword))
	   {
		   return true;
	   } 
	   else 
	   {
		   System.out.println();
		   return false;
	   }
   }

}
