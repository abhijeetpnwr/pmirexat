package com.pmirexat.nabi.model;

import java.io.IOException;
import java.util.List;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;

import com.pmirexat.nabi.dao.DbCommands;

public class scriptexecuter
{
	 public String testrun(String filename ,String outputfilename, String pident ,String mismatch,String querycoverage)
	   {
		     String status = null;
	         CommandLine command = new CommandLine("/bin/sh");
	             command.addArguments(new String[] {
	            "/satamas/pmiRexatresults/multifasta_vs_117DBcountmatrix5.sh ",filename,outputfilename,pident,mismatch,querycoverage},false);
	           try
	           {
			
				DefaultExecutor executor = new DefaultExecutor();			
				
				System.out.println(" Executed ");
				
				int exitValue = executor.execute(command);
			
				System.out.println(" -- After execute execute --"+exitValue);
				
				
				 if (executor.isFailure(exitValue))
				 {
				     System.out.println("It wasa failure ,I can't do anything now");
				     status = "Execution failed";
				    
				 }
				 
				 else
				 {
					 System.out.println("yo, it was successful experiment man");
					 status = "Execution Success";
				 }
			
			   }
	           
	           catch (ExecuteException e) 
	           {
	        	   System.out.println("Error in execution");
	        	   
	        	   e.printStackTrace();
	        	   
	        	   return "Execution exvception";
	           }
	           
	           catch (IOException e)
	           {
				e.printStackTrace();
				return "IO exception";
			   }
	           
			return status ;
	   }
	 
	 public String runnewdsscript(String filename,String outputfilename,String tosearchagainst,String realsraname)
	 {
		 String status = null;
         CommandLine command = new CommandLine("/bin/sh");
             command.addArguments(new String[] {
            "/satamas/Pmirexat_datasetsprocessing/Datasetprocessingfor73plantSps.sh",filename,tosearchagainst,outputfilename,realsraname},false);
           try
           {		
			DefaultExecutor executor = new DefaultExecutor();					
			System.out.println(" Executed ");		
			int exitValue = executor.execute(command);
			System.out.println(" -- After execute execute --"+exitValue);	
			 if (executor.isFailure(exitValue))
			 {
			     System.out.println("It wasa failure ,I can't do anything now");
			     status = "Execution failed";		    
			 }		 
			 else
			 {
				 status = "Execution Success";
			 }
		
		   }
           
           catch (ExecuteException e) 
           {
        	   System.out.println("Error in execution");
        	   
        	   e.printStackTrace();
        	   
        	   return "Execution exception";
           }
           
           catch (IOException e)
           {
			e.printStackTrace();
			return "IO exception";
		   }        
		return status ;
	 }
	 
//	 public List<String> getEdgervalues(String scriptname ,String  dataset1 ,String dataset2)
//	 {
//		 String status = null;
//         CommandLine command = new CommandLine("/bin/sh");
//             command.addArguments(new String[] {
//            "/satamas/Pmirexat_datasetsprocessing/Datasetprocessingfor73plantSps.sh",dataset1,dataset2},false);
//	 }
	 
}


