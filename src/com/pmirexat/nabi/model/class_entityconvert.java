package com.pmirexat.nabi.model;

public class class_entityconvert 
{
   String tablename;
   
   String species;
   
   String classname;
	
   public String gettablename(String species)
   {
	    if(species.equals("wheat") || species.equals("Wheat") )
	    {
	    	tablename= "wheatdb";
	    }
	    
	    if(species.equals("maize") || species.equals("Maize"))
	    {
	    	tablename = "maizedb";
	    }
	    
		if(species.equals("rice") || species.equals("Rice"))
		{
			tablename = "ricedb";
		}
		
		if(species.equals("arab") || species.equals("Arab"))
		{
			tablename = "arabdb";
		}
		
		
	    return tablename;
   }
   
   public String getclassname(String species)
   {
	    if(species.equals("wheat") || species.equals("Wheat"))
	    {
	    	classname= "WheatPojo";
	    }
	    
	    if(species.equals("maize") || species.equals("Maize"))
	    {
	    	classname = "MaizePojo";
	    }
	    
		if(species.equals("rice") || species.equals("Rice"))
		{
			classname = "RicePojo";
		}
		
		if(species.equals("arab") || species.equals("Arab"))
		{
			classname = "ArabPojo";
		}
	    return classname;
   }
   
   public String getspecies(String tablename)
   {
	    if(tablename.equals("wheatdb"))
	    {
	    	species = "wheat";
	    }
	    
	    if(tablename.equals("maizedb"))
	    {
	    	species = "maize";
	    }
	    
		if(species.equals("ricedb"))
		{
			species = "ricedb";
		}
	    return species;
   }
   
   
}
