package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pmirexat.nabi.dao.DbCommands;

public class datasetcompletermodel
{
   public static Map<String, List<String>> getdatasets(String species) 
   {
	   
	   String tablename = "";
	   
	   if(species.equals("wheat"))
	   {
		   tablename="wheatdiff";
	   }
	   
	   if(species.equals("maize"))
	   {
		   tablename="maizediff";
	   }
	   if(species.equals("rice"))
	   {
		   tablename="ricediff";
	   }
	   
	     Map<String, List<String>> dsmap = new HashMap<>();
	     
    
        
       /*
        *   Code only will work for arabidopsis
        *   
        *   /////
        *   
        */
         
         if(species.equals("Arabidopsis"))
         {
        	 dsmap.put("dataset1", new datasetgetter().getdatasets("arab")); 
             
             dsmap.put("dataset2", new datasetgetter().getdatasets("arab"));
         }
         
         else
         {
             dsmap.put("dataset1", new DbCommands().getuniquecols(tablename, "dataset1")); 
             
             dsmap.put("dataset2", new DbCommands().getuniquecols(tablename, "dataset2"));
         }
         
         
       return dsmap;
   }
}
