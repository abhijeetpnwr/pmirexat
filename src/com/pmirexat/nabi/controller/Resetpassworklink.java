package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.Cryptoworker;
import com.pmirexat.nabi.model.SimpleEmail;
import com.pmirexat.nabi.pojos.passwordreset;

/**
 * Servlet implementation class Resetpassword
 */
@WebServlet("/Resetpasswordlink")
public class Resetpassworklink extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Resetpassworklink() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println(" ----- Reached passwor dreset servlet ---------");
	
		DbCommands daoobject = new DbCommands();
		String email = request.getParameter("email");
		
		Cryptoworker cw = new Cryptoworker();
		String resetkey = RandomStringUtils.randomAlphanumeric(8);
		String hashedresetkey = cw.hashgenrator(resetkey,new DbCommands().getsalttableobject(email).getSalt());
	
	
	    daoobject.saveEntry(new passwordreset(email, Calendar.getInstance().getTime(), cw.hashgenrator(resetkey, daoobject.getsalttableobject(email).getSalt()))); 

	    new SimpleEmail().sendemail("Please click the link to reset your password : http://pmirexat.nabi.res.in//validateresetpwd?key="+URLEncoder.encode(resetkey,"UTF-8")+"&emailid="+email,"pmirexat@gmail.com",email);
	}

}
