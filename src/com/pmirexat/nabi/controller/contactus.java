package com.pmirexat.nabi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.SimpleEmail;

/**
 * Servlet implementation class contactus
 */
@WebServlet("/contactus")
public class contactus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public contactus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("get method called");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		
		System.out.println("Post method called");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		final String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String message = request.getParameter("message");
		
		System.out.println("fname : "+fname);
		System.out.println("lname : "+lname);
		System.out.println("phone :"+phone);
		
		System.out.println("email id is :"+email);
		System.out.println("message is :"+message);
		
		final String body = "Sent by : "+fname+" "+lname+"\n"+"Email address is : "+email+"\n"+"Phone no. :"+phone+"\n"+"Message :"+message;
		
		System.out.println("Body message :"+body);
		
		System.out.println("Now will call email.. ");
		
		final SimpleEmail se = new SimpleEmail();
		
		System.out.println(" Object created ");

		 se.sendemail(body,email,"bioinfoman3@gmail.com");
		 
		
		
	}

}
