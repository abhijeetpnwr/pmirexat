package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.Commonsequencegetter;

/**
 * Servlet implementation class crossSpeciestables
 */
@WebServlet("/crossSpeciestables")
public class crossSpeciestables extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crossSpeciestables() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	
		ServletContext context = request.getSession().getServletContext();
		
	   System.out.println("Doo post of crossspecies getter is called");
	   System.out.println("Do get method is called in the crossspecies getter ");
		
		String tosearch = request.getParameter("toget");
		
		List<String> sequencelist = new ArrayList<String>();
		
		List<String> finalresultlist = new ArrayList<String>();
		
		ApiClass obj =  new ApiClass();
		
		System.out.println("To get's value is : "+tosearch);
		
		String finalresult = "";
		
		if(tosearch.equals("w_m"))
		{
			System.out.println("Need to get Wheat and Maize common");
			
			sequencelist = (List<String>) context.getAttribute("commonlist_wm");
		
			
			
		    for (String string : sequencelist) 
		    { 
			    finalresult =finalresult+"\n"+string;
		    }
		
		}
		
		if(tosearch.equals("w_r"))
		{
			System.out.println("Need to get wheat and rice common");
			sequencelist = (List<String>) context.getAttribute("commonlist_wr");
			
		    for (String string : sequencelist) 
		    { 
			   // String toaddString = string + "@" + obj.findmiRidbysequenceinWheat(string) + "@" +obj.findmiRidbysequenceinRice(string); 	
			    
			    finalresult = finalresult + "\n"+string;
		    }
		}
	
		if(tosearch.equals("m_r"))
		{
			System.out.println("Need to get Maize and rice common");
			sequencelist = (List<String>) context.getAttribute("commonlist_mr");
			
		    for (String string : sequencelist) 
		    { 
			    //String toaddString = string + "@" + obj.findmiRidbysequenceinMaize(string) + "@" +obj.findmiRidbysequenceinRice(string); 	
			    
		    	finalresult = finalresult + "\n"+string;
		    }
		}
		
		if(tosearch.equals("all"))
		{
			System.out.println("Need to get all");
			sequencelist = (List<String>) context.getAttribute("commonlist_all");
			
		    for (String string : sequencelist) 
		    { 
			   // String toaddString = string + "@" + obj.findmiRidbysequenceinWheat(string) + "@" +obj.findmiRidbysequenceinMaize(string)+"@"+obj.findmiRidbysequenceinRice(string); 			    
		    	finalresult = finalresult + "\n"+string;
		    }
		}
		
		System.out.println("To return ---"+finalresult);
		
		PrintWriter out = response.getWriter();
		out.print(finalresult);
		
	}

}
