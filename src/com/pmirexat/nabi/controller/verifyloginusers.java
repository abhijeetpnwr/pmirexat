package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.Signuperuser;
import com.pmirexat.nabi.pojos.newdsprocessing;

/**
 * Servlet implementation class verifyloginusers
 */
@WebServlet("/verifyloginusers")
public class verifyloginusers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public verifyloginusers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		 PrintWriter writer = response.getWriter();
	      response.setContentType("application/json");
		
		DbCommands dbcommandsobj = new DbCommands();
		JSONArray json = new JSONArray();
		
	    	List<Signuperuser> verification_awaiting_users = dbcommandsobj.usersawaitingadminverification();
	    	try
		    {
		    for (Signuperuser signuperuser : verification_awaiting_users) 
		    {
		    	 JSONObject jsono = new JSONObject();
	             jsono.put("Email Id",signuperuser.getEmail());
	             jsono.put("Request Date", signuperuser.getStartdate());
	             jsono.put("First Name",signuperuser.getFirst_name());
	             jsono.put("Last Name",signuperuser.getLast_name());
	             jsono.put("Institute",signuperuser.getInstitutename());	      
	             jsono.put("Department",signuperuser.getDepartment());
	             jsono.put("Contact No.",signuperuser.getContactno());
	             jsono.put("Address",signuperuser.getAddress());
	             jsono.put("Reason",signuperuser.getReasontosignup());            
	             json.put(jsono);	
			}
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
		    //System.out.println("Result values are :"+results);
		  
		    writer.write(json.toString());
		    writer.close();
	}

}
