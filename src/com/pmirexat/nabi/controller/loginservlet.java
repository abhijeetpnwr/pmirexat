package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.AdminChecker;
import com.pmirexat.nabi.model.Uservalidator;
import com.pmirexat.nabi.model.VerifyRecaptcha;

/**
 * Servlet implementation class loginservlet
 */
@WebServlet("/loginservlet")
public class loginservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		System.out.println(" --- Do post method called : ---- ");
		
		RequestDispatcher rd = null;
		
		String emailaddress = request.getParameter("email");
		String password = request.getParameter("password");	
		
		 String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
	        System.out.println(gRecaptchaResponse);
	        boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
	 
		
		
		Uservalidator uv_obj = new Uservalidator();
		System.out.println("Authonicity for the user :"+uv_obj.checkauthenicity(emailaddress, password));
		
		if(uv_obj.checkauthenicity(emailaddress, password) && verify)
		{
				HttpSession session = request.getSession(true);
				
				session.setAttribute("Email",emailaddress);
				
				session.setAttribute("Firstname",new DbCommands().getuser(emailaddress).getFirst_name());
	
				if(new AdminChecker().checkadmin(emailaddress))
				{
					System.out.println("This is an admin id.Need to give priv. to approve login ids");
					session.setAttribute("role", "Admin");
				}	
				
				else
				{
					System.out.println("No a admin id . Ir should now have admin priev.");
				}
				
				System.out.println("Sessio id :"+session.getId());
		
				System.out.println(" -------- Now need to forward it to upload page ----  ");
					
				PrintWriter writer = response.getWriter();
				writer.write("authenticated");
		}
		
		else
		{ 
			 PrintWriter writer = response.getWriter();
			if(!verify)
			{
				writer.write("Wrong captcha");
			}
			else
			{
				writer.write("Wrong Emailid or Password !");
			}
			 
		}
			
		
	}

}
