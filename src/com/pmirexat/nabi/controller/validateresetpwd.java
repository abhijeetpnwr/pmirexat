package com.pmirexat.nabi.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.Passwordresetter;

/**
 * Servlet implementation class resetpassword
 */
@WebServlet("/validateresetpwd")
public class validateresetpwd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public validateresetpwd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		String key = request.getParameter("key");
		String emailid = request.getParameter("emailid");
		
		
		System.out.println("Key is :"+key);
		
		System.out.println("Password reset functionality is called");
		
		RequestDispatcher rd=request.getRequestDispatcher("passwordreset.jsp"); 
		
		request.setAttribute("Key",key);
		request.setAttribute("email",emailid);
		request.setAttribute("Name", new DbCommands().getuser(emailid).getFirst_name());
		
		if(new Passwordresetter().checkresetpassword(emailid, key))
		{
			//servlet2 is the url-pattern of the second servlet  			  
			 rd.forward(request, response);//method may be include or forward  
		}
		else
		{
		   	System.out.println("Password reset failed");
		}
	}

}
