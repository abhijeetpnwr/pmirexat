package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.AdminChecker;
import com.pmirexat.nabi.model.SimpleEmail;
import com.pmirexat.nabi.model.Uservalidator;
import com.pmirexat.nabi.pojos.Signuperuser;

/**
 * Servlet implementation class Userverification
 */
@WebServlet("/userverification")
public class Userverification extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Userverification() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		System.out.println("User verification will be called by this servlet");
		
	    String email = URLDecoder.decode(request.getParameter("email"),"UTF-8");
		
	    String receivedkey = request.getParameter("key");
	    System.out.println("Received key is :"+receivedkey);
	
		Uservalidator validatobj = new Uservalidator();	
		if(validatobj.verifyaccount(email, receivedkey))
		{
			 
		    System.out.println("This account is verified by user , will wait for Admins to approve requests.");
		    Signuperuser user = new DbCommands().getuser(email);
		    user.setVerificationstatus("Needs admin approval");
		    new DbCommands().updateEntry(user);
		    
		    String[] adminarr = new AdminChecker().getAdminids();
		    
		    for (int i = 0; i < adminarr.length; i++) 
		    {
			    new SimpleEmail().sendemail("Dear Admin. You have received a new signup request. Please login to your PmiRExAt account to check signup request","pmirexat@gmail.com",adminarr[i]);
			}
		    
		    request.setAttribute("message","Our team will approve your request soon and will inform your on your mail.");
		    
		    
		    
		    RequestDispatcher rd=request.getRequestDispatcher("signup.jsp");  
		
		    //servlet2 is the url-pattern of the second servlet  
		    
		  rd.forward(request, response);//method may be include or forward  
		}		
		else
		{
			System.out.println("This account can not be verified account");
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	}

}
