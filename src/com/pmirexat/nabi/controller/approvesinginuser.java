package com.pmirexat.nabi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.SimpleEmail;
import com.pmirexat.nabi.pojos.Signuperuser;

/**
 * Servlet implementation class approvesinginuser
 */
@WebServlet("/approvesinginuser")
public class approvesinginuser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public approvesinginuser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		 String email = request.getParameter("email");		
		 Signuperuser user = new DbCommands().getuser(email);
		 user.setVerificationstatus("Verified");
		 System.out.println("Sign in user verified");
		 new SimpleEmail().sendemail("Your PmiRExAt account has been verified. You can login to your account and start doing dataset profiling ", "pmriexat@gmail.com", email);
		 new DbCommands().updateEntry(user);
	}

}
