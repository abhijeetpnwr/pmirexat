package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class tisssuespecific_shannonwheat
 */
@WebServlet("/tisssuespecific_shannonwheat")
public class tisssuespecific_shannonwheat extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tisssuespecific_shannonwheat() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		System.out.println("get method called");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		System.out.println("post method called");
		
		
	    String received_tissue = request.getParameter("wheattissuelist");
	    float shannonentropy = Float.parseFloat(request.getParameter("shannon_entropy"));
		
		System.out.println("Reached in Tissuespecific_Wheat"+"With tissue : "+received_tissue +" shannon entropy = "+shannonentropy);
		
	   List<String> Wheatrna_list = new ApiClass().getAllWheatMirs();
	
	   List<String> searched_micro_rnas = new ArrayList<String>();
		
		
	   int loopcount = 0;
	   
	   String wheatrnas = "";
	   
		List wheatgetterlist=(List<String>)new datasetgetter().getdatasets("wheat");
		
	   for (Iterator iterator = Wheatrna_list.iterator(); iterator.hasNext();) 
	   {
		
		   String string = (String) iterator.next();
		   
		   if(loopcount == 0)
		   {
			   wheatrnas = wheatrnas+string;
		   }
		   else
		   {
			   wheatrnas = wheatrnas + ","+string;
		   }	
		   
		   loopcount++;
	   }
	   
	   System.out.println("wheatrnas --- :"+wheatrnas);
	   
	   System.out.println("Getter list is : --"+wheatgetterlist);
	   
	   List<List<String>> finalresults = new search_result_generator().searchdb_shannonentropy(wheatrnas,"wheat", wheatgetterlist, received_tissue, shannonentropy);
   
	   int forcoutnchk = 0;
	   
	 
	   searched_micro_rnas.addAll(finalresults.get(0));
	   finalresults.remove(0);    	   
		    	   
	   
	  // System.out.println("Result list is : "+finalresults);
	   
	   request.setAttribute("searched_micro_rnas",searched_micro_rnas);
	   request.setAttribute("result_list",finalresults);
		  
		  RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
			
		reqdispatcher.forward(request, response);
		  
	}

}
