package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.pmirexat.nabi.model.datasetgetter;

/**
 * Servlet implementation class Ricedataset_autocompleter
 */
@WebServlet("/Ricedataset_autocompleter")
public class Ricedataset_autocompleter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ricedataset_autocompleter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 System.out.println("reached do get");
		    doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 System.out.println("Reached is do post method ");
		   
		   String tobesearch_dataset=request.getParameter("select_datasets");
		   
		   /*
		       --> Normally we can only return text type from response from servlet  
		       --> To return object as in this case we need to return list object to ajax.
		       -->We will convert out List object to json type and will write  json type  to response  
		    */
		   
		   response.setContentType("application/json");
		 
		   String param=request.getParameter("term");
		   
		    //default parameter sent by autocpmleter.Which probably contains value being typed by user
		    List<String> dataset_list=new datasetgetter().getdatasets("rice");
		   	   
		   final List<String> auto_dataset = new ArrayList<String>();
	       
		   //This loop compares typed data with available in dataset list.If matches then adds it to list
		   for (final String dataset : dataset_list)
	       {
			   System.out.print("dataset: "+dataset.toLowerCase()+" param:"+param.toLowerCase());
			   
			   if (dataset.toLowerCase().startsWith(param.toLowerCase()))
	           {
	               auto_dataset.add(dataset);          
	           }
	       }
		   
		   
		   response.getWriter().write(new Gson().toJson(auto_dataset));	
	}

}
