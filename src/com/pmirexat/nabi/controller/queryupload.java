package com.pmirexat.nabi.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;




import org.apache.commons.lang3.RandomStringUtils;
import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.SimpleEmail;
import com.pmirexat.nabi.model.fastaReader;
import com.pmirexat.nabi.pojos.querydb;



/**
 * Servlet implementation class queryupload
 */
@WebServlet("/queryupload")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)   // 50MB
public class queryupload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public queryupload()
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		
		System.out.println("Called File upload's get method");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	 protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws ServletException, IOException {
		 
		 System.out.println("----------------- Reached yo yo ------------- ");
	        // gets absolute path of the web application
	        String appPath = getServletContext().getRealPath("");
	        
	        final String key =  RandomStringUtils.randomAlphanumeric(8);
	        
	        // constructs path of the directory to save uploaded file
	        String savePath = "";
	        if(System.getProperty("os.name").toLowerCase().contains("windows"))
	        {
	        	
	           savePath = "d:\\temp"; 
	           System.out.println("Test system is windows --so path is : "+savePath);                  
	        }
	        
	        else
	        {
	        	System.out.println("Linux environemt so relativce name");
	           savePath = "/satamas/PmiRExAtInputs/";
	        }
	        
	        // creates the save directory if it does not exists
	        File fileSaveDir = new File(savePath);
	        
	        if (!fileSaveDir.exists())
	        {
	            fileSaveDir.mkdir();
	        }
	        
	       
	            Part part =  request.getPart("file");
	           
	   
	        	System.out.println("Part check : "+part.getName());
	        	
	            String fileName = key+".fasta";
	            
	            String sb  = " ";
	            
	            part.write(savePath + File.separator + fileName);
	            
	  
	            //new fastaReader(savePath+File.separator + fileName);
	            
	            System.out.println(" ---- File to change permission  -----"+savePath + File.separator + fileName);
     
	            Process p = Runtime.getRuntime().exec("chmod 777  " +savePath + File.separator + fileName);
	            
	            try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

	            BufferedReader reader = 
	                 new BufferedReader(new InputStreamReader(p.getInputStream()));

	            String line = "";			
	            while ((line = reader.readLine())!= null) 
	            {
	            sb = sb+line+"\n";	
	        	//sb.append(line + "\n");
	            }
	            
	            System.out.println(" output of chmod -- "+sb);
	            
	            
	            request.setAttribute("message", "Upload has been done successfully!");
	            
	            final String emailid = (String) request.getParameter("emailid");	            
	           
	            System.out.println("Emailaddress is :"+emailid);
	            
	            Date currenttime = Calendar.getInstance().getTime();
	            System.out.println("Current time is :"+currenttime);

	           
	           
	            String pident = request.getParameter("pident");
	            System.out.println("Value for pident"+pident);
	            
	            String mismatch = request.getParameter("mismatch");
	            System.out.println("Value for mismatc should be :"+mismatch);
	            
	            String querycoverage = request.getParameter("querycoverage");
	            
	            System.out.println("our file_key will be :"+key);
	            
	            
	            querydb obj = new querydb();
	            obj.setEmail(emailid);
	            obj.setFile_key(key);
	            obj.setMismatch(mismatch);
	            obj.setPident(pident);
	            obj.setStarttime(currenttime);
	            obj.setStatus("Waiting for execution");
	            obj.setQuerycoverage(querycoverage);	
	             
	            new DbCommands().saveEntry(obj);  
	            
	               
			    new SimpleEmail().sendemail(" You can track your query status in 'Search Results' submenu in 'Custom Search' menu using  following Query ID key : "+key,emailid, emailid);
			 
	            
	            PrintWriter po_obj = response.getWriter();
	           po_obj.write(key);
	    }
	 
	    /**
	     * Extracts file name from HTTP header content-disposition
	     */
	    private String extractFileName(Part part) 
	    {
	        String contentDisp = part.getHeader("content-disposition");
	        String[] items = contentDisp.split(";");
	        for (String s : items) {
	            if (s.trim().startsWith("filename")) 
	            {
	            	System.out.println("Will return filename");
	                return s.substring(s.indexOf("=") + 2, s.length()-1);	                
	            }
	        }
	        return "";
	    }

}
