package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.filetotablegenerator;

/**
 * Servlet implementation class newdsresultcontroller
 */
@WebServlet("/newdsresultcontroller")
public class newdsresultcontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public newdsresultcontroller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	
	    PrintWriter pw = response.getWriter();
		
		// TODO Auto-generated method stub
		System.out.println("Do post method called for displaying output results");
	    String outputresultvalue = request.getParameter("filename");
	    
	    String filepath = "/satamas/Pmirexat_datasetsprocessing/";
	    
	    if(System.getProperty("os.name").contains("Windows"))
	    {
	    	System.out.println("Windows system so  fle check from your directory :");
	    	filepath = "D:/temp/";
	    }
	    
	   pw.write(new filetotablegenerator().fietotableconvertor(outputresultvalue,filepath)); 
	    
	    
	    System.out.println("Output file name is :"+outputresultvalue);
	}

}
