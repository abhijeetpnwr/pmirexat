package com.pmirexat.nabi.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.filetotablegenerator;
import com.pmirexat.nabi.pojos.querydb;

/**
 * Servlet implementation class statuscheck
 */
@WebServlet("/statuscheck")
public class statuscheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public statuscheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	System.out.println("Do post method is called");
		
		String file_key = request.getParameter("keyval").trim();
		
		System.out.println("file key is :"+file_key);
		
		String button1 = request.getParameter("selected");
		
		System.out.println("button 1 :"+button1);
	
		PrintWriter po = response.getWriter();
	
			System.out.println("Check status button has submitted the foem");
			querydb qdbobj =  new DbCommands().getfilestatus(file_key);		
			
			if(qdbobj == null)
			{
				po.write("This key does't exist in ourdatabase.Please recheck your entered key");
			}
			
			else
			{
				if(qdbobj.getStatus().equals("Execution Success"))
				{
					po.write("you file has been processed"+"$");			
					filetotablegenerator obj = new filetotablegenerator();
					String tablecode = obj.fietotableconvertor(file_key+"_output","/satamas/pmiRexatresults/");				
					po.write(tablecode);
				}
				
				else
				{
					po.write("your file been not processed yet.We we inform you by email when its done");
				}
			}
		}
		

}
