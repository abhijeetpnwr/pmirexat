package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.ArabDiffPojo;
import com.pmirexat.nabi.pojos.MaizeDiffPojo;
import com.pmirexat.nabi.pojos.RiceDiffPojo;
import com.pmirexat.nabi.pojos.WheatDiffPojo;

/**
 * Servlet implementation class diffservlet
 */
@WebServlet("/diffservlet")
public class diffservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public diffservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Reached here happily");
		
		
		String species = request.getParameter("dropdown");
			
		String dataset1 = request.getParameter("dataset1").trim();
		String dataset2 = request.getParameter("dataset2").trim();
		
		if(species==null || species.length()<1)
		{
			if(dataset1.startsWith("W"))
			{
				species="wheat";
			}
			if(dataset1.startsWith("M"))
			{
				species="maize";
			}
			if(dataset1.startsWith("R"))
			{
				species="rice";
			}
			if(dataset1.startsWith("A"))
			{
				species="arab";
			}
		}
		
		
		/* call this code only if it is Arabidopsis */
		
	
		
		///////////////////////////////////////////////
		
		String pojoclass = "";
	
		response.setContentType("application/json"); 
		
		List resullist = new DbCommands().getdiffresult(species, dataset1, dataset2);
		
		System.out.println("Result list : "+resullist);
		
		
		if(resullist == null || resullist.size()<1)
		{
			System.out.println("No results for db1 and db2. So now checking for reverse");
			resullist = new DbCommands().getdiffresult(species, dataset2, dataset1);
		}
		
		else
		{
			System.out.println("Match found for db1 and db2");
		}
		
		
		JsonArray jsarr = new JsonArray();
		
		Gson gsobj = new Gson();
	

		
		System.out.println("Result list :"+resullist);
			 response.getWriter().write(new Gson().toJson(resullist));	
    
	}

}
