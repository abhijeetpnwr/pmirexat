package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.Commonsequencegetter;
import com.pmirexat.nabi.model.crossspecies_resultgen;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class crossspeceisController
 */
@WebServlet("/crossspeceisController")
public class crossspeceisController extends HttpServlet
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public crossspeceisController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	      String toget = request.getParameter("toget");
		 
		  System.out.println("chh 1 "+toget);
	  
		  @SuppressWarnings("unchecked")
			List ricegetterlist=(List<String>) new datasetgetter().getdatasets("rice");
		  
		    @SuppressWarnings("unchecked")
			List wheatgetterlist=(List<String>)new datasetgetter().getdatasets("wheat");
		    
		    @SuppressWarnings("unchecked")
			List maizegetterlist=(List<String>)new datasetgetter().getdatasets("maize");
		    
		    
		    @SuppressWarnings("unchecked")
			List arabgetterlist=(List<String>)new datasetgetter().getdatasets("arab");
		    
		    
		  String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
		  String arabrbas = "";
		  
		  List<String> searched_micro_rnas=new ArrayList<String>();
		  List<String> finalresult = new ArrayList<String>();
		 
		  List<List<String>> resultlist = new ArrayList<List<String>>();
		  
		  
		  
		  ServletContext sc = getServletContext();
		  System.out.println("chk 4 "+toget);
		  
	
			  
			  ArrayList<String> temp = new ArrayList<String>();
	   		   
			  
			  String[] chararr = toget.split("_");
			  
			  
			  for(String c : chararr)
			  {
				  System.out.println("Character is :"+c);
                  if(c.equals("w"))
                  {
                	  temp.add("Wheat");
                	  
                	  System.out.println(" -- Adding wheat --");
                  }
                  if(c.equals("m"))
                  {
        	   		  temp.add("maize");
        	   		  System.out.println(" -- Adding maize --");
                  }
                  if(c.equals("r"))
                  {
                	  temp.add("Rice");  
                	  System.out.println(" -- Adding rice --");
                  }
                  if(c.equals("a"))
                  {
                	  temp.add("Arab");
                	  System.out.println(" -- Adding arab --");
                  }
                  
			  }

	   		  resultlist = new crossspecies_resultgen().getcommon(temp);
			    
			    System.out.println("Returned :"+resultlist);
		
			 
			  searched_micro_rnas = resultlist.get(0);
	
			  finalresult = resultlist.get(1);
			
			  System.out.println("Counts in results-------------------------------------------------------- :"+finalresult.size());
			  
			  request.setAttribute("searched_micro_rnas",searched_micro_rnas);
			  
			  request.setAttribute("result_list",finalresult);
			  
			  request.setAttribute("tableid","resulttable"+toget);
		  
		  	 
		 RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
			
		 reqdispatcher.forward(request, response);
		
	}

}
