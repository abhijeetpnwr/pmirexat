package com.pmirexat.nabi.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class csvtoheatmapservlet
 */
@WebServlet("/csvtoheatmapservlet")
public class csvtoheatmapservlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public csvtoheatmapservlet() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
		// TODO Auto-generated method stub
    	System.out.println("called Csv to heatmap get method");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("csv to heatmap post method"+request.getParameter("fileno"));
		
		  String[] filenames = new String[20];
		
		  filenames[0]= "TangZetal_Countmatrix.csv"; filenames[1]= "Ritupandeyetal_Countmatrix.csv"; filenames[2]= "SunFetal_CountMatrix.csv";
		  
		  filenames[3]= "Mayeretal_Countmatrix.csv"; filenames[4]= "XingliMaetal_CountMatrix.csv";
		  
		  filenames[5]= "miRBase21_56miRCountmatrix.csv";filenames[6]= "PNRD_260miRCountmatrix.csv";filenames[7]= "Jianyangetal_Countmatrix.csv";
		  
		  filenames[8]= "DDingetal_Countmatrix.csv";
		  
		  filenames[9]= "FangliWuetal_Countmatrix.csv";filenames[10]= "HongjunLiuetal_Countmatrix.csv";filenames[11]= "Thiebautetal_Countmatrix.csv";
	
		  
		  String filename = filenames[Integer.parseInt(request.getParameter("fileno")) - 1];
		
		  System.out.println("File to get is :"+filename);
		
	       InputStream  inStream =  Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
	        
	       BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
	       StringBuilder out = new StringBuilder();
	       
	       String line;
	       
	       
	       String tablestring = "<table class=\"table  table-striped table-bordered table-hover table-condensed\" id=\"resultable\" style=\"max-height:270px;overflow:auto;width:100%;display:block;margin-top:20px;\">";
	       
	       while ((line = reader.readLine()) != null) 
	       {
	       	
	       	tablestring =  tablestring +"<tr style='width:100%;'>";
	           out.append(line);
	         
	            
	           String[] splitelem = line.split(",");

	           
	           for (String string : splitelem)
	           {
	          
					 tablestring = tablestring+ "<td>"+string.toString()+"</td>";
				}		            
	           
	           tablestring = tablestring + "</tr>";
	       }
	       
	       tablestring = tablestring + "</table>";  //Prints the string content read from input stream
	       
	       reader.close();
	       
	       System.out.println("Table strign is :"+tablestring);
	       
	       PrintWriter print_out = response.getWriter();
	       print_out.print(tablestring);
	       
		
	}

}
