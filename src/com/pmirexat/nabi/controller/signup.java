package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.pmirexat.nabi.model.Cryptoworker;
import com.pmirexat.nabi.model.SignupExecuter;
import com.pmirexat.nabi.pojos.Signuperuser;

/**
 * Servlet implementation class signup
 */
@WebServlet("/signup")
public class signup extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public signup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Reached  doget sign up action");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{			
		
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");	
		String Institution = request.getParameter("Institution");
		String Department = request.getParameter("Department");
		if(Department == null)
		{
			Department = "";
		}
		String contact = request.getParameter("contact");
		String Address = request.getParameter("Address");
		String reason = request.getParameter("reason");
		
		Date currenttime = Calendar.getInstance().getTime();
		
		
		//write code to check that whether an accoutn already exists in our database
		 Cryptoworker cryptobj = new Cryptoworker();	  
		 String salt =  cryptobj.generatesalt();
		 
		 
		 
		//String signupstatus = SignupExecuter.signup(new Signuperuser(first_name, last_name, email, password, "Awaiting Verification",currenttime),salt); //Sign up called for the account	
		 String signupstatus = SignupExecuter.signup(new Signuperuser(first_name, last_name, email, password,"Awaiting Verification", currenttime, Institution, Address, contact, reason, Department), salt);
		 
		PrintWriter outobj =   response.getWriter();
		outobj.write(signupstatus);
	
	}

}
