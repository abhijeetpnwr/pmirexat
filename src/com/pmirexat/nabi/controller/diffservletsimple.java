package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.cxf.binding.corba.wsdl.Array;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.MaizeDiffPojo;
import com.pmirexat.nabi.pojos.RiceDiffPojo;
import com.pmirexat.nabi.pojos.WheatDiffPojo;

/**
 * Servlet implementation class diffservletsimple
 */
@WebServlet("/diffservletsimple")
public class diffservletsimple extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public diffservletsimple() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
System.out.println("Reached here happily");
		
		
		String species = request.getParameter("dropdown");
		
		
		
		String dataset1 = request.getParameter("dataset1").trim();
		String dataset2 = request.getParameter("dataset2").trim();

		List<List<String>> results = new ArrayList<>();
		
		List<String> templist = new ArrayList<>();

	
		templist.add("MiRNA_ID");
		
		templist.add("LogFC");
		templist.add("LogCPM");
		templist.add("PValue");
		templist.add("FDR");
		
		results.add(templist);
		
		System.out.println("Speceis:"+species+" Dataset1:"+dataset1+"Dataset2 :"+dataset2);
		
		templist = null;
		
		if(species==null || species.length()<1)
		{
			if(dataset1.startsWith("W"))
			{
				species="wheat";
				List<WheatDiffPojo> resullist = new DbCommands().getdiffresult(species, dataset1, dataset2);
				
				System.out.println("Reuslts ---- "+resullist);
				
				for(WheatDiffPojo ob : resullist)
				{
					 templist = new ArrayList<>();
					templist.add(ob.getMiRNA_ID());
					templist.add(Float.toString(ob.getLogFC()));
					templist.add(Float.toString(ob.getLogCPM()));
					templist.add(Float.toString(ob.getPValue()));
					templist.add(Float.toString(ob.getFDR()));
				    results.add(templist);
				}
				
			}
			if(dataset1.startsWith("M"))
			{
				species="maize";
				List<MaizeDiffPojo> resullist = new DbCommands().getdiffresult(species, dataset1, dataset2);
			  

				System.out.println("Reuslts ---- "+resullist);
				
				for(MaizeDiffPojo ob:resullist)
				{
					 templist = new ArrayList<>();
						
						templist.add(ob.getMiRNA_ID());
						templist.add(Float.toString(ob.getLogFC()));
						templist.add(Float.toString(ob.getLogCPM()));
						templist.add(Float.toString(ob.getPValue()));
						templist.add(Float.toString(ob.getFDR()));
					    results.add(templist);
				}
			 
			}
			if(dataset1.startsWith("R"))
			{
				species="rice";
				
				
				
				List<RiceDiffPojo> resullist = new DbCommands().getdiffresult(species, dataset1, dataset2);
			
				

				System.out.println("Reuslts ---- "+resullist);
				for(RiceDiffPojo ob:resullist)
				{
					 templist = new ArrayList<>();
						
						templist.add(ob.getMiRNA_ID());
						templist.add(Float.toString(ob.getLogFC()));
						templist.add(Float.toString(ob.getLogCPM()));
						templist.add(Float.toString(ob.getPValue()));
						templist.add(Float.toString(ob.getFDR()));
					    results.add(templist);
				}
			
			}
		}
		
		String pojoclass = "";
		request.setAttribute("results",results);
		request.setAttribute("Dataset1",dataset1);
		request.setAttribute("Dataset2",dataset2);
		
		 RequestDispatcher reqdispatcher=request.getRequestDispatcher("browsediffresult.jsp");
		 reqdispatcher.forward(request, response);
		
	}

}
