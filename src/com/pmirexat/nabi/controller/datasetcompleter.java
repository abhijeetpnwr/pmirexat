package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.datasetcompletermodel;

/**
 * Servlet implementation class datasetcompletermodel
 */
@WebServlet("/datasetcompleter")
public class datasetcompleter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public datasetcompleter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//final List<String> micro_rnas=(List<String>) new RnaGetter().getmiRlist("wheat");
		
		 response.setContentType("application/json");

	         String param = request.getParameter("species");
	         
	         Map<String,List<String>> result = datasetcompletermodel.getdatasets(param); 
	        
	         System.out.println("map is :"+result);
	         
	         Gson gsobj = new Gson();
	         
	         response.getWriter().write(new Gson().toJson(result));	
	         
	        
	         
	}

}
