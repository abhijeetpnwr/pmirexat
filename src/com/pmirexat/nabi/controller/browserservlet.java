package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;
import com.pmirexat.nabi.pojos.ArabTarget;
import com.pmirexat.nabi.pojos.MaizeTarget;
import com.pmirexat.nabi.pojos.RiceTarget;
import com.pmirexat.nabi.pojos.WheatTarget;

/**
 * Servlet implementation class browserservlet
 */
@WebServlet("/browserservlet")
public class browserservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public browserservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub

		String miRId = request.getParameter("mirId");
		
		System.out.println("Reached in do get of browser servlet with miRid : "+miRId);
		
		List<String> wheatmirs = new ApiClass().getAllWheatMirs();
		
		List<String> maizemirs = new ApiClass().getAllMaizeMirs();
		
		List<String> ricemirs = new ApiClass().getAllRiceMirs();
		
	
		List<String> arabmirs = new RnaGetter().getmiRlist("arab");
		
		List<List<String>> resultlist = null;
		
		
		
		List<String> searched_micro_rnas = new ArrayList<String>();
		
		searched_micro_rnas.add("Searched Dataset Name");
		
		searched_micro_rnas.add(miRId);
		
		List<List<String>> targetlist = new ArrayList<List<String>>();
		
		//List<List<String>> targetmatrix = null;
		
		if(wheatmirs.contains(miRId))
		{
			System.out.println("Need to search it in Wheat");
			resultlist=new search_result_generator().searchdb(miRId, "wheat" ,new datasetgetter().getdatasets("wheat"));   
		    System.out.println("Result --------"+resultlist);
            List<Object> temp =   new DbCommands().getdetail(miRId, "WheatTarget");
            
            for(Object obj : temp)
            {
            	ArrayList<String> rowtemp = new ArrayList<String>();
            	WheatTarget tmpobj = (WheatTarget)obj;
            	
            	rowtemp.add(tmpobj.getTarget_Acc());
            	rowtemp.add(Integer.toString(tmpobj.getExpectation()));
            	
            	
            	rowtemp.add(Float.toString(tmpobj.getUPE()));
            	
            	rowtemp.add(tmpobj.getInhibition());
            	rowtemp.add(tmpobj.getMiRNA_Acc());
            	rowtemp.add(tmpobj.getMiRNA_aligned_fragment());
            	
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_start()));
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_end()));
            	
            	rowtemp.add(Integer.toString(tmpobj.getTarget_start()));
            	rowtemp.add(Integer.toString(tmpobj.getTarget_end()));
            	
            	rowtemp.add(tmpobj.getTarget_aligned_fragment());
  
            	rowtemp.add(tmpobj.getTarget_Desc());
            	
            	System.out.println("Wheat object :"+tmpobj.toString());
          
                targetlist.add(rowtemp);
          }
            
		}
		
		if(ricemirs.contains(miRId))
		{
			System.out.println("Need to search it in Rice");
			resultlist=new search_result_generator().searchdb(miRId, "rice" ,new datasetgetter().getdatasets("rice"));   
		    System.out.println("Result ------------- "+resultlist);
		   
            List<Object> temp =   new DbCommands().getdetail(miRId, "RiceTarget");
            
            for(Object obj : temp)
            {
            	RiceTarget tmpobj = (RiceTarget)obj;
            	
            	System.out.println("Rice object :"+tmpobj.toString());
            	
            	ArrayList<String> rowtemp = new ArrayList<String>();
            	
            	rowtemp.add(tmpobj.getTarget_Acc());
            	rowtemp.add(Integer.toString(tmpobj.getExpectation()));
            	
            	rowtemp.add(Float.toString(tmpobj.getUPE()));
            	
            	rowtemp.add(tmpobj.getInhibition());
            	rowtemp.add(tmpobj.getMiRNA_Acc());
            	rowtemp.add(tmpobj.getMiRNA_aligned_fragment());
            	
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_start()));
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_end()));
            	
            	rowtemp.add(Integer.toString(tmpobj.getTarget_start()));
            	rowtemp.add(Integer.toString(tmpobj.getTarget_end()));
            	
            	rowtemp.add(tmpobj.getTarget_aligned_fragment());
  
            	rowtemp.add(tmpobj.getTarget_Desc());
            	
            	System.out.println("Rice object :"+tmpobj.toString());
         
            	targetlist.add(rowtemp);
            }
		
		}
		
		if(maizemirs.contains(miRId))
		{
			System.out.println("Need to search it in Maize");
			resultlist=new search_result_generator().searchdb(miRId, "maize" ,new datasetgetter().getdatasets("maize"));   
		    System.out.println("Result list ------"+resultlist);
		  
		    List<Object> temp =   new DbCommands().getdetail(miRId, "RiceTarget");
     
            for(Object obj : temp)
            {
            	MaizeTarget tmpobj = (MaizeTarget)obj;
            	
            	System.out.println("Rice object :"+tmpobj.toString());
        
            	
            	ArrayList<String> rowtemp = new ArrayList<String>();
            	
            	rowtemp.add(tmpobj.getTarget_Acc());
            	rowtemp.add(Integer.toString(tmpobj.getExpectation()));
            	
            	rowtemp.add(Float.toString(tmpobj.getUPE()));
            	rowtemp.add(tmpobj.getInhibition());
            	rowtemp.add(tmpobj.getMiRNA_Acc());
            	rowtemp.add(tmpobj.getMiRNA_aligned_fragment());
            	
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_start()));
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_end()));
            	rowtemp.add(Integer.toString(tmpobj.getTarget_start()));
            	rowtemp.add(Integer.toString(tmpobj.getTarget_end()));
            	
            	rowtemp.add(tmpobj.getTarget_aligned_fragment());
  
            	rowtemp.add(tmpobj.getTarget_Desc());
            	
            	System.out.println("Maize object  :"+tmpobj.toString());
            	targetlist.add(rowtemp);
            	System.out.println("Row is :"+rowtemp);
            }
		
		}
		
		System.out.println(" -- mir list : --"+arabmirs);
		
		if(arabmirs.contains(miRId))
		{
			System.out.println("Need to search it in arab");
			resultlist=new search_result_generator().searchdb(miRId, "arab" ,new datasetgetter().getdatasets("arab"));   
		    System.out.println("Result list ------"+resultlist);
		  
		    List<Object> temp =   new DbCommands().getdetail(miRId, "RiceTarget");
     
            for(Object obj : temp)
            {
            	ArabTarget tmpobj = (ArabTarget)obj;
            	
            	System.out.println("Rice object :"+tmpobj.toString());
        
            	
            	ArrayList<String> rowtemp = new ArrayList<String>();
            	
            	rowtemp.add(tmpobj.getTarget_Acc());
            	rowtemp.add(Integer.toString(tmpobj.getExpectation()));
            	
            	rowtemp.add(Float.toString(tmpobj.getUPE()));
            	rowtemp.add(tmpobj.getInhibition());
            	rowtemp.add(tmpobj.getMiRNA_Acc());
            	rowtemp.add(tmpobj.getMiRNA_aligned_fragment());
            	
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_start()));
            	rowtemp.add(Integer.toString(tmpobj.getMiRNA_end()));
            	rowtemp.add(Integer.toString(tmpobj.getTarget_start()));
            	rowtemp.add(Integer.toString(tmpobj.getTarget_end()));
            	
            	rowtemp.add(tmpobj.getTarget_aligned_fragment());
  
            	rowtemp.add(tmpobj.getTarget_Desc());
            	
            	System.out.println("arab object  :"+tmpobj.toString());
            	targetlist.add(rowtemp);
            	System.out.println("Row is :"+rowtemp);
            }
		
		}
		
		System.out.println(" --------- generated double list is : ----- "+targetlist);
		
		
		request.setAttribute("targetlist", targetlist);
		
		request.setAttribute("searched_micro_rnas", searched_micro_rnas);
		
		request.setAttribute("result_list", resultlist);

        RequestDispatcher reqdispatcher=request.getRequestDispatcher("browsedetails.jsp");
		
		 reqdispatcher.forward(request, response);
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Reached in do post of browser servlet");
	}

}
