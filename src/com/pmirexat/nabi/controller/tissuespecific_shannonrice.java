package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class tissuespecific_shannonrice
 */
@WebServlet("/tissuespecific_shannonrice")
public class tissuespecific_shannonrice extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tissuespecific_shannonrice() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		System.out.println("Get method is called");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Post method is called :");

	    String received_tissue = request.getParameter("ricetissuelist");
	    float foldcount = Float.parseFloat(request.getParameter("shannon_entropy"));
		
		System.out.println("Reached in Tissuespecific_rice"+"With tissue : "+received_tissue +" and with foldcount = "+foldcount);
		
	   List<String> ricerna_list = new ApiClass().getAllRiceMirs();
	
	   List<String> searched_micro_rnas = new ArrayList<String>();

	   int loopcount = 0;
	   
	   String ricernas = "";
	   
		List ricegetterlist=(List<String>)new datasetgetter().getdatasets("rice");
		
	   for (Iterator iterator = ricerna_list.iterator(); iterator.hasNext();) 
	   {
		
		   String string = (String) iterator.next();
		   
		   if(loopcount == 0)
		   {
			   ricernas = ricernas+string;
		   }
		   else
		   {
			   ricernas = ricernas + ","+string;
		   }	
		   
		   loopcount++;
	   }
	   
	   System.out.println("ricernas --- :"+ricernas);
	   
	   System.out.println("Getter list is : --"+ricegetterlist);
	   
	   List<List<String>> finalresults = new search_result_generator().searchdb_shannonentropy(ricernas,"rice", ricegetterlist, received_tissue, foldcount);
	   //List<List<String>> finalresults = new search_result_generator().searchdb_byfold(wheatrnas,"wheat", wheatgetterlist, received_tissue, foldcount);
	   
	   int forcoutnchk = 0;
	   
	 
	   searched_micro_rnas.addAll(finalresults.get(0));
	   finalresults.remove(0);    	   
		    	   
	   
	   System.out.println("Result list is : "+finalresults);
	   
	   request.setAttribute("searched_micro_rnas",searched_micro_rnas);
	   request.setAttribute("result_list",finalresults);
		  
		  RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
			
		reqdispatcher.forward(request, response);
		
		
	}

}
