package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.google.gson.Gson;
import com.pmirexat.nabi.dao.HibernateUtils;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;
import com.pmirexat.nabi.pojos.EdgeRProp;
import com.pmirexat.nabi.pojos.Maize5Doc_Edger;
import com.pmirexat.nabi.pojos.MaizeAnther_Edger;
import com.pmirexat.nabi.pojos.MaizeEar_Edger;
import com.pmirexat.nabi.pojos.MaizeLeaf_Edger;
import com.pmirexat.nabi.pojos.MaizePollen_Edger;
import com.pmirexat.nabi.pojos.MaizeRoot_Edger;
import com.pmirexat.nabi.pojos.MaizeShoot_Edger;
import com.pmirexat.nabi.pojos.MaizeSilk_Edger;
import com.pmirexat.nabi.pojos.MaizeTassel_Edger;
import com.pmirexat.nabi.pojos.RiceAnther_Edger;
import com.pmirexat.nabi.pojos.RiceEmbryo_Edger;
import com.pmirexat.nabi.pojos.RiceEndosperm_Edger;
import com.pmirexat.nabi.pojos.RiceLeaf_Edger;
import com.pmirexat.nabi.pojos.RicePanicle_Edger;
import com.pmirexat.nabi.pojos.RiceRoot_Edger;
import com.pmirexat.nabi.pojos.RiceShoot_Edger;
import com.pmirexat.nabi.pojos.WheatLeaf_Edger;
import com.pmirexat.nabi.pojos.WheatSpike_Edger;

/**
 * Servlet implementation class EdgeRFilter
 */
@WebServlet("/EdgeRFilter")
public class EdgeRFilter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EdgeRFilter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		response.setContentType("application/json");
		
		System.out.println("Reached do post of edgeR calculator servlet");
		
		String species = request.getParameter("species");
		String tissue = request.getParameter("tissue"); 
		String filterparam = request.getParameter("filterparam");
		String  value = request.getParameter("value");		
		
		EdgeRProp edge_ref = null;
		
		
		System.out.println(species+","+tissue+","+filterparam+","+value);
		
		 Session ses=new HibernateUtils().getSession();
    	 ses.beginTransaction();
    	 String sql = "select * from "+species.toLowerCase()+tissue.toLowerCase()+"_edger where "+filterparam+" >"+value+" and "+filterparam+"!=100001.0";
    	 
    	 System.out.println("Query generated is :"+sql);
    	 
    	 SQLQuery query = ses.createSQLQuery(sql);  
    	 
    	 List<String> mir_ids_list= new ArrayList<>();
    	 
    	 List<List<String>> jsonresult_array = new ArrayList<>(); 
    	 
	 
    	 if(species.equalsIgnoreCase("Wheat"))
    	 {
    		 if(tissue.equalsIgnoreCase("Leaf"))
    		 {
    			 query.addEntity(WheatLeaf_Edger.class);
    		 }
    		 
    		 if(tissue.equalsIgnoreCase("Spike"))
    		 {
    			 query.addEntity(WheatSpike_Edger.class);
    		 }		
    	 }  
    	 
    	 
    	 

    	 
    	 if(species.equalsIgnoreCase("rice"))
    	 {
    		 if(tissue.equalsIgnoreCase("root"))
    		 {
    			 query.addEntity(RiceRoot_Edger.class);
    		 }
    		 
    		 if(tissue.equalsIgnoreCase("Shoot"))
    		 {
    			 query.addEntity(RiceShoot_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Embryo"))
    		 {
    			 query.addEntity(RiceEmbryo_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Anther"))
    		 {
    			 query.addEntity(RiceAnther_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Leaf"))
    		 {
    			 query.addEntity(RiceLeaf_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Panicle"))
    		 {
    			 query.addEntity(RicePanicle_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Endosperm"))
    		 {
    			 query.addEntity(RiceEndosperm_Edger.class);
    		 }	
    	
    	 }  
    	 
    	 
    	 
    	 
    	 if(species.equalsIgnoreCase("maize"))
    	 {
    		 if(tissue.equalsIgnoreCase("root"))
    		 {
    			 query.addEntity(MaizeRoot_Edger.class);
    		 }
    		 
    		 if(tissue.equalsIgnoreCase("Shoot"))
    		 {
    			 query.addEntity(MaizeShoot_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Ear"))
    		 {
    			 query.addEntity(MaizeEar_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Anther"))
    		 {
    			 query.addEntity(MaizeAnther_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Leaf"))
    		 {
    			 query.addEntity(MaizeLeaf_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Silk"))
    		 {
    			 query.addEntity(MaizeSilk_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Pollen"))
    		 {
    			 query.addEntity(MaizePollen_Edger.class);
    		 }	
    		 if(tissue.equalsIgnoreCase("Tassel"))
    		 {
    			 query.addEntity(MaizeTassel_Edger.class);
    		 }	
    	
    		 if(tissue.equalsIgnoreCase("5Doc"))
    		 {
    			 query.addEntity(Maize5Doc_Edger.class);
    		 }	
    	
    	
    	 } 
    	 
    	 
    	 List<String> jsonstring_arr = new ArrayList<>();
    	 
    	 List results = query.list();
    	  	 
    	 for(Object obj : results)
    	 {
    		 if(obj instanceof WheatLeaf_Edger)
    		 {
    			 WheatLeaf_Edger w = (WheatLeaf_Edger) obj;
    			 
    			 edge_ref = w;
    			 
    			
    	
    			 
    		 }
    		 
    		 if(obj instanceof WheatSpike_Edger)
    		 {
    			 WheatSpike_Edger w = (WheatSpike_Edger) obj;	
    			 edge_ref = w;
    		 }
    		 
    		 
    		 
    		 // ---------------------------------------- //
    		 
    		 
    		 if(obj instanceof RiceRoot_Edger)
    		 {
    			 RiceRoot_Edger w = (RiceRoot_Edger) obj;
    			 edge_ref = w;
    			
    			 
    		 }
    		 if(obj instanceof RiceShoot_Edger)
    		 {
    			 RiceShoot_Edger w = (RiceShoot_Edger) obj;
    			 edge_ref = w;
    			
    			 
    		 }
    		 if(obj instanceof RiceAnther_Edger)
    		 {
    			 RiceAnther_Edger w = (RiceAnther_Edger) obj;
    			 edge_ref = w;
    		
    			 
    		 }
    		 if(obj instanceof RiceLeaf_Edger)
    		 {
    			 RiceLeaf_Edger w = (RiceLeaf_Edger) obj;
    			 edge_ref = w;
    	
    			 
    		 }
    		 if(obj instanceof RiceEmbryo_Edger)
    		 {
    			 RiceEmbryo_Edger w = (RiceEmbryo_Edger) obj;
    			 edge_ref = w;
    		
    			 
    		 }
    		 if(obj instanceof RiceEndosperm_Edger)
    		 {
    			 RiceEndosperm_Edger w = (RiceEndosperm_Edger) obj;
    			 edge_ref = w;
    		
    			 
    		 }
    		 if(obj instanceof RicePanicle_Edger)
    		 {
    			 RicePanicle_Edger w = (RicePanicle_Edger) obj;
    			 edge_ref = w;
    			 
    			 
    	
    			 
    		 }
    		 
    		// -------------------------------------------- //
    		 
    		 
    		 
    		 if(obj instanceof Maize5Doc_Edger)
    		 {
    			 Maize5Doc_Edger w = (Maize5Doc_Edger) obj;
    			 edge_ref = w;
    			
    			 
    			 
    		 }
    		 if(obj instanceof MaizeAnther_Edger)
    		 {
    			 MaizeAnther_Edger w = (MaizeAnther_Edger) obj;
    			 edge_ref = w;
    			
    			 
    		 }
    		 if(obj instanceof MaizeLeaf_Edger)
    		 {
    			 MaizeLeaf_Edger w = (MaizeLeaf_Edger) obj;
    			 edge_ref = w;
   
    			 
    		 }
    		 if(obj instanceof MaizePollen_Edger)
    		 {
    			 MaizePollen_Edger w = (MaizePollen_Edger) obj;
    			 edge_ref = w;
    			
    			 
    		 }
    		 if(obj instanceof MaizeSilk_Edger)
    		 {
    			 MaizeSilk_Edger w = (MaizeSilk_Edger) obj;
    			 edge_ref = w;
    			
    			 
    		 }
    		 if(obj instanceof MaizeTassel_Edger)
    		 {
    			 MaizeTassel_Edger w = (MaizeTassel_Edger) obj;
    			 edge_ref = w;
    
    			 
    		 }
    		 if(obj instanceof MaizeEar_Edger)
    		 {
    			 MaizeEar_Edger w = (MaizeEar_Edger) obj;
    			 edge_ref = w;
    	
    			 
    		 }
    		 if(obj instanceof MaizeRoot_Edger)
    		 {
    			 MaizeRoot_Edger w = (MaizeRoot_Edger) obj;
    			 edge_ref = w;
    		
    			 
    		 }
    		 if(obj instanceof MaizeShoot_Edger)
    		 {
    			 MaizeShoot_Edger w = (MaizeShoot_Edger) obj;
    			 edge_ref = w;
    			
    			 
    		 }
    		 
    		 
    		 List<String> temp = new ArrayList<>();
    		 
    		 temp.add(edge_ref.getMir_ids());
    		 temp.add(Float.toString(edge_ref.getLogFC()));
    		 temp.add(Float.toString(edge_ref.getLogCPM()));
    		 temp.add(Float.toString(edge_ref.getPValue()));
    		 temp.add(Float.toString(edge_ref.getFDR()));
    		 
    		 jsonresult_array.add(temp);
    		 
  
    	 }
    	 
    	
    	 ses.getTransaction().commit();
    	 ses.close();
    	 
    	 
    	 Gson gsobj = new Gson();
    	 response.getWriter().write(new Gson().toJson(jsonresult_array));	
	
	}

}
