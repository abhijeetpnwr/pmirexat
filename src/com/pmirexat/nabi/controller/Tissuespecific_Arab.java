package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class Tissuespecific_Arab
 */
@WebServlet("/Tissuespecific_Arab")
public class Tissuespecific_Arab extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Tissuespecific_Arab() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String received_tissue = request.getParameter("arabtissuelist");
	    float foldcount = Float.parseFloat(request.getParameter("foldcount"));
		
		System.out.println("Reached in Tissuespecific_arab"+"With tissue : "+received_tissue +" and with foldcount = "+foldcount);
		
	   List<String> arabrna_list = new RnaGetter().getmiRlist("arab");
	
	   List<String> searched_micro_rnas = new ArrayList<String>();

	   int loopcount = 0;
	   
	   String arabrnas = "";
	   
		List arabgetterlist=(List<String>)new datasetgetter().getdatasets("arab");
		
	   for (Iterator iterator = arabrna_list.iterator(); iterator.hasNext();) 
	   {
		
		   String string = (String) iterator.next();
		   
		   if(loopcount == 0)
		   {
			   arabrnas = arabrnas+string;
		   }
		   else
		   {
			   arabrnas = arabrnas + ","+string;
		   }	
		   
		   loopcount++;
	   }
	   
	   System.out.println("arabrnas --- :"+arabrnas);
	   
	   System.out.println("Getter list is : --"+arabgetterlist);
	   
	   List<List<String>> finalresults = new search_result_generator().searchdb_byfold(arabrnas,"arab", arabgetterlist, received_tissue, foldcount);
	   //List<List<String>> finalresults = new search_result_generator().searchdb_byfold(wheatrnas,"wheat", wheatgetterlist, received_tissue, foldcount);
	   
	   int forcoutnchk = 0;
	   
	 
	   searched_micro_rnas.addAll(finalresults.get(0));
	   finalresults.remove(0);    	   
		    	   
	   
	   System.out.println("Result list is : "+finalresults);
	   
	   request.setAttribute("searched_micro_rnas",searched_micro_rnas);
	   request.setAttribute("result_list",finalresults);
		  
		  RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
			
		reqdispatcher.forward(request, response);
		// TODO Auto-generated method stub
	}

}
