package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.pojos.newdsprocessing;
import com.sun.corba.se.spi.legacy.connection.GetEndPointInfoAgainException;

/**
 * Servlet implementation class newdsresultdisplay
 */
@WebServlet("/newdsresultdisplay")
public class newdsresultdisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public newdsresultdisplay()
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		System.out.println("Do get method called for preivious result diplayer :");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
		  PrintWriter writer = response.getWriter();
	      response.setContentType("application/json");
		
		String tocheckfor = (String) request.getSession().getAttribute("Email");
		//System.out.println("Need to find history for :"+tocheckfor);
		DbCommands dbcommandsobj = new DbCommands();
		  JSONArray json = new JSONArray();
		  
	    List<newdsprocessing> results =	dbcommandsobj.gethistorytableobject(tocheckfor);
	    try
	    {
	    for (newdsprocessing newdsprocessing : results) 
	    {
	    	 JSONObject jsono = new JSONObject();
             jsono.put("Filename", newdsprocessing.getRealname());
             jsono.put("Searched against", newdsprocessing.getSpeciesearchedagainst());
             jsono.put("Status",newdsprocessing.getStatus());
             jsono.put("Submission time",newdsprocessing.getEnterytime());
             if(newdsprocessing.getStatus().equals("Execution Success"))
            		 {
            	 		jsono.put("Output",newdsprocessing.getnewfilename()+".out.txt");
            		 }
             else
             {
            	 jsono.put("Output","None");
             }
             json.put(jsono);	
		}
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    //System.out.println("Result values are :"+results);
	  
	    writer.write(json.toString());
	    writer.close();
	    
	    
	}

}
