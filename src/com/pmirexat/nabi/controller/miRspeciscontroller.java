package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.*;

import com.pmirexat.nabi.model.miRspeciesloader;

/**
 * Servlet implementation class miRspeciscontroller
 */
@WebServlet("/miRspeciscontroller")
public class miRspeciscontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public miRspeciscontroller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		List<String> resultlist = miRspeciesloader.getmirspecies();
	 
		  PrintWriter writer = response.getWriter();
	      response.setContentType("application/json");
	      
	      JSONArray jsonarr = new JSONArray();
		
	      JSONObject jsobj = null;
	      
	      System.out.println("Returned :"+resultlist);
	      
	for (Iterator iterator = resultlist.iterator(); iterator.hasNext();)
	{
		String string = (String) iterator.next();
		StringTokenizer st = new StringTokenizer(string);
		
		String speciesname = "";
		String speciesval = "";	
		boolean firstpartcutter = false;		
		 jsobj = new JSONObject();		
		while (st.hasMoreElements()) 
		{	
			if (firstpartcutter)
			{
			    	speciesname=speciesname+" "+st.nextToken();
			}		
			else
			{
			    speciesval = st.nextToken();
			    firstpartcutter=true;
			}	
		}
		
		try {
			jsobj.put("speciesname", speciesname);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			jsobj.put("specieval", speciesval);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jsonarr.put(jsobj);
	}
	

	
	writer.write(jsonarr.toString());
    writer.close();
    
	}

}
