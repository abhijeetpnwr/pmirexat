package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class tissuespecific_shannonmaize
 */
@WebServlet("/tissuespecific_shannonmaize")
public class tissuespecific_shannonmaize extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tissuespecific_shannonmaize() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		System.out.println("Get method is called");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Post method is called");
		
	    String received_tissue = request.getParameter("maizetissuelist");
	    float shannon_entropy = Float.parseFloat(request.getParameter("shannon_entropy"));
		
		System.out.println("Reached in Tissuespecific_maize"+"With tissue : "+received_tissue +" and with foldcount = "+shannon_entropy);
		
	   List<String> maizerna_list = new ApiClass().getAllMaizeMirs();
	
	   List<String> searched_micro_rnas = new ArrayList<String>();
		
	
		
	   int loopcount = 0;
	   
	   String maizernas = "";
	   
		List maizegetterlist=(List<String>)new datasetgetter().getdatasets("maize");
		
	   for (Iterator iterator = maizerna_list.iterator(); iterator.hasNext();) 
	   {
		
		   String string = (String) iterator.next();
		   
		   if(loopcount == 0)
		   {
			   maizernas = maizernas+string;
		   }
		   else
		   {
			   maizernas = maizernas + ","+string;
		   }	
		   
		   loopcount++;
	   }
	   
	   System.out.println("maizernas --- :"+maizernas);
	   
	   System.out.println("Getter list is : --"+maizegetterlist);
	   
	   List<List<String>> finalresults = new search_result_generator().searchdb_shannonentropy(maizernas,"maize", maizegetterlist, received_tissue, shannon_entropy);
	
	   
	   int forcoutnchk = 0;
	   
	 
	   searched_micro_rnas.addAll(finalresults.get(0));
	   finalresults.remove(0);    	   
		    	   
	   
	   System.out.println("Result list is : "+finalresults);
	   
	   request.setAttribute("searched_micro_rnas",searched_micro_rnas);
	   request.setAttribute("result_list",finalresults);
		  
		  RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
			
		reqdispatcher.forward(request, response);
		
		
	}

}
