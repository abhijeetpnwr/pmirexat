package com.pmirexat.nabi.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

	public class HibernateUtils {
		private static SessionFactory factory;
		private Session session;
		static
		 {
			System.out.println("in static init block");
			// create hib config inst --- empty & load the same from hib.cfg.xml
			
			Configuration cfg = new Configuration().configure();
			// create service registry inst
			ServiceRegistry service = new ServiceRegistryBuilder().applySettings(
					cfg.getProperties()).buildServiceRegistry();
			factory=cfg.buildSessionFactory(service);
		 }
		
		public static SessionFactory getFactory() {
			return factory;
		}
	
		public  Session getSession() 
		{
			session=factory.openSession();
			return session;
		}
		

	}

