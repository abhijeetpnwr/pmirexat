package com.pmirexat.nabi.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.lang3.StringUtils;
import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.apache.wss4j.policy.SPConstants;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

//import com.opera.core.systems.scope.protos.SelftestProtos.SelftestResult.Result;
import com.pmirexat.nabi.model.ExecuteSystemCommands;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.class_entityconvert;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.pojos.ArabDiffPojo;
import com.pmirexat.nabi.pojos.ArabPojo;
import com.pmirexat.nabi.pojos.ArabTarget;
import com.pmirexat.nabi.pojos.MaizeDiffPojo;
import com.pmirexat.nabi.pojos.MaizePojo;
import com.pmirexat.nabi.pojos.RiceDiffPojo;
import com.pmirexat.nabi.pojos.RicePojo;
import com.pmirexat.nabi.pojos.Saltpojo;
import com.pmirexat.nabi.pojos.Seqno;
import com.pmirexat.nabi.pojos.Signuperuser;
import com.pmirexat.nabi.pojos.WheatDiffPojo;
import com.pmirexat.nabi.pojos.WheatPojo;
import com.pmirexat.nabi.pojos.newdsprocessing;
import com.pmirexat.nabi.pojos.passwordreset;
import com.pmirexat.nabi.pojos.querydb;
//import com.thoughtworks.selenium.webdriven.commands.Check;


public class DbCommands
{
	//--------to save object in db.Used for mapping excelsheet to db---------//
	Session ses;
	
    public  void saveEntry(Object objecttosave)
    {
    	 
    	try
    	{
    	
    	 ses=new HibernateUtils().getSession();
   
    	 ses.beginTransaction();
       
   	     ses.save(objecttosave);   	
      
   	     ses.getTransaction().commit();
      
    	}
    	
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	
    	finally
    	{
    		
    		if(ses != null)
    		{
    		 ses.close();
    		}
    	}
 
    }
    
    public  void updateEntry(Object objecttosave)
    {
    	 
    	try
    	{
    	
    	 ses=new HibernateUtils().getSession();
   
    	 ses.beginTransaction();
       
   	     ses.update(objecttosave);   	
      
   	     ses.getTransaction().commit();
      
    	}
    	
    	catch(Exception obj)
    	{
    		////System.out.println(" --Exception found in save object method --- ");
    	    ////System.out.println("ecception obj : "+obj.toString());
    	}
    	
    	finally
    	{
    		
    		if(ses != null)
    		{
    		 ses.close();
    		}
    	}
 
    }
    
    public void savesequence(Seqno record)
    {
    	
    	
    	try
    	{
    	//// ////System.out.println("---- chk1 -------");
    	 ses=new HibernateUtils().getSession();
    //	// ////System.out.println("---- chk2 -------");
    	 ses.beginTransaction();
       
    	 ses.save(record);   	
      
    	 ses.getTransaction().commit();
      
    	}
    	
    	catch(Exception e)
    	{
    	   ////System.out.println("Exception in saving object : "+ e.toString());	
    	}
    	
    	finally
    	{
    		if(ses != null)
    		ses.close();
    			
    	}
    	 
     
   	 // ////System.out.println("----------- done with saving object ----------");
    
    }
    
    // to save mir's data 

    
    
    // --------------------------- method ends --------------------------- //
    
    //My method to fetch sequence number for datasets
    public  Double getsequenceno(String datasetname)
    {
       String hql="SELECT E.sequence FROM Seqno E WHERE E.datasetname = :datasetname"; 

       Double seqno = 0.0;
  
       try
       {
    	  ses=new HibernateUtils().getSession();     // -- session started
       	
       	Query query = ses.createQuery(hql).setString("datasetname",datasetname);

       	
       	List<Double> resultobj= query.list();
       	
       seqno=0.0;
       	
       	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
       	{
       		seqno=(Double)iterator.next();
   		
       	}
	   } 
       catch (Exception e)
       {
		  System.out.println("Exception in getsequence no. method : "+e.toString());
	   }
       
    	finally
    	{
    		
    		if(ses != null)
    		{
    		 ses.close();   //session Closed in finally     
    		}
    	}
    	
    	
		return seqno; 	
    }
    
    public  String getaccessurlname(String miRname)
    {
    	//////System.out.println("miRname to get url is :"+miRname);
    	
    	 List<String> Wheat = new RnaGetter().getmiRlist("wheat");
    	 List<String> rice = new RnaGetter().getmiRlist("rice");
    	 List<String> maize = new RnaGetter().getmiRlist("maize");
    	 String accessurl = "";
    	 String pojoname = "";
    	 
    	 if(Wheat.contains(miRname))
    	 {
    		 pojoname = new class_entityconvert().getclassname("Wheat");
    	 }
    	 
    	 if(rice.contains(miRname))
    	 {
    		 pojoname = new class_entityconvert().getclassname("Rice");
    	 }
    	 
    	 if(maize.contains(miRname))
    	 {
    		 pojoname = new class_entityconvert().getclassname("Maize");
    	 }
    	 
    //	 ////System.out.println("Pjp name is :"+pojoname);
    	
    	 if(!(pojoname.equals("")) )
    	{
        String hql="SELECT E.accessurl FROM "+ pojoname +" E WHERE E.mir_ids = :miRname"; 
    	
    	// ////System.out.println("sequence to be set : "+sequence);
    	
        try 
        {
        	 ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("miRname",miRname);
        	
        	List<String> resultobj= query.list();
        	
        	
        	
        	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
        	{
        		accessurl=(String)iterator.next();
    			//////System.out.println("----- URL  ------"+accessurl);
        	}
        	
		} 
        catch (Exception e) 
        {
			////System.out.println("Exception in getaccessurl method : "+e.toString());
		}
        
      finally
      {
    	  if(ses != null)
    	  {
    	  ses.close();
    	  }
      }
    }
		return accessurl; 	
    }
       
    
   //-------Listener uses this method to check that whether there is already mapped data avaialable at database or not
    
    public  boolean checkDatabase(String tablename)
    {	
    	String query="select count(*) from "+tablename;	
    	System.out.println("---"+query);
    	
    	int count = 0;
    	
    	try
    	{
    		 ses=new HibernateUtils().getSession();
      	    count = ((Long)ses.createQuery(query).uniqueResult()).intValue();
           
		      System.out.println("So total value rows in " +tablename+ " are :"+count);
    	
    	}
    	
    	catch (Exception e)
    	{
			////System.out.println("Exception found in checkl database :"+e.toString());
		    e.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses != null)
    		{
    		ses.close();
    		}
    	}
  
    	if(count>0)
        {
        	return true;
        }
        
        else
        {
		    return false;	
		}
        
    }
    
    //-------------------------------------- method ends ---------------------------------//
    
    

    // Search rnas , Search categorytosearch ,getterstocall(datasetnames)
    
    public  List<List<String>>  searchDatabase(String Searchattributes,String categorytosearch, List<String>  gettertocall) 
    {
    	
    	System.out.println(" !!!!!!!!!!!!!! search attributes !!!!!!!!!!!!!! "+Searchattributes);
    	
    	
 	// ////System.out.println("================================== Reached in Dao Searchdatabase Method  =================================================");
    	 Method methodtoinvoke;
    	 
    	 //System.out.println("Search attributes :"+Searchattributes);
    	
    	 
    	 
    	 String filename="";
    	 String towrite="";
    	 
    	 towrite=towrite+"Dataset Name \t"+Searchattributes.replace(",","\t");
    	 
    	 List<List<String>> resultlist = new ArrayList<List<String>>();	
    	 List<List<HashMap<String, List<Integer>>>> finalresuultslists=new ArrayList<List<HashMap<String,List<Integer>>>>();
    	 
    	 List<Integer> innerlistintgvalue;
    	 
    	 List<String> getters = gettertocall ;  	 
    	 try
    	 {
    	 
    	 ses = new HibernateUtils().getSession();

		StringTokenizer st2 = new StringTokenizer(Searchattributes,",");

		String	hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids in ("; 	
	    
		String fieldvals = "(mir_ids";
		
		
		while (st2.hasMoreElements())
		{		    
			//System.out.println("------ Inside while loop ---------- ");
			
		    String rnatosearch = st2.nextElement().toString();
		    hql = hql+"'"+rnatosearch+"'"+",";
          
		    if (!fieldvals.equals("(") )
		    {
		    	 fieldvals = fieldvals+","+"'"+rnatosearch+"'";
			}
		    else
		    {
		    	fieldvals =  fieldvals+"'"+rnatosearch+"'";
		    } 
	    } 
		
		System.out.println("RNA's to search :"+fieldvals);
		
		
		fieldvals = fieldvals+")";
		
		hql = hql.substring(0,hql.length()-1)+")" + " ORDER BY FIELD"+fieldvals;
		
		////System.out.println(" -----  field vals are ------ :"+fieldvals);
		
		System.out.println("Final hql string is :"+hql);
		
		
		
		
		  Query query = ses.createQuery(hql);
			
		  List returnedresult = query.list();
		  int dataset_length = getters.size();
		  
		  for(int datasetcount = 0 ;datasetcount<dataset_length;datasetcount++)
		  {		
			  List<String> rowvals = new ArrayList<String>();
			   rowvals.add(getters.get(datasetcount));
			  for (Object object : returnedresult) //Returned objects beign iterated 
			   {
					  methodtoinvoke =  object.getClass().getMethod("get"+getters.get(datasetcount)); 
					  rowvals.add(String.valueOf(((Integer)methodtoinvoke.invoke(object))));
		       }//Inner for ends

			  String tabbedline = StringUtils.join(rowvals, "\t");
	       	     towrite=towrite+"\n"+tabbedline;
	       	     
			  resultlist.add(rowvals);
		  } //outer for ends
		  
		  
		  
		  //System.out.println(resultlist);
		  
		  PrintWriter writer;
			 
			 Properties prop = new Properties();
			 InputStream input = null;
			 String path="";
			 
			

		     Random randomGenerator = new Random();
		     int rand = randomGenerator.nextInt();

				// get the property value and print it out
				//System.out.println(prop.getProperty("database"));
				
			
		  
    	 } 
    	
    	catch(Exception e)
    	{
    		////System.out.println("Exception found");
    		e.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses != null)
    		{
    				ses.close();
    		}
    	}
		
    	 
    		List<String> tempfilename = new ArrayList<>();

    		
    		
    		
    		//Name of temporary created csv file will be required to generate morphesus utility.So added it as last element
    		//resultlist.add(tempfilename); 
    		
    		System.out.println("Result list :"+resultlist);
    		
    		
		return  resultlist;

	}
    
    
    
    
    public List<String> tissuespecific(String Searchattributes,String categorytosearch, List<String>  gettertocall,String tissue_tochk,float foldcheck)
    {
    	ses = new HibernateUtils().getSession();

   	 
		StringTokenizer st2 = new StringTokenizer(Searchattributes,",");

  
	    String	hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids in ("; 	
	    
	    List<String> header = new ArrayList<String>();
	    header.add("Searched Dataset");
	    
		while (st2.hasMoreElements())
		{		    
			 
		    String rnatosearch = st2.nextElement().toString();
		    
		    //System.out.println("Rna to search : "+rnatosearch);

		    hql = hql+"'"+rnatosearch+"'"+",";
		    
	    } //While ends
	

		hql = hql.substring(0,hql.length()-1)+")"+ " and "+tissue_tochk +" > "+foldcheck; 
		//System.out.println("Final hql string is : "+hql);
		
		  Query query = ses.createQuery(hql);
			
		  List returnedresult = query.list();
		return returnedresult;
    }
    
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    
    public  List<List<String>> searchDatabase_byfolchange(String Searchattributes,String categorytosearch, List<String>  gettertocall,String tissue_tochk,float foldcheck) 
    {
    	 Method methodtoinvoke;
     	
    	 List<List<String>> resultlist = new ArrayList<List<String>>();	
    	 List<List<Map<String, List<Integer>>>> finalresuultslists=new ArrayList<List<Map<String,List<Integer>>>>();
    	 
    	 List<Integer> innerlistintgvalue;
    	 
    	 List<String> getters = gettertocall ;  	 
    	 try
    	 {
    	 
    	 ses = new HibernateUtils().getSession();

    	 //System.out.println("Search attributes :"+Searchattributes);
    	 
		StringTokenizer st2 = new StringTokenizer(Searchattributes,",");

   
	    String	hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids in ("; 	
	    
	    List<String> header = new ArrayList<String>();
	    header.add("Searched Dataset");
	    
		while (st2.hasMoreElements())
		{		    
			 
		    String rnatosearch = st2.nextElement().toString();
		    
		    //System.out.println("Rna to search : "+rnatosearch);

		    hql = hql+"'"+rnatosearch+"'"+",";
		    
	    } //While ends
	
		
		System.out.println("");

		hql = hql.substring(0,hql.length()-1)+")"+ " and "+tissue_tochk +" > "+foldcheck; 
		//System.out.println("Final hql string is : "+hql);
		
		
		System.out.println( " query to fire is :"+hql);
		
		  Query query = ses.createQuery(hql);
			
		  List returnedresult = query.list();
		  	  
		  int dataset_length = getters.size();
		  
		 
		  // for fetching headers 
		  
		  for (Object object : returnedresult) //Returned objects beign iterated 
		   {
			  methodtoinvoke =  object.getClass().getMethod("getMir_ids");
		      header.add((String)methodtoinvoke.invoke(object));
	       }//Inne
		  
	      resultlist.add(header);
		  
		  
		  for(int datasetcount = 0 ;datasetcount<dataset_length;datasetcount++)
		  {		
			  List<String> rowvals = new ArrayList<String>();
			   rowvals.add(getters.get(datasetcount));
			  for (Object object : returnedresult) //Returned objects beign iterated 
			   {
				    
				   
					  methodtoinvoke =  object.getClass().getMethod("get"+getters.get(datasetcount)); 
					  rowvals.add(String.valueOf(((Integer)methodtoinvoke.invoke(object))));
		       }//Inner for ends

			  resultlist.add(rowvals);
		  } //outer for ends
		  
		  
		  
		  //System.out.println(resultlist);
		  
    	 } 
    	
    	catch(Exception e)
    	{
    		////System.out.println("Exception found");
    		e.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses != null)
    		{
    				ses.close();
    		}
    	}
		
		return  resultlist;

	}
    
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------//      
    
    // ---------------------------------------------  Search by shannon entropy ---------------------------------------//
    
    
    public  List<List<String>>  searchDatabase_byshannnentropy(String Searchattributes,String categorytosearch, List<String>  gettertocall,String tissue_tochk,float  Shannon_Entropy) 
    {
   	 Method methodtoinvoke;
  	
   
	 List<List<String>> resultlist = new ArrayList<List<String>>();	
	 List<List<Map<String, List<Integer>>>> finalresuultslists=new ArrayList<List<Map<String,List<Integer>>>>();
	 
	 List<Integer> innerlistintgvalue;
	 
	 List<String> getters = gettertocall ;  	 
	 try
	 {
	 
	 ses = new HibernateUtils().getSession();

	StringTokenizer st2 = new StringTokenizer(Searchattributes,",");


	
	
	
	
    
    String	hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids in ("; 	
    
	while (st2.hasMoreElements())
	{		    
		 
	    String rnatosearch = st2.nextElement().toString();
	    hql = hql+"'"+rnatosearch+"'"+",";
	    
    } //While ends
	
	
	hql = hql.substring(0,hql.length()-1)+")"+ " and Shannon_Entropy<="+Shannon_Entropy; 
	
	//System.out.println("Generated String for it is:"+hql);
	
	  Query query = ses.createQuery(hql);
		
	  List returnedresult = query.list();
	  	  
	  int dataset_length = getters.size();
	  
	  List<String> header = new ArrayList<String>();
	  header.add("Searched Dataset");
	 
	  for (Object object : returnedresult) //Returned objects beign iterated 
	   {
		  methodtoinvoke =  object.getClass().getMethod("getMir_ids");
	      header.add((String)methodtoinvoke.invoke(object));
      }
	  
	  resultlist.add(header);
	  
	  for(int datasetcount = 0 ;datasetcount<dataset_length;datasetcount++)
	  {		
		  List<String> rowvals = new ArrayList<String>();
		   rowvals.add(getters.get(datasetcount));
		  for (Object object : returnedresult) //Returned objects beign iterated 
		   {
				  methodtoinvoke =  object.getClass().getMethod("get"+getters.get(datasetcount)); 
				  rowvals.add(String.valueOf(((Integer)methodtoinvoke.invoke(object))));
	       }//Inner for ends

		  resultlist.add(rowvals);
	  } //outer for ends
	  
	  List<String> shannonlist = new ArrayList<String>();
	  shannonlist.add("Value");
	  for (Object object : returnedresult) //Returned objects beign iterated 
	   {
		  methodtoinvoke =  object.getClass().getMethod("getShannon_Entropy");
	      shannonlist.add(Float.toString((Float)methodtoinvoke.invoke(object)));
     }
	  
	  resultlist.add(shannonlist);
	  
	  //System.out.println(resultlist);
	  
	 } 
	
	catch(Exception e)
	{
		////System.out.println("Exception found");
		e.printStackTrace();
	}
	
	finally
	{
		if(ses != null)
		{
				ses.close();
		}
	}
	
	return  resultlist;


	}
    
    
    
    // --------------------------------------------------------------------------------------------------------------- //
    
   
    //Listener calls it get all column values so it can generate array list for category based getters.
    public  ArrayList<String>  getTableDesc(String tableName) 
    {
      //  // ////System.out.println("getFieldNames:start" + tableName);
        Object[] a;
        List<Object[]> fieldNames = new ArrayList<Object[]>();
        ArrayList<String> tabFieldNames = new ArrayList<String>();
        try {
        	 ses = new HibernateUtils().getSession();
       
            String queryStr = "DESCRIBE "  + tableName;                 
            fieldNames = (List<Object[]>) ses.createSQLQuery(queryStr).list();
           
            for (int i = 0; i < fieldNames.size(); i++) 
            {
                a = fieldNames.get(i);
                tabFieldNames.add(a[0].toString());
            }
            
        } 
        catch (Exception e) 
          {
        	
        	////System.out.println("Exception found in gettabledesc method");
            e.printStackTrace();
            // ////System.out.println("exception " + e);
        } finally
        {
        	if(ses != null)
        	{
        		ses.close();
        	}
        }
        // ////System.out.println("getFieldNames:end" + tabFieldNames.toString());
        return tabFieldNames;
    }  
    
    
    public  ArrayList<String>  getuniquecols(String tableName,String columnnamee) 
    {
      //  // ////System.out.println("getFieldNames:start" + tableName);
        Object[] a;
        List<Object[]> fieldNames = new ArrayList<Object[]>();
        ArrayList<String> tabFieldNames = new ArrayList<String>();
        try {
        	 ses = new HibernateUtils().getSession();
       
            String queryStr = "select distinct "  +columnnamee +" from "+tableName;                 
            tabFieldNames = (ArrayList<String>) ses.createSQLQuery(queryStr).list();
           
//            for (int i = 0; i < fieldNames.size(); i++) 
//            {
//                a = fieldNames.get(i);
//                tabFieldNames.add(a[0].toString());
//            }
            
        } 
        catch (Exception e) 
          {
        	
        	////System.out.println("Exception found in gettabledesc method");
            e.printStackTrace();
            // ////System.out.println("exception " + e);
        } finally
        {
        	if(ses != null)
        	{
        		ses.close();
        	}
        }
        // ////System.out.println("getFieldNames:end" + tabFieldNames.toString());
        return tabFieldNames;
    } 
    
    
  /*Method for obtaining values of 1st column of a table.Will be used for autocomplete box
   * Returns a List of string which contains mature_rnas ,when called fom listners.
   * 
  */
    
    public  List<String>  get_mature_microrna(String pojoclass) 
    {
    	////System.out.println("---reached  get matured rna method------");
    	
       	String hql = "SELECT E.mir_ids FROM " +pojoclass+ " E where E.mir_ids is not null";
       	// ////System.out.println("---reached  get matured rna method 1------");
    	
        List<String> results = null;
       	try 
       	{
       		ses = new HibernateUtils().getSession();
           	
       		////System.out.println("Value of ses in get mature rna "+ses);
       		
           	Query query = ses.createQuery(hql);
           	
        	 results = query.list();
        	
        	 ////System.out.println("Result from get_matureRna");
		} 
       	catch (Exception e)
       	{
			////System.out.println("Exception in get_mature_micrornas");
			e.printStackTrace();
		}
       	
       	
       	finally
       	{
      
       	 if((ses != null))
       	 {
       		 ses.close();
       	 }
       	}
  
       	return (List<String>) results;
    }  
    
    
    /*
        This method will search for specific dataset values in the table   
     */
    
    
    public  List<String>  get_sequences(String pojoclass) 
    {
    	////System.out.println("---reached  get matured rna method------");
    	
       	String hql = "SELECT E.sequence FROM " +pojoclass+ " E where E.sequence is not null";
       	// ////System.out.println("---reached  get matured rna method 1------");
    	
        List<String> results = null;
       	try 
       	{
       		ses = new HibernateUtils().getSession();
           	
       		////System.out.println("Value of ses in get mature rna "+ses);
       		
           	Query query = ses.createQuery(hql);
           	
        	 results = query.list();
        	
        	 ////System.out.println("Result from get_matureRna");
        	 
		} 
       	catch (Exception e)
       	{
			////System.out.println("Exception in get_mature_micrornas");
			e.printStackTrace();
		}
       	
       	
       	finally
       	{
      
       	 if((ses != null))
       	 {
       		 ses.close();
       	 }
       	}
  
       	return (List<String>) results;
    }  
    
    
    public List<List<String>> Specfic_ds_search0(String microrna_tosearch,String datasets_tosearch,String pojoname)
    {
    	
    	StringTokenizer rna_tokenizer = new StringTokenizer(microrna_tosearch,",");
    	
    	StringTokenizer datasetTokenizer;
 
    	String query_part1="SELECT ";
    	
    	String query_part2=null;
    	
    	List<List<String>> specific_ds_resultList=new ArrayList<List<String>>();
    
    	int multirna_check = 0;
    	
   	    while (rna_tokenizer.hasMoreElements())
    	{ 	
   	     	
   	    	
   		int first_status = 0;  		
   		query_part2 = "";	
   		datasetTokenizer = new StringTokenizer(datasets_tosearch,",");
   		
    		while(datasetTokenizer.hasMoreElements())
        	{
        	if(first_status<1)
       		{        			
       			query_part2=query_part2+(String) datasetTokenizer.nextElement();
       			first_status++;
       		}
     		
       		else
       		{
       			    multirna_check++;
        			query_part2=query_part2+","+(String) datasetTokenizer.nextElement();		
       		}
       	}
       			
        	String query_part3=" FROM " +pojoname + " WHERE mir_ids = :name";  //xxx
			
        	String hql=query_part1+query_part2+query_part3;
       	
        	String topass=rna_tokenizer.nextToken();
        	
        	try
        	{
        	
        	ses=new HibernateUtils().getSession();
        	
           	Query query = ses.createQuery(hql).setString("name",topass);
        	 
        	 List<String> row_list=new ArrayList<String>();

        	 if(multirna_check > 0) 	 
        	 {	 
       
        	//	////System.out.println(" ------- More then one values were searched ,should return array of objects ");
        	
        		 List<Object[]> resulobj= (List<Object[]>)query.list();
       	
        		 
        		 for (Object[] objects : resulobj)
        		 {
				 
        	        for (Object object : objects)
				    {

					     row_list.add(((Integer)object).toString()); //object first casted to integer and then typecasted to String type
				
			    	 }
			    	 specific_ds_resultList.add(row_list);
			    }
        }
         
        else
        {
        	List<Object> resulobj= query.list();
        		      for (Iterator iterator = resulobj.iterator(); iterator.hasNext();) 
    				   {
						  Object object = (Object) iterator.next();
						
						  row_list.add(((Integer)object).toString());
					   }
    			  
    				   specific_ds_resultList.add(row_list);
        }
        

        	}
        	
        	
        	catch(Exception e)
        	{
        		////System.out.println(" Exception found ");
        		e.printStackTrace();
        	}
        	
        	finally
        	{
        		if(ses != null)
        		{
        			ses.close();
        		}
        	}
       }
        

    	
   	
   	    for (List<String> list : specific_ds_resultList) 
   	    {
   	    	// ////System.out.println("");
		     for (String string : list)
		     {
			   // ////System.out.print(string);	
			 }
		}
    	
		return specific_ds_resultList;	
	}
    
    
    public String getmirnam(String sequence,String pojoname)
    {
    	String hql="SELECT E.mir_ids FROM " + pojoname +" E WHERE E.sequence = :sequence"; 
    	
    	// ////System.out.println("sequence to be set : "+sequence);
    	
    	List<String> resultobj = null ;
    	
    	try 
    	{
    		 ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("sequence",sequence);
        	
        	resultobj= query.list();
        	
        	//////System.out.println("Got mir name");
        	
		} 
    	
    	catch (Exception e) 
    	{
			 ////System.out.println("Exception in getmirname db connectivity");
		}
    	
    	finally
    	{

    		if(ses != null)
    		{
    			ses.close();
    		}
    	}
   
    	
    	
    	String mir_id="";
    	
    	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
    	{
    		if(mir_id.equals(""))
    		{
    			mir_id = (String) iterator.next();
    		}
    		else
    		{
			     mir_id=mir_id+","+(String) iterator.next();
			// ////System.out.println("mir id for it is:"+mir_id);
    		}
		} 	
    	
		return mir_id;
    }
    
    
    public querydb getfiletoprocess()
    {
    	List results = null;
    	querydb qdb = null;
    	////System.out.println("Reacehd to get filename");
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(querydb.class);
    	cr.add(Restrictions.eq("status", "Waiting for execution")).setMaxResults(1); 
    
    	 results = cr.list();
    
    	 ////System.out.println("Retuened values : "+results);
    	 
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
    	////System.out.println("REsult :"+results.toString());
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (querydb) iterator.next();
		    ////System.out.println("should be:"+qdb);	
		}
        
        if(qdb != null)
		return  qdb;
      }
    	return null;
    }
    
    
    public newdsprocessing getnewdsfiletoprocess()
    {
    	List results = null;
    	newdsprocessing qdb = null;
    
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(newdsprocessing.class);
    	cr.add(Restrictions.eq("status", "Waiting")).setMaxResults(1); 
    
    	 results = cr.list();
    
    	 ////System.out.println("Retuened values : "+results);
    	 
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (newdsprocessing) iterator.next();
		}
        
        if(qdb != null)
		return  qdb;
    	}
    	return null;
    }
    
    
    public Boolean checkrunstatus()
    {
    	List results = null;
    	querydb qdb = null;
    	
    	Boolean checkvar = false;
    	
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(querydb.class);
    	cr.add(Restrictions.eq("status", "Processing")).setMaxResults(1); 
    
    	 results = cr.list();
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}  	 	
    	if(results != null)
    	{       
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (querydb) iterator.next();
        	////System.out.println("Inside cluster check status -- Running querydb file is :"+qdb.toString());
		}    
        if(qdb != null)
        {	
        	checkvar = true;
        }        
      }  	
    	return checkvar;
    }
    
    
    public Boolean checkrunstatusfornewds()
    {
    	List results = null;
    	newdsprocessing qdb = null;
    	
    	Boolean checkvar = false;
    	
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(newdsprocessing.class);
    	cr.add(Restrictions.eq("status", "Processing")).setMaxResults(1); 
    
    	 results = cr.list();
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
    
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (newdsprocessing) iterator.next();
        	////System.out.println("Inside cluster check status -- Running querydb file is :"+qdb.toString());

		}
        
        if(qdb != null)
        {	
        	checkvar = true;
        }
        
      }
    	
    	return checkvar;
    }
    
    
    
    public String getsequence(String miRid,String pojoname)
    {
    	//String hql="SELECT E.sequence FROM " + pojoname +" E WHERE E.sequence = :sequence"; 
    	
    	String hql="SELECT E.sequence FROM " + pojoname +" E WHERE E.mir_ids = :mir_ids"; 
    	// ////System.out.println("sequence to be set : "+sequence);
    	
    	String sequence="";
    	try 
    	{ 		
    		ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("mir_ids",miRid);
        	

        	
        	List<String> resultobj= query.list();
        	
        	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
        	{
        		if(sequence.equals(""))
        		{
        			sequence = (String) iterator.next();
        		}
        		else
        		{
        			sequence=sequence+","+(String) iterator.next();
    		
        		}
    		} 	
		} 
    	catch (Exception e)
    	{
			////System.out.println("Exception found in get sequence");
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
		return sequence;
    }
    
    
    //-------------------------------------------------------------
    
    
    public List<Object> getdetail(String miRid,String pojoname)
    {
    	//String hql="SELECT E.sequence FROM " + pojoname +" E WHERE E.sequence = :sequence"; 
    	
    	String hql="FROM " + pojoname +" WHERE miRNA_Acc = :mir_ids"; 
    
    	String sequence="";
    	try 
    	{ 		
    		
    		ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("mir_ids",miRid);
        	
        	List<Object> resultobj= query.list();
        	
        	System.out.println("Query list :"+resultobj);
        	
        	return resultobj;
		} 
    	catch (Exception e)
    	{
			////System.out.println("Exception found in get sequence");
    		e.printStackTrace();
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
		return null;
    }
    
    
    
    
    //--------------------------------------------------------------
    
    
    public void updatequerydb(String filekey,Date endtime ,String status)
    {
    	 ses =  new HibernateUtils().getSession();
    	
    	ses.beginTransaction();
    	////System.out.println(" ----------------------------------------------- ------------------------------- In Upadte query -- file_key is :"+filekey );
    	
    
    	 
    	try
    	{
    	
    	Criteria cr = ses.createCriteria(querydb.class);
    	Criteria crt =  cr.add(Restrictions.eq("file_key",filekey)).setMaxResults(1); 
        List<querydb> qdb = crt.list();
        
        querydb querydb = null;
        for (Iterator iterator = qdb.iterator(); iterator.hasNext();) 
        {
			 querydb = (querydb) iterator.next();	
			 ////System.out.println("In for loop , Query object check check : "+querydb.getFile_key());
		}
        
        ////System.out.println("Querydb object received is : "+querydb.toString());
       
        querydb.setStatus(status);
        
        querydb.setEndtime(endtime);
      
        
        if(ses == null)
        {
        	////System.out.println("Value of session is null");
        }
        
        else
        {
        	////System.out.println("Session valie is not null");
        }
        
        ses.update(querydb);
  
        ses.getTransaction().commit();
        
        ////System.out.println("In DB saved");
        
    	}
    	catch(Exception obj)
    	{
    		////System.out.println("Exception in it");
    		obj.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses!=null)
    		{
    			ses.close();
    		}
    	}
    }
    
    
    
    public querydb getfilestatus(String file_key)
    {
    	List results = null;
    	querydb qdb = null;
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(querydb.class);
    	cr.add(Restrictions.eq("file_key", file_key)).setMaxResults(1); 
    	 results = cr.list();
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
    	////System.out.println("REsult :"+results.toString());
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (querydb) iterator.next();
		    ////System.out.println("should be:"+qdb);	
		}
        
        if(qdb != null)
		return  qdb;
      }
    	
    	////System.out.println("Reached so far means, no results were found, I will return null");
    	return null;
    }
    
    
    public Saltpojo getsalttableobject(String email)
    {   	
    	Saltpojo saltpojo = null;
    	try 
    	{ 			
    		ses=new HibernateUtils().getSession();
    		Criteria cr = ses.createCriteria(Saltpojo.class);
    		
    		//System.out.println("Received email address is :"+email);
        	cr.add(Restrictions.eq("emailid", email));

        	List<Saltpojo> results = cr.list();

         
        	for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        	{
				saltpojo = (Saltpojo) iterator.next();	
			}
        	
		} 
    	catch (Exception e)
    	{
		e.printStackTrace();
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
    	return saltpojo;
    	
    }
    
    
    public List<newdsprocessing> gethistorytableobject(String email)
    {   	
    	List<newdsprocessing> list_result = new ArrayList<newdsprocessing>();
    	
    	try 
    	{ 			
    		ses=new HibernateUtils().getSession();
    		Criteria cr = ses.createCriteria(newdsprocessing.class);
    		
    		//System.out.println("Received email address is :"+email);
        	cr.add(Restrictions.eq("owneremail", email)); 

        	List<newdsprocessing> results = cr.list();
        	
        	////System.out.println("// ----- check check check ----");
        	////System.out.println("//Reswults object :"+results);
         
        	for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        	{
        		newdsprocessing dsentry = (newdsprocessing) iterator.next();
				list_result.add(dsentry);
			}
        	
		} 
    	catch (Exception e)
    	{
		e.printStackTrace();
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
    	return list_result;
    	
    }
    
    
    
    public Signuperuser getuser(String email)
    {   	
       Signuperuser su = null;
    	try 
    	{ 			
    		ses=new HibernateUtils().getSession();
    		Criteria cr = ses.createCriteria(Signuperuser.class);
        	cr.add(Restrictions.eq("email", email));

        	List<Signuperuser> results = cr.list();
         
        	for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        	{
				 su = (Signuperuser) iterator.next();
			}
		} 
    	catch (Exception e)
    	{
			////System.out.println("Exception found in get sequence");
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
    	return su;
    	
    }
    
    public List<Signuperuser> usersawaitingadminverification()
    {   	
       Signuperuser su = null;
       List<Signuperuser> waitingusers = new ArrayList<>();
    	try 
    	{ 			
    		ses=new HibernateUtils().getSession();
    		Criteria cr = ses.createCriteria(Signuperuser.class);
        	cr.add(Restrictions.eq("verificationstatus", "Needs admin approval"));

        	List<Signuperuser> results = cr.list();
         
        	for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        	{
				 su = (Signuperuser) iterator.next();
				 waitingusers.add(su);
			}
		} 
    	catch (Exception e)
    	{
			////System.out.println("Exception found in get sequence");
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
    	return waitingusers;
    	
    }
    
    public passwordreset getpasswordresetobject(String email)
    {   	
    	passwordreset su = null;
    	try 
    	{ 			
    		ses=new HibernateUtils().getSession();
    		Criteria cr = ses.createCriteria(passwordreset.class);
        	cr.add(Restrictions.eq("emailid", email));

        	List<passwordreset> results = cr.list();
         
        	for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        	{
				 su = (passwordreset) iterator.next();
			}
		} 
    	catch (Exception e)
    	{
			////System.out.println("Exception found in get sequence");
    		e.printStackTrace();
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}  	
    	return su;   	
    }  
    
    
    
    
    public  List  getdiffresult(String species,String dataset1,String dataset2) 
    {
    	////System.out.println("---reached  get matured rna method------");
    	
       //	String hql = "SELECT from " +pojoclass+ " where duataset1="+dataset1+" AND dataset2="+dataset2;
    	
    	System.out.println("Species entered :"+species);
    	
    	List results = new ArrayList<>();
    	
    	if(species.equals("Arabidopsis"))
    	{
//    		String command = "/satamas/anoop/Arabidopsis/Exp_Matrix/matrix/edgeR_DE/DE.sh	"+dataset1+"	"+dataset2;
    		
    		CommandLine command = new CommandLine("/bin/sh");
            command.addArguments(new String[] {
           "/satamas/anoop/Arabidopsis/Exp_Matrix/matrix/edgeR_DE/DE.sh",dataset1,dataset2},false);
    		
    		String result = new ExecuteSystemCommands().ExecuteScript(command);	
    		
    		
    		System.out.println("Commadn to send :"+command.toString());
    		
    		try {
    			System.out.println("Going for a sleep :) ");
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		System.out.println(" RESULT for ARABIDPSIS ----");
    		
    		System.out.println(" ----------------- "+result);
    		
    		System.out.println("###################################################");
    		 String textStr[] = result.split("\\r\\n|\\n|\\r");
    		
    		 int linecount = 0;
    		 
    		 for(String st: textStr)
    		 {
    			 System.out.println("Linecount : "+linecount);
    			 if(linecount>2)
    			 {
    					String[] vals = st.split("\\s+");
    					
    					List<String> dic = new ArrayList<String>();
    					
    					for(String s : vals) {
    					       if(s != null && s.length() > 0) {
    					          dic.add(s);
    					       }
    					    }
    					
    					
    					System.out.println(" --- Dict --"+dic);
    					
    					results.add(new ArabDiffPojo(dataset1, dataset2, dic.get(0), Float.parseFloat(dic.get(1)), Float.parseFloat(dic.get(2)), Float.parseFloat(dic.get(3)), Float.parseFloat(dic.get(4))));
    					 
    			 }
    			 
    			 linecount++;
    			 
    		 }
    		 
    		 System.out.println("Resulttt ---"+results);
    	}
    	
    	else
    	{
    	
    	Criteria cr=null;
       	
    	ses = new HibernateUtils().getSession();
    	
    	if(species.equals("wheat"))
    	{
    		 cr = ses.createCriteria(WheatDiffPojo.class);
    	}
    	
    	if(species.equals("maize"))
    	{
    		 cr = ses.createCriteria(MaizeDiffPojo.class);
    	}
    	
    	if(species.equals("rice"))
    	{
    		 cr = ses.createCriteria(RiceDiffPojo.class);
    	}
    	
    	
    	cr.add(Restrictions.eq("Dataset1",dataset1)).add(Restrictions.eq("Dataset2",dataset2));
    	
    	results = cr.list();
    
    	}
    	
    	System.out.println("Result list is : "+results);
    	return results;
    }  
}
    
    




