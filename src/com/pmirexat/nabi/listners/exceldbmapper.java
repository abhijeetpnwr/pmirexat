package com.pmirexat.nabi.listners;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.Exceltodbmapper;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.SendEmailWithAttachments;
import com.pmirexat.nabi.model.crossspecies_resultgen;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.scriptexecuter;
import com.pmirexat.nabi.model.search_result_generator;
import com.pmirexat.nabi.pojos.newdsprocessing;
import com.pmirexat.nabi.pojos.querydb;

@WebListener
public class exceldbmapper implements ServletContextListener {

    public exceldbmapper()
    {
           // TODO Auto-generated constructor stub
    }
 
    
    //So this method will run when servlet will be initialized
    public void contextInitialized(final ServletContextEvent event)
    { 

    	final ServletContext sc=event.getServletContext();
         	  
            try 
        		{  	
            	           Runnable dbchecks = new Runnable() 
            	           {
							@Override
								public void run() 
									{
										// TODO Auto-generated method stub
								   
								       while(true)
								       {   			    	   
								    	   Boolean runcheck = true;
								    	   querydb qdbobject = null;
								    	   
								    	   try {
												Thread.sleep(600000); //runs in every 30 seconds
												
											} catch (InterruptedException e3) 
									        {
												
												e3.printStackTrace();
											}
								    	   	 
								    		  
								    	   runcheck =  new DbCommands().checkrunstatus();
								    		   
								    		  
								    		   
								    		   if(runcheck) //if true,means running
								    		   {
								    			   //	  //System.out.println(" ------- Something else is being processed, So will wait for it to complete ----");								
								    		   }
								    		   
								    		   
								    		   else
								    		   {
								    			  // //System.out.println("//Cluster should be free to run script");								    			   
													    			   
								    			     qdbobject =  new DbCommands().getfiletoprocess();
								    			   
								    				  if(qdbobject != null)
										    		  {	
										    		  
										    		 // //System.out.println(" ------- To run :----"+qdbobject.toString());								
										    		  
										    		  scriptexecuter scriptexce = new scriptexecuter();
										  
										    		  String checkvar="Execution Error";					
										    		
										    		  new DbCommands().updatequerydb(qdbobject.getFile_key(),null,"Processing");
										    		  
										    		  try
										    		  {
										    			//  //System.out.println("Now eill execute script on cluster");
										    			  
										    			  checkvar = scriptexce.testrun("/satamas/PmiRExAtInputs/"+qdbobject.getFile_key()+".fasta", qdbobject.getFile_key()+"_output" , qdbobject.getPident() , qdbobject.getMismatch(), qdbobject.getQuerycoverage());
										    		  }
										    		  
										    		  catch(Exception e)
										    		  {
										    			  //System.out.println("Error in executing script");
										    			  e.printStackTrace();  
										    		  }
										    		  
										    		  Date currenttime = Calendar.getInstance().getTime();
										    		  
										    		  new DbCommands().updatequerydb(qdbobject.getFile_key(),currenttime,checkvar);
										    		  
										    		  SendEmailWithAttachments scobj = new SendEmailWithAttachments();
				                        		    	
				                        		    	try 
				                        		    	{
				                        					 scobj.sendEmail(qdbobject.getEmail(), "Pmirexquery REsults", "Please find your result file attached with this email", "/satamas/pmiRexatresults/"+qdbobject.getFile_key()+"_output");
				                        				   
				                        		    	} 
				                        		    	
				                        		    	catch (MessagingException e1) 
				                        		    	{
				                        					// TODO Auto-generated catch block
				                        					e1.printStackTrace();
				                        				}
				                        		    	//	//System.out.println(" ------------------------------- This thread is done with its  work  ----------------------------------------------");
									    		      }
								    				  
								    				  else
								    				  {
								    					//  //System.out.println(" --- No  query work on ---");
								 
								    				  }
								    		   }
								       }
								
									}
						    };
						    
						    //Thread for keep maintaing new Ds search functionality
						    
						    Runnable newdscheckrunnable = new Runnable() 
	            	           {
								@Override
									public void run() 
										{
											// TODO Auto-generated method stub
									   
									       while(true)
									       {   
									  		 
									    	
									    	   Boolean runcheck = true;
									    	   newdsprocessing qdbobject = null;
									    	   
									    	   try {
									    		  
													Thread.sleep(300000); //5 minutes
													
												}
									    	   catch (InterruptedException e3) 
										        {
													e3.printStackTrace();
												}
									    	   	  
									    		  
									    	   runcheck =  new DbCommands().checkrunstatusfornewds();
									    	   
									    	   if(!runcheck)
									    		   {
										    			   
														    			   
									    			     qdbobject =  new DbCommands().getnewdsfiletoprocess();
									    			   
									    				  if(qdbobject != null)
											    		  {	
											    		  						
											    		  
											    		  scriptexecuter scriptexce = new scriptexecuter();
											  
											    		  String checkvar="";					
											    		
											    		  String sraname = qdbobject.getRealname();
											    		  System.out.println("Srnaname is :"+sraname);
											    		  
											    	
											    		  
											    		 String[] parts = sraname.split("\\.");
											    		  
											    		  sraname = parts[0];
											    		  System.out.println("SRA name is :"+sraname);
											    		  qdbobject.setStatus("Processing");
											    		  new DbCommands().updateEntry(qdbobject);
											    		  
											    		  try
											    		  {		
											    			  String speciesforscript = qdbobject.getSpeciesearchedagainst().split("@")[0];
											
											    			checkvar = scriptexce.runnewdsscript(qdbobject.getnewfilename(), qdbobject.getnewfilename()+".out",speciesforscript,sraname);
											    		  }
											    		  
											    		  catch(Exception e)
											    		  {
											    			  //System.out.println("Error in executing script");
											    			  e.printStackTrace();  
											    		  }
											    		  
											    		  System.out.println("--- Processing done ---- ");
											    		  
											    		  Date currenttime = Calendar.getInstance().getTime();
											    		  
											    		  qdbobject.setCompletiontime(currenttime);
											    		  qdbobject.setStatus(checkvar);
											    		  
											    		 new DbCommands().updateEntry(qdbobject);
											    	
			    }						
									    		   }
									       }
									
										}
							    };
            	
                        	Runnable basic = new Runnable() 
                        	{
                        		public void run()
                        		{
                        			try
                        			{              
                        				//	//System.out.println(" ---- Excel to db mapper should be called ----");
                        				  if(!(new DbCommands().checkDatabase("MaizePojo")))
                                          {
                        					  new Exceltodbmapper().Readexcel("inproject","maize.xlsx");
                                          }
                        				  
                        				  if(!(new DbCommands().checkDatabase("WheatPojo")))
                        				  {
                        				     new Exceltodbmapper().Readexcel("inproject","wheat.xlsx");	     	                      				
                        				  }
                        				  
                        				  if(!(new DbCommands().checkDatabase("RicePojo")))
                        				  {
                        				      new Exceltodbmapper().Readexcel("inproject","rice.xlsx");
                        				  }
                        				
                        				  if(!(new DbCommands().checkDatabase("ArabPojo")))
                        				  {
                        					  new Exceltodbmapper().Readexcel("inproject","arab.xlsx");
                        				  }
                        				  if(!(new DbCommands().checkDatabase("Seqno")))
                        				  {
                        				   new Exceltodbmapper().Readexcel2("inproject","sequencefile.xlsx");
                        				  }
                        				  
                        				 new datasetgetter().getdatasets("wheat");
                    		             new datasetgetter().getdatasets("rice");
                    		             new datasetgetter().getdatasets("maize");
                    		             new datasetgetter().getdatasets("arab");
                    		             
                    		             
                    		             
                    		             new RnaGetter().getmiRlist("wheat");
                    		             new RnaGetter().getmiRlist("rice");
                    		             new RnaGetter().getmiRlist("maize");
                    		             new RnaGetter().getmiRlist("arab");
                    		             
                    		             crossspecies_resultgen crossgen = new crossspecies_resultgen();
                    		             
//                    		          
//                    		              if(!(new DbCommands().checkDatabase("WheatTarget")))
//                       				     {
//                    		            	 System.out.println(" ----- Need to do entries in wheattarget ---");
//                       				       new Exceltodbmapper().Readexcel2("inproject","sequenceexcel.xlsx");
//                       				         new Exceltodbmapper().targetmatrixmappers("inproject","wheattarget.xlsx");
//                       				     }
//                    		         
//                    		             if(!(new DbCommands().checkDatabase("MaizeTarget")))
//                       				     {
//                    		            	 System.out.println("Need to do entries in maizetarget");
//                       				         new Exceltodbmapper().Readexcel2("inproject","sequenceexcel.xlsx");
//                    		            	 new Exceltodbmapper().targetmatrixmappers("inproject","maizetarget.xlsx");
//                       				    }
//                    		             
//                    		             if(!(new DbCommands().checkDatabase("RiceTarget")))
//                       				     {
//                    		            	 System.out.println("Need to do entries in riectarget");
//                       				        new Exceltodbmapper().Readexcel2("inproject","sequenceexcel.xlsx");
//                    		            	 new Exceltodbmapper().targetmatrixmappers("inproject","ricetarget.xlsx");
//                       				    }
//                    		             
//                    		             if(!(new DbCommands().checkDatabase("ArabTarget")))
//                       				     {
//                    		            	 System.out.println("Need to do entries in arabtarget");
//                       				         new Exceltodbmapper().Readexcel2("inproject","sequenceexcel.xlsx");
//                    		            	 new Exceltodbmapper().targetmatrixmappers("inproject","arabtarget.xlsx");
//                       				    }
//                    		             
                    		           
                    		             
//                    		             if(!(new DbCommands().checkDatabase("WheatDiffPojo")))
//                       				     {
//                    		            	 System.out.println(" ----- Need to do entries in wheattarget ---");
//                       				         new Exceltodbmapper().mapwheatexcel("inproject","Wheat21db_DE_EdgeRresults.xlsx");
//                       				         Thread.sleep(2000);
//                       				     }
//                    		         
//                    		             if(!(new DbCommands().checkDatabase("MaizeDiffPojo")))
//                       				     {
//                    		            	 System.out.println(" ----- Need to do entries in Maietarget ---");
//                    		            	 new Exceltodbmapper().mapMaizeexcel("inproject","Maize43db_DE_EdgeRresults.xlsx");
//                    		            	 Thread.sleep(2000);
//                       				    }
//                    		             
//                    		             if(!(new DbCommands().checkDatabase("RiceDiffPojo")))
//                       				     {
//                    	
//                    		            	 System.out.println(" ----- Need to do entries in Ricetarget ---");
//                    		            	 Thread.sleep(2000);
//                    		            	 new Exceltodbmapper().mapRiceexcel("inproject","Rice53db_DE_EdgeRresults.xlsx");
//                       				    }
                    		             
                    		          ArrayList<String> temp = new ArrayList<String>();
                    		          
                    	          	   
                    		   		  temp.add("Wheat");
                    		   		  temp.add("maize");
                    		          
                    		          //Maintais species name and tisssues in it
                    		           Map<String, List<String>> tissuemap=  new HashMap<>();
                    		           
                    		           ArrayList<String> wheattisuues = new ArrayList<>();
                    		           
                    		           wheattisuues.add("Leaf");
                    		           wheattisuues.add("Spike");   
                    		           
                    		           
                    		           ArrayList<String> ricetisuues = new ArrayList<>();
                    		           ricetisuues.add("Embryo");
                    		           ricetisuues.add("Root");
                    		           ricetisuues.add("Shoot");
                    		           ricetisuues.add("Panicle");
                    		           ricetisuues.add("Anther");
                    		           ricetisuues.add("Endosperm");
                    		           ricetisuues.add("Leaf");
                    		           
                    		           
                    		           ArrayList<String> maizetisuues = new ArrayList<>();
                    		           
                    		           maizetisuues.add("Root");
                    		           maizetisuues.add("Shoot");
                    		           maizetisuues.add("Anther");
                    		           maizetisuues.add("Leaf");
                    		           maizetisuues.add("Ear");
                    		           maizetisuues.add("Pollen");
                    		           maizetisuues.add("Tassel");
                    		           maizetisuues.add("5Doc");
                    		           maizetisuues.add("Silk");
                    		           
                    		           tissuemap.put("Wheat",wheattisuues);
                    		           tissuemap.put("Rice", ricetisuues);
                    		           tissuemap.put("Maize", maizetisuues);
                    		           
                    		           for (Map.Entry<String,List<String>> mapset : tissuemap.entrySet())
                    		           {
                    		                String species =  mapset.getKey();
                    		                List<String> tissList = mapset.getValue();
                    		                
                    		                for(String tissue:tissList)
                    		                {
                    		                	String tocheck = species+tissue+"_Edger";
                    		                	System.out.println("To check ----- "+tocheck);
                    		                	
                    		                	
                    		                	
                    		                	if(!(new DbCommands().checkDatabase(tocheck)))
                              				  	{
                              				  	  System.out.println("Need to do entry to db by excel file:"+tocheck+".xlsx");
                              				  	  
                              				  	  new Exceltodbmapper().maptissues("inproject",tocheck+".xlsx");
                              				  	}
                    		                }	    
                    		                
                    		           }                  		           

                    		       
                    		             
                    		          
                    		          
                    		            System.out.println(" Will see mapping for target database tables");
                    		            
                    		            
                    		            
                    		             while(true)
                    		              {
                    		                 
                    		            	 String time = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
                
                    		            	 // Code for running sql command on 11:45
                    		            	 
                    		            	 if(time.equals("23:45"))
                    		            	 {
                    		            		 ////System.out.println("Time for running bot");
                    		            		 new search_result_generator().searchdb("zma-miR168a-3p", "maize" ,new datasetgetter().getdatasets("maize"));   
                    		            	 }
                    		            	 
                    		            	 // ----------- Code ends for this
                    		            	 
                    		            	 Thread.sleep(60000);
                    		             }     
                    		             
                        			}
                        			
                        			catch (SecurityException e)
                        			{
                        				// TODO Auto-generated catch block
                        				e.printStackTrace();
                        			} catch (NoSuchMethodException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
                        		}
							};
							
							//Thread is executed
							
							Thread dbcheck_thread = new Thread(dbchecks);
							dbcheck_thread.start();
							
							Thread basicthread=new Thread(basic);
							basicthread.start();	
							
							Thread newdscdheck = new Thread(newdscheckrunnable);
							newdscdheck.start();
							
        		}
        		finally
        		{
        			//System.out.println(" -- Finally called -- ");
        		}

    }


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
