
<%@page import="java.util.ArrayList"%>
<%@page import="com.pmirexat.nabi.dao.DbCommands"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PmiRExAt@NABI</title>

 <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
      
      <script src="designs/js/jquery-1.11.1.min.js"></script> <!-- jquery added -->
       <script src="js/jquery-ui.js"></script>  <!--  jquery ui js added -->
  <script src="Bootstrap/js/bootstrap.min.js"></script>  <!--  bootstrap js added -->
  <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
       
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  
      <!--  scripts to be included -->
   
       <script src="designs/js/bootstrap-switch.min.js"></script>  
       
         <script src="js/tableExport.js"></script>  
           <script src="js/jquery.base64.js"></script>     
        
       <script src="http://code.highcharts.com/4.1.6/highcharts.js"></script>
      <script src="http://code.highcharts.com/4.1.6/modules/data.js"></script>
      <script src="http://code.highcharts.com/4.1.5/modules/heatmap.js"></script>
       <script src="http://code.highcharts.com/4.1.6/modules/exporting.js"></script>            
      
      <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
       
   
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
       <script src="js/blockui.js"></script>  <!--  jquery ui js added -->
      
      <script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script> <!-- tagit js added-->
      
      
      
      <!-- css added-->
      
      
       <link rel="stylesheet" href="css/jquery-ui.css"> <!--  jquery ui compnents css -->
      
      <!--  bootstrap css -->
      
      
      <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
       
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

    
      <!--  tabit css -->
  
      <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
      <link href="css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
     
     <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
     <link href="designs/css/bootstrap-switch.css" rel="stylesheet">
  
      <!-- css added-->
      


     
<script>

$(document).ready(function()
	      {
   	  min = -5 ;
	    	 
	    	  max = 10 ;
	    	 
	    	  $(this).toggleClass('active');
	    	  
	    	 ourdataset = new Array();

	    	 
	    	  ourrna = new Array(); 
	    	
	    	 
	    	 $("#csv").empty(); 
	    	  
	        var table = document.getElementById("resultable");
	        
	        //selects all colums in table        
	        
	          row = table.rows[0]; 


                 $("#csv").append("dataset,rna,value"+"\r\n");
	        
	         for (j = 1; j < row.cells.length; j++) 
	         {
	        	  cell = row.cells[j];	
	        	
	        	 ourrna.push(cell.textContent.trim());
	         } 
	          
	        //iterates over rows
	        for (i = 1; i < table.rows.length; i += 1)
	        { 
	        	   row = table.rows[i];
	        	   
	        	   var seqno;
	        	   
	        	   //iterate over columns
	        	   for (j = 0; j < row.cells.length; j += 1) 
	        	   {
	        		   cell = row.cells[j];	
	        		   var t=cell.textContent.trim();
	        		   //alert(t);
	        
	        		   if(j==0)
	        		   {
	        			   ourdataset.push(t);	  //Inserting value into array dataset 
	        			   $.ajax({  
	                  		 type:"get",  
	                  		 url:"sequenceget", 
	                  		 async:false,
	                  		 data:{
	                                   term : t,
	                                },
	                			 success:function(result)
	                   			{                    			
	                   			  seqno=result;
	                   			  
	                  			}  
	                                
	                		 });    
	        		
	        		   }	   
	        		   
	        		   else
	        		   {
	        			   if(j==1)
	            		   {
	            			     var x = cell.headers;       
	            		   }
	            		   
	        			  
	            		   heatmapval1= parseInt((cell.innerHTML).trim()) + 1;	
	            		   
	            		   
	            		   
	                	   heatmapval2=((heatmapval1*1000000)/seqno).toFixed(6);
	                	
	            		   heatmapval=(Math.log(heatmapval2)/Math.LN2).toFixed(6);
	                	   
	            		   if(Math.round(heatmapval) > max)
	            		   {
	            		    
	            			   max = Math.round(heatmapval);   
	                           max = (max - (max % 5))  + 5;
	                           
	                          // alert("max  chacnged to : " + max);
	            		   }
	            		  
	            		   if(Math.round(heatmapval) < min)
	            		   {
	            			  // alert("heatmap val is:"+Math.round(heatmapval)+"min valuie is :" + min)
	            			    min = Math.round(heatmapval);  
	            			  
	            			 // alert("min is :"+min +"-- val of rem"+(min % 5 ));
	            			  
	            			  min = (min - (min % 5 )) - 5;
	            			  
	            			//  alert("min changed to : " + min);
	            			   //min = ((min/5) * 5) -5
	            		   }
	            		   
	            		   var arr = new Array(3);
	            		   arr[0] = i-1;
	            		   arr[1] = j-1;
	            		   arr[2] = heatmapval;
	            		   $("#csv").append(arr.join(",")+"\r\n");
	        		   }	   
	        		   
	        		  
	        	    }
	        }

	            foo();

	       
	      });


function foo()
{


	
	    /**
     * This plugin extends Highcharts in two ways:
     * - Use HTML5 canvas instead of SVG for rendering of the heatmap squares. Canvas
     *   outperforms SVG when it comes to thousands of single shapes.
     * - Add a K-D-tree to find the nearest point on mouse move. Since we no longer have SVG shapes
     *   to capture mouseovers, we need another way of detecting hover points for the tooltip.
     */
    (function (H) {
        var wrap = H.wrap,
            seriesTypes = H.seriesTypes;

        /**
         * Recursively builds a K-D-tree
         */
        function KDTree(points, depth) {
            var axis, median, length = points && points.length;

            if (length) {

                // alternate between the axis
                axis = ['plotX', 'plotY'][depth % 2];

                // sort point array
                points.sort(function (a, b) {
                    return a[axis] - b[axis];
                });

                median = Math.floor(length / 2);

                // build and return node
                return {
                    point: points[median],
                    left: KDTree(points.slice(0, median), depth + 1),
                    right: KDTree(points.slice(median + 1), depth + 1)
                };

            }
        }

        /**
         * Recursively searches for the nearest neighbour using the given K-D-tree
         */
        function nearest(search, tree, depth) {
            var point = tree.point,
                axis = ['plotX', 'plotY'][depth % 2],
                tdist,
                sideA,
                sideB,
                ret = point,
                nPoint1,
                nPoint2;

            // Get distance
            point.dist = Math.pow(search.plotX - point.plotX, 2) +
                Math.pow(search.plotY - point.plotY, 2);

            // Pick side based on distance to splitting point
            tdist = search[axis] - point[axis];
            sideA = tdist < 0 ? 'left' : 'right';

            // End of tree
            if (tree[sideA]) {
                nPoint1 = nearest(search, tree[sideA], depth + 1);

                ret = (nPoint1.dist < ret.dist ? nPoint1 : point);

                sideB = tdist < 0 ? 'right' : 'left';
                if (tree[sideB]) {
                    // compare distance to current best to splitting point to decide wether to check side B or not
                    if (Math.abs(tdist) < ret.dist) {
                        nPoint2 = nearest(search, tree[sideB], depth + 1);
                        ret = (nPoint2.dist < ret.dist ? nPoint2 : ret);
                    }
                }
            }
            return ret;
        }

        // Extend the heatmap to use the K-D-tree to search for nearest points
        H.seriesTypes.heatmap.prototype.setTooltipPoints = function () {
            var series = this;

            this.tree = null;
            setTimeout(function () {
                series.tree = KDTree(series.points, 0);
            });
        };
        H.seriesTypes.heatmap.prototype.getNearest = function (search) {
            if (this.tree) {
                return nearest(search, this.tree, 0);
            }
        };

        H.wrap(H.Pointer.prototype, 'runPointActions', function (proceed, e) {
            var chart = this.chart;
            proceed.call(this, e);

            // Draw independent tooltips
            H.each(chart.series, function (series) {
                var point;
                if (series.getNearest) {
                    point = series.getNearest({
                        plotX: e.chartX - chart.plotLeft,
                        plotY: e.chartY - chart.plotTop
                    });
                    if (point) {
                        point.onMouseOver(e);
                    }
                }
            })
        });

        /**
         * Get the canvas context for a series
         */
        H.Series.prototype.getContext = function () {
            var canvas;
            if (!this.ctx) {
                canvas = document.createElement('canvas');
                canvas.setAttribute('width', this.chart.plotWidth);
                canvas.setAttribute('height', this.chart.plotHeight);
                canvas.style.position = 'absolute';
                canvas.style.left = this.group.translateX + 'px';
                canvas.style.top = this.group.translateY + 'px';
                canvas.style.zIndex = 0;
                canvas.style.cursor = 'crosshair';
                this.chart.container.appendChild(canvas);
                if (canvas.getContext) {
                    this.ctx = canvas.getContext('2d');
                }
            }
            return this.ctx;
        }

        /**
         * Wrap the drawPoints method to draw the points in canvas instead of the slower SVG,
         * that requires one shape each point.
         */
        H.wrap(H.seriesTypes.heatmap.prototype, 'drawPoints', function (proceed) {

            var ctx;
            if (this.chart.renderer.forExport) {
                // Run SVG shapes
                proceed.call(this);

            } else {

                if (ctx = this.getContext()) {

                    // draw the columns
                    H.each(this.points, function (point) {
                        var plotY = point.plotY,
                            shapeArgs;

                        if (plotY !== undefined && !isNaN(plotY) && point.y !== null) {
                            shapeArgs = point.shapeArgs;

                            ctx.fillStyle = point.pointAttr[''].fill;
                            ctx.fillRect(shapeArgs.x, shapeArgs.y, shapeArgs.width, shapeArgs.height);
                        }
                    });

                } else {
                    this.chart.showLoading("Your browser doesn't support HTML5 canvas, <br>please use a modern browser");

                    // Uncomment this to provide low-level (slow) support in oldIE. It will cause script errors on
                    // charts with more than a few thousand points.
                    //proceed.call(this);
                }
            }
        });
    }(Highcharts));


    var start;
    $('#container').highcharts({

        data: {
            csv: document.getElementById('csv').innerHTML,
            parsed: function () 
            {
            	
                start = start+1;
                
            }
        },

        credits: {
            text: 'Generated By: PmiRExAT',
            href: 'http://pmirexat.nabi.res.in/'
        },
        
        chart: {
            type: 'heatmap',
            height: 800,
            zoomType: 'xy',
            resetZoomButton: {
                position: {
                    // align: 'right', // by default
                    // verticalAlign: 'top', // by default
                    x: -40,
                    y: 10
                },
                relativeTo: 'chart'
              }

            
        },


        title: {
            text: 'Heatmap for Datasets and miRNAs',
            align: 'left',
            x: 40
        },

        subtitle: {
            text: 'Heatmap for log TPM(Transcript per million) values calculated after adding pseudo count of one to each miRNA count',
            align: 'left',
            x: 40
        },

        tooltip: {
        	formatter: function() 
        	{       		
                return '<b> Dataset : '+ ourdataset[this.point.x] +'</br> Rna :  '+ ourrna[this.point.y] + '</br> TPM Value :  ' + this.point.value + '</b>';
            },
            backgroundColor: null,
            borderWidth: 0,
            distance: 10,
            shadow: false,
            useHTML: true,
            style: {
                padding: 0,
                color: 'black'
            }
        },

        xAxis: {
            title: {
                enabled: true,
                text: 'Datasets'
                },
                
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
   
            categories: ourdataset
        },

        yAxis: {
        	
        	title:
                {
                enabled: true,     
                text: 'microRNA'
                },               
                categories: ourrna,
                title:null,
               minorTickLength: 1,
        },


        colorAxis: {
            stops: [
                [0, '#3060cf'],
                [0.5, '#fffbbc'],
                [0.9, '#c4463a'],
                [1, '#c4463a']
            ],
            min: min,
            max: max,
            startOnTick: false,
            endOnTick: false,
            labels: {
                format: '{value}'
            }
        },

        series: [{
        	
            borderWidth: 0,
            nullColor: '#EFEFEF',
           
           
            turboThreshold: Number.MAX_VALUE // #3404, remove after 4.0.5 release
        }]

    });

};


</script>

<style>

 .nav>li>a
	{
		padding-left: 10px;
	}
	
	
	.highcharts-tooltip>span
 {
	background: rgba(255,255,255,0.85);
	border: 1px solid silver;
	border-radius: 3px;
	box-shadow: 1px 1px 2px #888;
	padding: 8px;
	z-index: 2;
}


}

</style>

</head>
<body>

<div class="container" style="padding:0;" id="main">
                 
  <header class="navbar navbar-default " role="banner">
        <div class="container ">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                 <a class="navbar-brand" href="index.html">  <span class="glyphicon glyphicon-home"></span> PmiRExAt </a>
            </div>
            <nav class="collapse navbar-collapse  bs-navbar-collapse" role="navigation">
       <ul class="nav navbar-nav ">
       
         <li><a href="about.html"> <i class="fa fa-users"></i>
          About </a></li>
           <li><a href="datashow.html"> <i class="glyphicon glyphicon-book"></i>
          Related Data </a></li>
          
           <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Browse <b class="caret"></b></a>
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="browse.html">By Species</a></li>
                    <li><a href="crossspecies.html">By Across Species</a></li> 
                     <li><a href="bylatestpublications.html">By Latest Publications</a></li>     
                     <li><a href="tissuebrowse.html">By Tissue Preferential</a></li>    
                      <li><a href="diffexpbrowse.html">By Differential Expression</a></li>                 
           </ul>
           </li>
           
           <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search"></i>Search <b class="caret"></b></a>
          
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="searchdb.html">Expression Search</a></li>
                    <li><a href="tissuespecific.html"> Tissue preferential expression in species (Foldchange and ROKU)</a></li>   
                    <li><a href="tissuepreferentialde.html"> Tissue preferential expression in species (edgeR)</a></li>                           
          			 <li><a href="diffexpsearch.html">Differential Expression</a></li>
           </ul>
           </li>
           
          
           
         <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search-plus"></i> Custom Search <b class="caret"></b></a>
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="file.html">miRNA Profiling</a></li>
                    <li><a href="checkstatus.html">Search miRNA  profiling results</a></li>  
                    <li><a href="dsupload.jsp">Dataset Profiling</a></li>                
           </ul>
          </li> 
            <li ><a href="downloads.html"><span class="glyphicon glyphicon-download-alt"></span> Downloads    </a></li>
        
         <li ><a href="api.html">PmiRExAt API</a></li>
        
         <li ><a href="contactus.html"><span class="glyphicon glyphicon-earphone"></span> Contact Us    </a></li>

         <li ><a href="support.html"><span class="glyphicon glyphicon-download-alt"></span>   Support </a></li> 
     
      </ul>
 
                 
        
    
            </nav>
        </div>
    </header>


    
           <div id = "recordsuccess" class="alert alert-success alert-dismissible" role="alert" style="width: 95%;margin-left: auto;margin-right: auto;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <i class="fa fa-check-circle-o"></i>&nbsp; Search completed, Following are the expression counts record of clicked miRNA against all datasets </div>        
       

       
       <c:choose>
    <c:when test="${not empty requestScope.result_list}">
        
         <table class="table  table-striped table-bordered table-hover table-condensed" id="resultable" style="max-height:240px;margin-top:20px;overflow:auto;width:100%;display:block;padding:0">
           
             <tr  style="width: 100%">
             
             <% 
             
             System.out.println(" ------------ Now starts result.jsp -------------- ");
             
                   ArrayList<String> rnas = (ArrayList<String>)request.getAttribute("searched_micro_rnas");
                
             //System.out.println("Searched Rnas : "+rnas);
             
                 for(String st : rnas)
                 {
                	
                	 System.out.println(" Searcehd rna : "+rnas);
                	 
                	 String tochklink = st;
                	 
                	 if(st.contains("("))
                	 {
                		 tochklink = st.substring(0,st.indexOf("(")).trim();	 
                	 }
                	 
         
                	 String tolink = new DbCommands().getaccessurlname(tochklink.trim());
                	 
                	 if(st.equals("Searched Dataset Name"))
                	 {
                		 String chk = "<td  class='header' style='text-align:center'> <strong>"+"Dataset"+"</strong></td>";
                 	  
                		 
                 	     out.print(chk);
                	 }
                	 
                	 else
                	 {
                	    String chk ="<td class='header'  style='text-align:center;'><a href='"+tolink+"' target=_blank"+">"+"<strong>"+st+"</strong> </a> </td>";
                	  
                	    out.print(chk);
                	 }
                 }
                 
                 System.out.println("For ended in jsp page");
               
             %>
             
             </tr>        
            
              
              <c:forEach var="result" items="${requestScope.result_list}">
               
               <tr style="width:100%">
                 <c:forEach var="intval" items="${result}">
                       <td class="col-xs-1">
                         <c:out value="${intval}"></c:out>
                         </td>
                    
               
                 </c:forEach>
               </tr>
             </c:forEach>
             
          </table>   
    </c:when>
   
    <c:otherwise>
  
      
          
  <div class="alert alert-danger alert-dismissible" role="alert" style="width: 90%;margin-left: auto;margin-right: auto;">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4><strong><i class="fa fa-times"></i> &nbsp;  No records found!</strong></h4> No search results were found as per your search criteria.
</div>
          
       
</c:otherwise>
</c:choose>
 
 
       
      
 
 
 
     
       <pre id="csv" style="display:none;overflow:scroll; height:400px;">

 </pre>
 
 <br/>
 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Enter your search parameters</h4>
      </div>
      <div class="modal-body">
      
    <form id="Request_Search" action="customsearch" method="post" class="col-md-12">
    
    
    <div class="form-group">
        <input type="text" class="form-control" name="miRSequence" id="miRSequence" placeholder="Enter miR Sequence">
    </div>
    
    <div class="form-group">
        <input type="text" id="enteredmiR" class="form-control"  name="enteredmiR"  placeholder="Enter miR Name">
    </div>
    
    <div class="form-group">
        <input type="text" class="form-control" name="dataset1" id="dataset1" placeholder="Enter dataset">
    </div>
    
       <div class="form-group">
        <input type="text" class="form-control" name="species" id="species" placeholder="Enter Species">
    </div>
    
    
    
     <div class="form-group">
        <textarea class="form-control" name="details"  placeholder="Enter other details regarding search"></textarea>
    </div>
    
       <div class="form-group">
        <input type="text" class="form-control" name="emailid" id="emailid" placeholder="Enter emailid so we can send you outputs">
    </div>
        
    <div class="form-group">
        <input type="submit" value="Submit for Search" class="btn btn-primary btn-lg btn-block"></input>
        
    </div>
    
           <div id = "querysuccess" class="alert alert-success alert-dismissible" role="alert" style="width: 100%;margin-right: auto;display:none"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong> <i class="fa fa-check-circle-o"></i>&nbsp; Your Search Request Sent</strong> 
         <p> &nbsp;  We will process your request and will get back to you.
  
    </p> </div>
    
     <div id="alertmessage" class="alert alert-danger alert-dismissible" role="alert" style="width: 100%;margin-left: auto;margin-right: auto;display:none">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4><strong><i class="fa fa-times"></i> You missed something</strong></h4>Either miRsequence or miRname should be entered and  Species,emailid's are compulsory field and 
</div>
    
     </form>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

 
       <div id="container" class="panel panel-default" style="margin-top: 20px;"> should be displayed heree</div>
      
      
      
       <c:choose>
    <c:when test="${not empty requestScope.targetlist}">
        
          <h4 style="margin-top: 50px;">  Micro RNA targets Predicted by psRNAtarget at default parameters  </h4>
      
        
         <table class="table  table-striped table-bordered table-hover table-condensed" id="targettable" style="max-height:340px;margin-top:20px;overflow:auto;width:100%;display:block;padding:0">
           
             <tr  style="width: 100%">
             <th>
             Target_Acc.
             </th><th>
             Expectation
             </th>
              <th>
             UPE
             </th>
             <th>
              Inhibition
             </th>
             <th>
             MiRNA_Acc
             </th>
             <th>
             MiRNA_aligned_fragment
             </th>
             <th>
             miRNA_start
             </th>
             <th>
            miRNA_end
             </th>
             <th>
             Target_start
             </th>
             <th>
             Target_end
             </th>
             <th>
             Target_aligned_fragment
             </th>
             <th>
             Target_Desc
             </th>
            
             
        
             
             
             
             </tr>        
            
              
              <c:forEach var="result" items="${requestScope.targetlist}">
               
               <tr style="width:100%">
                 <c:forEach var="intval" items="${result}">
                       <td class="col-xs-1">
                         <c:out value="${intval}"></c:out>
                         </td>
                    
               
                 </c:forEach>
               </tr>
             </c:forEach>
             
          </table>   
    </c:when>
   
    <c:otherwise>
  
      <h4 style="margin-top: 50px;">  Micro RNA targets Predicted by psRNAtarget at default parameters  </h4>
          
  <div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <i class="fa fa-times"></i> &nbsp;  No targets predicted by psRNAtarget at default parameters!
</div>
          
       
</c:otherwise>
</c:choose>
      
      
      <div id="disqus_thread"></div>
<script>
/**
* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
*/

var disqus_config = function () {
this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');

s.src = '//pmirexat.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
      </div>
      
      
      
        
</body>   
          
     
                 

        
    


 
 

 

             
