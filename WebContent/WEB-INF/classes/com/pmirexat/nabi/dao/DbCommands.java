package com.pmirexat.nabi.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.class_entityconvert;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.pojos.MaizePojo;
import com.pmirexat.nabi.pojos.Seqno;
import com.pmirexat.nabi.pojos.WheatPojo;
import com.pmirexat.nabi.pojos.querydb;
import com.thoughtworks.selenium.webdriven.commands.Check;


public class DbCommands
{
	//--------to save object in db.Used for mapping excelsheet to db---------//
	Session ses;
	
    public  void saveEntry(Object excelobj)
    {
    	 
    	try
    	{
    	
    	 ses=new HibernateUtils().getSession();
   
    	 ses.beginTransaction();
       
   	     ses.save(excelobj);   	
      
   	     ses.getTransaction().commit();
      
    	}
    	
    	catch(Exception obj)
    	{
    		////System.out.println(" --Exception found in save object method --- ");
    	    ////System.out.println("ecception obj : "+obj.toString());
    	}
    	
    	finally
    	{
    		
    		if(ses != null)
    		{
    		 ses.close();
    		}
    	}
    	
   	    
     
   	 
    
    }
    
    public void savesequence(Seqno record)
    {
    	
    	
    	try
    	{
    	//// ////System.out.println("---- chk1 -------");
    	 ses=new HibernateUtils().getSession();
    //	// ////System.out.println("---- chk2 -------");
    	 ses.beginTransaction();
       
    	 ses.save(record);   	
      
    	 ses.getTransaction().commit();
      
    	}
    	
    	catch(Exception e)
    	{
    	   ////System.out.println("Exception in saving object : "+ e.toString());	
    	}
    	
    	finally
    	{
    		if(ses != null)
    		ses.close();
    			
    	}
    	 
     
   	 // ////System.out.println("----------- done with saving object ----------");
    
    }
    
    // to save mir's data 

    
    
    // --------------------------- method ends --------------------------- //
    
    //My method to fetch sequence number for datasets
    public  Double getsequenceno(String datasetname)
    {
       String hql="SELECT E.sequence FROM Seqno E WHERE E.datasetname = :datasetname"; 
    	
    	// ////System.out.println("sequence to be set : "+sequence);
       Double seqno = 0.0;
       
       ////System.out.println("Reached in getsequence no. ");
       
       try
       {
    	  ses=new HibernateUtils().getSession();     // -- session started
       	
       	Query query = ses.createQuery(hql).setString("datasetname",datasetname);
       	
       	////System.out.println("datasetname is"+datasetname);
       	
       	////System.out.println("query to fire is"+hql);
    
       	// ////System.out.println(query.getReturnTypes());
       	
       	List<Double> resultobj= query.list();
       	
       seqno=0.0;
       	
       	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
       	{
       		seqno=(Double)iterator.next();
   			////System.out.println("----- Iterator  ------"+seqno);
   		
       	}
	   } 
       catch (Exception e)
       {
		  ////System.out.println("Exception in getsequence no. method : "+e.toString());
	   }
       
    	finally
    	{
    		
    		if(ses != null)
    		{
    		 ses.close();   //session Closed in finally     
    		}
    	}
    	
    	
		return seqno; 	
    }
    
    public  String getaccessurlname(String miRname)
    {
    	//////System.out.println("miRname to get url is :"+miRname);
    	
    	 List<String> Wheat = new RnaGetter().getmiRlist("wheat");
    	 List<String> rice = new RnaGetter().getmiRlist("rice");
    	 List<String> maize = new RnaGetter().getmiRlist("maize");
    	 String accessurl = "";
    	 String pojoname = "";
    	 
    	 if(Wheat.contains(miRname))
    	 {
    		 pojoname = new class_entityconvert().getclassname("Wheat");
    	 }
    	 
    	 if(rice.contains(miRname))
    	 {
    		 pojoname = new class_entityconvert().getclassname("Rice");
    	 }
    	 
    	 if(maize.contains(miRname))
    	 {
    		 pojoname = new class_entityconvert().getclassname("Maize");
    	 }
    	 
    //	 ////System.out.println("Pjp name is :"+pojoname);
    	
    	 if(!(pojoname.equals("")) )
    	{
        String hql="SELECT E.accessurl FROM "+ pojoname +" E WHERE E.mir_ids = :miRname"; 
    	
    	// ////System.out.println("sequence to be set : "+sequence);
    	
        try 
        {
        	 ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("miRname",miRname);
        	
        	List<String> resultobj= query.list();
        	
        	
        	
        	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
        	{
        		accessurl=(String)iterator.next();
    			//////System.out.println("----- URL  ------"+accessurl);
        	}
        	
		} 
        catch (Exception e) 
        {
			////System.out.println("Exception in getaccessurl method : "+e.toString());
		}
        
      finally
      {
    	  if(ses != null)
    	  {
    	  ses.close();
    	  }
      }
    }
		return accessurl; 	
    }
       
    
   //-------Listener uses this method to check that whether there is already mapped data avaialable at database or not
    
    public  boolean checkDatabase(String tablename)
    {
    	////System.out.println("----- reached checkDatabase function ---- ");
    	
    	String query="select count(*) from "+tablename;
    	
    	////System.out.println("---"+query);
    	
    	int count = 0;
    	
    	try
    	{
    		 ses=new HibernateUtils().getSession();
      	    count = ((Long)ses.createQuery(query).uniqueResult()).intValue();
            // ////System.out.println("value returnde : "+count);
		}
    	
    	catch (Exception e)
    	{
			////System.out.println("Exception found in checkl database :"+e.toString());
		}
    	
    	finally
    	{
    		if(ses != null)
    		{
    		ses.close();
    		}
    	}
  
    	if(count>0)
        {
        	return true;
        }
        
        else
        {
		    return false;	
		}
        
    }
    
    //-------------------------------------- method ends ---------------------------------//
    
    
    
   
    //method for performing searching operation on excel sheet

 //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------//   
   
    //		list           of 					Map 			 of 					list		 
    // 		|									|										|
    //	rows fetched						wheat/rice/maize						integer values inside

    
    
    public  List<List<Map<String, List<Integer>>>>  searchDatabase(String Searchattributes,String categorytosearch, List<String>  gettertocall) 
    {
 	// ////System.out.println("================================== Reached in Dao Searchdatabase Method  =================================================");
    	 Method methodtoinvoke;
    	
         
    	 List<List<Map<String, List<Integer>>>> finalresuultslists=new ArrayList<List<Map<String,List<Integer>>>>();
    	 
    	 List<Integer> innerlistintgvalue;
    	 
    	 List<String> getters = gettertocall ;  	 
    	 
    	try
    	{
    	 
    	 ses = new HibernateUtils().getSession();

		StringTokenizer st2 = new StringTokenizer(Searchattributes,",");

		
		while (st2.hasMoreElements())
		{
			List<Map<String, List<Integer>>> mapcontainglist;
			
			String tosetval=st2.nextElement().toString();
			
			String hql = "";
			
		
				// hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids = :name and "+tissue_tochk +">"+folcheck; 
	
			
			
		     hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids = :name"; 
			
			
			
			Query query = ses.createQuery(hql).setString("name",tosetval);

			Object resultobject = null ;			
			
			if(categorytosearch.equals("wheat"))
			{				
				resultobject = new WheatPojo();							
			}
			
			if(categorytosearch.equals("maize"))
			{				
				resultobject = new MaizePojo();								
			}
			
			if(categorytosearch.equals("rice"))
			{			
                resultobject = new MaizePojo();        
            }
			
			
			List<Object> resultobj= query.list();
	
			
			for(Object object : resultobj)
			{
		
				 mapcontainglist=new ArrayList<Map<String,List<Integer>>>();
		
				Map<String, List<Integer>> innermap=new LinkedHashMap<String, List<Integer>>();
		
					
					
				    innerlistintgvalue=new ArrayList<Integer>();
				    
				
				    
				    for (String tobecalledgetter : getters)
				    {
				    	
					   try 
					   {
						    methodtoinvoke=  object.getClass().getMethod("get"+tobecalledgetter,null); 
					        try 
					        {
			
					        	
					        	innerlistintgvalue.add((Integer) methodtoinvoke.invoke(object));
							} 
					        catch (IllegalAccessException
									| IllegalArgumentException
									| InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					   } 
					   catch (NoSuchMethodException e) 
					   {
						// TODO Auto-generated catch block
						e.printStackTrace();
				       }
					   catch (SecurityException e) 
					   {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
				    
				    innermap.put("wheatinnermap",innerlistintgvalue);
				    
				mapcontainglist.add(innermap);
				
				finalresuultslists.add(mapcontainglist);
			}			
			 //----------------------------------------------------------------------------------------
	}
	
		//------------------------------------------------------------------------------------------------------
    	
    	}
    	
    	catch(Exception e)
    	{
    		////System.out.println("Exception found");
    		e.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses != null)
    		{
    				ses.close();
    		}
    	}
		
		return finalresuultslists;

	}
    
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //
    
    public  List<List<Map<String, List<String>>>>  searchDatabase_byfolchange(String Searchattributes,String categorytosearch, List<String>  gettertocall,String tissue_tochk,float foldcheck) 
    {
 	// ////System.out.println("================================== Reached in Dao Searchdatabase Method  =================================================");
    	 Method methodtoinvoke;
    	
         
    	 List<List<Map<String, List<String>>>> finalresuultslists=new ArrayList<List<Map<String,List<String>>>>();
    	 
    	 List<String> innerlistintgvalue;
    	 
    	 List<String> getters = gettertocall ;  	 
    	 
    	try
    	{
    	 
    	 ses = new HibernateUtils().getSession();

		StringTokenizer st2 = new StringTokenizer(Searchattributes,",");

		
		while (st2.hasMoreElements())
		{
			List<Map<String, List<String>>> mapcontainglist;
			
			String tosetval=st2.nextElement().toString();
			
			String hql = "";
			
		
				 hql = "FROM " + new class_entityconvert().getclassname(categorytosearch)+ " WHERE mir_ids = :name and "+tissue_tochk +" > "+foldcheck; 
	
				 
				 //System.out.println("Genrated hql strign is : "+hql);
				 
			Query query = ses.createQuery(hql).setString("name",tosetval);

			Object resultobject = null ;			
			
			if(categorytosearch.equals("wheat"))
			{				
				resultobject = new WheatPojo();							
			}
			
			if(categorytosearch.equals("maize"))
			{				
				resultobject = new MaizePojo();								
			}
			
			if(categorytosearch.equals("rice"))
			{			
                resultobject = new MaizePojo();        
            }
			
			
			List<Object> resultobj= query.list();
	
			
			for(Object object : resultobj)
			{
		
				 mapcontainglist=new ArrayList<Map<String,List<String>>>();
		
				Map<String, List<String>> innermap=new LinkedHashMap<String, List<String>>();
		
					
					
				    innerlistintgvalue=new ArrayList<String>();
				    
				    methodtoinvoke=  object.getClass().getMethod("getMir_ids",null); 
				    innerlistintgvalue.add((String) methodtoinvoke.invoke(object));
				
				    
				    for (String tobecalledgetter : getters)
				    {
				    	
					   try 
					   {
						    methodtoinvoke=  object.getClass().getMethod("get"+tobecalledgetter,null); 
					        try 
					        {
			
					        	
					        	innerlistintgvalue.add(String.valueOf(methodtoinvoke.invoke(object)));
							} 
					        catch (IllegalAccessException
									| IllegalArgumentException
									| InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					   } 
					   catch (NoSuchMethodException e) 
					   {
						// TODO Auto-generated catch block
						e.printStackTrace();
				       }
					   catch (SecurityException e) 
					   {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
				    
				    innermap.put("wheatinnermap",innerlistintgvalue);
				    
				mapcontainglist.add(innermap);
				
				finalresuultslists.add(mapcontainglist);
				
				//System.out.println("Final results : "+finalresuultslists);
			}			
			 //----------------------------------------------------------------------------------------
	}
	
		//------------------------------------------------------------------------------------------------------
    	
    	}
    	
    	catch(Exception e)
    	{
    		////System.out.println("Exception found");
    		e.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses != null)
    		{
    				ses.close();
    		}
    	}
		
    	//System.out.println("Returned result from db methods : "+finalresuultslists);
		return finalresuultslists;

	}
    
    
    
    
    
    
    
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------//      
    
    
    
   
    //Listener calls it get all column values so it can generate array list for category based getters.
    public  ArrayList<String>  getTableDesc(String tableName) 
    {
      //  // ////System.out.println("getFieldNames:start" + tableName);
        Object[] a;
        List<Object[]> fieldNames = new ArrayList<Object[]>();
        ArrayList<String> tabFieldNames = new ArrayList<String>();
        try {
        	 ses = new HibernateUtils().getSession();
       
            String queryStr = "DESCRIBE "  + tableName;                 
            fieldNames = (List<Object[]>) ses.createSQLQuery(queryStr).list();
           
            for (int i = 0; i < fieldNames.size(); i++) 
            {
                a = fieldNames.get(i);
                tabFieldNames.add(a[0].toString());
            }
            
        } 
        catch (Exception e) 
          {
        	
        	////System.out.println("Exception found in gettabledesc method");
            e.printStackTrace();
            // ////System.out.println("exception " + e);
        } finally
        {
        	if(ses != null)
        	{
        		ses.close();
        	}
        }
        // ////System.out.println("getFieldNames:end" + tabFieldNames.toString());
        return tabFieldNames;
    }  
    
    
  /*Method for obtaining values of 1st column of a table.Will be used for autocomplete box
   * Returns a List of string which contains mature_rnas ,when called fom listners.
   * 
  */
    
    public  List<String>  get_mature_microrna(String pojoclass) 
    {
    	////System.out.println("---reached  get matured rna method------");
    	
       	String hql = "SELECT E.mir_ids FROM " +pojoclass+ " E where E.mir_ids is not null";
       	// ////System.out.println("---reached  get matured rna method 1------");
    	
        List<String> results = null;
       	try 
       	{
       		ses = new HibernateUtils().getSession();
           	
       		////System.out.println("Value of ses in get mature rna "+ses);
       		
           	Query query = ses.createQuery(hql);
           	
        	 results = query.list();
        	
        	 ////System.out.println("Result from get_matureRna");
        	 
		} 
       	catch (Exception e)
       	{
			////System.out.println("Exception in get_mature_micrornas");
			e.printStackTrace();
		}
       	
       	
       	finally
       	{
      
       	 if((ses != null))
       	 {
       		 ses.close();
       	 }
       	}
  
       	return (List<String>) results;
    }  
    
    
    /*
        This method will search for specific dataset values in the table   
     */
    
    public List<List<String>> Specfic_ds_search0(String microrna_tosearch,String datasets_tosearch,String pojoname)
    {
    	
    	StringTokenizer rna_tokenizer = new StringTokenizer(microrna_tosearch,",");
    	
    	StringTokenizer datasetTokenizer;
 
    	String query_part1="SELECT ";
    	
    	String query_part2=null;
    	
    	List<List<String>> specific_ds_resultList=new ArrayList<List<String>>();
    
    	int multirna_check = 0;
    	
   	    while (rna_tokenizer.hasMoreElements())
    	{ 	
   	     	
   	    	
   		int first_status = 0;  		
   		query_part2 = "";	
   		datasetTokenizer = new StringTokenizer(datasets_tosearch,",");
   		
    		while(datasetTokenizer.hasMoreElements())
        	{
        	if(first_status<1)
       		{        			
       			query_part2=query_part2+(String) datasetTokenizer.nextElement();
       			first_status++;
       		}
     		
       		else
       		{
       			    multirna_check++;
        			query_part2=query_part2+","+(String) datasetTokenizer.nextElement();		
       		}
       	}
       			
        	String query_part3=" FROM " +pojoname + " WHERE mir_ids = :name";  //xxx
			
        	String hql=query_part1+query_part2+query_part3;
       	
        	String topass=rna_tokenizer.nextToken();
        	
        	try
        	{
        	
        	ses=new HibernateUtils().getSession();
        	
           	Query query = ses.createQuery(hql).setString("name",topass);
        	 
        	 List<String> row_list=new ArrayList<String>();

        	 if(multirna_check > 0) 	 
        	 {	 
       
        	//	////System.out.println(" ------- More then one values were searched ,should return array of objects ");
        	
        		 List<Object[]> resulobj= (List<Object[]>)query.list();
       	
        		 
        		 for (Object[] objects : resulobj)
        		 {
				 
        	        for (Object object : objects)
				    {

					     row_list.add(((Integer)object).toString()); //object first casted to integer and then typecasted to String type
				
			    	 }
			    	 specific_ds_resultList.add(row_list);
			    }

        }
         
        else
        {
  
        	
        	List<Object> resulobj= query.list();
    	 
   
                  		 
            	  
    				   for (Iterator iterator = resulobj.iterator(); iterator.hasNext();) 
    				   {
						  Object object = (Object) iterator.next();
						
						  row_list.add(((Integer)object).toString());
					   }
    			  
    				   specific_ds_resultList.add(row_list);
        }
        

        	}
        	
        	
        	catch(Exception e)
        	{
        		////System.out.println(" Exception found ");
        		e.printStackTrace();
        	}
        	
        	finally
        	{
        		if(ses != null)
        		{
        			ses.close();
        		}
        	}
       }
        

    	
   	
   	    for (List<String> list : specific_ds_resultList) 
   	    {
   	    	// ////System.out.println("");
		     for (String string : list)
		     {
			   // ////System.out.print(string);	
			 }
		}
    	
		return specific_ds_resultList;	
	}
    
    
    public String getmirnam(String sequence,String pojoname)
    {
    	String hql="SELECT E.mir_ids FROM " + pojoname +" E WHERE E.sequence = :sequence"; 
    	
    	// ////System.out.println("sequence to be set : "+sequence);
    	
    	List<String> resultobj = null ;
    	
    	try 
    	{
    		 ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("sequence",sequence);
        	
        	resultobj= query.list();
        	
        	//////System.out.println("Got mir name");
        	
		} 
    	
    	catch (Exception e) 
    	{
			 ////System.out.println("Exception in getmirname db connectivity");
		}
    	
    	finally
    	{

    		if(ses != null)
    		{
    			ses.close();
    		}
    	}
   
    	
    	
    	String mir_id="";
    	
    	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
    	{
    		if(mir_id.equals(""))
    		{
    			mir_id = (String) iterator.next();
    		}
    		else
    		{
			     mir_id=mir_id+","+(String) iterator.next();
			// ////System.out.println("mir id for it is:"+mir_id);
    		}
		} 	
    	
		return mir_id;
    }
    
    
    public querydb getfiletoprocess()
    {
    	List results = null;
    	querydb qdb = null;
    	////System.out.println("Reacehd to get filename");
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(querydb.class);
    	cr.add(Restrictions.eq("status", "Waiting for execution")).setMaxResults(1); 
    
    	 results = cr.list();
    
    	 //System.out.println("Retuened values : "+results);
    	 
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
    	////System.out.println("REsult :"+results.toString());
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (querydb) iterator.next();
		    ////System.out.println("should be:"+qdb);	
		}
        
        if(qdb != null)
		return  qdb;
      }
    	
    	////System.out.println("Reached so far means, no results were found, I will return null");
    	return null;
    }
    
    
    
    public Boolean checkrunstatus()
    {
    	List results = null;
    	querydb qdb = null;
    	
    	Boolean checkvar = false;
    	
    	////System.out.println("Reacehd to cluster check status in checkrunstatus method");
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(querydb.class);
    	cr.add(Restrictions.eq("status", "Processing")).setMaxResults(1); 
    
    	 results = cr.list();
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
    	////System.out.println("REsult :"+results.toString());
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (querydb) iterator.next();
        	//System.out.println("Inside cluster check status -- Running querydb file is :"+qdb.toString());
		    ////System.out.println("should be:"+qdb);	
		}
        
        if(qdb != null)
        {	
        	checkvar = true;
        }
        
      }
    	
    	////System.out.println("Reached so far means, no results were found, I will return null");
    	return checkvar;
    }
    
    
    
    public String getsequence(String miRid,String pojoname)
    {
    	//String hql="SELECT E.sequence FROM " + pojoname +" E WHERE E.sequence = :sequence"; 
    	
    	String hql="SELECT E.sequence FROM " + pojoname +" E WHERE E.mir_ids = :mir_ids"; 
    	// ////System.out.println("sequence to be set : "+sequence);
    	
    	String sequence="";
    	
    	try 
    	{ 		
    		
    		ses=new HibernateUtils().getSession();
        	
        	Query query = ses.createQuery(hql).setString("mir_ids",miRid);
        	

        	
        	List<String> resultobj= query.list();
        	
        	for (Iterator iterator = resultobj.iterator(); iterator.hasNext();)
        	{
        		if(sequence.equals(""))
        		{
        			sequence = (String) iterator.next();
        		}
        		else
        		{
        			sequence=sequence+","+(String) iterator.next();
    		
        		}
    		} 	
		} 
    	catch (Exception e)
    	{
			////System.out.println("Exception found in get sequence");
		}
    	
    	finally
    	{	
    		if(ses != null)
    		{	
    			//////System.out.println("Session closed is called");
    		    ses.close();
    		}
    	}
    	
		return sequence;
    }
    
    
    public void updatequerydb(String filekey,Date endtime ,String status)
    {
    	 ses =  new HibernateUtils().getSession();
    	
    	ses.beginTransaction();
    	////System.out.println(" ----------------------------------------------- ------------------------------- In Upadte query -- file_key is :"+filekey );
    	
    
    	 
    	try
    	{
    	
    	Criteria cr = ses.createCriteria(querydb.class);
    	Criteria crt =  cr.add(Restrictions.eq("file_key",filekey)).setMaxResults(1); 
        List<querydb> qdb = crt.list();
        
        querydb querydb = null;
        for (Iterator iterator = qdb.iterator(); iterator.hasNext();) 
        {
			 querydb = (querydb) iterator.next();	
			 ////System.out.println("In for loop , Query object check check : "+querydb.getFile_key());
		}
        
        ////System.out.println("Querydb object received is : "+querydb.toString());
       
        querydb.setStatus(status);
        
        querydb.setEndtime(endtime);
      
        
        if(ses == null)
        {
        	////System.out.println("Value of session is null");
        }
        
        else
        {
        	////System.out.println("Session valie is not null");
        }
        
        ses.update(querydb);
  
        ses.getTransaction().commit();
        
        ////System.out.println("In DB saved");
        
    	}
    	catch(Exception obj)
    	{
    		////System.out.println("Exception in it");
    		obj.printStackTrace();
    	}
    	
    	finally
    	{
    		if(ses!=null)
    		{
    			ses.close();
    		}
    	}
    }
    
    
    
    public querydb getfilestatus(String file_key)
    {
    	List results = null;
    	querydb qdb = null;
    	try
    	{
    	ses =  new HibernateUtils().getSession();
    	Criteria cr = ses.createCriteria(querydb.class);
    	cr.add(Restrictions.eq("file_key", file_key)).setMaxResults(1); 
    	 results = cr.list();
    	}
    	catch(Exception obj)
    	{
    		obj.printStackTrace();
    	}
    	finally
    	{
    		if(ses!=null)
    		{
    		ses.close();
    		}
    	}
    	
    	
    	if(results != null)
    	{
    	////System.out.println("REsult :"+results.toString());
        
        for (Iterator iterator = results.iterator(); iterator.hasNext();) 
        {
			
        	qdb =  (querydb) iterator.next();
		    ////System.out.println("should be:"+qdb);	
		}
        
        if(qdb != null)
		return  qdb;
      }
    	
    	////System.out.println("Reached so far means, no results were found, I will return null");
    	return null;
    }
    
}
    
    




