package com.pmirexat.nabi.listners;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.persistence.criteria.Order;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;






import org.apache.commons.collections.functors.WhileClosure;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.Commonsequencegetter;
import com.pmirexat.nabi.model.Exceltodbmapper;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.SendEmailWithAttachments;
import com.pmirexat.nabi.model.crossspecies_resultgen;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.scriptexecuter;
import com.pmirexat.nabi.model.search_result_generator;
import com.pmirexat.nabi.pojos.TablePojo;
import com.pmirexat.nabi.pojos.querydb;

@WebListener
public class exceldbmapper implements ServletContextListener {

    public exceldbmapper()
    {
           // TODO Auto-generated constructor stub
    }
 
    
    //So this method will run when servlet will be initialized
    public void contextInitialized(final ServletContextEvent event)
    { 

    	final ServletContext sc=event.getServletContext();
         	  
            final String excelsheet_path;
            try 
        		{  	
            	           Runnable dbchecks = new Runnable() 
            	           {
							@Override
								public void run() 
									{
										// TODO Auto-generated method stub
								   
								       while(true)
								       {   
								    	   //System..out.println(" ------------------------------- This thread has responsibility of dbs , will wait 10 seconds ----------------------------------------------");
								    	   
								    	   Boolean runcheck = true;
								    	   querydb qdbobject = null;
								    	   
								    	   try {
								    		   //System..out.println("Thread will eait for 10 seconds to execute");
												Thread.sleep(10000); //runs in every 30 seconds
												
											} catch (InterruptedException e3) 
									        {
												// TODO Auto-generated catch block
												e3.printStackTrace();
											}
								    	   
								    	  
								    	   
								    	  
								    	   //System..out.println(" ------------------------------- boolean assigned  ----------------------------------------------"+runcheck);
								    
				  
								    		   //System..out.println("Thread should check cluster run status");
								    		   runcheck =  new DbCommands().checkrunstatus();
								    		   
								    		  // //System..out.println("Cluster Check  run status is "+runcheck);
								    		   
								    		   if(runcheck) //if true,means running
								    		   {
								    			   	  //System..out.println(" ------- Something else is being processed, So will wait for it to complete ----");								
								    		   }
								    		   
								    		   
								    		   else
								    		   {
								    			   //System..out.println("//Cluster should be free to run script");								    			   
													    			   
								    			     qdbobject =  new DbCommands().getfiletoprocess();
								    			   
								    				  if(qdbobject != null)
										    		  {	
										    		  
										    		  //System..out.println(" ------- To run :----"+qdbobject.toString());								
										    		  
										    		  scriptexecuter scriptexce = new scriptexecuter();
										  
										    		  String checkvar="Execution Error";					
										    		
										    		  new DbCommands().updatequerydb(qdbobject.getFile_key(),null,"Processing");
										    		  
										    		  try
										    		  {
										    			  //System..out.println("Now eill execute script on cluster");
										    			  
										    			  checkvar = scriptexce.testrun("/satamas/PmiRExAtInputs/"+qdbobject.getFile_key()+".fasta", qdbobject.getFile_key()+"_output" , qdbobject.getPident() , qdbobject.getMismatch(), qdbobject.getQuerycoverage());
										    		  }
										    		  
										    		  catch(Exception e)
										    		  {
										    			  //System..out.println("Error in executing script");
										    			  e.printStackTrace();  
										    		  }
										    		  
										    		  Date currenttime = Calendar.getInstance().getTime();
										    		  
										    		  new DbCommands().updatequerydb(qdbobject.getFile_key(),currenttime,checkvar);
										    		  
										    		  SendEmailWithAttachments scobj = new SendEmailWithAttachments();
				                        		    	
				                        		    	try 
				                        		    	{
				                        					 scobj.sendEmail(qdbobject.getEmail(), "Pmirexquery REsults", "Please find your result file attached with this email", "/satamas/pmiRexatresults/"+qdbobject.getFile_key()+"_output");
				                        				   
				                        		    	} 
				                        		    	
				                        		    	catch (MessagingException e1) 
				                        		    	{
				                        					// TODO Auto-generated catch block
				                        					e1.printStackTrace();
				                        				}
				                        		    		//System..out.println(" ------------------------------- This thread is done with its  work  ----------------------------------------------");
									    		      }
								    				  
								    				  else
								    				  {
								    					  //System..out.println(" --- No  query work on ---");
								 
								    				  }
								    		   }
								       }
								
									}
						    };
            	
            	
            	
                        	Runnable basic = new Runnable() 
                        	{
                        		public void run()
                        		{
                        			try
                        			{              
                        				//	//System..out.println(" ---- Excel to db mapper should be called ----");
                        				  if(!(new DbCommands().checkDatabase("MaizePojo")))
                                          {
                        					  new Exceltodbmapper().Readexcel("inproject","maize.xlsx");
                                          }
                        				  
                        				  if(!(new DbCommands().checkDatabase("WheatPojo")))
                        				  {
                        				     new Exceltodbmapper().Readexcel("inproject","wheat.xlsx");	     	                      				
                        				  }
                        				  
                        				  if(!(new DbCommands().checkDatabase("RicePojo")))
                        				  {
                        				      new Exceltodbmapper().Readexcel("inproject","rice.xlsx");
                        				  }
                        				
                        				  if(!(new DbCommands().checkDatabase("Seqno")))
                        				  {
                        				   new Exceltodbmapper().Readexcel2("inproject","sequenceexcel.xlsx");
                        				  }
                        				  
                        				 new datasetgetter().getdatasets("wheat");
                    		             new datasetgetter().getdatasets("rice");
                    		             new datasetgetter().getdatasets("maize");
                    		             
                    		             
                    		             
                    		             new RnaGetter().getmiRlist("wheat");
                    		             new RnaGetter().getmiRlist("rice");
                    		             new RnaGetter().getmiRlist("maize");
                    		          
                    		             crossspecies_resultgen crossgen = new crossspecies_resultgen();
                    		             
                    		          
                    		             
                    		             sc.setAttribute("result_wm",crossgen.get_wm());
                    		             
                    		             sc.setAttribute("result_wr", crossgen.get_wr());
                    		             
                    		            sc.setAttribute("result_mr", crossgen.get_mr());
                    		            
                    		            sc.setAttribute("result_all", crossgen.get_all());
                    		             
                    		             
                    		             // --- Code for getting common list bw Wheat and Rice ends ----- //
    		             
                    		          //   new scriptexecuter().testrun("/satamas/PmiRExAtInputs/wheatmiR.txt","Resultfile","30","30","30");
                    		             
                    		             while(true)
                    		              {
                    		                 
                    		            	 String time = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
                
                    		            	 // Code for running sql command on 11:45
                    		            	 
                    		            	 if(time.equals("23:45"))
                    		            	 {
                    		            		 ////System..out.println("Time for running bot");
                    		            		 new search_result_generator().searchdb("zma-miR168a-3p", "maize" ,new datasetgetter().getdatasets("maize"));   
                    		            	 }
                    		            	 
                    		            	 // ----------- Code ends for this
                    		            	 
                    		            	 Thread.sleep(60000);
                    		             }     
                    		             
                        			}
                        			
                        			catch (SecurityException e)
                        			{
                        				// TODO Auto-generated catch block
                        				e.printStackTrace();
                        			} catch (NoSuchMethodException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
                        		}
							};
							
							//Thread is executed
							
							Thread dbcheck_thread = new Thread(dbchecks);
							dbcheck_thread.start();
							
							Thread basicthread=new Thread(basic);
							basicthread.start();	
							
							
        		}
        		finally
        		{
        			//System..out.println(" -- Finally called -- ");
        		}

    }


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
