package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class mircategory 
{
	 @Id	
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int primkey;
	 
	 public int getPrimkey() {
		return primkey;
	}
	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}
	public String getMir_ids() {
		return mir_ids;
	}
	public void setMir_ids(String mir_ids) {
		this.mir_ids = mir_ids;
	}
	public String getSpecies() {
		return species;
	}
	public void setSpecies(String species) {
		this.species = species;
	}
	public String getDatabasesource() {
		return databasesource;
	}
	public void setDatabasesource(String databasesource) {
		this.databasesource = databasesource;
	}
	
	
	
	 String mir_ids;
	 String species;
	 String databasesource;
     String accessurl;

	public String getAccessurl() {
		return accessurl;
	}
	public void setAccessurl(String accessurl) {
		this.accessurl = accessurl;
	}
}
