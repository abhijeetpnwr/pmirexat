package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class student
{
	 @Id
	 private int userId;
	 @Override
	public String toString() {
		return "student [userId=" + userId + ", userName=" + userName + "]";
	}
	private String userName;

	 public int getUserId()
	 {
		return userId;
	 } 
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
}
