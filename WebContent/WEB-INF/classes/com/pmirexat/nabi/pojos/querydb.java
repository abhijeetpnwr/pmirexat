package com.pmirexat.nabi.pojos;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class querydb 
{
	
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int primarykey;

@Override
public String toString() {
	return "querydb [primarykey=" + primarykey + ", starttime=" + starttime
			+ ", endtime=" + endtime + ", file_key=" + file_key + ", email="
			+ email + ", status=" + status + ", querycoverage=" + querycoverage
			+ ", pident=" + pident + ", mismatch=" + mismatch + "]";
}

public int getPrimarykey() {
	return primarykey;
}

public void setPrimarykey(int primarykey)
{
	this.primarykey = primarykey;
}

public Date getStarttime() 
{
	return starttime;
}

public void setStarttime(Date starttime) 
{
	this.starttime = starttime;
}

public Date getEndtime() 
{
	return endtime;
}

public void setEndtime(Date endtime)
{
	this.endtime = endtime;
}



public String getFile_key() {
	return file_key;
}

public void setFile_key(String file_key) {
	this.file_key = file_key;
}

public String getEmail() 
{
	return email;
}

public void setEmail(String email) 
{
	this.email = email;
}

private Date starttime;

private Date endtime;

private String file_key;

private String email;

private String status;

private String querycoverage;

public String getQuerycoverage() {
	return querycoverage;
}

public void setQuerycoverage(String querycoverage) {
	this.querycoverage = querycoverage;
}

public String getStatus()
{
	return status;
}

public String getPident() {
	return pident;
}

public void setPident(String pident) {
	this.pident = pident;
}

public String getMismatch() {
	return mismatch;
}

public void setMismatch(String mismatch) {
	this.mismatch = mismatch;
}

public String pident;

public String mismatch;


public void setStatus(String status) 
{
	this.status = status;
}
	
}


