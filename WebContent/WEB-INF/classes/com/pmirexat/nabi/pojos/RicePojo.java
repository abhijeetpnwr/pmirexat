package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name="ricedb")
public class RicePojo 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	public int getPrimkey() {
		return primkey;
	}
	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}
	private String mir_ids,sequence,sourcedb,accessurl;
	private int Rice_SRR037746_Pooledtisssuesample,Rice_ERR035780_SeedlingRoot14days,Rice_SRR172845_RootCG14cultivar_sRNAlibrary,Rice_SRR521267_Root_4leafstageseedling,Rice_SRR771494_Root_nipponbare,Rice_SRR771495_Root_nipponbare,Rice_SRR771496_Shoot_NipponbareRSF,Rice_SRR771497_Shoot_NipponbareRSG,Rice_SRR711312_leaf_wildtype,Rice_SRR711313_leaf_wildtype,Rice_SRR711314_leaf_wildtype,Rice_SRR711315_leafT2_PiZt11_R,Rice_SRR711316_leafT2_PiZt11_R,Rice_SRR711317_leafT4_PiZt_11_R,Rice_SRR711318_leafT4_PiZt_11_S,Rice_SRR711319_leafT6_PiZt_11_R,Rice_SRR711320_leafT6_Pi9_R,Rice_SRR711321_leafT6_Spin1i_1_R,Rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary,Rice_SRR020461_leaf_floweringStage,Rice_SRR020462_leaf_floweringStage,Rice_SRR521266_rufi_leaf_unique,Rice_ERR035779_14dayseedlingleaf,Rice_SRR643880_leaf_Control,Rice_SRR172846_O_glaberrimapaniclesRNAlibrary,Rice_SRX272936_4Runpanicle,Rice_SRR203039_Anther_TCP,Rice_ERR035777_Anther_BCP,Rice_ERR035778_seedlingOnemonthCallus,Rice_ERR035781_Anther_TCP,Rice_ERR035782_Anther_UNM,Rice_SRR771499_Embryo_Kitaake,Rice_SRR771501_Embryo_Kitaake,Rice_SRR771504_Embryo_F1kitaake_nipponbare,Rice_SRR771506_Embryo_F1kitaake_nipponbare,Rice_SRR771498_Endosperm_Nipponbare,Rice_SRR771500_Endosperm_KITAAKE,Rice_SRR771505_Endosperm_F1kitaake_nipponbare,Rice_SRR771507_Endosperm_F1kitaake_nipponbare,Rice_ERR037689_seedlingCold14Day,Rice_ERR037690_seedlingDraught14Day,Rice_ERR037691_seedlingDrought14Day_SKUDTl4,Rice_ERR037692_seedlingSaltstress14Day,Rice_ERR037693_seedlingSalt14Day_SKUSTL2,Rice_ERR037694_seedlingControl14Day,Rice_ERR037695_seedlingControl14Day,Rice_SRR032099_seedling_ProteinComplex_AGO1a,Rice_SRR032100_seedling_ProteinComplex_AGO1b,Rice_SRR032101_seedling_proteinComplex_AGO1c,Rice_SRR032102_seedling_ProteinComplex_totalextreact,Rice_SRR062265_seedlingControl12Day,Rice_SRR062266_seedlingH2O212Day,Rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage;
	
	private float root_foldchange,shoot_foldchange,leaf_foldchange,panicle_foldchange,anther_foldchange,embryo_foldchange,endosperm_foldchange,seedling_foldchange;
	
	public float getRoot_foldchange() {
		return root_foldchange;
	}
	public void setRoot_foldchange(float root_foldchange) {
		this.root_foldchange = root_foldchange;
	}
	public float getShoot_foldchange() {
		return shoot_foldchange;
	}
	public void setShoot_foldchange(float shoot_foldchange) {
		this.shoot_foldchange = shoot_foldchange;
	}
	public float getLeaf_foldchange() {
		return leaf_foldchange;
	}
	public void setLeaf_foldchange(float leaf_foldchange) {
		this.leaf_foldchange = leaf_foldchange;
	}
	public float getPanicle_foldchange() {
		return panicle_foldchange;
	}
	public void setPanicle_foldchange(float panicle_foldchange) {
		this.panicle_foldchange = panicle_foldchange;
	}
	public float getAnther_foldchange() {
		return anther_foldchange;
	}
	public void setAnther_foldchange(float anther_foldchange) {
		this.anther_foldchange = anther_foldchange;
	}
	public float getEmbryo_foldchange() {
		return embryo_foldchange;
	}
	public void setEmbryo_foldchange(float embryo_foldchange) {
		this.embryo_foldchange = embryo_foldchange;
	}
	public float getEndosperm_foldchange() {
		return endosperm_foldchange;
	}
	public void setEndosperm_foldchange(float endosperm_foldchange) {
		this.endosperm_foldchange = endosperm_foldchange;
	}
	public float getSeedling_foldchange() {
		return seedling_foldchange;
	}
	public void setSeedling_foldchange(float seedling_foldchange) {
		this.seedling_foldchange = seedling_foldchange;
	}
	public String getMir_ids() {
		return mir_ids;
	}
	public void setMir_ids(String mir_ids) {
		this.mir_ids = mir_ids;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getSourcedb() {
		return sourcedb;
	}
	public void setSourcedb(String sourcedb) {
		this.sourcedb = sourcedb;
	}
	public String getAccessurl() {
		return accessurl;
	}
	public void setAccessurl(String accessurl) {
		this.accessurl = accessurl;
	}
	public int getRice_SRR037746_Pooledtisssuesample() {
		return Rice_SRR037746_Pooledtisssuesample;
	}
	public void setRice_SRR037746_Pooledtisssuesample(
			int rice_SRR037746_Pooledtisssuesample) {
		Rice_SRR037746_Pooledtisssuesample = rice_SRR037746_Pooledtisssuesample;
	}
	public int getRice_ERR035780_SeedlingRoot14days() {
		return Rice_ERR035780_SeedlingRoot14days;
	}
	public void setRice_ERR035780_SeedlingRoot14days(
			int rice_ERR035780_SeedlingRoot14days) {
		Rice_ERR035780_SeedlingRoot14days = rice_ERR035780_SeedlingRoot14days;
	}
	public int getRice_SRR172845_RootCG14cultivar_sRNAlibrary() {
		return Rice_SRR172845_RootCG14cultivar_sRNAlibrary;
	}
	public void setRice_SRR172845_RootCG14cultivar_sRNAlibrary(
			int rice_SRR172845_RootCG14cultivar_sRNAlibrary) {
		Rice_SRR172845_RootCG14cultivar_sRNAlibrary = rice_SRR172845_RootCG14cultivar_sRNAlibrary;
	}
	public int getRice_SRR521267_Root_4leafstageseedling() {
		return Rice_SRR521267_Root_4leafstageseedling;
	}
	public void setRice_SRR521267_Root_4leafstageseedling(
			int rice_SRR521267_Root_4leafstageseedling) {
		Rice_SRR521267_Root_4leafstageseedling = rice_SRR521267_Root_4leafstageseedling;
	}
	public int getRice_SRR771494_Root_nipponbare() {
		return Rice_SRR771494_Root_nipponbare;
	}
	public void setRice_SRR771494_Root_nipponbare(int rice_SRR771494_Root_nipponbare) {
		Rice_SRR771494_Root_nipponbare = rice_SRR771494_Root_nipponbare;
	}
	public int getRice_SRR771495_Root_nipponbare() {
		return Rice_SRR771495_Root_nipponbare;
	}
	public void setRice_SRR771495_Root_nipponbare(int rice_SRR771495_Root_nipponbare) {
		Rice_SRR771495_Root_nipponbare = rice_SRR771495_Root_nipponbare;
	}
	public int getRice_SRR771496_Shoot_NipponbareRSF() {
		return Rice_SRR771496_Shoot_NipponbareRSF;
	}
	public void setRice_SRR771496_Shoot_NipponbareRSF(
			int rice_SRR771496_Shoot_NipponbareRSF) {
		Rice_SRR771496_Shoot_NipponbareRSF = rice_SRR771496_Shoot_NipponbareRSF;
	}
	public int getRice_SRR771497_Shoot_NipponbareRSG() {
		return Rice_SRR771497_Shoot_NipponbareRSG;
	}
	public void setRice_SRR771497_Shoot_NipponbareRSG(
			int rice_SRR771497_Shoot_NipponbareRSG) {
		Rice_SRR771497_Shoot_NipponbareRSG = rice_SRR771497_Shoot_NipponbareRSG;
	}
	public int getRice_SRR711312_leaf_wildtype() {
		return Rice_SRR711312_leaf_wildtype;
	}
	public void setRice_SRR711312_leaf_wildtype(int rice_SRR711312_leaf_wildtype) {
		Rice_SRR711312_leaf_wildtype = rice_SRR711312_leaf_wildtype;
	}
	public int getRice_SRR711313_leaf_wildtype() {
		return Rice_SRR711313_leaf_wildtype;
	}
	public void setRice_SRR711313_leaf_wildtype(int rice_SRR711313_leaf_wildtype) {
		Rice_SRR711313_leaf_wildtype = rice_SRR711313_leaf_wildtype;
	}
	public int getRice_SRR711314_leaf_wildtype() {
		return Rice_SRR711314_leaf_wildtype;
	}
	public void setRice_SRR711314_leaf_wildtype(int rice_SRR711314_leaf_wildtype) {
		Rice_SRR711314_leaf_wildtype = rice_SRR711314_leaf_wildtype;
	}
	public int getRice_SRR711315_leafT2_PiZt11_R() {
		return Rice_SRR711315_leafT2_PiZt11_R;
	}
	public void setRice_SRR711315_leafT2_PiZt11_R(int rice_SRR711315_leafT2_PiZt11_R) {
		Rice_SRR711315_leafT2_PiZt11_R = rice_SRR711315_leafT2_PiZt11_R;
	}
	public int getRice_SRR711316_leafT2_PiZt11_R() {
		return Rice_SRR711316_leafT2_PiZt11_R;
	}
	public void setRice_SRR711316_leafT2_PiZt11_R(int rice_SRR711316_leafT2_PiZt11_R) {
		Rice_SRR711316_leafT2_PiZt11_R = rice_SRR711316_leafT2_PiZt11_R;
	}
	public int getRice_SRR711317_leafT4_PiZt_11_R() {
		return Rice_SRR711317_leafT4_PiZt_11_R;
	}
	public void setRice_SRR711317_leafT4_PiZt_11_R(
			int rice_SRR711317_leafT4_PiZt_11_R) {
		Rice_SRR711317_leafT4_PiZt_11_R = rice_SRR711317_leafT4_PiZt_11_R;
	}
	public int getRice_SRR711318_leafT4_PiZt_11_S() {
		return Rice_SRR711318_leafT4_PiZt_11_S;
	}
	public void setRice_SRR711318_leafT4_PiZt_11_S(
			int rice_SRR711318_leafT4_PiZt_11_S) {
		Rice_SRR711318_leafT4_PiZt_11_S = rice_SRR711318_leafT4_PiZt_11_S;
	}
	public int getRice_SRR711319_leafT6_PiZt_11_R() {
		return Rice_SRR711319_leafT6_PiZt_11_R;
	}
	public void setRice_SRR711319_leafT6_PiZt_11_R(
			int rice_SRR711319_leafT6_PiZt_11_R) {
		Rice_SRR711319_leafT6_PiZt_11_R = rice_SRR711319_leafT6_PiZt_11_R;
	}
	public int getRice_SRR711320_leafT6_Pi9_R() {
		return Rice_SRR711320_leafT6_Pi9_R;
	}
	public void setRice_SRR711320_leafT6_Pi9_R(int rice_SRR711320_leafT6_Pi9_R) {
		Rice_SRR711320_leafT6_Pi9_R = rice_SRR711320_leafT6_Pi9_R;
	}
	public int getRice_SRR711321_leafT6_Spin1i_1_R() {
		return Rice_SRR711321_leafT6_Spin1i_1_R;
	}
	public void setRice_SRR711321_leafT6_Spin1i_1_R(
			int rice_SRR711321_leafT6_Spin1i_1_R) {
		Rice_SRR711321_leafT6_Spin1i_1_R = rice_SRR711321_leafT6_Spin1i_1_R;
	}
	public int getRice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary() {
		return Rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary;
	}
	public void setRice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary(
			int rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary) {
		Rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary = rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary;
	}
	public int getRice_SRR020461_leaf_floweringStage() {
		return Rice_SRR020461_leaf_floweringStage;
	}
	public void setRice_SRR020461_leaf_floweringStage(
			int rice_SRR020461_leaf_floweringStage) {
		Rice_SRR020461_leaf_floweringStage = rice_SRR020461_leaf_floweringStage;
	}
	public int getRice_SRR020462_leaf_floweringStage() {
		return Rice_SRR020462_leaf_floweringStage;
	}
	public void setRice_SRR020462_leaf_floweringStage(
			int rice_SRR020462_leaf_floweringStage) {
		Rice_SRR020462_leaf_floweringStage = rice_SRR020462_leaf_floweringStage;
	}
	public int getRice_SRR521266_rufi_leaf_unique() {
		return Rice_SRR521266_rufi_leaf_unique;
	}
	public void setRice_SRR521266_rufi_leaf_unique(
			int rice_SRR521266_rufi_leaf_unique) {
		Rice_SRR521266_rufi_leaf_unique = rice_SRR521266_rufi_leaf_unique;
	}
	public int getRice_ERR035779_14dayseedlingleaf() {
		return Rice_ERR035779_14dayseedlingleaf;
	}
	public void setRice_ERR035779_14dayseedlingleaf(
			int rice_ERR035779_14dayseedlingleaf) {
		Rice_ERR035779_14dayseedlingleaf = rice_ERR035779_14dayseedlingleaf;
	}
	public int getRice_SRR643880_leaf_Control() {
		return Rice_SRR643880_leaf_Control;
	}
	public void setRice_SRR643880_leaf_Control(int rice_SRR643880_leaf_Control) {
		Rice_SRR643880_leaf_Control = rice_SRR643880_leaf_Control;
	}
	public int getRice_SRR172846_O_glaberrimapaniclesRNAlibrary() {
		return Rice_SRR172846_O_glaberrimapaniclesRNAlibrary;
	}
	public void setRice_SRR172846_O_glaberrimapaniclesRNAlibrary(
			int rice_SRR172846_O_glaberrimapaniclesRNAlibrary) {
		Rice_SRR172846_O_glaberrimapaniclesRNAlibrary = rice_SRR172846_O_glaberrimapaniclesRNAlibrary;
	}
	public int getRice_SRX272936_4Runpanicle() {
		return Rice_SRX272936_4Runpanicle;
	}
	public void setRice_SRX272936_4Runpanicle(int rice_SRX272936_4Runpanicle) {
		Rice_SRX272936_4Runpanicle = rice_SRX272936_4Runpanicle;
	}
	public int getRice_SRR203039_Anther_TCP() {
		return Rice_SRR203039_Anther_TCP;
	}
	public void setRice_SRR203039_Anther_TCP(int rice_SRR203039_Anther_TCP) {
		Rice_SRR203039_Anther_TCP = rice_SRR203039_Anther_TCP;
	}
	public int getRice_ERR035777_Anther_BCP() {
		return Rice_ERR035777_Anther_BCP;
	}
	public void setRice_ERR035777_Anther_BCP(int rice_ERR035777_Anther_BCP) {
		Rice_ERR035777_Anther_BCP = rice_ERR035777_Anther_BCP;
	}
	public int getRice_ERR035778_seedlingOnemonthCallus() {
		return Rice_ERR035778_seedlingOnemonthCallus;
	}
	public void setRice_ERR035778_seedlingOnemonthCallus(
			int rice_ERR035778_seedlingOnemonthCallus) {
		Rice_ERR035778_seedlingOnemonthCallus = rice_ERR035778_seedlingOnemonthCallus;
	}
	public int getRice_ERR035781_Anther_TCP() {
		return Rice_ERR035781_Anther_TCP;
	}
	public void setRice_ERR035781_Anther_TCP(int rice_ERR035781_Anther_TCP) {
		Rice_ERR035781_Anther_TCP = rice_ERR035781_Anther_TCP;
	}
	public int getRice_ERR035782_Anther_UNM() {
		return Rice_ERR035782_Anther_UNM;
	}
	public void setRice_ERR035782_Anther_UNM(int rice_ERR035782_Anther_UNM) {
		Rice_ERR035782_Anther_UNM = rice_ERR035782_Anther_UNM;
	}
	public int getRice_SRR771499_Embryo_Kitaake() {
		return Rice_SRR771499_Embryo_Kitaake;
	}
	public void setRice_SRR771499_Embryo_Kitaake(int rice_SRR771499_Embryo_Kitaake) {
		Rice_SRR771499_Embryo_Kitaake = rice_SRR771499_Embryo_Kitaake;
	}
	public int getRice_SRR771501_Embryo_Kitaake() {
		return Rice_SRR771501_Embryo_Kitaake;
	}
	public void setRice_SRR771501_Embryo_Kitaake(int rice_SRR771501_Embryo_Kitaake) {
		Rice_SRR771501_Embryo_Kitaake = rice_SRR771501_Embryo_Kitaake;
	}
	public int getRice_SRR771504_Embryo_F1kitaake_nipponbare() {
		return Rice_SRR771504_Embryo_F1kitaake_nipponbare;
	}
	public void setRice_SRR771504_Embryo_F1kitaake_nipponbare(
			int rice_SRR771504_Embryo_F1kitaake_nipponbare) {
		Rice_SRR771504_Embryo_F1kitaake_nipponbare = rice_SRR771504_Embryo_F1kitaake_nipponbare;
	}
	public int getRice_SRR771506_Embryo_F1kitaake_nipponbare() {
		return Rice_SRR771506_Embryo_F1kitaake_nipponbare;
	}
	public void setRice_SRR771506_Embryo_F1kitaake_nipponbare(
			int rice_SRR771506_Embryo_F1kitaake_nipponbare) {
		Rice_SRR771506_Embryo_F1kitaake_nipponbare = rice_SRR771506_Embryo_F1kitaake_nipponbare;
	}
	public int getRice_SRR771498_Endosperm_Nipponbare() {
		return Rice_SRR771498_Endosperm_Nipponbare;
	}
	public void setRice_SRR771498_Endosperm_Nipponbare(
			int rice_SRR771498_Endosperm_Nipponbare) {
		Rice_SRR771498_Endosperm_Nipponbare = rice_SRR771498_Endosperm_Nipponbare;
	}
	public int getRice_SRR771500_Endosperm_KITAAKE() {
		return Rice_SRR771500_Endosperm_KITAAKE;
	}
	public void setRice_SRR771500_Endosperm_KITAAKE(
			int rice_SRR771500_Endosperm_KITAAKE) {
		Rice_SRR771500_Endosperm_KITAAKE = rice_SRR771500_Endosperm_KITAAKE;
	}
	public int getRice_SRR771505_Endosperm_F1kitaake_nipponbare() {
		return Rice_SRR771505_Endosperm_F1kitaake_nipponbare;
	}
	public void setRice_SRR771505_Endosperm_F1kitaake_nipponbare(
			int rice_SRR771505_Endosperm_F1kitaake_nipponbare) {
		Rice_SRR771505_Endosperm_F1kitaake_nipponbare = rice_SRR771505_Endosperm_F1kitaake_nipponbare;
	}
	public int getRice_SRR771507_Endosperm_F1kitaake_nipponbare() {
		return Rice_SRR771507_Endosperm_F1kitaake_nipponbare;
	}
	public void setRice_SRR771507_Endosperm_F1kitaake_nipponbare(
			int rice_SRR771507_Endosperm_F1kitaake_nipponbare) {
		Rice_SRR771507_Endosperm_F1kitaake_nipponbare = rice_SRR771507_Endosperm_F1kitaake_nipponbare;
	}
	public int getRice_ERR037689_seedlingCold14Day() {
		return Rice_ERR037689_seedlingCold14Day;
	}
	public void setRice_ERR037689_seedlingCold14Day(
			int rice_ERR037689_seedlingCold14Day) {
		Rice_ERR037689_seedlingCold14Day = rice_ERR037689_seedlingCold14Day;
	}
	public int getRice_ERR037690_seedlingDraught14Day() {
		return Rice_ERR037690_seedlingDraught14Day;
	}
	public void setRice_ERR037690_seedlingDraught14Day(
			int rice_ERR037690_seedlingDraught14Day) {
		Rice_ERR037690_seedlingDraught14Day = rice_ERR037690_seedlingDraught14Day;
	}
	public int getRice_ERR037691_seedlingDrought14Day_SKUDTl4() {
		return Rice_ERR037691_seedlingDrought14Day_SKUDTl4;
	}
	public void setRice_ERR037691_seedlingDrought14Day_SKUDTl4(
			int rice_ERR037691_seedlingDrought14Day_SKUDTl4) {
		Rice_ERR037691_seedlingDrought14Day_SKUDTl4 = rice_ERR037691_seedlingDrought14Day_SKUDTl4;
	}
	public int getRice_ERR037692_seedlingSaltstress14Day() {
		return Rice_ERR037692_seedlingSaltstress14Day;
	}
	public void setRice_ERR037692_seedlingSaltstress14Day(
			int rice_ERR037692_seedlingSaltstress14Day) {
		Rice_ERR037692_seedlingSaltstress14Day = rice_ERR037692_seedlingSaltstress14Day;
	}
	public int getRice_ERR037693_seedlingSalt14Day_SKUSTL2() {
		return Rice_ERR037693_seedlingSalt14Day_SKUSTL2;
	}
	public void setRice_ERR037693_seedlingSalt14Day_SKUSTL2(
			int rice_ERR037693_seedlingSalt14Day_SKUSTL2) {
		Rice_ERR037693_seedlingSalt14Day_SKUSTL2 = rice_ERR037693_seedlingSalt14Day_SKUSTL2;
	}
	public int getRice_ERR037694_seedlingControl14Day() {
		return Rice_ERR037694_seedlingControl14Day;
	}
	public void setRice_ERR037694_seedlingControl14Day(
			int rice_ERR037694_seedlingControl14Day) {
		Rice_ERR037694_seedlingControl14Day = rice_ERR037694_seedlingControl14Day;
	}
	public int getRice_ERR037695_seedlingControl14Day() {
		return Rice_ERR037695_seedlingControl14Day;
	}
	public void setRice_ERR037695_seedlingControl14Day(
			int rice_ERR037695_seedlingControl14Day) {
		Rice_ERR037695_seedlingControl14Day = rice_ERR037695_seedlingControl14Day;
	}
	public int getRice_SRR032099_seedling_ProteinComplex_AGO1a() {
		return Rice_SRR032099_seedling_ProteinComplex_AGO1a;
	}
	public void setRice_SRR032099_seedling_ProteinComplex_AGO1a(
			int rice_SRR032099_seedling_ProteinComplex_AGO1a) {
		Rice_SRR032099_seedling_ProteinComplex_AGO1a = rice_SRR032099_seedling_ProteinComplex_AGO1a;
	}
	public int getRice_SRR032100_seedling_ProteinComplex_AGO1b() {
		return Rice_SRR032100_seedling_ProteinComplex_AGO1b;
	}
	public void setRice_SRR032100_seedling_ProteinComplex_AGO1b(
			int rice_SRR032100_seedling_ProteinComplex_AGO1b) {
		Rice_SRR032100_seedling_ProteinComplex_AGO1b = rice_SRR032100_seedling_ProteinComplex_AGO1b;
	}
	public int getRice_SRR032101_seedling_proteinComplex_AGO1c() {
		return Rice_SRR032101_seedling_proteinComplex_AGO1c;
	}
	public void setRice_SRR032101_seedling_proteinComplex_AGO1c(
			int rice_SRR032101_seedling_proteinComplex_AGO1c) {
		Rice_SRR032101_seedling_proteinComplex_AGO1c = rice_SRR032101_seedling_proteinComplex_AGO1c;
	}
	public int getRice_SRR032102_seedling_ProteinComplex_totalextreact() {
		return Rice_SRR032102_seedling_ProteinComplex_totalextreact;
	}
	public void setRice_SRR032102_seedling_ProteinComplex_totalextreact(
			int rice_SRR032102_seedling_ProteinComplex_totalextreact) {
		Rice_SRR032102_seedling_ProteinComplex_totalextreact = rice_SRR032102_seedling_ProteinComplex_totalextreact;
	}
	public int getRice_SRR062265_seedlingControl12Day() {
		return Rice_SRR062265_seedlingControl12Day;
	}
	public void setRice_SRR062265_seedlingControl12Day(
			int rice_SRR062265_seedlingControl12Day) {
		Rice_SRR062265_seedlingControl12Day = rice_SRR062265_seedlingControl12Day;
	}
	public int getRice_SRR062266_seedlingH2O212Day() {
		return Rice_SRR062266_seedlingH2O212Day;
	}
	public void setRice_SRR062266_seedlingH2O212Day(
			int rice_SRR062266_seedlingH2O212Day) {
		Rice_SRR062266_seedlingH2O212Day = rice_SRR062266_seedlingH2O212Day;
	}
	public int getRice_SRX272880_8Run_inoculationB_glumaeSeedlingstage() {
		return Rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage;
	}
	public void setRice_SRX272880_8Run_inoculationB_glumaeSeedlingstage(
			int rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage) {
		Rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage = rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage;
	}


}
