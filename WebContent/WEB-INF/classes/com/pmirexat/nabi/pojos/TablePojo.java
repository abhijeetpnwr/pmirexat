package com.pmirexat.nabi.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Table;

@Entity
@javax.persistence.Table(name="tablepojo")
public class TablePojo
{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int primkey;
	
	private String mir_ids;
	 
	private String sequence;
	
	public int getPrimkey()
	{
		return primkey;
	}

	public void setPrimkey(int primkey) {
		this.primkey = primkey;
	}
	
	int Wheat_SRR101427_youngleafControl,Wheat_SRR101428_youngleafinfectionpowderymildew,Wheat_SRR101429_youngleafControl,Wheat_SRR101430_youngleafinfectionpowderymildew,Wheat_SRR101431_youngleafControl,Wheat_SRR101432_youngleafstress40Cheatstress,Wheat_SRR057406_FlagleafNAM_RNAi,Wheat_SRR449364_spiketissuesControl,Wheat_SRR449365_spiketissuesColdtreatment,Wheat_SRR449366_spiketissuesColdtreatment,Wheat_SRR449367_spiketissuesColdtreatment,Wheat_SRR449368_spiketissuesControl,Wheat_SRR449369_spiketissuesControl,Wheat_SRR449370_spiketissuesControl,Wheat_SRR800157_Genericsample,Wheat_SRR800158_Genericsample,Wheat_SRR017199_Genericsample,Wheat_SRR203489_wholeplantparentBBAA,Wheat_SRR203490_wholeplantparentDD,Wheat_SRR203491_wholeplantHybridBAD,Wheat_SRR203492_wholeplantPolyploidBBAADD,Rice_SRR037746_Pooledtisssuesample,Rice_ERR035780_SeedlingRoot14days,Rice_SRR172845_RootCG14cultivar_sRNAlibrary,Rice_SRR521267_Root_4leafstageseedling,Rice_SRR771494_Root_nipponbare,Rice_SRR771495_Root_nipponbare,Rice_SRR771496_Shoot_NipponbareRSF,Rice_SRR771497_Shoot_NipponbareRSG,Rice_SRR711312_leaf_wildtype,Rice_SRR711313_leaf_wildtype,Rice_SRR711314_leaf_wildtype,Rice_SRR711315_leafT2_PiZt11_R,Rice_SRR711316_leafT2_PiZt11_R,Rice_SRR711317_leafT4_PiZt_11_R,Rice_SRR711318_leafT4_PiZt_11_S,Rice_SRR711319_leafT6_PiZt_11_R,Rice_SRR711320_leafT6_Pi9_R,Rice_SRR711321_leafT6_Spin1i_1_R,Rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary,Rice_SRR020461_leaf_floweringStage,Rice_SRR020462_leaf_floweringStage,Rice_SRR521266_rufi_leaf_unique,Rice_ERR035779_14dayseedlingleaf,Rice_SRR643880_leaf_Control,Rice_SRR172846_O_glaberrimapaniclesRNAlibrary,Rice_SRX272936_4Runpanicle,Rice_SRR203039_Anther_TCP,Rice_ERR035777_Anther_BCP,Rice_ERR035778_seedlingOnemonthCallus,Rice_ERR035781_Anther_TCP,Rice_ERR035782_Anther_UNM,Rice_SRR771499_Embryo_Kitaake,Rice_SRR771501_Embryo_Kitaake,Rice_SRR771504_Embryo_F1kitaake_nipponbare,Rice_SRR771506_Embryo_F1kitaake_nipponbare,Rice_SRR771498_Endosperm_Nipponbare,Rice_SRR771500_Endosperm_KITAAKE,Rice_SRR771505_Endosperm_F1kitaake_nipponbare,Rice_SRR771507_Endosperm_F1kitaake_nipponbare,Rice_ERR037689_seedlingCold14Day,Rice_ERR037690_seedlingDraught14Day,Rice_ERR037691_seedlingDrought14Day_SKUDTl4,Rice_ERR037692_seedlingSaltstress14Day,Rice_ERR037693_seedlingSalt14Day_SKUSTL2,Rice_ERR037694_seedlingControl14Day,Rice_ERR037695_seedlingControl14Day,Rice_SRR032099_seedling_ProteinComplex_AGO1a,Rice_SRR032100_seedling_ProteinComplex_AGO1b,Rice_SRR032101_seedling_proteinComplex_AGO1c,Rice_SRR032102_seedling_ProteinComplex_totalextreact,Rice_SRR062265_seedlingControl12Day,Rice_SRR062266_seedlingH2O212Day,Rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage,Maize_SRR899546_wholeplant_Control,Maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae,Maize_SRR218319_roottipB73,Maize_SRR768485_15DaySeedlingroottipOptimalnitrate,Maize_SRR768487_15DaySeedlingroottipLownitrate,Maize_SRR488770_seedlingShootapexB73_11Day,Maize_SRR488771_seedlingShootapexMo17_11Day,Maize_SRR488772_seedlingShootapexB73xMo17_11Day,Maize_SRR488773_seedlingShootapexMo17xB73_11Day,Maize_SRR488774_seedlingShootapexB73_14Day,Maize_SRR488775_seedlingshootapexMo17_14Day,Maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant,Maize_SRR924284_SeedlingleafB73_hen1_1mutant,Maize_SRR034096_SeedlingleafB73,Maize_SRR768489_15DaySeedlingleafLownitrate,Maize_SRR768492_15DaySeedlingleafOptimalnitrate,Maize_SRR488776_developingearB73HighN,Maize_SRR488777_developingearMo17HighN,Maize_SRR488778_developingearB73xMo17HighN,Maize_SRR488779_developingearB73LowN,Maize_SRR488780_developingearMo17LowN,Maize_SRR488781_developingearB73xMo17LowN,Maize_SRR317192_anthers1MM,Maize_SRR317193_anthers1_5MM,Maize_SRR317194_anthers2MM,Maize_SRR032087_ear,Maize_SRR032088_pollen,Maize_SRR032089_RootB73,Maize_SRR032090_seedlingB73,Maize_SRR032091_tassels,Maize_SRR034097_Femaleinflorescenceears_PoMS,Maize_SRR034098_Maleinflorescencetassels_PrMS,Maize_SRR768248_maturepollen,Maize_SRR768249_invitrogerminatedpollens,Maize_SRR768250_maturesilks,Maize_SRR768251_pollinatedsilks,Maize_SRR408793_unfertilizedouterear,Maize_GSM958935_13run_5DOC_B73_smRNA_rep1,Maize_SRR521124_5DOC_B73_smRNA_rep2,Maize_SRR521125_5DOC_B73_smRNA_rep3,Maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1,Maize_SRR521139_5DOC_Mo17_smRNA_rep2,Maize_SRR521140_5DOC_Mo17_smRNA_rep3;

	
	
	public String getMir_ids() {
		return mir_ids;
	}

	public void setMir_ids(String mir_ids) {
		this.mir_ids = mir_ids;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public int getWheat_SRR101427_youngleafControl() {
		return Wheat_SRR101427_youngleafControl;
	}

	public void setWheat_SRR101427_youngleafControl(
			int wheat_SRR101427_youngleafControl) {
		Wheat_SRR101427_youngleafControl = wheat_SRR101427_youngleafControl;
	}

	public int getWheat_SRR101428_youngleafinfectionpowderymildew() {
		return Wheat_SRR101428_youngleafinfectionpowderymildew;
	}

	public void setWheat_SRR101428_youngleafinfectionpowderymildew(
			int wheat_SRR101428_youngleafinfectionpowderymildew) {
		Wheat_SRR101428_youngleafinfectionpowderymildew = wheat_SRR101428_youngleafinfectionpowderymildew;
	}

	public int getWheat_SRR101429_youngleafControl() {
		return Wheat_SRR101429_youngleafControl;
	}

	public void setWheat_SRR101429_youngleafControl(
			int wheat_SRR101429_youngleafControl) {
		Wheat_SRR101429_youngleafControl = wheat_SRR101429_youngleafControl;
	}

	public int getWheat_SRR101430_youngleafinfectionpowderymildew() {
		return Wheat_SRR101430_youngleafinfectionpowderymildew;
	}

	public void setWheat_SRR101430_youngleafinfectionpowderymildew(
			int wheat_SRR101430_youngleafinfectionpowderymildew) {
		Wheat_SRR101430_youngleafinfectionpowderymildew = wheat_SRR101430_youngleafinfectionpowderymildew;
	}

	public int getWheat_SRR101431_youngleafControl() {
		return Wheat_SRR101431_youngleafControl;
	}

	public void setWheat_SRR101431_youngleafControl(
			int wheat_SRR101431_youngleafControl) {
		Wheat_SRR101431_youngleafControl = wheat_SRR101431_youngleafControl;
	}

	public int getWheat_SRR101432_youngleafstress40Cheatstress() {
		return Wheat_SRR101432_youngleafstress40Cheatstress;
	}

	public void setWheat_SRR101432_youngleafstress40Cheatstress(
			int wheat_SRR101432_youngleafstress40Cheatstress) {
		Wheat_SRR101432_youngleafstress40Cheatstress = wheat_SRR101432_youngleafstress40Cheatstress;
	}

	public int getWheat_SRR057406_FlagleafNAM_RNAi() {
		return Wheat_SRR057406_FlagleafNAM_RNAi;
	}

	public void setWheat_SRR057406_FlagleafNAM_RNAi(
			int wheat_SRR057406_FlagleafNAM_RNAi) {
		Wheat_SRR057406_FlagleafNAM_RNAi = wheat_SRR057406_FlagleafNAM_RNAi;
	}

	public int getWheat_SRR449364_spiketissuesControl() {
		return Wheat_SRR449364_spiketissuesControl;
	}

	public void setWheat_SRR449364_spiketissuesControl(
			int wheat_SRR449364_spiketissuesControl) {
		Wheat_SRR449364_spiketissuesControl = wheat_SRR449364_spiketissuesControl;
	}

	public int getWheat_SRR449365_spiketissuesColdtreatment() {
		return Wheat_SRR449365_spiketissuesColdtreatment;
	}

	public void setWheat_SRR449365_spiketissuesColdtreatment(
			int wheat_SRR449365_spiketissuesColdtreatment) {
		Wheat_SRR449365_spiketissuesColdtreatment = wheat_SRR449365_spiketissuesColdtreatment;
	}

	public int getWheat_SRR449366_spiketissuesColdtreatment() {
		return Wheat_SRR449366_spiketissuesColdtreatment;
	}

	public void setWheat_SRR449366_spiketissuesColdtreatment(
			int wheat_SRR449366_spiketissuesColdtreatment) {
		Wheat_SRR449366_spiketissuesColdtreatment = wheat_SRR449366_spiketissuesColdtreatment;
	}

	public int getWheat_SRR449367_spiketissuesColdtreatment() {
		return Wheat_SRR449367_spiketissuesColdtreatment;
	}

	public void setWheat_SRR449367_spiketissuesColdtreatment(
			int wheat_SRR449367_spiketissuesColdtreatment) {
		Wheat_SRR449367_spiketissuesColdtreatment = wheat_SRR449367_spiketissuesColdtreatment;
	}

	public int getWheat_SRR449368_spiketissuesControl() {
		return Wheat_SRR449368_spiketissuesControl;
	}

	public void setWheat_SRR449368_spiketissuesControl(
			int wheat_SRR449368_spiketissuesControl) {
		Wheat_SRR449368_spiketissuesControl = wheat_SRR449368_spiketissuesControl;
	}

	public int getWheat_SRR449369_spiketissuesControl() {
		return Wheat_SRR449369_spiketissuesControl;
	}

	public void setWheat_SRR449369_spiketissuesControl(
			int wheat_SRR449369_spiketissuesControl) {
		Wheat_SRR449369_spiketissuesControl = wheat_SRR449369_spiketissuesControl;
	}

	public int getWheat_SRR449370_spiketissuesControl() {
		return Wheat_SRR449370_spiketissuesControl;
	}

	public void setWheat_SRR449370_spiketissuesControl(
			int wheat_SRR449370_spiketissuesControl) {
		Wheat_SRR449370_spiketissuesControl = wheat_SRR449370_spiketissuesControl;
	}

	public int getWheat_SRR800157_Genericsample() {
		return Wheat_SRR800157_Genericsample;
	}

	public void setWheat_SRR800157_Genericsample(int wheat_SRR800157_Genericsample) {
		Wheat_SRR800157_Genericsample = wheat_SRR800157_Genericsample;
	}

	public int getWheat_SRR800158_Genericsample() {
		return Wheat_SRR800158_Genericsample;
	}

	public void setWheat_SRR800158_Genericsample(int wheat_SRR800158_Genericsample) {
		Wheat_SRR800158_Genericsample = wheat_SRR800158_Genericsample;
	}

	public int getWheat_SRR017199_Genericsample() {
		return Wheat_SRR017199_Genericsample;
	}

	public void setWheat_SRR017199_Genericsample(int wheat_SRR017199_Genericsample) {
		Wheat_SRR017199_Genericsample = wheat_SRR017199_Genericsample;
	}

	public int getWheat_SRR203489_wholeplantparentBBAA() {
		return Wheat_SRR203489_wholeplantparentBBAA;
	}

	public void setWheat_SRR203489_wholeplantparentBBAA(
			int wheat_SRR203489_wholeplantparentBBAA) {
		Wheat_SRR203489_wholeplantparentBBAA = wheat_SRR203489_wholeplantparentBBAA;
	}

	public int getWheat_SRR203490_wholeplantparentDD() {
		return Wheat_SRR203490_wholeplantparentDD;
	}

	public void setWheat_SRR203490_wholeplantparentDD(
			int wheat_SRR203490_wholeplantparentDD) {
		Wheat_SRR203490_wholeplantparentDD = wheat_SRR203490_wholeplantparentDD;
	}

	public int getWheat_SRR203491_wholeplantHybridBAD() {
		return Wheat_SRR203491_wholeplantHybridBAD;
	}

	public void setWheat_SRR203491_wholeplantHybridBAD(
			int wheat_SRR203491_wholeplantHybridBAD) {
		Wheat_SRR203491_wholeplantHybridBAD = wheat_SRR203491_wholeplantHybridBAD;
	}

	public int getWheat_SRR203492_wholeplantPolyploidBBAADD() {
		return Wheat_SRR203492_wholeplantPolyploidBBAADD;
	}

	public void setWheat_SRR203492_wholeplantPolyploidBBAADD(
			int wheat_SRR203492_wholeplantPolyploidBBAADD) {
		Wheat_SRR203492_wholeplantPolyploidBBAADD = wheat_SRR203492_wholeplantPolyploidBBAADD;
	}

	public int getRice_SRR037746_Pooledtisssuesample() {
		return Rice_SRR037746_Pooledtisssuesample;
	}

	public void setRice_SRR037746_Pooledtisssuesample(
			int rice_SRR037746_Pooledtisssuesample) {
		Rice_SRR037746_Pooledtisssuesample = rice_SRR037746_Pooledtisssuesample;
	}

	public int getRice_ERR035780_SeedlingRoot14days() {
		return Rice_ERR035780_SeedlingRoot14days;
	}

	public void setRice_ERR035780_SeedlingRoot14days(
			int rice_ERR035780_SeedlingRoot14days) {
		Rice_ERR035780_SeedlingRoot14days = rice_ERR035780_SeedlingRoot14days;
	}

	public int getRice_SRR172845_RootCG14cultivar_sRNAlibrary() {
		return Rice_SRR172845_RootCG14cultivar_sRNAlibrary;
	}

	public void setRice_SRR172845_RootCG14cultivar_sRNAlibrary(
			int rice_SRR172845_RootCG14cultivar_sRNAlibrary) {
		Rice_SRR172845_RootCG14cultivar_sRNAlibrary = rice_SRR172845_RootCG14cultivar_sRNAlibrary;
	}

	public int getRice_SRR521267_Root_4leafstageseedling() {
		return Rice_SRR521267_Root_4leafstageseedling;
	}

	public void setRice_SRR521267_Root_4leafstageseedling(
			int rice_SRR521267_Root_4leafstageseedling) {
		Rice_SRR521267_Root_4leafstageseedling = rice_SRR521267_Root_4leafstageseedling;
	}

	public int getRice_SRR771494_Root_nipponbare() {
		return Rice_SRR771494_Root_nipponbare;
	}

	public void setRice_SRR771494_Root_nipponbare(int rice_SRR771494_Root_nipponbare) {
		Rice_SRR771494_Root_nipponbare = rice_SRR771494_Root_nipponbare;
	}

	public int getRice_SRR771495_Root_nipponbare() {
		return Rice_SRR771495_Root_nipponbare;
	}

	public void setRice_SRR771495_Root_nipponbare(int rice_SRR771495_Root_nipponbare) {
		Rice_SRR771495_Root_nipponbare = rice_SRR771495_Root_nipponbare;
	}

	public int getRice_SRR771496_Shoot_NipponbareRSF() {
		return Rice_SRR771496_Shoot_NipponbareRSF;
	}

	public void setRice_SRR771496_Shoot_NipponbareRSF(
			int rice_SRR771496_Shoot_NipponbareRSF) {
		Rice_SRR771496_Shoot_NipponbareRSF = rice_SRR771496_Shoot_NipponbareRSF;
	}

	public int getRice_SRR771497_Shoot_NipponbareRSG() {
		return Rice_SRR771497_Shoot_NipponbareRSG;
	}

	public void setRice_SRR771497_Shoot_NipponbareRSG(
			int rice_SRR771497_Shoot_NipponbareRSG) {
		Rice_SRR771497_Shoot_NipponbareRSG = rice_SRR771497_Shoot_NipponbareRSG;
	}

	public int getRice_SRR711312_leaf_wildtype() {
		return Rice_SRR711312_leaf_wildtype;
	}

	public void setRice_SRR711312_leaf_wildtype(int rice_SRR711312_leaf_wildtype) {
		Rice_SRR711312_leaf_wildtype = rice_SRR711312_leaf_wildtype;
	}

	public int getRice_SRR711313_leaf_wildtype() {
		return Rice_SRR711313_leaf_wildtype;
	}

	public void setRice_SRR711313_leaf_wildtype(int rice_SRR711313_leaf_wildtype) {
		Rice_SRR711313_leaf_wildtype = rice_SRR711313_leaf_wildtype;
	}

	public int getRice_SRR711314_leaf_wildtype() {
		return Rice_SRR711314_leaf_wildtype;
	}

	public void setRice_SRR711314_leaf_wildtype(int rice_SRR711314_leaf_wildtype) {
		Rice_SRR711314_leaf_wildtype = rice_SRR711314_leaf_wildtype;
	}

	public int getRice_SRR711315_leafT2_PiZt11_R() {
		return Rice_SRR711315_leafT2_PiZt11_R;
	}

	public void setRice_SRR711315_leafT2_PiZt11_R(int rice_SRR711315_leafT2_PiZt11_R) {
		Rice_SRR711315_leafT2_PiZt11_R = rice_SRR711315_leafT2_PiZt11_R;
	}

	public int getRice_SRR711316_leafT2_PiZt11_R() {
		return Rice_SRR711316_leafT2_PiZt11_R;
	}

	public void setRice_SRR711316_leafT2_PiZt11_R(int rice_SRR711316_leafT2_PiZt11_R) {
		Rice_SRR711316_leafT2_PiZt11_R = rice_SRR711316_leafT2_PiZt11_R;
	}

	public int getRice_SRR711317_leafT4_PiZt_11_R() {
		return Rice_SRR711317_leafT4_PiZt_11_R;
	}

	public void setRice_SRR711317_leafT4_PiZt_11_R(
			int rice_SRR711317_leafT4_PiZt_11_R) {
		Rice_SRR711317_leafT4_PiZt_11_R = rice_SRR711317_leafT4_PiZt_11_R;
	}

	public int getRice_SRR711318_leafT4_PiZt_11_S() {
		return Rice_SRR711318_leafT4_PiZt_11_S;
	}

	public void setRice_SRR711318_leafT4_PiZt_11_S(
			int rice_SRR711318_leafT4_PiZt_11_S) {
		Rice_SRR711318_leafT4_PiZt_11_S = rice_SRR711318_leafT4_PiZt_11_S;
	}

	public int getRice_SRR711319_leafT6_PiZt_11_R() {
		return Rice_SRR711319_leafT6_PiZt_11_R;
	}

	public void setRice_SRR711319_leafT6_PiZt_11_R(
			int rice_SRR711319_leafT6_PiZt_11_R) {
		Rice_SRR711319_leafT6_PiZt_11_R = rice_SRR711319_leafT6_PiZt_11_R;
	}

	public int getRice_SRR711320_leafT6_Pi9_R() {
		return Rice_SRR711320_leafT6_Pi9_R;
	}

	public void setRice_SRR711320_leafT6_Pi9_R(int rice_SRR711320_leafT6_Pi9_R) {
		Rice_SRR711320_leafT6_Pi9_R = rice_SRR711320_leafT6_Pi9_R;
	}

	public int getRice_SRR711321_leafT6_Spin1i_1_R() {
		return Rice_SRR711321_leafT6_Spin1i_1_R;
	}

	public void setRice_SRR711321_leafT6_Spin1i_1_R(
			int rice_SRR711321_leafT6_Spin1i_1_R) {
		Rice_SRR711321_leafT6_Spin1i_1_R = rice_SRR711321_leafT6_Spin1i_1_R;
	}

	public int getRice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary() {
		return Rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary;
	}

	public void setRice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary(
			int rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary) {
		Rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary = rice_SRR172844_leaf_O_glaberrimaleafsRNAlibrary;
	}

	public int getRice_SRR020461_leaf_floweringStage() {
		return Rice_SRR020461_leaf_floweringStage;
	}

	public void setRice_SRR020461_leaf_floweringStage(
			int rice_SRR020461_leaf_floweringStage) {
		Rice_SRR020461_leaf_floweringStage = rice_SRR020461_leaf_floweringStage;
	}

	public int getRice_SRR020462_leaf_floweringStage() {
		return Rice_SRR020462_leaf_floweringStage;
	}

	public void setRice_SRR020462_leaf_floweringStage(
			int rice_SRR020462_leaf_floweringStage) {
		Rice_SRR020462_leaf_floweringStage = rice_SRR020462_leaf_floweringStage;
	}

	public int getRice_SRR521266_rufi_leaf_unique() {
		return Rice_SRR521266_rufi_leaf_unique;
	}

	public void setRice_SRR521266_rufi_leaf_unique(
			int rice_SRR521266_rufi_leaf_unique) {
		Rice_SRR521266_rufi_leaf_unique = rice_SRR521266_rufi_leaf_unique;
	}

	public int getRice_ERR035779_14dayseedlingleaf() {
		return Rice_ERR035779_14dayseedlingleaf;
	}

	public void setRice_ERR035779_14dayseedlingleaf(
			int rice_ERR035779_14dayseedlingleaf) {
		Rice_ERR035779_14dayseedlingleaf = rice_ERR035779_14dayseedlingleaf;
	}

	public int getRice_SRR643880_leaf_Control() {
		return Rice_SRR643880_leaf_Control;
	}

	public void setRice_SRR643880_leaf_Control(int rice_SRR643880_leaf_Control) {
		Rice_SRR643880_leaf_Control = rice_SRR643880_leaf_Control;
	}

	public int getRice_SRR172846_O_glaberrimapaniclesRNAlibrary() {
		return Rice_SRR172846_O_glaberrimapaniclesRNAlibrary;
	}

	public void setRice_SRR172846_O_glaberrimapaniclesRNAlibrary(
			int rice_SRR172846_O_glaberrimapaniclesRNAlibrary) {
		Rice_SRR172846_O_glaberrimapaniclesRNAlibrary = rice_SRR172846_O_glaberrimapaniclesRNAlibrary;
	}

	public int getRice_SRX272936_4Runpanicle() {
		return Rice_SRX272936_4Runpanicle;
	}

	public void setRice_SRX272936_4Runpanicle(int rice_SRX272936_4Runpanicle) {
		Rice_SRX272936_4Runpanicle = rice_SRX272936_4Runpanicle;
	}

	public int getRice_SRR203039_Anther_TCP() {
		return Rice_SRR203039_Anther_TCP;
	}

	public void setRice_SRR203039_Anther_TCP(int rice_SRR203039_Anther_TCP) {
		Rice_SRR203039_Anther_TCP = rice_SRR203039_Anther_TCP;
	}

	public int getRice_ERR035777_Anther_BCP() {
		return Rice_ERR035777_Anther_BCP;
	}

	public void setRice_ERR035777_Anther_BCP(int rice_ERR035777_Anther_BCP) {
		Rice_ERR035777_Anther_BCP = rice_ERR035777_Anther_BCP;
	}

	public int getRice_ERR035778_seedlingOnemonthCallus() {
		return Rice_ERR035778_seedlingOnemonthCallus;
	}

	public void setRice_ERR035778_seedlingOnemonthCallus(
			int rice_ERR035778_seedlingOnemonthCallus) {
		Rice_ERR035778_seedlingOnemonthCallus = rice_ERR035778_seedlingOnemonthCallus;
	}

	public int getRice_ERR035781_Anther_TCP() {
		return Rice_ERR035781_Anther_TCP;
	}

	public void setRice_ERR035781_Anther_TCP(int rice_ERR035781_Anther_TCP) {
		Rice_ERR035781_Anther_TCP = rice_ERR035781_Anther_TCP;
	}

	public int getRice_ERR035782_Anther_UNM() {
		return Rice_ERR035782_Anther_UNM;
	}

	public void setRice_ERR035782_Anther_UNM(int rice_ERR035782_Anther_UNM) {
		Rice_ERR035782_Anther_UNM = rice_ERR035782_Anther_UNM;
	}

	public int getRice_SRR771499_Embryo_Kitaake() {
		return Rice_SRR771499_Embryo_Kitaake;
	}

	public void setRice_SRR771499_Embryo_Kitaake(int rice_SRR771499_Embryo_Kitaake) {
		Rice_SRR771499_Embryo_Kitaake = rice_SRR771499_Embryo_Kitaake;
	}

	public int getRice_SRR771501_Embryo_Kitaake() {
		return Rice_SRR771501_Embryo_Kitaake;
	}

	public void setRice_SRR771501_Embryo_Kitaake(int rice_SRR771501_Embryo_Kitaake) {
		Rice_SRR771501_Embryo_Kitaake = rice_SRR771501_Embryo_Kitaake;
	}

	public int getRice_SRR771504_Embryo_F1kitaake_nipponbare() {
		return Rice_SRR771504_Embryo_F1kitaake_nipponbare;
	}

	public void setRice_SRR771504_Embryo_F1kitaake_nipponbare(
			int rice_SRR771504_Embryo_F1kitaake_nipponbare) {
		Rice_SRR771504_Embryo_F1kitaake_nipponbare = rice_SRR771504_Embryo_F1kitaake_nipponbare;
	}

	public int getRice_SRR771506_Embryo_F1kitaake_nipponbare() {
		return Rice_SRR771506_Embryo_F1kitaake_nipponbare;
	}

	public void setRice_SRR771506_Embryo_F1kitaake_nipponbare(
			int rice_SRR771506_Embryo_F1kitaake_nipponbare) {
		Rice_SRR771506_Embryo_F1kitaake_nipponbare = rice_SRR771506_Embryo_F1kitaake_nipponbare;
	}

	public int getRice_SRR771498_Endosperm_Nipponbare() {
		return Rice_SRR771498_Endosperm_Nipponbare;
	}

	public void setRice_SRR771498_Endosperm_Nipponbare(
			int rice_SRR771498_Endosperm_Nipponbare) {
		Rice_SRR771498_Endosperm_Nipponbare = rice_SRR771498_Endosperm_Nipponbare;
	}

	public int getRice_SRR771500_Endosperm_KITAAKE() {
		return Rice_SRR771500_Endosperm_KITAAKE;
	}

	public void setRice_SRR771500_Endosperm_KITAAKE(
			int rice_SRR771500_Endosperm_KITAAKE) {
		Rice_SRR771500_Endosperm_KITAAKE = rice_SRR771500_Endosperm_KITAAKE;
	}

	public int getRice_SRR771505_Endosperm_F1kitaake_nipponbare() {
		return Rice_SRR771505_Endosperm_F1kitaake_nipponbare;
	}

	public void setRice_SRR771505_Endosperm_F1kitaake_nipponbare(
			int rice_SRR771505_Endosperm_F1kitaake_nipponbare) {
		Rice_SRR771505_Endosperm_F1kitaake_nipponbare = rice_SRR771505_Endosperm_F1kitaake_nipponbare;
	}

	public int getRice_SRR771507_Endosperm_F1kitaake_nipponbare() {
		return Rice_SRR771507_Endosperm_F1kitaake_nipponbare;
	}

	public void setRice_SRR771507_Endosperm_F1kitaake_nipponbare(
			int rice_SRR771507_Endosperm_F1kitaake_nipponbare) {
		Rice_SRR771507_Endosperm_F1kitaake_nipponbare = rice_SRR771507_Endosperm_F1kitaake_nipponbare;
	}

	public int getRice_ERR037689_seedlingCold14Day() {
		return Rice_ERR037689_seedlingCold14Day;
	}

	public void setRice_ERR037689_seedlingCold14Day(
			int rice_ERR037689_seedlingCold14Day) {
		Rice_ERR037689_seedlingCold14Day = rice_ERR037689_seedlingCold14Day;
	}

	public int getRice_ERR037690_seedlingDraught14Day() {
		return Rice_ERR037690_seedlingDraught14Day;
	}

	public void setRice_ERR037690_seedlingDraught14Day(
			int rice_ERR037690_seedlingDraught14Day) {
		Rice_ERR037690_seedlingDraught14Day = rice_ERR037690_seedlingDraught14Day;
	}

	public int getRice_ERR037691_seedlingDrought14Day_SKUDTl4() {
		return Rice_ERR037691_seedlingDrought14Day_SKUDTl4;
	}

	public void setRice_ERR037691_seedlingDrought14Day_SKUDTl4(
			int rice_ERR037691_seedlingDrought14Day_SKUDTl4) {
		Rice_ERR037691_seedlingDrought14Day_SKUDTl4 = rice_ERR037691_seedlingDrought14Day_SKUDTl4;
	}

	public int getRice_ERR037692_seedlingSaltstress14Day() {
		return Rice_ERR037692_seedlingSaltstress14Day;
	}

	public void setRice_ERR037692_seedlingSaltstress14Day(
			int rice_ERR037692_seedlingSaltstress14Day) {
		Rice_ERR037692_seedlingSaltstress14Day = rice_ERR037692_seedlingSaltstress14Day;
	}

	public int getRice_ERR037693_seedlingSalt14Day_SKUSTL2() {
		return Rice_ERR037693_seedlingSalt14Day_SKUSTL2;
	}

	public void setRice_ERR037693_seedlingSalt14Day_SKUSTL2(
			int rice_ERR037693_seedlingSalt14Day_SKUSTL2) {
		Rice_ERR037693_seedlingSalt14Day_SKUSTL2 = rice_ERR037693_seedlingSalt14Day_SKUSTL2;
	}

	public int getRice_ERR037694_seedlingControl14Day() {
		return Rice_ERR037694_seedlingControl14Day;
	}

	public void setRice_ERR037694_seedlingControl14Day(
			int rice_ERR037694_seedlingControl14Day) {
		Rice_ERR037694_seedlingControl14Day = rice_ERR037694_seedlingControl14Day;
	}

	public int getRice_ERR037695_seedlingControl14Day() {
		return Rice_ERR037695_seedlingControl14Day;
	}

	public void setRice_ERR037695_seedlingControl14Day(
			int rice_ERR037695_seedlingControl14Day) {
		Rice_ERR037695_seedlingControl14Day = rice_ERR037695_seedlingControl14Day;
	}

	public int getRice_SRR032099_seedling_ProteinComplex_AGO1a() {
		return Rice_SRR032099_seedling_ProteinComplex_AGO1a;
	}

	public void setRice_SRR032099_seedling_ProteinComplex_AGO1a(
			int rice_SRR032099_seedling_ProteinComplex_AGO1a) {
		Rice_SRR032099_seedling_ProteinComplex_AGO1a = rice_SRR032099_seedling_ProteinComplex_AGO1a;
	}

	public int getRice_SRR032100_seedling_ProteinComplex_AGO1b() {
		return Rice_SRR032100_seedling_ProteinComplex_AGO1b;
	}

	public void setRice_SRR032100_seedling_ProteinComplex_AGO1b(
			int rice_SRR032100_seedling_ProteinComplex_AGO1b) {
		Rice_SRR032100_seedling_ProteinComplex_AGO1b = rice_SRR032100_seedling_ProteinComplex_AGO1b;
	}

	public int getRice_SRR032101_seedling_proteinComplex_AGO1c() {
		return Rice_SRR032101_seedling_proteinComplex_AGO1c;
	}

	public void setRice_SRR032101_seedling_proteinComplex_AGO1c(
			int rice_SRR032101_seedling_proteinComplex_AGO1c) {
		Rice_SRR032101_seedling_proteinComplex_AGO1c = rice_SRR032101_seedling_proteinComplex_AGO1c;
	}

	public int getRice_SRR032102_seedling_ProteinComplex_totalextreact() {
		return Rice_SRR032102_seedling_ProteinComplex_totalextreact;
	}

	public void setRice_SRR032102_seedling_ProteinComplex_totalextreact(
			int rice_SRR032102_seedling_ProteinComplex_totalextreact) {
		Rice_SRR032102_seedling_ProteinComplex_totalextreact = rice_SRR032102_seedling_ProteinComplex_totalextreact;
	}

	public int getRice_SRR062265_seedlingControl12Day() {
		return Rice_SRR062265_seedlingControl12Day;
	}

	public void setRice_SRR062265_seedlingControl12Day(
			int rice_SRR062265_seedlingControl12Day) {
		Rice_SRR062265_seedlingControl12Day = rice_SRR062265_seedlingControl12Day;
	}

	public int getRice_SRR062266_seedlingH2O212Day() {
		return Rice_SRR062266_seedlingH2O212Day;
	}

	public void setRice_SRR062266_seedlingH2O212Day(
			int rice_SRR062266_seedlingH2O212Day) {
		Rice_SRR062266_seedlingH2O212Day = rice_SRR062266_seedlingH2O212Day;
	}

	public int getRice_SRX272880_8Run_inoculationB_glumaeSeedlingstage() {
		return Rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage;
	}

	public void setRice_SRX272880_8Run_inoculationB_glumaeSeedlingstage(
			int rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage) {
		Rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage = rice_SRX272880_8Run_inoculationB_glumaeSeedlingstage;
	}

	public int getMaize_SRR899546_wholeplant_Control() {
		return Maize_SRR899546_wholeplant_Control;
	}

	public void setMaize_SRR899546_wholeplant_Control(
			int maize_SRR899546_wholeplant_Control) {
		Maize_SRR899546_wholeplant_Control = maize_SRR899546_wholeplant_Control;
	}

	public int getMaize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae() {
		return Maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae;
	}

	public void setMaize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae(
			int maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae) {
		Maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae = maize_SRR899547_wholeplantInoculatedHerbaspirillumseropedicae;
	}

	public int getMaize_SRR218319_roottipB73() {
		return Maize_SRR218319_roottipB73;
	}

	public void setMaize_SRR218319_roottipB73(int maize_SRR218319_roottipB73) {
		Maize_SRR218319_roottipB73 = maize_SRR218319_roottipB73;
	}

	public int getMaize_SRR768485_15DaySeedlingroottipOptimalnitrate() {
		return Maize_SRR768485_15DaySeedlingroottipOptimalnitrate;
	}

	public void setMaize_SRR768485_15DaySeedlingroottipOptimalnitrate(
			int maize_SRR768485_15DaySeedlingroottipOptimalnitrate) {
		Maize_SRR768485_15DaySeedlingroottipOptimalnitrate = maize_SRR768485_15DaySeedlingroottipOptimalnitrate;
	}

	public int getMaize_SRR768487_15DaySeedlingroottipLownitrate() {
		return Maize_SRR768487_15DaySeedlingroottipLownitrate;
	}

	public void setMaize_SRR768487_15DaySeedlingroottipLownitrate(
			int maize_SRR768487_15DaySeedlingroottipLownitrate) {
		Maize_SRR768487_15DaySeedlingroottipLownitrate = maize_SRR768487_15DaySeedlingroottipLownitrate;
	}

	public int getMaize_SRR488770_seedlingShootapexB73_11Day() {
		return Maize_SRR488770_seedlingShootapexB73_11Day;
	}

	public void setMaize_SRR488770_seedlingShootapexB73_11Day(
			int maize_SRR488770_seedlingShootapexB73_11Day) {
		Maize_SRR488770_seedlingShootapexB73_11Day = maize_SRR488770_seedlingShootapexB73_11Day;
	}

	public int getMaize_SRR488771_seedlingShootapexMo17_11Day() {
		return Maize_SRR488771_seedlingShootapexMo17_11Day;
	}

	public void setMaize_SRR488771_seedlingShootapexMo17_11Day(
			int maize_SRR488771_seedlingShootapexMo17_11Day) {
		Maize_SRR488771_seedlingShootapexMo17_11Day = maize_SRR488771_seedlingShootapexMo17_11Day;
	}

	public int getMaize_SRR488772_seedlingShootapexB73xMo17_11Day() {
		return Maize_SRR488772_seedlingShootapexB73xMo17_11Day;
	}

	public void setMaize_SRR488772_seedlingShootapexB73xMo17_11Day(
			int maize_SRR488772_seedlingShootapexB73xMo17_11Day) {
		Maize_SRR488772_seedlingShootapexB73xMo17_11Day = maize_SRR488772_seedlingShootapexB73xMo17_11Day;
	}

	public int getMaize_SRR488773_seedlingShootapexMo17xB73_11Day() {
		return Maize_SRR488773_seedlingShootapexMo17xB73_11Day;
	}

	public void setMaize_SRR488773_seedlingShootapexMo17xB73_11Day(
			int maize_SRR488773_seedlingShootapexMo17xB73_11Day) {
		Maize_SRR488773_seedlingShootapexMo17xB73_11Day = maize_SRR488773_seedlingShootapexMo17xB73_11Day;
	}

	public int getMaize_SRR488774_seedlingShootapexB73_14Day() {
		return Maize_SRR488774_seedlingShootapexB73_14Day;
	}

	public void setMaize_SRR488774_seedlingShootapexB73_14Day(
			int maize_SRR488774_seedlingShootapexB73_14Day) {
		Maize_SRR488774_seedlingShootapexB73_14Day = maize_SRR488774_seedlingShootapexB73_14Day;
	}

	public int getMaize_SRR488775_seedlingshootapexMo17_14Day() {
		return Maize_SRR488775_seedlingshootapexMo17_14Day;
	}

	public void setMaize_SRR488775_seedlingshootapexMo17_14Day(
			int maize_SRR488775_seedlingshootapexMo17_14Day) {
		Maize_SRR488775_seedlingshootapexMo17_14Day = maize_SRR488775_seedlingshootapexMo17_14Day;
	}

	public int getMaize_SRR924283_SeedlingleafB73xW22_hen1_1mutant() {
		return Maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant;
	}

	public void setMaize_SRR924283_SeedlingleafB73xW22_hen1_1mutant(
			int maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant) {
		Maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant = maize_SRR924283_SeedlingleafB73xW22_hen1_1mutant;
	}

	public int getMaize_SRR924284_SeedlingleafB73_hen1_1mutant() {
		return Maize_SRR924284_SeedlingleafB73_hen1_1mutant;
	}

	public void setMaize_SRR924284_SeedlingleafB73_hen1_1mutant(
			int maize_SRR924284_SeedlingleafB73_hen1_1mutant) {
		Maize_SRR924284_SeedlingleafB73_hen1_1mutant = maize_SRR924284_SeedlingleafB73_hen1_1mutant;
	}

	public int getMaize_SRR034096_SeedlingleafB73() {
		return Maize_SRR034096_SeedlingleafB73;
	}

	public void setMaize_SRR034096_SeedlingleafB73(
			int maize_SRR034096_SeedlingleafB73) {
		Maize_SRR034096_SeedlingleafB73 = maize_SRR034096_SeedlingleafB73;
	}

	public int getMaize_SRR768489_15DaySeedlingleafLownitrate() {
		return Maize_SRR768489_15DaySeedlingleafLownitrate;
	}

	public void setMaize_SRR768489_15DaySeedlingleafLownitrate(
			int maize_SRR768489_15DaySeedlingleafLownitrate) {
		Maize_SRR768489_15DaySeedlingleafLownitrate = maize_SRR768489_15DaySeedlingleafLownitrate;
	}

	public int getMaize_SRR768492_15DaySeedlingleafOptimalnitrate() {
		return Maize_SRR768492_15DaySeedlingleafOptimalnitrate;
	}

	public void setMaize_SRR768492_15DaySeedlingleafOptimalnitrate(
			int maize_SRR768492_15DaySeedlingleafOptimalnitrate) {
		Maize_SRR768492_15DaySeedlingleafOptimalnitrate = maize_SRR768492_15DaySeedlingleafOptimalnitrate;
	}

	public int getMaize_SRR488776_developingearB73HighN() {
		return Maize_SRR488776_developingearB73HighN;
	}

	public void setMaize_SRR488776_developingearB73HighN(
			int maize_SRR488776_developingearB73HighN) {
		Maize_SRR488776_developingearB73HighN = maize_SRR488776_developingearB73HighN;
	}

	public int getMaize_SRR488777_developingearMo17HighN() {
		return Maize_SRR488777_developingearMo17HighN;
	}

	public void setMaize_SRR488777_developingearMo17HighN(
			int maize_SRR488777_developingearMo17HighN) {
		Maize_SRR488777_developingearMo17HighN = maize_SRR488777_developingearMo17HighN;
	}

	public int getMaize_SRR488778_developingearB73xMo17HighN() {
		return Maize_SRR488778_developingearB73xMo17HighN;
	}

	public void setMaize_SRR488778_developingearB73xMo17HighN(
			int maize_SRR488778_developingearB73xMo17HighN) {
		Maize_SRR488778_developingearB73xMo17HighN = maize_SRR488778_developingearB73xMo17HighN;
	}

	public int getMaize_SRR488779_developingearB73LowN() {
		return Maize_SRR488779_developingearB73LowN;
	}

	public void setMaize_SRR488779_developingearB73LowN(
			int maize_SRR488779_developingearB73LowN) {
		Maize_SRR488779_developingearB73LowN = maize_SRR488779_developingearB73LowN;
	}

	public int getMaize_SRR488780_developingearMo17LowN() {
		return Maize_SRR488780_developingearMo17LowN;
	}

	public void setMaize_SRR488780_developingearMo17LowN(
			int maize_SRR488780_developingearMo17LowN) {
		Maize_SRR488780_developingearMo17LowN = maize_SRR488780_developingearMo17LowN;
	}

	public int getMaize_SRR488781_developingearB73xMo17LowN() {
		return Maize_SRR488781_developingearB73xMo17LowN;
	}

	public void setMaize_SRR488781_developingearB73xMo17LowN(
			int maize_SRR488781_developingearB73xMo17LowN) {
		Maize_SRR488781_developingearB73xMo17LowN = maize_SRR488781_developingearB73xMo17LowN;
	}

	public int getMaize_SRR317192_anthers1MM() {
		return Maize_SRR317192_anthers1MM;
	}

	public void setMaize_SRR317192_anthers1MM(int maize_SRR317192_anthers1MM) {
		Maize_SRR317192_anthers1MM = maize_SRR317192_anthers1MM;
	}

	public int getMaize_SRR317193_anthers1_5MM() {
		return Maize_SRR317193_anthers1_5MM;
	}

	public void setMaize_SRR317193_anthers1_5MM(int maize_SRR317193_anthers1_5MM) {
		Maize_SRR317193_anthers1_5MM = maize_SRR317193_anthers1_5MM;
	}

	public int getMaize_SRR317194_anthers2MM() {
		return Maize_SRR317194_anthers2MM;
	}

	public void setMaize_SRR317194_anthers2MM(int maize_SRR317194_anthers2MM) {
		Maize_SRR317194_anthers2MM = maize_SRR317194_anthers2MM;
	}

	public int getMaize_SRR032087_ear() {
		return Maize_SRR032087_ear;
	}

	public void setMaize_SRR032087_ear(int maize_SRR032087_ear) {
		Maize_SRR032087_ear = maize_SRR032087_ear;
	}

	public int getMaize_SRR032088_pollen() {
		return Maize_SRR032088_pollen;
	}

	public void setMaize_SRR032088_pollen(int maize_SRR032088_pollen) {
		Maize_SRR032088_pollen = maize_SRR032088_pollen;
	}

	public int getMaize_SRR032089_RootB73() {
		return Maize_SRR032089_RootB73;
	}

	public void setMaize_SRR032089_RootB73(int maize_SRR032089_RootB73) {
		Maize_SRR032089_RootB73 = maize_SRR032089_RootB73;
	}

	public int getMaize_SRR032090_seedlingB73() {
		return Maize_SRR032090_seedlingB73;
	}

	public void setMaize_SRR032090_seedlingB73(int maize_SRR032090_seedlingB73) {
		Maize_SRR032090_seedlingB73 = maize_SRR032090_seedlingB73;
	}

	public int getMaize_SRR032091_tassels() {
		return Maize_SRR032091_tassels;
	}

	public void setMaize_SRR032091_tassels(int maize_SRR032091_tassels) {
		Maize_SRR032091_tassels = maize_SRR032091_tassels;
	}

	public int getMaize_SRR034097_Femaleinflorescenceears_PoMS() {
		return Maize_SRR034097_Femaleinflorescenceears_PoMS;
	}

	public void setMaize_SRR034097_Femaleinflorescenceears_PoMS(
			int maize_SRR034097_Femaleinflorescenceears_PoMS) {
		Maize_SRR034097_Femaleinflorescenceears_PoMS = maize_SRR034097_Femaleinflorescenceears_PoMS;
	}

	public int getMaize_SRR034098_Maleinflorescencetassels_PrMS() {
		return Maize_SRR034098_Maleinflorescencetassels_PrMS;
	}

	public void setMaize_SRR034098_Maleinflorescencetassels_PrMS(
			int maize_SRR034098_Maleinflorescencetassels_PrMS) {
		Maize_SRR034098_Maleinflorescencetassels_PrMS = maize_SRR034098_Maleinflorescencetassels_PrMS;
	}

	public int getMaize_SRR768248_maturepollen() {
		return Maize_SRR768248_maturepollen;
	}

	public void setMaize_SRR768248_maturepollen(int maize_SRR768248_maturepollen) {
		Maize_SRR768248_maturepollen = maize_SRR768248_maturepollen;
	}

	public int getMaize_SRR768249_invitrogerminatedpollens() {
		return Maize_SRR768249_invitrogerminatedpollens;
	}

	public void setMaize_SRR768249_invitrogerminatedpollens(
			int maize_SRR768249_invitrogerminatedpollens) {
		Maize_SRR768249_invitrogerminatedpollens = maize_SRR768249_invitrogerminatedpollens;
	}

	public int getMaize_SRR768250_maturesilks() {
		return Maize_SRR768250_maturesilks;
	}

	public void setMaize_SRR768250_maturesilks(int maize_SRR768250_maturesilks) {
		Maize_SRR768250_maturesilks = maize_SRR768250_maturesilks;
	}

	public int getMaize_SRR768251_pollinatedsilks() {
		return Maize_SRR768251_pollinatedsilks;
	}

	public void setMaize_SRR768251_pollinatedsilks(
			int maize_SRR768251_pollinatedsilks) {
		Maize_SRR768251_pollinatedsilks = maize_SRR768251_pollinatedsilks;
	}

	public int getMaize_SRR408793_unfertilizedouterear() {
		return Maize_SRR408793_unfertilizedouterear;
	}

	public void setMaize_SRR408793_unfertilizedouterear(
			int maize_SRR408793_unfertilizedouterear) {
		Maize_SRR408793_unfertilizedouterear = maize_SRR408793_unfertilizedouterear;
	}

	public int getMaize_GSM958935_13run_5DOC_B73_smRNA_rep1() {
		return Maize_GSM958935_13run_5DOC_B73_smRNA_rep1;
	}

	public void setMaize_GSM958935_13run_5DOC_B73_smRNA_rep1(
			int maize_GSM958935_13run_5DOC_B73_smRNA_rep1) {
		Maize_GSM958935_13run_5DOC_B73_smRNA_rep1 = maize_GSM958935_13run_5DOC_B73_smRNA_rep1;
	}

	public int getMaize_SRR521124_5DOC_B73_smRNA_rep2() {
		return Maize_SRR521124_5DOC_B73_smRNA_rep2;
	}

	public void setMaize_SRR521124_5DOC_B73_smRNA_rep2(
			int maize_SRR521124_5DOC_B73_smRNA_rep2) {
		Maize_SRR521124_5DOC_B73_smRNA_rep2 = maize_SRR521124_5DOC_B73_smRNA_rep2;
	}

	public int getMaize_SRR521125_5DOC_B73_smRNA_rep3() {
		return Maize_SRR521125_5DOC_B73_smRNA_rep3;
	}

	public void setMaize_SRR521125_5DOC_B73_smRNA_rep3(
			int maize_SRR521125_5DOC_B73_smRNA_rep3) {
		Maize_SRR521125_5DOC_B73_smRNA_rep3 = maize_SRR521125_5DOC_B73_smRNA_rep3;
	}

	public int getMaize_GSM958938_13run_5DOC_Mo17_smRNA_rep1() {
		return Maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1;
	}

	public void setMaize_GSM958938_13run_5DOC_Mo17_smRNA_rep1(
			int maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1) {
		Maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1 = maize_GSM958938_13run_5DOC_Mo17_smRNA_rep1;
	}

	public int getMaize_SRR521139_5DOC_Mo17_smRNA_rep2() {
		return Maize_SRR521139_5DOC_Mo17_smRNA_rep2;
	}

	public void setMaize_SRR521139_5DOC_Mo17_smRNA_rep2(
			int maize_SRR521139_5DOC_Mo17_smRNA_rep2) {
		Maize_SRR521139_5DOC_Mo17_smRNA_rep2 = maize_SRR521139_5DOC_Mo17_smRNA_rep2;
	}

	public int getMaize_SRR521140_5DOC_Mo17_smRNA_rep3() {
		return Maize_SRR521140_5DOC_Mo17_smRNA_rep3;
	}

	public void setMaize_SRR521140_5DOC_Mo17_smRNA_rep3(
			int maize_SRR521140_5DOC_Mo17_smRNA_rep3) {
		Maize_SRR521140_5DOC_Mo17_smRNA_rep3 = maize_SRR521140_5DOC_Mo17_smRNA_rep3;
	}

	public TablePojo() {
		super();
	}

	

	
}
