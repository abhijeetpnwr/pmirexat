
/*defines class for json type.. might be.
  I need json object in json processing.Where it is being used.As we create class of Rna_Json
*/

package com.pmirexat.nabi.pojos;

public class Rna_Json 
{
    private final String label;
    private final String value;

    public Rna_Json(String _label, String _value) 
    {
        super();
        this.label = _label;
        this.value = _value;
    }

    public final String getLabel() {
        return this.label;
    }

    public final String getValue() {
        return this.value;
    }
}