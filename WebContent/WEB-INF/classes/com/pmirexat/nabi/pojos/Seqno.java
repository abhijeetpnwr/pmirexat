package com.pmirexat.nabi.pojos;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Seqno
{
    @Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int primkey;
	
   public int getPrimkey() {
	return primkey;
}
public double getSequence() {
	return sequence;
}
public void setSequence(double sequence) {
	this.sequence = sequence;
}
public void setPrimkey(int primkey) {
	this.primkey = primkey;
}
public String getDatasetname() {
	return datasetname;
}
public void setDatasetname(String datasetname) {
	this.datasetname = datasetname;
}

private String datasetname;
   private double sequence;


}
