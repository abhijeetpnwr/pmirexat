package com.pmirexat.nabi.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jws.WebService;

import sun.nio.cs.ext.TIS_620;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.class_entityconvert;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.pojos.WheatPojo;

@WebService(targetNamespace = "http://api.nabi.pmirexat.com/", endpointInterface = "com.pmirexat.nabi.api.PmiRExAtSEI", portName = "ApiClassPort", serviceName = "ApiClassService")
public class ApiClass implements PmiRExAtSEI 
{ 
	
	//Functionality Methods to get all MiRs, Sequence or datasets for a species  
    public List<String> getAllWheatMirs()
    {
    	return new RnaGetter().getmiRlist("wheat");
    }
    
    public List<String> getAllMaizeMirs()
    {
    	//System.out.println(" --------- Reached in getAllmaizemirs -----");
    	return new RnaGetter().getmiRlist("maize");	
    }
    
    public List<String> getAllRiceMirs()
    {
    	return new RnaGetter().getmiRlist("rice");
    }
    
    ////////////////////////////////////////
    public List<String> getAllWheatSequence()
    {
    	////System.out.println("In get all wheat seqs");
		List<String> wheatmirs = getAllWheatMirs();
		
		List<String> sequencelist = new ArrayList<String>();
		
		for (String string : wheatmirs) 
		{
		     sequencelist.add(getSequencebymiRid(string));	
		}
		
		System.out.println("Api will return ----------- "+sequencelist.size());
		
		
		return sequencelist;
    }
    
    public List<String> getAllMaizeSequence()
    {
    	
    	//System.out.println("In get all maize seqs");
    	
        List<String> maizemirs = getAllMaizeMirs();
		
		List<String> sequencelist = new ArrayList<String>();
		
		for (String string : maizemirs) 
		{
			////System.out.println(" adding :"+getSequencebymiRid(string)+"in maize seqs");
		     sequencelist.add(getSequencebymiRid(string));	
		}
		
		////System.out.println("From maizeseq. functionr esult is :"+sequencelist);
		
		return sequencelist; 	
    }
    
    public List<String> getAllRiceSequence()
    {
    	////System.out.println("In get all rice seqs");
        List<String> riceemirs = getAllRiceMirs();
		
		List<String> sequencelist = new ArrayList<String>();
		
		for (String string : riceemirs) 
		{
		     sequencelist.add(getSequencebymiRid(string));	   
		}
		
		//System.out.println("result from riceseqs :"+sequencelist);
		
		return sequencelist; 	
    }
    
    public List<String> getAllRiceDatasets()
    {
    	return new datasetgetter().getdatasets("rice");
    }
    
    ////////////////////////////////////////////////
    public List<String> getAllWheatDatasets()
    {
    	return new datasetgetter().getdatasets("wheat");
    }
    
    public List<String> getAllMaizeDatasets()
    {
    	return new datasetgetter().getdatasets("maize");
    }
    
    //Function -2 ---> For returning all miRids avaialbale with that sequence
    public List<String> getmiRidbysequene(String sequence)
    {
    	List<String> resultmirs = new ArrayList<String>();
    	
    	 String mirname = new DbCommands().getmirnam(sequence,new class_entityconvert().getclassname("wheat"));
    
    	 if(!(mirname == null))
    	 {
    	     resultmirs.add(mirname);	 
    	 }
    	 
    	 
    	 String mirname2 = new DbCommands().getmirnam(sequence,new class_entityconvert().getclassname("maize"));
    	 if(!(mirname2 == null))
    	 {
    		 resultmirs.add(mirname2);
    	 }
    	 
    	 String mirname3 = new DbCommands().getmirnam(sequence,new class_entityconvert().getclassname("rice"));
        
    	 if(!(mirname3 == null))
    	 {
    		 resultmirs.add(mirname3);
    	 }
    	 
    	 //System.out.println("Result is:"+resultmirs);
    	 
    	 return resultmirs;
    }
    
    
    //Function -3 --> Methods to find miR for a sequence in a specific species
    public String findmiRidbysequenceinWheat(String Sequence)
    {
    	 String mirname = new DbCommands().getmirnam(Sequence,new class_entityconvert().getclassname("wheat"));
       	 return mirname;  	
    }
    
    public String findmiRidbysequenceinMaize(String Sequence)
    {
    	 String mirname = new DbCommands().getmirnam(Sequence,new class_entityconvert().getclassname("maize"));
       	 return mirname;  		
    }
    
    
    public String findmiRidbysequenceinRice(String Sequence)
    {
    	String mirname = new DbCommands().getmirnam(Sequence,new class_entityconvert().getclassname("rice"));
      	 return mirname;  	 	
    }
    
    //Function -4 ----> Method to get sequence for a miR Id
    public String getSequencebymiRid(String miRid)
    {
    	
    	////System.out.println("  ------ Reached in getSequenceBymirId ----- ");
    	
    	String sequence = null;
    	
    	sequence = new DbCommands().getsequence(miRid,"RicePojo");
  	if(sequence != "")
    	{	
          	return sequence;
    	}
    	
    	sequence= new DbCommands().getsequence(miRid,"WheatPojo");
    	
    	if(sequence.trim() != "")
    	{
    		return sequence;
    	}
    	
    	sequence = new DbCommands().getsequence(miRid,"MaizePojo");  
    
    	if(sequence != "")
    	{
    		
    		 return sequence;
    	}
	
    	return sequence;
    }
 
    
    //Function 5 -->Method to search miRId against a dataset
    
    public Integer searchdsbymiRid(String miRid,String Dataset)
    {
    	Boolean chk =false;
    	////System.out.println("this is functionality 5 check , have not returned values , frist check it and then do it correct");
    	
    	int result = -1;
    	
        List<List<String>> temp = new ArrayList<List<String>>();
    	
    	if(getAllWheatMirs().contains(miRid) && getAllWheatDatasets().contains(Dataset))
    	{
    		chk = true;
    		
    		////System.out.println(" -- Both are in rice table -- ");
        
    		temp = new DbCommands().Specfic_ds_search0(miRid, Dataset, "WheatPojo");
    	}
    	
    	if(getAllMaizeMirs().contains(miRid) && getAllMaizeDatasets().contains(Dataset))
    	{
    		chk = true;
    		
    		////System.out.println(" -- Both falls in Maize -- ");
    		temp = new DbCommands().Specfic_ds_search0(miRid, Dataset, "MaizePojo");
    		
    	}
    	
    	if(getAllRiceMirs().contains(miRid) && getAllRiceDatasets().contains(Dataset))
    	{
    		chk =true;
    	//	//System.out.println(" -- both falls in Rice -- ");
    		
    		temp = new DbCommands().Specfic_ds_search0(miRid, Dataset, "RicePojo");
    	}
    	
    	for (Iterator iterator = temp.iterator(); iterator.hasNext();)
    	{
			List<String> list = (List<String>) iterator.next();
			 
			for (Iterator iterator2 = list.iterator(); iterator2.hasNext();)
			{
				String string = (String) iterator2.next();			
		         result = Integer.parseInt(string);
			}
		}
    	
    	//System.out.println(" \n ");
    	
        return result;
    }
    
    
    //Function 6 --> Method to search sequence for dataset
    public Integer searchWheatDbbySequence(String sequence,String dataset)
    {
    	
    	//System.out.println(" method searchWheatDbbySequence called ");
    	
    	    String miRid = findmiRidbysequenceinWheat(sequence);
    	  
    	
    	   	return searchdsbymiRid(miRid,dataset);
    	    
    }
    
    public Integer searchRiceDbbySequence(String sequence,String dataset)
    {
    	
    	//System.out.println(" method searchMaizeDbbySequence called ");
    	
    	    String miRid = findmiRidbysequenceinRice(sequence);
    	   
    	
    	   	return searchdsbymiRid(miRid,dataset);
    	    
    }
    
    public Integer searchMaizeDbbySequence(String sequence,String dataset)
    {
    	//System.out.println(" method searchRiceDbbySequence called ");
    	
    	    String miRid = findmiRidbysequenceinMaize(sequence);
    	   
    	
    	   	return searchdsbymiRid(miRid,dataset);
    	    
    }
    
    
    //Function 7---> Methods  to find tissues in a species
    public List<String> getalltisssuesinwheat()
    {
		List<String> tissuesinwheat = new ArrayList<String>();
		tissuesinwheat.add("Genericsample");
		tissuesinwheat.add("wholeplant");
		tissuesinwheat.add("leaf");
		tissuesinwheat.add("spike");
		
		return tissuesinwheat;
    }
    
    public List<String> getalltissuesinMaize()
    {
    	List<String> tissuesinmaize = new ArrayList<String>();
    	tissuesinmaize.add("Pooledtisssue");    	
    	tissuesinmaize.add("seedling");
    	tissuesinmaize.add("root"); 
    	tissuesinmaize.add("shoot"); 
    	tissuesinmaize.add("leaf"); 
    	tissuesinmaize.add("panicle"); 
    	tissuesinmaize.add("Anther"); 
    	tissuesinmaize.add("Embryo"); 
    	tissuesinmaize.add("Endosperm"); 
    	return tissuesinmaize;
    }
    
    public List<String> getalltissuesinRice()
    {
    	List<String> tissuesinrice = new ArrayList<String>();
    	tissuesinrice.add("Wholeplant");
    	tissuesinrice.add("seedling");
    	tissuesinrice.add("root");
    	tissuesinrice.add("ear");
    	tissuesinrice.add("Anther");
    	tissuesinrice.add("pollen");
    	tissuesinrice.add("tassel");
    	tissuesinrice.add("silk");
    	tissuesinrice.add("5DOC");
    	
    	return tissuesinrice;
    	
    }
    
    
    //  -- Methods to find dataset of a tissue in a species
    
    public List<String> getdsfortissueinwheat(String tissue)
    {
    	List<String> resulList = new ArrayList<String>() ;
    	if(getalltisssuesinwheat().contains(tissue))
    	{
    	
    		   List<String> wheatgetterlist =  getAllWheatDatasets();
				
    		   
    		   
    		   for (Object gettername : wheatgetterlist)
				{
				      // //System.out.println("value :"+gettername);	
				       
				       if((gettername.toString().toLowerCase()).contains( tissue.toLowerCase()))
				       {
				    	  // //System.out.println("Added :"+gettername.toString());
				    	   resulList.add(gettername.toString());
				       }
				}
			
    	}
    	else
    	{
    		//System.out.println("This tissue was not found in our wheat db");
    	}
    	
    	return resulList;
    }
    
    public List<String> getdsfortissueinMaize(String tissue)
    {
    	List<String> resulList = new ArrayList<String>();
    	
    	//System.out.println("maize ds: "+getalltissuesinMaize());
    	if(getalltissuesinMaize().contains(tissue))
    	{
    	
    		   List<String> maizegetterlist =  getAllMaizeDatasets();
				
    		   
    		   
    		   for (Object gettername :  maizegetterlist)
				{
				      // //System.out.println("value :"+gettername);	
				       
				       if((gettername.toString().toLowerCase()).contains( tissue.toLowerCase()))
				       {
				    	  //System.out.println("Added :"+gettername.toString());
				    	   resulList.add(gettername.toString());
				       }
				}
			
    	}
    	else
    	{
    		//System.out.println("This tissue was not found in our Maize db");
    	}
    	
    	return resulList;
    }
    
    
    public List<String> getdsfortissueinRice(String tissue)
    {
    	List<String> resulList = new ArrayList<String>();
    	
    	//System.out.println("Rice ds :"+getalltissuesinRice());
    	
    	if(getalltissuesinRice().contains(tissue))
    	{
    	
    		   List<String> ricegetterlist =  getAllRiceDatasets();
				
    		   
    		   
    		   for (Object gettername : ricegetterlist)
				{
				       ////System.out.println("value :"+gettername);	
				       
				       if((gettername.toString().toLowerCase()).contains( tissue.toLowerCase()))
				       {
				    	   //System.out.println("Gettername :"+gettername+" tissue name :"+tissue);
				    	   //System.out.println("Added :"+gettername.toString());
				    	   resulList.add(gettername.toString());
				       }
				}
			
    	}
    	else
    	{
    		//System.out.println("This tissue was not found in our rice db");
    	}
    	
    	return resulList;
    }
    
}
