package com.pmirexat.nabi.api;

import java.util.List;

import javax.jws.WebService;

@WebService(name = "PmiRExAtSEI", targetNamespace = "http://api.nabi.pmirexat.com/")
public interface PmiRExAtSEI {

	//Functionality Methods to get all MiRs, Sequence or datasets for a species  
	public List<String> getAllWheatMirs();

	public List<String> getAllMaizeMirs();

	public List<String> getAllRiceMirs();

	////////////////////////////////////////
	public List<String> getAllWheatSequence();

	public List<String> getAllMaizeSequence();

	public List<String> getAllRiceSequence();

	public List<String> getAllRiceDatasets();

	////////////////////////////////////////////////
	public List<String> getAllWheatDatasets();

	public List<String> getAllMaizeDatasets();

	//Function -2 ---> For returning all miRids avaialbale with that sequence
	public List<String> getmiRidbysequene(String sequence);

	//Function -3 --> Methods to find miR for a sequence in a specific species
	public String findmiRidbysequenceinWheat(String Sequence);

	public String findmiRidbysequenceinMaize(String Sequence);

	public String findmiRidbysequenceinRice(String Sequence);

	//Function -4 ----> Method to get sequence for a miR Id
	public String getSequencebymiRid(String miRid);

	public Integer searchdsbymiRid(String miRid, String Dataset);

	//Function 6 --> Method to search sequence for dataset
	public Integer searchWheatDbbySequence(String sequence, String dataset);

	public Integer searchRiceDbbySequence(String sequence, String dataset);

	public Integer searchMaizeDbbySequence(String sequence, String dataset);

	//Function 7---> Methods  to find tissues in a species
	public List<String> getalltisssuesinwheat();

	public List<String> getalltissuesinMaize();

	public List<String> getalltissuesinRice();

	public List<String> getdsfortissueinwheat(String tissue);

	public List<String> getdsfortissueinMaize(String tissue);

	public List<String> getdsfortissueinRice(String tissue);

}