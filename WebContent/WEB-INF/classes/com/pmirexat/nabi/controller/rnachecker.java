package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.RnaGetter;

/**
 * Servlet implementation class rnachecker
 */
@WebServlet("/rnachecker")
public class rnachecker extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public rnachecker() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("do get This functions work is to check that whether the word being tagged is in valid rna as seleceted or not");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("This functions work is to check that whether the word being tagged is in valid rna as seleceted or not");
	   
	    ArrayList<String> wheatmirs= (ArrayList<String>)new RnaGetter().getmiRlist("wheat");
	    ArrayList<String> Maizemirs= (ArrayList<String>)new RnaGetter().getmiRlist("maize");
	    ArrayList<String> Ricemirs= (ArrayList<String>)new RnaGetter().getmiRlist("rice");
		
		String tochek= request.getParameter("checkedcategory");
		String tagvalue = request.getParameter("tagval");
		
		System.out.println("Checked category is :"+tochek);
		System.out.println("to check tag value is :"+tagvalue);
		
		PrintWriter out = response.getWriter();
		
		if(tochek.equals("Wheat"))
		{
		   if(wheatmirs.contains(tagvalue))
				{
			      out.println("True");
				}
		   
		   else {
			       out.print("False");
		         }
		}
		
		if(tochek.equals("Maize"))
		{
		   if(Maizemirs.contains(tagvalue))
				{
			  out.println("True");
				}
		   else
		   {
			 out.println("False");   
		   }
		}
		
		if(tochek.equals("Rice"))
		{
		   if(Ricemirs.contains(tagvalue))
				{
			   out.println("True");
				}
		   else {
			      out.println("False ");
		        }
		}
		
	}

}
