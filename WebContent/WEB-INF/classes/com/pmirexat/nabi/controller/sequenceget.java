package com.pmirexat.nabi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.SequenceFetcher;

/**
 * Servlet implementation class sequenceget
 */
@WebServlet("/sequenceget")
public class sequenceget extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public sequenceget() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		//System.out.println("---------- Sequence geter is called ----------- "+request.getParameter("term"));
		
		SequenceFetcher seqfet=new SequenceFetcher();
		
		double sequenceno=seqfet.getseq(request.getParameter("term"));
		
		System.out.println("sequence number :"+sequenceno);
		
		response.getWriter().print(sequenceno);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
