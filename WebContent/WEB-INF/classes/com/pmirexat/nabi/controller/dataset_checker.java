package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.datasetgetter;

/**
 * Servlet implementation class dataset_checker
 */
@WebServlet("/dataset_checker")
public class dataset_checker extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dataset_checker() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Main work of this functin is to check that whether being tagged word is valid dataset as per selected crieteria or not");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		
		System.out.println("Main work of this functin is to check that whether being tagged word is valid dataset as per selected crieteria or not");
		//tagval
		
		ArrayList<String> wheatdatasets = (ArrayList<String>) new datasetgetter().getdatasets("wheat");
	    ArrayList<String> Maizedatasets = (ArrayList<String>) new datasetgetter().getdatasets("maize");
	    ArrayList<String> Ricedatasets = (ArrayList<String>) new datasetgetter().getdatasets("rice");
		
	    
	    
		String tochek= request.getParameter("checkedcategory");
		String tagvalue = request.getParameter("tagval");
		
		PrintWriter out = response.getWriter();
		
		System.out.println("Checked category is :"+tochek);
		System.out.println("to check tag value is :"+tagvalue);
		
		if(tochek.equals("Wheat"))
		{
		   if(wheatdatasets.contains(tagvalue))
				{
			      out.println("True");
				}
		   
		   else {
			   out.println("False");
		         }
		}
		
		if(tochek.equals("Maize"))
		{
		   if(Maizedatasets.contains(tagvalue))
				{
			   out.println("True");
				}
		   else
		   {
			 out.println("False");   
		   }
		}
		
		if(tochek.equals("Rice"))
		{
			System.out.println("Rice dataset check :"+Ricedatasets);
		   if(Ricedatasets.contains(tagvalue))
				{
			   out.println("True");
				}
		   else {
			      out.println("False ");
		        }
		}
		
	}

}
