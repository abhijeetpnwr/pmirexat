package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.pmirexat.nabi.model.RnaGetter;

/**
 * Servlet implementation class MaizeRna_Autocompleter
 */
@WebServlet("/MaizeRna_Autocompleter")
public class MaizeRna_Autocompleter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MaizeRna_Autocompleter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Reached do get ");
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
System.out.println("reached do post");
		
		@SuppressWarnings("unchecked")
		
		final List<String> micro_rnas=(List<String>) new RnaGetter().getmiRlist("maize");
				
		 response.setContentType("application/json");

	         String param = request.getParameter("term");
	         
	        int comma_index=param.lastIndexOf(",");
	        
	        if(comma_index>0)
	        {
	            param=param.substring(comma_index+1);
	            System.out.println("value of new param is:"+param);
	            
	        }
	        		
	        final List<String> result = new ArrayList<String>();
       
	        for (final String micro_rna : micro_rnas)
	        {
            if (micro_rna.toLowerCase().startsWith(param.toLowerCase()))
	            {
                    result.add(micro_rna);          
	           }
	        }
 
	        response.getWriter().write(new Gson().toJson(result));	
		
	}

}
