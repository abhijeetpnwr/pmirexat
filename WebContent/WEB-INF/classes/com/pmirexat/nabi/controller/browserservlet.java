package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class browserservlet
 */
@WebServlet("/browserservlet")
public class browserservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public browserservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub

		String miRId = request.getParameter("mirId");
		
		System.out.println("Reached in do get of browser servlet with miRid : "+miRId);
		
		List<String> wheatmirs = new ApiClass().getAllWheatMirs();
		
		List<String> maizemirs = new ApiClass().getAllMaizeMirs();
		
		List<String> ricemirs = new ApiClass().getAllRiceMirs();
		
		List<List<String>> resultlist = null;
		
		List<String> searched_micro_rnas = new ArrayList<String>();
		
		searched_micro_rnas.add("Searched Dataset Name");
		
		searched_micro_rnas.add(miRId);
		
		if(wheatmirs.contains(miRId))
		{
			System.out.println("Need to search it in Wheat");
			resultlist=new search_result_generator().searchdb(miRId, "wheat" ,new datasetgetter().getdatasets("wheat"));   
		    System.out.println("Result --------"+resultlist);
		
		}
		
		if(ricemirs.contains(miRId))
		{
			System.out.println("Need to search it in Rice");
			resultlist=new search_result_generator().searchdb(miRId, "rice" ,new datasetgetter().getdatasets("rice"));   
		    System.out.println("Result ------------- "+resultlist);
		}
		
		if(maizemirs.contains(miRId))
		{
			System.out.println("Need to search it in Maize");
			resultlist=new search_result_generator().searchdb(miRId, "maize" ,new datasetgetter().getdatasets("maize"));   
		    System.out.println("Result list ------"+resultlist);
		}
		
		request.setAttribute("searched_micro_rnas", searched_micro_rnas);
		
		request.setAttribute("result_list", resultlist);

        RequestDispatcher reqdispatcher=request.getRequestDispatcher("browsedetails.jsp");
		
		 reqdispatcher.forward(request, response);
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Reached in do post of browser servlet");
	}

}
