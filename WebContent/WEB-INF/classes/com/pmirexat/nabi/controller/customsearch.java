package com.pmirexat.nabi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.model.SimpleEmail;

/**
 * Servlet implementation class customsearch
 */
@WebServlet("/customsearch")
public class customsearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public customsearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Custom Search doget method called");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub

		System.out.println("Custom Search dopost method called");
		
		String mirsequence = request.getParameter("miRSequence");
		
		String mirname =  request.getParameter("enteredmiR");
		
		if(mirname.equals(""))
		{
		   mirname = "Not provided";
		}
		
		String dataset = request.getParameter("dataset1");
		if(dataset.equals(""))
		{
			 dataset = "Not provided";
		}
		
		String species = request.getParameter("species");

		System.out.println("Spcies check :"+species);
		
		if(species.equals(""))
		{
			species = "Not provided";
		}
		
		String details = request.getParameter("details");
		if(details.equals(""))
		{
			details = "Not provided";
		}
		
		
		String email = request.getParameter("emailid");
		if(email.equals(""))
		{
			email = "Not provided";
		}
		
		
		String body = "Mirsequence : "+mirsequence+" \n Mirname : "+mirname+"\n"+"Dataset : "+dataset+"\n"+"Species :"+species+"\n"+"Details :"+details+" \n Email: "+email;
			
		System.out.println("Email body to Send : "+body);
		
		SimpleEmail se = new SimpleEmail();
		se.sendemail(body,email,"abhijeetpnwr@gmail.com");
		
	}

}
