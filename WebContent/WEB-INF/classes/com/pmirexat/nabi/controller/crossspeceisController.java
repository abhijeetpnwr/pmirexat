package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.ListUtils;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.model.Commonsequencegetter;
import com.pmirexat.nabi.model.crossspecies_resultgen;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.search_result_generator;

/**
 * Servlet implementation class crossspeceisController
 */
@WebServlet("/crossspeceisController")
public class crossspeceisController extends HttpServlet
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crossspeceisController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	      String toget = request.getParameter("toget");
		 
		  System.out.println("chh 1 "+toget);
	  
		  @SuppressWarnings("unchecked")
			List ricegetterlist=(List<String>) new datasetgetter().getdatasets("rice");
		  
		    @SuppressWarnings("unchecked")
			List wheatgetterlist=(List<String>)new datasetgetter().getdatasets("wheat");
		    
		    @SuppressWarnings("unchecked")
			List maizegetterlist=(List<String>)new datasetgetter().getdatasets("maize");
		  
		    System.out.println("chk 2 "+toget);
		    
		  String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
		  
		  List<String> searched_micro_rnas=new ArrayList<String>();
		  List<String> finalresult = new ArrayList<String>();
		 
		  List<List<String>> resultlist = new ArrayList<List<String>>();
		  
		  
		  
		  ServletContext sc = request.getServletContext();
		  System.out.println("chk 4 "+toget);
		  
		  if(toget.equals("w_m"))
		  {
			  
			    resultlist =  (List<List<String>>) sc.getAttribute("result_wm");
		
			  System.out.println(" ------------------------------------------------------------ Reached in w&m -------------------------------------");
			  searched_micro_rnas = resultlist.get(0);
	
			  finalresult = resultlist.get(1);
			
			  System.out.println("Counts in results-------------------------------------------------------- :"+finalresult.size());
			  
			  request.setAttribute("searched_micro_rnas",searched_micro_rnas);
			  
			  request.setAttribute("result_list",finalresult);
			  
			  request.setAttribute("tableid","resulttable1");
		  }
		  
		  
		  if(toget.equals("w_r"))
		  {
	
			  resultlist =  (List<List<String>>) sc.getAttribute("result_wr");
			  
			  searched_micro_rnas = resultlist.get(0);
			  finalresult = resultlist.get(1);
			
			  request.setAttribute("searched_micro_rnas",searched_micro_rnas);
			
			  
			  request.setAttribute("result_list",finalresult);
			  request.setAttribute("tableid","resulttable2");
		  }
		  
		  
		  
		
		  if(toget.equals("m_r"))
		  {
			  resultlist =  (List<List<String>>) sc.getAttribute("result_mr");
			  
			  searched_micro_rnas = resultlist.get(0);
			  finalresult = resultlist.get(1);
			
			  
			
			  request.setAttribute("searched_micro_rnas",searched_micro_rnas);
			  request.setAttribute("result_list",finalresult);
			  request.setAttribute("tableid","resulttable3");
		  }
		  
		  
		 if(toget.equals("all"))
		 {
			 resultlist =  (List<List<String>>) sc.getAttribute("result_all");
			 
			  searched_micro_rnas = resultlist.get(0);
			  finalresult = resultlist.get(1);
			
			
			  request.setAttribute("searched_micro_rnas",searched_micro_rnas);
			  request.setAttribute("result_list",finalresult);
			  request.setAttribute("tableid","resulttable4");
		 }
		  
		 
		 RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
			
		 reqdispatcher.forward(request, response);
		
	}

}
