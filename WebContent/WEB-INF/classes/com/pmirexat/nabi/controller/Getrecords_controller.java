package com.pmirexat.nabi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;  
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pmirexat.nabi.api.ApiClass;
import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.model.RnaGetter;
import com.pmirexat.nabi.model.class_entityconvert;
import com.pmirexat.nabi.model.datasetgetter;
import com.pmirexat.nabi.model.ds_dbsearch_result_generator;
import com.pmirexat.nabi.model.search_result_generator;
import com.pmirexat.nabi.pojos.TablePojo;


@WebServlet("/getrecords")
public class Getrecords_controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Getrecords_controller()
    {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("doGet called");
		 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
		List<String> getterstocall = null;
		
		Boolean sequencesearch_check = false; //We need this variable as I am showing diff. output in table, when seq. search is done , it should  display sequences too 
		
		System.out.println(" ---- Request parameter check ---- ");
		
		String checktry = request.getParameter("categ");
		System.out.println(" -- Request param check --- "+checktry);
		
		System.out.println("dopost called");
		
		 RequestDispatcher reqdispatcher=request.getRequestDispatcher("result.jsp");
	  
		 // this will check which optionw was checked
		 String checkedcategory = request.getParameter("checkedcategory");
		 
		 System.out.println("---- checked category is ---"+checkedcategory);
		 
		 String requestparamname = " ";
		 
		 //for sequence search , to get equivalent mirs of category
		 String mirlist_tocheck = "";
		 String dataset_requestparam = "";
		 if(checkedcategory.equals("Wheat"))
		 {
			 requestparamname = "Wheatsearch";
			 mirlist_tocheck = "Wheatmirs";
			 dataset_requestparam = "Wheat_datasets";
		 }
		 
		 if(checkedcategory.equals("Maize"))
		 {
			 requestparamname = "Maizesearch";
			 mirlist_tocheck = "Maizemirs";	 
			 dataset_requestparam = "Maize_datasets";
		 }
		 
		 if(checkedcategory.equals("Rice"))
		 {
			 requestparamname = "Riceesearch";
			 mirlist_tocheck = "Ricemirs";
			 dataset_requestparam = "Rice_datasets";
		 }

		 String tobesearch_rna=request.getParameter(requestparamname);
		 
		 
		 if(tobesearch_rna == null)
		 {
			 tobesearch_rna = "";
		 }
		 
    	 String tobesearch_rna_seuqnce;
		
    	 System.out.println("check 1 -- "+tobesearch_rna+"check 2 ---"+request.getParameter("tosearch_sequence"));
    	 
    	 
    	 
    	 //It is for the last funcionality , So if no sequence and rna's entered , so it means last functionality for seeing complete matrix is being used
    	 if( (tobesearch_rna == "" || tobesearch_rna == null) && (request.getParameter("tosearch_sequence") == "" || request.getParameter("tosearch_sequence") == null))
    	 {
    	     //System.out.println("-- Means now search for all the mirs  and datasets in species --");	 	     
    	     List<String> mirlist =  (List<String>)  new RnaGetter().getmiRlist(checkedcategory);
              for (Iterator iterator = mirlist.iterator(); iterator.hasNext();) 
    	     {
				String string = (String) iterator.next();	
				System.out.println("-- string -- "+string);
				tobesearch_rna = tobesearch_rna+string+",";
    	     }
              
              if(checkedcategory.equals("Rice"))
              {
            	 getterstocall = new datasetgetter().getdatasets("rice");
            	 
            	 System.out.println("will set getterstocall to :"+getterstocall);
            	 
              }
              
              if(checkedcategory.equals("Wheat"))
              {
            	  getterstocall = new datasetgetter().getdatasets("Wheat");
            	  System.out.println("will set getterstocall to :"+getterstocall);
              }
              
              if(checkedcategory.equals("Maize"))
              {
            	  getterstocall = new datasetgetter().getdatasets("Maize");
            	  System.out.println("will set getterstocall to :"+getterstocall);
              }
    	     
    	 }
		 
		 
		 // If no mirids were entered , it means , sequence search was done on it.
		 if(tobesearch_rna == "" && request.getParameter("tosearch_sequence") != "")
		 {
			 sequencesearch_check = true;
			// System.out.println("no mir ids entered but sequence entered --");
             tobesearch_rna_seuqnce = request.getParameter("tosearch_sequence");
           //  System.out.println(tobesearch_rna_seuqnce);
             
             StringTokenizer st2 = new StringTokenizer(tobesearch_rna_seuqnce, ",");
            System.out.println("Toc check mirlist name : "+ mirlist_tocheck);
             @SuppressWarnings("unchecked")
			List<String> tocheckmirs =  (List<String>)  new RnaGetter().getmiRlist(checkedcategory);
     		
             System.out.println("Mir content ::"+tocheckmirs.toString());
             
             while (st2.hasMoreElements())
     		 {
            	 String mirname = new DbCommands().getmirnam(st2.nextToken(),new class_entityconvert().getclassname(checkedcategory));
     			
            	 System.out.println("-- mirname found --- "+mirname);
            	 
            	 String[] stringarr = mirname.split(",");
            	 
            	 for(int i = 0 ;i<stringarr.length;i++)
            	 {
            		 System.out.println("Processing : "+stringarr[i]);
            		 if(tocheckmirs.contains(stringarr[i]))
         			 {
         				 tobesearch_rna=tobesearch_rna+stringarr[i]+",";
         				 System.out.println("mirname : "+stringarr[i]+"is valid");
         			 }	
                	 
                	 else
                	 {
                		 System.out.println("mirname : "+stringarr[i]+"is notvalid");
                	 }
            	 }
            	 
            	
     		 }
             
             
           //  System.out.println("values --"+tobesearch_rna_seuqnce);
             
          //   System.out.println("to be searched parameters"+tobesearch_rna);
             
		 }
	    
	    List<String> searched_micro_rnas=new ArrayList<String>();
	    
	    String select_datasets=null;
	    		
	    select_datasets=request.getParameter(dataset_requestparam);
	    
	    System.out.println("-------"+select_datasets);
	    
	    StringTokenizer st2 = new StringTokenizer(tobesearch_rna,",");
	    
	    
	    /*This loop takes multiple seached attributes.Applies a tokenizer.Splits them and  enters them in a list
	      
	      ---> Required as on result we need to show that which attribures were searched
	    
	    */
	    
	    searched_micro_rnas.add("Searched Dataset Name");     //I  need to edit its value
	    
	    @SuppressWarnings("unchecked")
		List<String> dataset= (List<String>)new RnaGetter().getmiRlist(checkedcategory);
	    
		while (st2.hasMoreElements())
		{
			
			String temp=(String) st2.nextElement();
			
			if(sequencesearch_check == true)
			{
				temp = temp + "\n"+"(" + new ApiClass().getSequencebymiRid(temp)+")" ;
			}
			
					searched_micro_rnas.add(temp);
		}
	    
		if(select_datasets == null)
		{
			System.out.println("select_dataset is null so will assign balnk to it");
			select_datasets = "";
		}
		
	    if(select_datasets.length()>0)
	    {	
	        
	    	System.out.println("------------in if case ------ will seach for specific database");
	    	
	    	System.out.println("Database check 1");
	    	
	    	List<List<String>> dbspecific_result= new ds_dbsearch_result_generator().generatetablle(tobesearch_rna, select_datasets,checkedcategory);
	       
	    	System.out.println("Database check 2");
	    	
	    	for (List<String> list : dbspecific_result) {
				for (String string : list) 
				{
					System.out.println(string);
				}
			}
	    	
	    	System.out.println("Database check 3");
	        request.setAttribute("result_list",dbspecific_result);
	    
	    }
	    
	    else
	    {
	    	System.out.println("------------in else case ------ will seach for complete datasets");
	    	
	       // String categorytosearch=request.getParameter("Catagory1")+request.getParameter("Catagory2")+request.getParameter("Catagory3");
	    	
	    	String categorytosearch=request.getParameter("checkedcategory").toLowerCase();
		    System.out.println("value of Catagory Search is"+categorytosearch);
		    
		    @SuppressWarnings("unchecked")
			List ricegetterlist=(List<String>) new datasetgetter().getdatasets("rice");
		    @SuppressWarnings("unchecked")
			List wheatgetterlist=(List<String>)new datasetgetter().getdatasets("wheat");
		    @SuppressWarnings("unchecked")
			List maizegetterlist=(List<String>)new datasetgetter().getdatasets("maize");
		    
		    
		    /* result_generator is a method of search_result_generator(mdoel class).Which call dao from its code 
		       Gets output from dao and molds the output as our requirement as we need kind of different structure on 
		       result jsp page.That could have been difficult using jstl tags on jsp.So this method gives us the output
		       as our required format
		    */
		    
           List<String> refinedlist = new ArrayList<String>();
			
          // --------------------------- for further spcific search in category -------------- //         
  
			// -- if not all selected checkboxes were null, means atleat one check box were selected
			
				if(categorytosearch.equalsIgnoreCase("Wheat"))
				{
					 String wheatctegory1 = request.getParameter("wheat1");
						
						String wheatctegory2 = request.getParameter("wheat2");
						
						String wheatctegory3 = request.getParameter("wheat3");
						
						String wheatctegory4 = request.getParameter("wheat4");
					
					System.out.println("  --  Need to manipulate wheat list -- ");
					
					if(!(wheatctegory1 == null && wheatctegory2 == null && wheatctegory3 == null && wheatctegory4 == null))
					{
					
					if(!(wheatctegory1 == null || wheatctegory1.trim().equals("")) )
					{
						for (Object gettername : wheatgetterlist)
						{
						       System.out.println("value :"+gettername);	
						       
						       if((gettername.toString().toLowerCase()).contains(wheatctegory1.toLowerCase()))
						       {
						    	   System.out.println("Added :"+gettername.toString());
						    	   refinedlist.add(gettername.toString());
						       }
						}
					}
						
					
					if(!(wheatctegory2 == null || wheatctegory2.trim().equals("")))
					{
						for (Object gettername : wheatgetterlist)
						{
						       System.out.println("value :"+gettername);	
						       
						       if((gettername.toString().toLowerCase()).contains(wheatctegory2.toLowerCase()))
						       {
						    	   System.out.println("Added :"+gettername.toString());
						    	   refinedlist.add(gettername.toString());
						       }
						}
					}
					
					
					if(!(wheatctegory3 == null || wheatctegory3.trim().equals("")))
					{
						for (Object gettername : wheatgetterlist)
						{
						       System.out.println("value :"+gettername);	
						       
						       if((gettername.toString().toLowerCase()).contains(wheatctegory3.toLowerCase()))
						       {
						    	   System.out.println("Added :"+gettername.toString());
						    	   refinedlist.add(gettername.toString());
						       }
						}
					}
					
					
					if(!(wheatctegory4 == null || wheatctegory4.trim().equals("")))
					{
						for (Object gettername : wheatgetterlist)
						{
						       System.out.println("value :"+gettername);	
						       
						       if((gettername.toString().toLowerCase()).contains(wheatctegory4.toLowerCase()))
						       {
						    	   System.out.println("Added :"+gettername.toString());
						    	   refinedlist.add(gettername.toString());
						       }
						}
					}
					
					
					getterstocall = refinedlist;
							
				}
			}
			
			
			
			// ---------------------------- for wheat spscific search part ends ------------------ //
			
			
			
				
				
							
							
				
				//   ------------------ for maize spcif searchs ------
			if(categorytosearch.equalsIgnoreCase("Maize"))
			{
				
				 String maizectegory1 = request.getParameter("maize1");
					
				 String maizectegory2 = request.getParameter("maize2");
					
				 String maizectegory3 = request.getParameter("maize3");
					
				 String maizectegory4 = request.getParameter("maize4");
				 
				 String maizectegory5 = request.getParameter("maize5");
						
				 String maizectegory6 = request.getParameter("maize6");
						
				 String maizectegory7 = request.getParameter("maize7");
						
				 String maizectegory8 = request.getParameter("maize8");
				 
				 String maizectegory9 = request.getParameter("maize9");
				
				if(!(maizectegory1 == null && maizectegory2 == null && maizectegory3 == null && maizectegory4 == null && maizectegory5 == null && maizectegory6==null &&  maizectegory7==null && maizectegory8== null && maizectegory9 == null))
				{
						System.out.println("  --  Need to manipulate maize list -- ");
						
						if(!(maizectegory1 == null || maizectegory1.trim().equals("")) )
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory1.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
							
						
						if(!(maizectegory2 == null || maizectegory2.trim().equals("")))
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory2.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(maizectegory3 == null || maizectegory3.trim().equals("")))
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory3.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(maizectegory4 == null || maizectegory4.trim().equals("")))
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory4.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						if(!(maizectegory5 == null || maizectegory5.trim().equals("")) )
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory5.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(maizectegory6 == null || maizectegory6.trim().equals("")) )
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory6.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(maizectegory7 == null || maizectegory7.trim().equals("")) )
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory7.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(maizectegory8 == null || maizectegory8.trim().equals("")) )
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory8.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(maizectegory9 == null || maizectegory9.trim().equals("")) )
						{
							for (Object gettername : maizegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(maizectegory9.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
										
						getterstocall = refinedlist;
								
					}
				}
				 
			
			
			if(categorytosearch.equalsIgnoreCase("Rice"))
			{
				System.out.println( "  --  Need to manipulate rice list -- "); 

				
				 String ricectegory1 = request.getParameter("rice1");
				
				 String ricectegory2 = request.getParameter("rice2");
					
				 String ricectegory3 = request.getParameter("rice3");
					
				 String ricectegory4 = request.getParameter("rice4");
				 
				 String ricectegory5 = request.getParameter("rice5");
						
				 String ricectegory6 = request.getParameter("rice6");
						
				 String ricectegory7 = request.getParameter("rice7");
						
				 String ricectegory8 = request.getParameter("rice8");
				 
				 String ricectegory9 = request.getParameter("rice9");
				
				if(!(ricectegory1 == null && ricectegory2 == null && ricectegory3 == null && ricectegory4 == null && ricectegory5 == null && ricectegory6==null && ricectegory7==null && ricectegory8== null && ricectegory9 == null))
				{
						System.out.println("  --  Need to manipulate maize list -- ");
						
						if(!(ricectegory1 == null || ricectegory1.trim().equals("")) )
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory1.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
							
						
						if(!(ricectegory2 == null || ricectegory2.trim().equals("")))
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory2.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(ricectegory3 == null || ricectegory3.trim().equals("")))
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory3.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(ricectegory4 == null || ricectegory4.trim().equals("")))
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory4.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						if(!(ricectegory5 == null || ricectegory5.trim().equals("")) )
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory5.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(ricectegory6 == null || ricectegory6.trim().equals("")) )
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory6.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(ricectegory7 == null || ricectegory7.trim().equals("")) )
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory7.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(ricectegory8 == null || ricectegory8.trim().equals("")) )
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory8.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
						
						
						if(!(ricectegory9 == null || ricectegory9.trim().equals("")) )
						{
							for (Object gettername : ricegetterlist)
							{
							       System.out.println("value :"+gettername);	
							       
							       if((gettername.toString().toLowerCase()).contains(ricectegory9.toLowerCase()))
							       {
							    	   System.out.println("Added :"+gettername.toString());
							    	   refinedlist.add(gettername.toString());
							       }
							}
						}
										
						getterstocall = refinedlist;
								
					}
			} 
		    
			System.out.println("I am passing getter list:"+getterstocall);
		    
		    List<List<String>> resultlist=new search_result_generator().searchdb(tobesearch_rna, categorytosearch,getterstocall);   
		    
			System.out.println("-----------------------------------------------------------------");
			
		
			System.out.println("Category to search :"+categorytosearch);
			
			System.out.println("Result list :"+resultlist);
			
		   request.setAttribute("result_list", resultlist);
		    
		    request.setAttribute("check", "abhijeet");
		     	     
	    }
	    
	    request.setAttribute("searched_micro_rnas",searched_micro_rnas);
	    
	    reqdispatcher.forward(request, response);
	
	}

}
