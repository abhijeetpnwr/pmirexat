package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.ListUtils;

import com.pmirexat.nabi.api.ApiClass;

public class crossspecies_resultgen 
{

	  Commonsequencegetter com_obj = new Commonsequencegetter();
	  
	  
	  
	  @SuppressWarnings("unchecked")
		List ricegetterlist=(List<String>) new datasetgetter().getdatasets("rice");
	    @SuppressWarnings("unchecked")
		static
		List wheatgetterlist=(List<String>)new datasetgetter().getdatasets("wheat");
	    @SuppressWarnings("unchecked")
		static
		List maizegetterlist=(List<String>)new datasetgetter().getdatasets("maize");
	  
	
		  
		  String wheatid = "";
		  String maizeid = "";
		  String riceid = "";
	  
	  
   public  List<List<String>> get_wm()
   {
	   String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
	   
   	  List<String> comm_wm = com_obj.getcommoninwheatandmaize();
		  
   	  List<String> searched_micro_rnas=new ArrayList<String>();
	  List<String> finalresult = new ArrayList<String>();
   	
		 List<String> generatedheaders = new ArrayList<String>();
		 generatedheaders.add("Searched Dataset Name");
		  searched_micro_rnas.add("Searched Dataset Name"); 
		  
		  searched_micro_rnas = ListUtils.union(searched_micro_rnas,comm_wm);
		  
		  System.out.println("I need to get results for wheat and maize");
	
		  
		  int i =0;
		  
		 for (Iterator iterator = comm_wm.iterator(); iterator.hasNext();)
		 {
			String sequence = (String) iterator.next();
			 
			wheatid = new ApiClass().findmiRidbysequenceinWheat(sequence);
			maizeid = new ApiClass().findmiRidbysequenceinMaize(sequence);
			
		    if(i == 0)
		    {
			wheatrnas = wheatrnas+wheatid;
		  	maizernas = maizernas+maizeid;
		  
		    }
		    else
		    {
		    	wheatrnas = wheatrnas+","+new ApiClass().findmiRidbysequenceinWheat(sequence);
			  	maizernas = maizernas+","+new ApiClass().findmiRidbysequenceinMaize(sequence);
			 	
		    }
		  
		    
		    generatedheaders.add(sequence+"<br>"+wheatid+","+maizeid);
		    
		    i++; //Counter increased
	   
		 }			
		 	
		finalresult =   ListUtils.union(new search_result_generator().searchdb(wheatrnas,"wheat", wheatgetterlist), new search_result_generator().searchdb(maizernas, "maize" , maizegetterlist));
		System.out.println("Counts in results in crossspecies -------------------------------------------------------- :"+finalresult.size());
		
		System.out.println("Final Result"+finalresult);
		
		List<List<String>> toreturn = new ArrayList<List<String>>();
		
		toreturn.add(generatedheaders); //added table header header
		toreturn.add(finalresult); //added table content
				
		return toreturn;
   }
   
   public List<List<String>> get_wr()
   {
	   String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
	   	 
	   	  
		  List<String> finalresult = new ArrayList<String>();
		  
		  List<String> searched_micro_rnas=new ArrayList<String>();
		  
		  searched_micro_rnas.add("Searched Dataset Name"); 
		  
		  List<String> comm_wr = com_obj.getcommoninwheatandrice();
		  
		  searched_micro_rnas = ListUtils.union(searched_micro_rnas,comm_wr);
		 
		 
		  
		  System.out.println("I need to get results for wheat and maize");
		  
		  int i =0;
		  
		 List<String> generatedheaders = new ArrayList<String>();
		 generatedheaders.add("Searched Dataset Name");
		 
		 for (Iterator iterator = comm_wr.iterator(); iterator.hasNext();)
		 {
			String sequence = (String) iterator.next();
			
			wheatid = new ApiClass().findmiRidbysequenceinWheat(sequence);
			riceid = new ApiClass().findmiRidbysequenceinRice(sequence);
			
		    if(i == 0)
		    {
		    	wheatrnas = wheatrnas+wheatid;
		    	ricernas = ricernas+riceid;
		    	
		    }
		    else
		    {
		    	wheatrnas = wheatrnas+","+wheatid;
			  	ricernas = ricernas+","+riceid;
			  	
		    }
		    
		    generatedheaders.add(sequence+"<br>"+wheatid+","+riceid);
		    
		    i++; //Counter increased
	   
		 }			
		 	
		finalresult =   ListUtils.union(new search_result_generator().searchdb(wheatrnas,"wheat", wheatgetterlist), new search_result_generator().searchdb(ricernas, "rice" , ricegetterlist));
		
		System.out.println("Final Result"+finalresult);
  
		
		List<List<String>> toreturn = new ArrayList<List<String>>();
		
		toreturn.add(generatedheaders); //added table header header
		toreturn.add(finalresult); //added table content
				
		return toreturn;
   }
   
   
   // -------------------  for wheat and rice --------------
   public List<List<String>> get_mr()
   {
	   
	   String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
	   List<String> searched_micro_rnas=new ArrayList<String>();
	   List<String> finalresult = new ArrayList<String>();
		  
	   List<String> generatedheaders = new ArrayList<String>();
	   generatedheaders.add("Searched Dataset Name");
	   
	   searched_micro_rnas.add("Searched Dataset Name"); 
		  
		  List<String> comm_mr = com_obj.getcommoninmaizeandrice();
		  
		  searched_micro_rnas = ListUtils.union(searched_micro_rnas,comm_mr);
		  
		  System.out.println("I need to get results for wheat and maize");
		  
		  int i =0;
		  
		  for (Iterator iterator = comm_mr.iterator(); iterator.hasNext();)
		  {
			String sequence = (String) iterator.next();
			
			maizeid = new ApiClass().findmiRidbysequenceinMaize(sequence);
			riceid = new ApiClass().findmiRidbysequenceinRice(sequence);
			
		    if(i == 0)
		    {
		    	
		  		maizernas = maizernas+maizeid;
		  		ricernas = ricernas+riceid;
		  	
		  		
		    }
		    else
		    {
		    	maizernas = maizernas+","+maizeid;
		    	ricernas = ricernas+","+riceid;
		    	
		    }
		    
		    generatedheaders.add(sequence+"<br>"+maizeid+","+riceid);
		    
		    i++; //Counter increased
	   
		 }			
		 	
		finalresult =   ListUtils.union(new search_result_generator().searchdb(maizernas,"maize",maizegetterlist), new search_result_generator().searchdb(ricernas, "rice" , ricegetterlist));
		
		System.out.println("Final Result"+finalresult);
		
		List<List<String>> toreturn = new ArrayList<List<String>>();
		
		toreturn.add(generatedheaders); //added table header header
		toreturn.add(finalresult); //added table content
				
		return toreturn;
   }
   
   
   public List<List<String>> get_all()
   {
	   String wheatrnas = "";
		  String maizernas  = "";
		  String ricernas = "";
	   List<String> searched_micro_rnas=new ArrayList<String>();
	   List<String> finalresult = new ArrayList<String>();
	 List<String> comm_all = com_obj.getcommoninallsequence();
		 
		  searched_micro_rnas = ListUtils.union(searched_micro_rnas,comm_all);
		  List<String> generatedheaders = new ArrayList<String>();
		  generatedheaders.add("Searched Dataset Name");
		  
		  int i =0;
		  
		 for (Iterator iterator = comm_all.iterator(); iterator.hasNext();)
		 {
			String sequence = (String) iterator.next();
			 
			wheatid = new ApiClass().findmiRidbysequenceinWheat(sequence);
			maizeid = new ApiClass().findmiRidbysequenceinMaize(sequence);
			riceid = new ApiClass().findmiRidbysequenceinRice(sequence);
					
		    if(i == 0)
		    {	      
			wheatrnas = wheatrnas+wheatid;
		  	maizernas = maizernas+maizeid;
		  	ricernas = ricernas+riceid;
		    }
		    
		    else
		    {
		    	wheatrnas = wheatrnas+","+wheatid;
			  	maizernas = maizernas+","+maizeid;
			  	ricernas = ricernas+","+riceid;		  
		    }
		    
			generatedheaders.add(sequence+"<br>"+wheatid+","+maizeid+","+riceid);
			
		    i++; //Counter increased
	   
		 }			
		 	
		finalresult =   ListUtils.union(new search_result_generator().searchdb(wheatrnas,"wheat", wheatgetterlist), new search_result_generator().searchdb(maizernas, "maize" , maizegetterlist));
		
		finalresult = ListUtils.union(finalresult, new search_result_generator().searchdb(ricernas, "rice" , ricegetterlist));
		
		System.out.println("Final Result"+finalresult);
		List<List<String>> toreturn = new ArrayList<List<String>>();		
		
		toreturn.add(generatedheaders); //added table header header
		
		toreturn.add(finalresult); //added table content
				
		return toreturn;
		
   }

}

