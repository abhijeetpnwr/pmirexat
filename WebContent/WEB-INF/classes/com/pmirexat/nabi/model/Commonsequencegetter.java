package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import com.pmirexat.nabi.api.ApiClass;

public class Commonsequencegetter
{

 
    List<String> wheatseqences = new ApiClass().getAllWheatSequence();
    
	List<String> maizesequences = new ApiClass().getAllMaizeSequence();
	
	List<String> ricesequences = new ApiClass().getAllRiceSequence();
	
	
	
    public List<String> getcommoninallsequence()
    {
    	List<String> common_all = new ArrayList<String>(wheatseqences);
    	common_all.retainAll(maizesequences);
    	common_all.retainAll(ricesequences);
    	return common_all;
	}
     
    public List<String> getcommoninwheatandmaize()
    {
    
    	List<String> common_wm = new ArrayList<String>(wheatseqences);
    	common_wm.retainAll(maizesequences);
    	 return common_wm;
    }
    
    public List<String> getcommoninwheatandrice()
    {
    	List<String> common_wr = new ArrayList<String>(wheatseqences);
    	common_wr.retainAll(ricesequences);
        return common_wr;
    }
    
    public List<String> getcommoninmaizeandrice()
    {
    	
    	List<String> common_mr = new ArrayList<String>(maizesequences);    	
    	common_mr.retainAll(ricesequences);	
    	return common_mr;
    }
    
}
