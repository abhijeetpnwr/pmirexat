package com.pmirexat.nabi.model;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmailWithAttachments 
{
		
		 public  void sendEmail(String toAddress,String subject, String message, String filePath) throws MessagingException
		             {
		        // sets SMTP server properties

			 final String fromEmail = "pmirexat@gmail.com"; //requires valid gmail id
		         final String password = "pmirexat@nabi"; // correct password for gmail id
		         String toEmail = toAddress; // can be any email id 
		          
		         System.out.println("TLSEmail Start");
		         
		         Properties props = new Properties();
		         props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		         props.put("mail.smtp.port", "587"); //TLS Port
		         props.put("mail.smtp.auth", "true"); //enable authentication
		         props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		         
		         System.out.println(" ---- Check point 1 ---- ");
		         
		         
		                 //create Authenticator object to pass in Session.getInstance argument
		         Authenticator auth = new Authenticator()
		         {
		             //override the getPasswordAuthentication method
		             protected PasswordAuthentication getPasswordAuthentication() {
		                 return new PasswordAuthentication(fromEmail, password);
		             }
		         };
		        
		        
		         
		         Session session = Session.getInstance(props, auth);       
		        
		         System.out.println(" ---- Check point 2 ---- ");
		 
		        // creates a new e-mail message
		        Message msg = new MimeMessage(session);
		 
		        System.out.println(" ---- Check point 3 ---- ");
		        
		        msg.setFrom(new InternetAddress(fromEmail));
		        
		        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
		        
		        System.out.println(" ---- Check point 4 ---- ");
		        
		        msg.setRecipients(Message.RecipientType.TO, toAddresses);
		        msg.setSubject(subject);
		        msg.setSentDate(new Date());
		 
		        System.out.println(" ---- Check point 5 ---- ");
		        // creates message part
		        MimeBodyPart messageBodyPart = new MimeBodyPart();
		        messageBodyPart.setContent(message, "text/html");
		 
		        // creates multi-part
		        Multipart multipart = new MimeMultipart();
		        multipart.addBodyPart(messageBodyPart);
		 
		        System.out.println(" ---- Check point 6 ---- ");
		        
		        // adds attachments
		        
		        if (filePath != null)
		        {
		                MimeBodyPart attachPart = new MimeBodyPart();
		 
		                System.out.println(" ---- Check point 6-if ---- ");
		                try
		                {
		                    attachPart.attachFile(filePath);
		                } catch (IOException ex) {
		                    ex.printStackTrace();
		                }
		 
		                multipart.addBodyPart(attachPart);
		           
		        }
		 
		        System.out.println(" ---- Check point 7 ---- ");
		        // sets the multi-part as e-mail's content
		        msg.setContent(multipart);
		 
		        System.out.println(" ---- Check point 8 ---- ");
		        // sends the e-mail
		   
		        for(int i = 0 ;i< 10;i++)
		        {
		        	System.out.println("Attemt No.  "+i+"To send mail ");
		        	
		        	 boolean checkmailstatus = true;
		        	 
		        	 try 
			          {
						Transport.send(msg);
			          } 
			          catch (MessagingException e)
			          {
			        	checkmailstatus = false;
						System.out.println("Failed to send mail on "+i+"th attempt");
					  }
		        	 
		        	 if(checkmailstatus == true)
		        	 {
		        		 System.out.println("hop , mail has been sent on "+i+"th attempt");
		        		 break;
		        	 }
		        	 
		        	 else
		        	 {
		        		 System.out.println("Can't ping smtp.gmail.com , will check after 20 seconds");
		        		 try 
		        		 {
							Thread.sleep(5000);
						 } catch (InterruptedException e) 
		        		 {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		        	 }
		        	 
		        }
		         
		    
		    
		        System.out.println(" ---- Check point 9 ---- ");
		 
		    }
		
		
}
