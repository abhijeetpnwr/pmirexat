package com.pmirexat.nabi.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;

import com.pmirexat.nabi.dao.DbCommands;
import com.pmirexat.nabi.dao.HibernateUtils;
import com.pmirexat.nabi.listners.exceldbmapper;
import com.pmirexat.nabi.pojos.MaizePojo;
import com.pmirexat.nabi.pojos.RicePojo;
import com.pmirexat.nabi.pojos.Seqno;
import com.pmirexat.nabi.pojos.TablePojo;
import com.pmirexat.nabi.pojos.WheatPojo;




public class Exceltodbmapper extends Thread
{

	private static FileInputStream Excel_istream;
	private static XSSFWorkbook Hw;
    @SuppressWarnings("rawtypes")
	private static Iterator rows;
	@SuppressWarnings("rawtypes")
	private Iterator cols;
    private static XSSFSheet sheet ;
    private XSSFCell cell;
    private XSSFRow row;
    int count;	 	
    int i=0;
    int colscounter;
    String temp_methodname;
	 
    
    //This function ig give file path and filename of excelfile,Does all basic setup for that.Creates xssf workbook object
	 public void doSetup(String path,String filename)	 
	 {
		 try 
		    {
			 System.out.println("path is"+path);
			 
			    System.out.println("I am in model class dosetup method.Path for excel file is "+path);
			    
			    if(path.equals("inproject"))
			    {
			    	System.out.println("excel file is kept in project resource folder");
			      InputStream Excel_istream1=Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);    	
			    
			      Hw=new XSSFWorkbook(Excel_istream1);
			    }
			 
			    else
			    {
				       Excel_istream=new FileInputStream(path+filename);
				       Hw=new XSSFWorkbook(Excel_istream); 
			    }

			    sheet=Hw.getSheetAt(0);
				   
				   rows= sheet.rowIterator();			
			}
		    catch (IOException e)
		    {
				e.printStackTrace();
			}
	 }
	 
	// ---------- function ends here --------------- //
	
	 

	//This function does all  work of mapping excel file to db 
	 
	public HashMap<Integer, String> Readexcel(String path,String filename) throws NoSuchMethodException, SecurityException
	 {
		System.out.println("--- Reached readexcel method ---");
		
		//dosetup , above defined method called
		
		System.out.println(" give excelfile name :"+filename+" and given path is :"+path);
		
		doSetup(path,filename); //opens all streams and does basic setup for displaying information
		   
		 //This will be returned from function
		   HashMap<Integer, String> settermap = new HashMap<Integer, String>();
		   
		   String setter_for= "";
		   
	    
		   Object obj = null ;
		   
		 //While there are rows in excel file,keeps iterating through column values
		    while (rows.hasNext())
		    {	
		    	colscounter=0;
		    	
		 	   if(filename.equals("wheat.xlsx"))
			   {
		 		 	obj = new WheatPojo();
				   setter_for = "wheat";
			   }
			   
			   if(filename.equals("maize.xlsx"))
			   {
				    obj = new MaizePojo();
				   setter_for = "maize";
			   }
			   
			   if(filename.equals("rice.xlsx"))
			   {
				    obj = new RicePojo();
				    setter_for = "rice";
			   }
		    	
		           //Excelpojo object created		         	         
		    	
		    	
		    	i++;
		    	
				//System.out.println("-----------index of row is------"+i);
		   	    
		    	//Get the next row 
		    	row= (XSSFRow) rows.next();
			    //  System.out.println();
		    	  
		    	    //Cols is an iterator , for iteratign columns
			        cols=row.cellIterator();
			        
			        
			        
			        while(cols.hasNext())
			        {
			        	colscounter++;
			        	cell=(XSSFCell) cols.next();
			        	
			        	//If the first row is being processed,So it will get name for datasets, and will set accordingly values in maps
			        	
			        	if(i==1)
			        	{
			                //    System.out.println("first line is being processed so I will use it to map to pojo-------");		
			        	        
			                    try 
			        	         {			                    	
			                     	 settermap=new processsetter().checkforsetter(cell.getStringCellValue(),colscounter,settermap,setter_for);			   	 
			        	         }
			        	         
			        	         catch (NoSuchMethodException e)
			        	         {
			        	        	 // TODO Auto-generated catch block
			        	        	 e.printStackTrace();
			        	        }
			        	         
			        	         catch (SecurityException e)
			        	         {
			        	        	 // TODO Auto-generated catch block
									e.printStackTrace();
			        	         }
			                       	
			        	}
			        	
			        	else
			        	{
			        		System.out.println("Column count :"+colscounter+"cell type :"+cell.getCellType());    
			        		
	            		        switch (cell.getCellType())
			             		{
			             			case HSSFCell.CELL_TYPE_NUMERIC:
			             			{
			             				// System.out.print(" "+cell.getNumericCellValue());
			             				
			             				 temp_methodname=settermap.get(colscounter);
			             			   	System.out.println("value is integer : "+temp_methodname+"col count is"+colscounter+"row count is"+i);
			             				
			             			   Method method = null;
			             			   	
			             			   if(filename.equals("wheat.xlsx"))
			            			   {
			             				  if(temp_methodname.contains("foldchange"))
			             				  {
			             					  System.out.println("Float type method should be called");
			             					 method = WheatPojo.class.getMethod(temp_methodname,float.class);
			             				  }
			             				  else
			             				  {
			             					 method = WheatPojo.class.getMethod(temp_methodname,int.class);
			             				  }
			             			      		  
			             				  
			            			   }
			            			   
			            			   if(filename.equals("maize.xlsx"))
			            			   {
			            				   if(temp_methodname.contains("foldchange"))
				             				  {
			            					   		method = MaizePojo.class.getMethod(temp_methodname,float.class);
				             				  }
			            				   else
			            				   {
			            					   method = MaizePojo.class.getMethod(temp_methodname,int.class);
			            				   }
			            			   }
			            			   
			            			   if(filename.equals("rice.xlsx"))
			            			   {
			            				   
			            				   if(temp_methodname.contains("foldchange"))
			            				   {
			            					   method = RicePojo.class.getMethod(temp_methodname,float.class);
			            				   }
			            				
			            				   else
			            				   {
			            					   method = RicePojo.class.getMethod(temp_methodname,int.class);
			            				   }
			            				   
			            				  
			            			   }
			             			   	
			             			   	
			             			   
			             				 
			             				 
			             				 
			             				 try
			             				 {
			             					// System.out.println("now I will call method");
			             					 
			             					 if(temp_methodname.contains("foldchange"))
			             					 {
			             						 float topass = (float) cell.getNumericCellValue();
			             						method.invoke(obj,topass);
			             					 }
			             					
			             					 else
			             					 {
			             					    int topass= (int) cell.getNumericCellValue();
			             					    method.invoke(obj,topass);
			             					 }
			             				//	System.out.println("Method to call"+method.getName()+" with value"+topass);
			             					 
											
									   	} 
			             				 
			             				 catch (IllegalAccessException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IllegalArgumentException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (InvocationTargetException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
			             				
			             				break;
			             			}
			             			
			             			case HSSFCell.CELL_TYPE_STRING:
					        			{
					        				
					        				System.out.println("-----------So this time value is String------"+colscounter);					  				
					        				
					        				
					        				System.out.println("---- chk val--"+settermap.get(colscounter));
					       			
					        				
					        				temp_methodname=settermap.get(colscounter);
					        				
					        				Method method = null;
					        				
					        				if(filename.equals("wheat.xlsx"))
					            			   {
					             				  method = WheatPojo.class.getMethod(temp_methodname,String.class);
					            			   }
					            			   
					            			   if(filename.equals("maize.xlsx"))
					            			   {
					            				   method = MaizePojo.class.getMethod(temp_methodname,String.class);
					            			   }
					            			   
					            			   if(filename.equals("rice.xlsx"))
					            			   {
					            				   method = RicePojo.class.getMethod(temp_methodname,String.class);
					            			   }
			 
												try
												{
													method.invoke(obj,cell.getStringCellValue());
												} catch (
														IllegalAccessException
														| IllegalArgumentException
														| InvocationTargetException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
										   
				             				
					        				
					        				
					        			}
		
			             			default:
					        		break;
			             		}
			        	}
			        }
			        		        
			        //Saving RowEntry object in table
			         
			        new DbCommands().saveEntry(obj);
			        
			      
			        
			        
			      
			      
	    	}
		  
			return settermap;
		    
	        
	 }

	public void Readexcel2(String excelsheet_path,String filename)
	{
		
		System.out.println("Excel sheet path is :"+excelsheet_path);
		
		System.out.println("filename is :"+filename);
		
		doSetup(excelsheet_path,filename);
		
		//row= (XSSFRow) rows.next();
		
		while (rows.hasNext())
	    {	
			
			System.out.println(" ------------------------------------------------------------------------------- ");
			
			 Seqno record = new  Seqno();
			 
	     	 row= (XSSFRow) rows.next();
			   
		     cols=row.cellIterator();
		
		    
		     int cellcount=1;
		     
             while(cols.hasNext())
             {
        
             	cell=(XSSFCell) cols.next();
             	
                 if(cellcount==1)
                 {  	   
                	  System.out.println("In if:"+cell.getStringCellValue());
                	  record.setDatasetname(cell.getStringCellValue());
                	  cellcount++;
                 }
                 
                 else
                 {
                	 System.out.print("In Else :"+cell.getNumericCellValue());
                	 record.setSequence(cell.getNumericCellValue());
                	 
                 }                 
       
             }            
             
             new DbCommands().savesequence(record);
        }    		        
        //Saving RowEntry object in table  
    }
	

		
	}


