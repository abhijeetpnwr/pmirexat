package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.pmirexat.nabi.dao.DbCommands;

public class ds_dbsearch_result_generator
{
	
	public List<List<String>> generatetablle(String tobesearch_rna,String select_datasets,String checkedcategory)
	{
		List<List<String>> dbspecific_result=new DbCommands().Specfic_ds_search0(tobesearch_rna, select_datasets,new class_entityconvert().getclassname(checkedcategory));
		
		StringTokenizer st=new StringTokenizer(select_datasets,",");
		
		List<List<String>> finalresultlist=new ArrayList<List<String>>();				
		
		List<String> datasets=new ArrayList<String>();
		
		while (st.hasMoreElements()) 
		{
			 String dataset = (String) st.nextElement();	
			 datasets.add(dataset);
		}
		
		int totaldatasets=datasets.size();
		
	//	System.out.println("size is"+totaldatasets);
  	  List<String> row_list;
  	  
		 for(int i=0;i<totaldatasets;i++)
	      {
       	   
			 row_list=new ArrayList<String>();
			 
		//	 System.out.println("value of dataset"+datasets.get(0));
			 
			 row_list.add(datasets.get(i));
       	 
 
       	  
       	    	for (List<String> innerlist : dbspecific_result)
       	    	{	 
       	    		row_list.add((innerlist.get(i)));
		        }
       	  
       	  
       	  
		    finalresultlist.add(row_list); 
       	  
	      }
		 
		 
		
		return finalresultlist;
	
	}
	  

}
