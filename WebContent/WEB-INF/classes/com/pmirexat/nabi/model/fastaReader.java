package com.pmirexat.nabi.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public final class fastaReader 
{

    private String [] description;
    private String [] sequence;
    List<String> seq;
    List<String> desc;
    //Pass name of file to this method to read Fasta file format
    public fastaReader(String filename)
    {
       	readSequenceFromFile(filename);
    }

    void readSequenceFromFile(String file)
    {
    	
    //Reads header , and will maintain headers in the array list	
	 desc= new ArrayList();
	
	//Stores sequences in this aray list
	 seq = new ArrayList();

	try
	{
	
        BufferedReader in     = new BufferedReader( new FileReader( file ) );
    
        
        StringBuffer   buffer = new StringBuffer();
        
        String         line   = in.readLine();
     
        //Check for an Empty file
        if( line == null )
            System.out.println(file + " is an empty file" );
     
        
        //File validations check
        if( line.charAt( 0 ) != '>' )
            System.out.println("This is not a proper fasta file , first character should be > ");
        else
            desc.add(line);
        
        
        for( line = in.readLine().trim(); line != null; line = in.readLine() )
        {
            if( line.length()>0 && line.charAt( 0 ) == '>' )
            {
            	System.out.println("  -----------   If called ----------- ");
            	String toaddsequence = buffer.toString();
            	
            	seq.add(toaddsequence);   
            	
            	System.out.println(" added to sequences : "+toaddsequence);
            	buffer = new StringBuffer();
            	
            	desc.add(line);
            	System.out.println("Added to description : "+line);
                System.out.println(" ------------- If Ended ------------------- ");            	
                
            
            }
            else  
            {
            	
            	buffer.append( line.trim() );
            	
            }
        }   
        
        	if( buffer.length() != 0 )
        		seq.add(buffer.toString());
      }
	
	catch(IOException e)
      {
        System.out.println("Error when reading "+file);
        e.printStackTrace();
      }

    }
    
    //return first sequence as a String
    public List<String> getSequence()
    { 
    	return seq;
    }

    //return first xdescription as String
    public  List<String> getDescription()
    {
    	return desc;
    }

    //return sequence as a String
    public String getSequence(int i)
    { 
    	return seq.get(i);
    }

    //return description as String
    public String getDescription(int i)
    {
    	return desc.get(i);
    }
    
    public int size()
    {
    	return sequence.length;
    }

}
