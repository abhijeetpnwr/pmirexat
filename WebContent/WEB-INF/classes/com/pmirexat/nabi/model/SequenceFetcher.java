package com.pmirexat.nabi.model;

import com.pmirexat.nabi.dao.DbCommands;

public class SequenceFetcher
{
   public double getseq(String datasetname)
   {
	   
        double seqno= new DbCommands().getsequenceno(datasetname);
        
        System.out.println("sequence number send in model : "+seqno);
        
		return seqno;
   }
}
