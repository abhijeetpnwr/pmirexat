package com.pmirexat.nabi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.pmirexat.nabi.dao.DbCommands;
import com.sun.xml.internal.ws.api.model.MEP;

// purpose of this class is to perform search operations on the to beasearched parameters and to set 

public class search_result_generator
{
	
    public  List<List<String>> searchdb(String tobesearchdataset,String categorytosearch,List getterstocall)
    {
    	 
	      List<List<Map<String, List<Integer>>>> resultlist= new DbCommands().searchDatabase(tobesearchdataset,categorytosearch,getterstocall);

          return createtable(resultlist, getterstocall);
    }
    
    public  List<List<String>> searchdb_byfold(String tobesearchdataset,String categorytosearch,List getterstocall,String tissuetosearh,float foldcount)
    {
    	 
	      //List<List<Map<String, List<Integer>>>> resultlist= new DbCommands().searchDatabase(tobesearchdataset,categorytosearch,getterstocall);

	      List<List<Map<String, List<String>>>> resultlist =new DbCommands().searchDatabase_byfolchange(tobesearchdataset,categorytosearch,getterstocall, tissuetosearh ,foldcount);

	      System.out.println("result is : "+resultlist);
		  
           List<List<String>> finalresult = createtable_tissuespecific(resultlist, getterstocall);
    
           System.out.println("Result -------- "+resultlist);
           
           return finalresult;
    }
  
    public  List<List<String>> createtable(List<List<Map<String, List<Integer>>>> resultlist,List getterstocall)
    {
        
	      int objectcount, counts;
	      
	      List<String> Datasets=new ArrayList<>(); //list which will contain datasets name which are searched.those 180.Which of them searched as floor/wheat/Maize selectively which are searched.
	       
	      List<List<Integer>> Tempresultlist=new ArrayList<List<Integer>>();
	     
	      List<List<String>> finalresultlist=new ArrayList<List<String>>();
	      
	      // A single list object represents a single object/attribure search result.For earch iterates all result values for this.
	      boolean datasetsetter=false;
	      
	      String resultdataset=null;
	      
	      for (List<Map<String, List<Integer>>> list : resultlist)
	      {
	    	      	  
			 
			 //above list resuld map.Map--->  wheat key-->List of all wheat recorde.Maize--->List of all maize recorde,rice-->List of all rice recorde
			 List<Integer> listtemp=new ArrayList<Integer>(); 
			 
			 
			
			 
			 //this loop iterates over all map result.and creates a list of list(objects retieved from query)
			 for (Map<String, List<Integer>> map : list)
			 {
				 
				// //System.out.println("----- iteration starts-----"+datasetsetter);
				 
				 for (Map.Entry<String, List<Integer>> maptemp : map.entrySet())
			      {
					
					 
		                  /*creates Dataset arraylist.Simple logic --> if a new key is retrieved means new type (wheat/rice/maize) resuts found
		                    			 
			    	        If yes--> call gettes of that and create an combined array list.
			    	        
			    	        reason--> on next jsp's format I need to create a format --> datasetname object1 value....object n's value
			    	      
			    	        So I to get datasets name i need this list 
			    	        
			    	      */
					 
			/*		 //System.out.println("val of mapkey"+maptemp.getKey()+" val of resultdat="+resultdataset);*/
					 
					 if(datasetsetter==false)
					 {
						// //System.out.println("------ first run.. reached inside if-----");
						 
			    	      if (!(maptemp.getKey().equals(resultdataset)) )
			    	      {
			    	    	 
			    	    	//  //System.out.println(" if is called"+maptemp.getKey());
			    	    	  
			    	    	  resultdataset=maptemp.getKey();
			    	    	  
			    	    	    if (maptemp.getKey().equalsIgnoreCase("wheatinnermap"))
			    	    	    {  
			    	    		    
			    	    	 //   	//System.out.println("1--wheatsetters called");
			    	    	    	//System.out.println("Adding : "+getterstocall);
									Datasets.addAll(getterstocall);
							    }	 
			    	    	    else if (maptemp.getKey().equalsIgnoreCase("maizeinnermap"))
			    	    	    {
			    	    	    //	//System.out.println("2-maize called");
								  Datasets.addAll(getterstocall);
			    	    	    }	
			    	    	  
			    	    	    else if (maptemp.getKey().equalsIgnoreCase("riceinnermap"))
			    	    	    {
			    	    	    //	//System.out.println("3-ricesetters called");
								Datasets.addAll(getterstocall);
			    	    	    }
			    	    	  
			    	    	    else
			    	    	    {
							//	//System.out.println("something is wrong with this code !!!"+maptemp.getKey());
			    	    	    }
			    	         }
					 	}
					 
			    	      // as means first time is has run .So status= true as dataset list has been set
					 
			    	      listtemp.addAll(maptemp.getValue()); //values for maptemp(Means rice/wheat/maize))'s list is fetchd
					      //So this adds object/fetched values to finalresult list.rice/wheat/maize searched indivisually as catagory base searched required.fetched in above map's result list.And as final added to this final list
				  }
				 
				 datasetsetter=true;
				
				 ////System.out.println("-------------------iteration ends----------"+datasetsetter);
			 }
			 
			 //This is the result created 
			 Tempresultlist.add(listtemp); 
			 
		
			 
		  }
	      
        counts=Datasets.size();
        
      //  //System.out.println("chl val is -------"+counts);
	      
        for(int i=0;i<counts;i++)
	      {
      	 // //System.out.println("chk"+i);
      	  List<String> row_list=new ArrayList<>();
      	  
      	  row_list.add((Datasets.get(i)));
      	 
      	  //String gettername="get"+colname.substring(0,1).toUpperCase() + colname.substring(1);
      	  
      	  for (List<Integer> value : Tempresultlist)
      	  {	 
				row_list.add((value.get(i).toString()));
			  }
      	  
      	  finalresultlist.add(row_list);
      	  
	      }
	    	    

	        
        
        System.out.println("Returned by table genrator : "+finalresultlist);
		return finalresultlist;
    }
    	
    
    public  List<List<String>> createtable_tissuespecific(List<List<Map<String, List<String>>>> resultlist,List getterstocall)
    {
        
	      int objectcount, counts;
	      
	      List<String> Datasets=new ArrayList<>(); 
	      
	      Datasets.add("Dataset");
	      
	      List<List<String>> Tempresultlist=new ArrayList<List<String>>();
	     
	      List<List<String>> finalresultlist=new ArrayList<List<String>>();
	      
	    
	      boolean datasetsetter=false;
	      
	      String resultdataset=null;
	      
	      for (List<Map<String, List<String>>> list : resultlist)
	      {
	    	      	  
			 
			
			 List<String> listtemp=new ArrayList<String>(); 
			 
			 
			
			 
			
			 for (Map<String, List<String>> map : list)
			 {
				 
				// //System.out.println("----- iteration starts-----"+datasetsetter);
				 
				 for (Map.Entry<String, List<String>> maptemp : map.entrySet())
			      {
					 if(datasetsetter==false)
					 {
						// //System.out.println("------ first run.. reached inside if-----");
						 
			    	      if (!(maptemp.getKey().equals(resultdataset)) )
			    	      {
			    	    	 
			    	    	//  //System.out.println(" if is called"+maptemp.getKey());
			    	    	  
			    	    	  resultdataset=maptemp.getKey();
			    	    	  
			    	    	    if (maptemp.getKey().equalsIgnoreCase("wheatinnermap"))
			    	    	    {  			    	    		   
									Datasets.addAll(getterstocall);
							    }	 
			    	    	    else if (maptemp.getKey().equalsIgnoreCase("maizeinnermap"))
			    	    	    {
								  Datasets.addAll(getterstocall);
			    	    	    }	
			    	    	  
			    	    	    else if (maptemp.getKey().equalsIgnoreCase("riceinnermap"))
			    	    	    {
								Datasets.addAll(getterstocall);
			    	    	    }
			    	    	  
			    	    	    else
			    	    	    {
							//	//System.out.println("something is wrong with this code !!!"+maptemp.getKey());
			    	    	    }
			    	         }
					 	}
					 
			    	      // as means first time is has run .So status= true as dataset list has been set
					 
			    	      listtemp.addAll(maptemp.getValue()); //values for maptemp(Means rice/wheat/maize))'s list is fetchd
					      //So this adds object/fetched values to finalresult list.rice/wheat/maize searched indivisually as catagory base searched required.fetched in above map's result list.And as final added to this final list
				  }
				 
				 datasetsetter=true;
				
				 ////System.out.println("-------------------iteration ends----------"+datasetsetter);
			 }
			 
			 //This is the result created 
			 Tempresultlist.add(listtemp); 
			 
		
			 
		  }
	      
	
	      
        counts=Datasets.size();
	      
        for(int i=0;i<counts;i++)
	      {
      
      	  List<String> row_list=new ArrayList<>();
      	  
      	  row_list.add((Datasets.get(i)));
      	  
      	  for (List<String> value : Tempresultlist)
      	  {	 
				row_list.add((value.get(i).toString()));
			  }
      	  
      	  finalresultlist.add(row_list);
      	  
	      }
   
        System.out.println("Returned by table genrator : "+finalresultlist);
		return finalresultlist;
    }
}
