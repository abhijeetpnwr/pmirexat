<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
      <title>Bootstrap 101 Template</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
      <!--  scripts to be included -->
       <script src="designs/js/jquery-1.11.1.min.js"></script>
      <script src="Bootstrap/js/bootstrap.min.js"></script>   
       <script src="js/enscroll-0.6.1.min.js"></script>   
      <!-- css added-->
      
      <link href="designs/css/style.css" rel="stylesheet">
      
      <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">
       
       <link href="Bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
 

       
<style type="text/css">
#resultdiv {
    overflow: auto;
    height: 420px;
    padding: 0;
    border: 1px solid #b7b7b7;
}

.track3 {
    width: 10px;
    background: rgba(0, 0, 0, 0);
    margin-right: 2px;
    border-radius: 10px;
    -webkit-transition: background 250ms linear;
    transition: background 250ms linear;
}

.track3:hover,
.track3.dragging {
    background: #d9d9d9; /* Browsers without rgba support */
    background: rgba(0, 0, 0, 0.15);
}

.handle3 {
    width: 7px;
    right: 0;
    background: #999;
    background: rgba(0, 0, 0, 0.4);
    border-radius: 7px;
   
}

.track3:hover .handle3,.track3.dragging .handle3 {
    width: 10px;
}

.horizontal-track2 {
    width: 100%;
    height: 13px;
     background: #d9d9d9; /* Browsers without rgba support */
    background: rgba(0, 0, 0, 0.15);
    
    transition: background 100ms;
}

.horizontal-handle2
 {
    height: 13px;
    width: 10px;
    background:background: #999;
    background: rgba(0, 0, 0, 0.4);
    position: absolute;
}

.horizontal-track2:hover,
.horizontal-track2.dragging {
    background: #d9d9d9; /* Browsers without rgba support */
    background: rgba(0, 0, 0, 0.15);
}

}


</style>
 

</head>
<body>

  <div class="container" id="main">
  
   <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
  
      <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.jsp">  <span class="glyphicon glyphicon-home"></span> Portal Name </a>
    </div>
  
  
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
   
       
    <!-- Brand and toggle get grouped for better mobile display -->
    
      <ul class="nav navbar-nav ">
       
         <li><a href="#"> About Us </a></li>
        <li><a href="searchdb.jsp"> <span class="glyphicon glyphicon-search"></span>   Start Search  </a></li>
               
        <li>
        
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a> 
          
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Search for results</a></li>
           
            <li><a href="#">Script2 Check</a></li>
          </ul>  <!--  drop down list content ends -->
          
        </li>
        
         <li ><a href="#"><span class="glyphicon glyphicon-earphone"></span> Contact Us    </a></li>
      </ul>
      
      
     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  
 <div class="page-header">
  <h1>Search Results: </h1>
</div>
  
  
  <div class="row" id="featuresheading">
                             
                          <div class="table-responsive col-sm-12"  id="resultdiv">
   
   
           <table class="table  table-striped table-bordered table-hover table-condensed">
           
          
             <tr>
             <c:forEach var="rna" items="${requestScope.searched_micro_rnas}">
                    <th class="header" >
                        <c:out value="${rna}"></c:out> 
                     </th>   
                          
             </c:forEach>   
             </tr>        
            
              
              <c:forEach var="result" items="${requestScope.result_list}">
               
               <tr>
                 <c:forEach var="intval" items="${result}">
                
                    
                     
                         <td class="col-xs-1">
                         <c:out value="${intval}"></c:out>
                         </td>
                    
               
                 </c:forEach>
               </tr>
             </c:forEach>
             
          </table>   
          </div> <!--  table responsive div ends -->
                 
   </div>
   
   
  
          
 
      </div><!--  container ends -->  
        
    
     <script type="text/javascript">
 $('#resultdiv').enscroll({
	    showOnHover: true,
	    verticalTrackClass: 'track3',
	    verticalHandleClass: 'handle3',
	    horizontalScrolling: true,
	    horizontalTrackClass: 'horizontal-track2',
	    horizontalHandleClass: 'horizontal-handle2',
	    cornerClass: 'corner2'
	});
 
 
 </script> 
 
 <div class="navbar navbar-default navbar-fixed-bottom">
       <div class="container">
       <div class="row">
          <div class="col-sm-12">
                <p class="navbar-text text-center"> Copyright � 2014 Nabi </p> 
          </div>        
       </div>      
       </div>
    </div>  <!--  navbar for the footer ends -->
             
        
</body>
</html>