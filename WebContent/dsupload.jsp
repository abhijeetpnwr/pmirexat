<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PmiRExAt@NABI</title>
  <script src="designs/js/jquery-1.11.1.min.js"></script> <!-- jquery added -->

  <script src="http://code.highcharts.com/highcharts.js"></script>
      <script src="http://code.highcharts.com/modules/data.js"></script>
      <script src="http://code.highcharts.com/modules/heatmap.js"></script>

  <script src="js/blockui.js"></script>  <!--  jquery ui js added -->
  <script src="Bootstrap/js/bootstrap.min.js"></script>  <!--  bootstrap js added -->
  <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet"> 

<link rel="stylesheet" href="//www.fuelcdn.com/fuelux/3.5.0/css/fuelux.min.css">

    <!-- Latest compiled and minified JavaScript -->
    
<link type="text/css" rel="stylesheet" href="http://gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>   
    
        <script src="js/tableExport.js"></script>  
           <script src="js/jquery.base64.js"></script>   

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    

<link type="text/css" rel="stylesheet" href="http://gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />
              
    <script>  
    
    jQuery(function () 
    	{
    	
		
    	
    	
    	var linkbuttons= function()
    	{
    		  $(".showoutput").click(function()
    		  {
    			  myblockui();
    			  $.ajax({
    		  			 type:"POST",
    		   			 url:'newdsresultcontroller?filename='+$(this).attr("value"),
    		   		     success: function(response, status, request)
    		   				{
    		   		    		
    							 var result=response;
    							 $('#results').html(result);
    							 $('#resultsprocessbtn').show();
    							 $.unblockUI(); 
    							 $("#messagetext2").show();
    				   		    	$("#messagetext2").empty();
    						         $("#messagetext2").append("<i class='fa fa-check-circle-o'></i>&nbsp; Here are your search records "); 		   				  	   				  
    						}   
    						    
    		   			});
    	      })
    	}
    	
       	
    	$("#genertateheatmap").click(function()
                {
               	generatemap("csv","resultable","#container");         	
               	scrollto("container");
               	
              });
    	
        function generatemap(csvid,tableid,containerid)
        {  
        	 min = -5 ;
           	 
          	  max = 10 ;
          	 
          	  
          	 ourdataset = new Array();

          	 
          	  ourrna = new Array(); 
          	
          
          	  
          	 $("#"+csvid).empty(); 
          	  
       
         	  
              var table = document.getElementById(tableid);
              
              //selects all colums in table        
              
                row = table.rows[0]; 
             

               $("#"+csvid).append("dataset,rna,value"+"\r\n");

               for (j = 1; j < row.cells.length; j++) 
               {
              	  cell = row.cells[j];	
              	
              	 ourrna.push(cell.textContent.trim());
              	 console.log(cell.textContent.trim());
              	
               } 
                
         
               
              //iterates over rows
              for (i = 3; i < table.rows.length; i += 1)
              { 
              	   row = table.rows[i];
              	   
              	   var seqno;
              	   
              	   //iterate over columns
              	   for (j = 0; j < row.cells.length; j += 1) 
              	   {
              		   cell = row.cells[j];	
              		   var t=cell.textContent.trim();
              		   //alert(t);
              
              		   if(j==0)
              		   {
              			   ourdataset.push(t);	  //Inserting value into array dataset 
              			   $.ajax({  
                        		 type:"get",  
                        		 url:"sequenceget", 
                        		 async:false,
                        		 data:{
                                         term : t,
                                      },
                      			 success:function(result)
                         			{                    			
                         			  seqno=result;
                         		
                        			}  
                                      
                      		 });    
              		
              		   }	   
              		   
              		   else
              		   {
              			   if(j==1)
                  		   {
                  			     var x = cell.headers;       
                  		   }
                  		   
              			  
                  		   heatmapval1= parseInt((cell.innerHTML).trim()) + 1;	
                  		   
                  		   
                  		   
                      	   heatmapval2=((heatmapval1*1000000)/seqno).toFixed(6);
                      	
                  		   heatmapval=(Math.log(heatmapval2)/Math.LN2).toFixed(6);
                      	   
                  		   if(Math.round(heatmapval) > max)
                  		   {
                  		    
                  			   max = Math.round(heatmapval);   
                                 max = (max - (max % 5))  + 5;
                                 
                                // alert("max  chacnged to : " + max);
                  		   }
                  		  
                  		   if(Math.round(heatmapval) < min)
                  		   {
                  			  min = Math.round(heatmapval);   			  
                  			  min = (min - (min % 5 )) - 5;
                  		   }
                  		   
                  		   var arr = new Array(3);
                  		   arr[0] = i-1;
                  		   arr[1] = j-1;
                  		   arr[2] = heatmapval;
                  		   $("#"+csvid).append(arr.join(",")+"\r\n");
              		   }	   
              		   
              		  
              	    }
              }

                  foo(csvid,containerid);
        }
    	
    	
    	var linkapprovebuttons    = function()
    	{
    		  $(".approvebtn").click(function()
    		  {
    			  
    			  $.ajax({
    		  			 type:"POST",
    		   			 url:'approvesinginuser?email='+$(this).attr("value"),
    		   		     success: function(response, status, request)
    		   				{
    		   		    		//console.log(" Result approved ");
    							
    						}   
    						    
    		   			});
    	      })
    	}
    	
    	
    	var loadlist = function()
    	{ 
    			  $.ajax({
    		  			 type:"POST",
    		   			 url:'miRspeciscontroller',
    		   		     success: function(response, status, request)
    		   				{
    		   		    	var s = $('<select />');
    		   		    	s.attr("name","searcagainst");
    		   		    	s.attr("id","miRspecies")
    		   		    	for(var i=0;i<response.length;i++)
  						      { 						      
  						         var toshow =  response[i]["speciesname"];
  						         var val = response[i]["specieval"]; 
  						         //console.log("Log val is :",val);
  						       	 $('<option />', {value: val+"@"+toshow, text: toshow}).appendTo(s);
						      }  
    
    		   		    	$("#specisselect").append(s);
    						}   
    						    
    		   			}); 
    			  
    			  setTimeout(
    					  function() 
    					  {
    						  $("select").selectBoxIt();
    					  }, 100);
    			  
    			  console.log("Now should change select box style")
    			  
    	}
    	
    	loadlist();
    	
    	 $("#download").click(function()
      			  {
  	                     
      		             $('#resultable').tableExport({type:'excel',escape:'false'});
      		             
      			  });
    	 
    		
		 $("#logout").click(function()
     			  {
 	                     
    		 $.ajax({
	  			 type:"POST",
	   			 url:'signout',
	   		     success: function(response, status, request)
	   				{
	   		    		window.location.href = "signup.jsp";		   				  	   				  
					}   
					    
	   			});
     		             
     			  });
    	 
    	 
			//Block ui implementation ---- 
    	    function myblockui()
    	    {	
    				 $.blockUI({ 
    	   		 
    				 message: ' <h5 style="line-height: 200%;"> We are processing your request.  Please be patient </h5> ',
    				 css: { 
    	           			 border: 'none', 
    	           			 width: '35%',
    	           			 padding: '0px', 
    	           			 backgroundColor: '#000', 
    	           			 '-webkit-border-radius': '10px', 
    	          			  '-moz-border-radius': '10px', 
    	           				 opacity: .7, 
    	           				 color: '#fff' 
    	       				 } 
    	   		 			});
    	    }
    	 
    	
    	 //To dynamically generating result table from received ajax data
    	 
  		var ajax_call = function() 
  		{
    		 
  			var email = "${sessionScope.Email}";
  	    	 $.ajax({
  		  			 type:"POST",
  		   			 url:'newdsresultdisplay?q='+email,
  		   		     success: function(response, status, request)
  		   				{
  		   			    $("#previousresult").empty();
  		   		    	 
  						 var result=response;
  						 //console.log(result);
  						 
  						if(result.length > 0)
  							{
  							
  						
  							$("#previousresult").append("Your previous search results :")
  							var tbl=$("<table/>").attr("id","mytable");
  	  						
  	  						    $("#previousresult").append(tbl);
  	  						    $("#mytable").append("<tr><td><strong>File Name</strong></td><td><strong>Searched Against</strong></td><td><strong>Submitted On</strong></td><td><strong>Current Status</strong></td><td><strong>Show Results</strong></td></tr>"); 
  	  						    for(var i=0;i<result.length;i++)
  	  						    {
  	  						        var tr="<tr>";
  	  						        var td1="<td>"+result[i]["Filename"]+"</td>";
  	  						        
  	  						        var searchagainst = result[i]["Searched against"]
  	  				
  	  						        if(searchagainst.indexOf("@")>0)
  	  						        	{
  	  						        		searchagainst = searchagainst.split("@")[0];
  	  						        	}
  	  						        
  	  						        var td2="<td>"+searchagainst+"</td>";
  	  						        var td4="<td>"+result[i]["Status"]+"</td>";
  	  						        var td3="<td>"+result[i]["Submission time"]+"</td>";

  	  						        if(result[i]["Output"]==="None")
  	  						        	{
  	  						        	   var td5 = "<td>Not yet processed</td></tr>";
  	  						        	}
  	  						        else
  	  						        	{
  	  						        	   var td5 = "<td>"+"<button class=\"showoutput\" name=\"downloadbtn\" value=\""+result[i]["Output"]+"\">Show Results </button>"+"</td>"+"</tr>";
  	  						        	   //console.log(td5);
  	  						        	}
  	  						        	
  	  						        	
  	  						       $("#mytable").append(tr+td1+td2+td3+td4+td5); 
  	  						       $("#mytable").addClass("table table-striped table-bordered table-hover");

  	  						    }   
  	  						    
  	  						
  	  						  linkbuttons();
  							}
  						
  						else
  							{
  							$("#previousresult").append("You have no previous search results in your database")
  							}
  						 
  						 
  						 
  		   			}
  	    	  });
  	    	 
  	    		
		};
		
		var ajax_approverequests = function() 
  		{
    		 
  	    	 
  	    	 $.ajax({
  		  			 type:"POST",
  		   			 url:'verifyloginusers',
  		   		     success: function(response, status, request)
  		   				{
  						 var result=response;
  						 //console.log(result);
  						 
  						$("#toverify").empty();
  						
  						    
  						    if(result.length > 0)
  						    {
  						    	 var tb=$("<table/>").attr("id","toverifytable");
  		  						
  						    	 
  		  						    $("#toverify").append(tb);
  		  						    $("#toverifytable").append("<tr><td><strong>Name</strong></td><td><strong>Email Id</strong></td><td><strong>Institute</strong></td><td><strong>Department</strong></td><td><strong>Contact No.</strong></td><td><td><strong>Address</strong></td><td><strong>Reason to Signup</strong></td> <td><strong>Time of request </strong></td><strong>Approve requests</strong></td></tr>"); 
  		  						    for(var i=0;i<result.length;i++)
  	  						    	{
  	  						        	var tr="<tr>";
  	  						    		var td1="<td>"+result[i]["First Name"]+"  "+result[i]["Last Name"]+"</td>";
  	  						        	var td2="<td>"+result[i]["Email Id"]+"</td>";  	  						       	  						        
  	  						        	var td3="<td>"+result[i]["Institute"]+"</td>";
  	  						    		var td4="<td>"+result[i]["Department"]+"</td>";
  	  									var td5="<td>"+result[i]["Contact No."]+"</td>";
  	  									var td6="<td>"+result[i]["Address"]+"</td>";
  	  									var td7="<td>"+result[i]["Reason"]+"</td>";
  	  						       
  	  						        	var td8="<td>"+result[i]["Request Date"]+"</td>";
  	  						       		var td9 = "<td>"+"<button class=\"approvebtn\" name=\"approvebtn\" value=\""+result[i]["Email Id"]+"\">Approve Request </button>"+"</td>"+"</tr>";	
  	  						      		 $("#toverifytable").append(tr+td1+td2+td3+td4+td5+td6+td7+td8+td9); 
  	  						       		$("#toverifytable").addClass("table table-striped table-bordered table-hover");

  	  						    	}   
  		  						linkapprovebuttons();
  						    }
  						    
  						    else
  						    	{
  						    	 $("#toverify").append("No user accounts to validate."); 
  						    	}
  						    
  						   				 
  		   			}
  	    	  });
  	    	 
  	    	  
  	    	 
  	    		
		};

		setInterval(ajax_approverequests,5000);
    	 
		setInterval(ajax_call, 2000);
		

    	var fileInput = document.getElementById('file'); 
    	
     	fileInput.addEventListener('change', function(e) 
     			{
     		 
     		    chkfile = true;
  		    
     		    var file = fileInput.files[0];
     		 

    				var reader = new FileReader();
    				
    				reader.onload = function(e)
    				{
    					filecontent =  reader.result.trim();

    					var res = filecontent.split("\n");
    					
    					 lines = res.length;
    					
    					 if(filecontent.length == 0)
    						 {
    						    msg = "You have entered an empty file ";
    						
    						    $( "#filediv" ).removeClass( "has-success" );
    						    chkfile = false;
    						 }
    					 
    				
    					 
    					 if(chkfile == true)
    						 {
    						   var todisplay = "file is selected to upload";
    						    
    						   $("#fileuploadinfo").val(todisplay)
    						 $( "#filediv" ).addClass( "has-success" );
    						   $( "#filediv" ).removeClass( "has-error" );
    						 }
    					 
    					 else
    						 {
    						 $("#fileuploadinfo").val("File does't match uplaod criteria");
    						 $( "#filediv" ).removeClass( "has-success" );
    						   $( "#filediv" ).addClass( "has-error" );
    						 }
    					
    					//lines = res.length;
    				}				
    				reader.readAsText(file);			
    		});
    	 
    	 
    	
    	   var form = $('#fileupload');
    	   
    	   form.submit(function () 
    	   {	

    		  var formData = new FormData($(this)[0]);
    		   $.ajax({
		  			 type: form.attr('method'),
		   			 url: form.attr('action'),
		   			data: formData,
		   	        cache: false,
		   	        contentType: false,
		   	        processData: false,
		   		     success: function(response, status, request)
		   				{
		   		    	$("#messagetext").show();
		   		    	$("#messagetext").empty();
				         $("#messagetext").append("<i class='fa fa-check-circle-o'></i>&nbsp; <strong> Your file  is sucessfully uploaded.</strong> You can check your submitted search status on Check Results tab on this page "); 		   				  	   				  
		   				}
		   			});
    		   return false;
    	   }); 
    	   
    	   
    	   
    	   $('#submitds').on('show.bs.tab', function (e) 
    	    {
    		   $('#results').empty();
    		   $('#resultsprocessbtn').hide();
    		   $('#messagetext2').hide();
    		 })
    	   
    		$('#checkresults').on('show.bs.tab', function (e) 
    	    {
    		   $('#messagetext').hide();
    		 })
    		 
    		  $('#approverequests').on('show.bs.tab', function (e) 
    	      {
    		   $('#results').empty();
    		   $('#resultsprocessbtn').hide();
    		   $('#messagetext2').hide();
    		   ajax_approverequests()
    		  })	
    		  
    		  
    		  
    		    //This funtion genrate main tables;
    		    
    		    function foo(csvid, containerid)
    		    {
    		  	    /**
    		         * This plugin extends Highcharts in two ways:
    		         * - Use HTML5 canvas instead of SVG for rendering of the heatmap squares. Canvas
    		         *   outperforms SVG when it comes to thousands of single shapes.
    		         * - Add a K-D-tree to find the nearest point on mouse move. Since we no longer have SVG shapes
    		         *   to capture mouseovers, we need another way of detecting hover points for the tooltip.
    		         */
    		    
    		        (function (H) {
    		            var wrap = H.wrap,
    		                seriesTypes = H.seriesTypes;

    		            /**
    		             * Recursively builds a K-D-tree
    		             */
    		            function KDTree(points, depth) {
    		                var axis, median, length = points && points.length;

    		                if (length) {

    		                    // alternate between the axis
    		                    axis = ['plotX', 'plotY'][depth % 2];

    		                    // sort point array
    		                    points.sort(function (a, b) {
    		                        return a[axis] - b[axis];
    		                    });

    		                    median = Math.floor(length / 2);

    		                    // build and return node
    		                    return {
    		                        point: points[median],
    		                        left: KDTree(points.slice(0, median), depth + 1),
    		                        right: KDTree(points.slice(median + 1), depth + 1)
    		                    };

    		                }
    		            }

    		            /**
    		             * Recursively searches for the nearest neighbour using the given K-D-tree
    		             */
    		            function nearest(search, tree, depth) {
    		                var point = tree.point,
    		                    axis = ['plotX', 'plotY'][depth % 2],
    		                    tdist,
    		                    sideA,
    		                    sideB,
    		                    ret = point,
    		                    nPoint1,
    		                    nPoint2;

    		                // Get distance
    		                point.dist = Math.pow(search.plotX - point.plotX, 2) +
    		                    Math.pow(search.plotY - point.plotY, 2);

    		                // Pick side based on distance to splitting point
    		                tdist = search[axis] - point[axis];
    		                sideA = tdist < 0 ? 'left' : 'right';

    		                // End of tree
    		                if (tree[sideA]) {
    		                    nPoint1 = nearest(search, tree[sideA], depth + 1);

    		                    ret = (nPoint1.dist < ret.dist ? nPoint1 : point);

    		                    sideB = tdist < 0 ? 'right' : 'left';
    		                    if (tree[sideB]) {
    		                        // compare distance to current best to splitting point to decide wether to check side B or not
    		                        if (Math.abs(tdist) < ret.dist) {
    		                            nPoint2 = nearest(search, tree[sideB], depth + 1);
    		                            ret = (nPoint2.dist < ret.dist ? nPoint2 : ret);
    		                        }
    		                    }
    		                }
    		                return ret;
    		            }

    		            // Extend the heatmap to use the K-D-tree to search for nearest points
    		            H.seriesTypes.heatmap.prototype.setTooltipPoints = function () {
    		                var series = this;

    		                this.tree = null;
    		                setTimeout(function () {
    		                    series.tree = KDTree(series.points, 0);
    		                });
    		            };
    		            H.seriesTypes.heatmap.prototype.getNearest = function (search) {
    		                if (this.tree) {
    		                    return nearest(search, this.tree, 0);
    		                }
    		            };

    		            H.wrap(H.Pointer.prototype, 'runPointActions', function (proceed, e) {
    		                var chart = this.chart;
    		                proceed.call(this, e);

    		                // Draw independent tooltips
    		                H.each(chart.series, function (series) {
    		                    var point;
    		                    if (series.getNearest) {
    		                        point = series.getNearest({
    		                            plotX: e.chartX - chart.plotLeft,
    		                            plotY: e.chartY - chart.plotTop
    		                        });
    		                        if (point) {
    		                            point.onMouseOver(e);
    		                        }
    		                    }
    		                })
    		            });

    		            /**
    		             * Get the canvas context for a series
    		             */
    		            H.Series.prototype.getContext = function () {
    		                var canvas;
    		                if (!this.ctx) {
    		                    canvas = document.createElement('canvas');
    		                    canvas.setAttribute('width', this.chart.plotWidth);
    		                    canvas.setAttribute('height', this.chart.plotHeight);
    		                    canvas.style.position = 'absolute';
    		                    canvas.style.left = this.group.translateX + 'px';
    		                    canvas.style.top = this.group.translateY + 'px';
    		                    canvas.style.zIndex = 0;
    		                    canvas.style.cursor = 'crosshair';
    		                    this.chart.container.appendChild(canvas);
    		                    if (canvas.getContext) {
    		                        this.ctx = canvas.getContext('2d');
    		                    }
    		                }
    		                return this.ctx;
    		            }

    		            /**
    		             * Wrap the drawPoints method to draw the points in canvas instead of the slower SVG,
    		             * that requires one shape each point.
    		             */
    		            H.wrap(H.seriesTypes.heatmap.prototype, 'drawPoints', function (proceed) {

    		                var ctx;
    		                if (this.chart.renderer.forExport) {
    		                    // Run SVG shapes
    		                    proceed.call(this);

    		                } else {

    		                    if (ctx = this.getContext()) {

    		                        // draw the columns
    		                        H.each(this.points, function (point) {
    		                            var plotY = point.plotY,
    		                                shapeArgs;

    		                            if (plotY !== undefined && !isNaN(plotY) && point.y !== null) {
    		                                shapeArgs = point.shapeArgs;

    		                                ctx.fillStyle = point.pointAttr[''].fill;
    		                                ctx.fillRect(shapeArgs.x, shapeArgs.y, shapeArgs.width, shapeArgs.height);
    		                            }
    		                        });

    		                    } else {
    		                        this.chart.showLoading("Your browser doesn't support HTML5 canvas, <br>please use a modern browser");

    		                        // Uncomment this to provide low-level (slow) support in oldIE. It will cause script errors on
    		                        // charts with more than a few thousand points.
    		                        //proceed.call(this);
    		                    }
    		                }
    		            });
    		        }(Highcharts));


    		        var start;
    		       
    		        
    		        $(containerid).highcharts({

    		            data: {
    		                csv: document.getElementById(csvid).innerHTML,
    		                parsed: function () 
    		                {
    		                	
    		                    start = start+1;
    		                    
    		                }
    		            },

    		            credits: false,
    		            
    		            chart: {
    		                type: 'heatmap',
    		                height: 800,
    		                zoomType: 'xy',
    		                resetZoomButton: {
    		                    position: {
    		                        // align: 'right', // by default
    		                        // verticalAlign: 'top', // by default
    		                        x: -40,
    		                        y: 10
    		                    },
    		                    relativeTo: 'chart'
    		                  }

    		                
    		            },


    		            title: {
    		                text: 'Heatmap for Datasets and RNAs',
    		                align: 'left',
    		                x: 40
    		            },

    		            subtitle: {
    		                text: 'Heatmap for log TPM(Transcript per million) values calculated after adding pseudo count of one to each miRNA count',
    		                align: 'left',
    		                x: 40
    		            },

    		            tooltip: {
    		            	formatter: function() 
    		            	{       		
    		                    return '<b> Dataset : '+ ourdataset[this.point.x] +'</br> Sequence :  '+ ourrna[this.point.y] + '</br> TPM Value :  ' + this.point.value + '</b>';
    		                },
    		                backgroundColor: null,
    		                borderWidth: 0,
    		                distance: 10,
    		                shadow: false,
    		                useHTML: true,
    		                style: {
    		                    padding: 0,
    		                    color: 'black'
    		                }
    		            },

    		            xAxis: {
    		                title: {
    		                    enabled: true,
    		                    text: 'Datasets'
    		                    },
    		                    
    		                    labels: {
    		                        rotation: -90,
    		                        style: {
    		                            fontSize: '12px',
    		                            fontFamily: 'Verdana, sans-serif'
    		                        }
    		                    },
    		       
    		                categories: ourdataset
    		            },

    		            yAxis: {
    		            	
    		            	title:
    		                    {
    		                    enabled: true,     
    		                    text: 'microRNA'
    		                    },
    		                  
    		                   minorTickLength: 1,
    		            },


    		            colorAxis: {
    		                stops: [
    		                    [0, '#3060cf'],
    		                    [0.5, '#fffbbc'],
    		                    [0.9, '#c4463a'],
    		                    [1, '#c4463a']
    		                ],
    		                min: min,
    		                max: max,
    		                startOnTick: false,
    		                endOnTick: false,
    		                labels: {
    		                    format: '{value}'
    		                }
    		            },

    		            series: [{
    		            	
    		                borderWidth: 0,
    		                nullColor: '#EFEFEF',
    		               
    		               
    		                turboThreshold: Number.MAX_VALUE // #3404, remove after 4.0.5 release
    		            }]

    		        });
    		    
    		    };
    		  
    		  
     	});
         
  
    
   </script> 
<style>
   
   
.nav>li>a
{
padding-left: 10px;
}
    
   
     .navbar a:hover { color: #428bca !important; }
   
   .navbar-inverse {
    background-color: #428BCA;
}      
.bgcontainer {    
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    padding-bottom:100px;
}


.highcharts-tooltip>span
 {
	background: rgba(255,255,255,0.85);
	border: 1px solid silver;
	border-radius: 3px;
	box-shadow: 1px 1px 2px #888;
	padding: 8px;
	z-index: 2;
}




    /***
Bootstrap Line Tabs by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
***/

/* Tabs panel */
.tabbable-panel {
  border:1px solid #eee;
  padding: 10px;
}

/* Default mode */
.tabbable-line > .nav-tabs {
  border: none;
  margin: 0px;
}
.tabbable-line > .nav-tabs > li {
  margin-right: 2px;
}
.tabbable-line > .nav-tabs > li > a {
  border: 0;
  margin-right: 0;
  color: #737373;
}
.tabbable-line > .nav-tabs > li > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
  border-bottom: 4px solid #fbcdcf;
}
.tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
  border: 0;
  background: none !important;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
  color: #a6a6a6;
}
.tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
  margin-top: 0px;
}
.tabbable-line > .nav-tabs > li.active {
  border-bottom: 4px solid #f3565d;
  position: relative;
}
.tabbable-line > .nav-tabs > li.active > a {
  border: 0;
  color: #333333;
}
.tabbable-line > .nav-tabs > li.active > a > i {
  color: #404040;
}
.tabbable-line > .tab-content {
  margin-top: -3px;
  background-color: #fff;
  border: 0;
  border-top: 1px solid #eee;
  padding: 15px 0;
}
.portlet .tabbable-line > .tab-content {
  padding-bottom: 0;
}

/* Below tabs mode */

.tabbable-line.tabs-below > .nav-tabs > li {
  border-top: 4px solid transparent;
}
.tabbable-line.tabs-below > .nav-tabs > li > a {
  margin-top: 0;
}
.tabbable-line.tabs-below > .nav-tabs > li:hover {
  border-bottom: 0;
  border-top: 4px solid #fbcdcf;
}
.tabbable-line.tabs-below > .nav-tabs > li.active {
  margin-bottom: -2px;
  border-bottom: 0;
  border-top: 4px solid #f3565d;
}
.tabbable-line.tabs-below > .tab-content {
  margin-top: -10px;
  border-top: 0;
  border-bottom: 1px solid #eee;
  padding-bottom: 15px;
}
    
</style>

</head>
<body>

<!--
Bootstrap Line Tabs by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
-->

<div class="container">

<header class="navbar navbar-default " role="banner">
        <div class="container ">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                 <a class="navbar-brand" href="index.html">  <span class="glyphicon glyphicon-home"></span> PmiRExAt </a>
            </div>
            <nav class="collapse navbar-collapse  bs-navbar-collapse" role="navigation">
       <ul class="nav navbar-nav ">
       
         <li><a href="about.html"> <i class="fa fa-users"></i>
          About </a></li>
           <li><a href="datashow.html"> <i class="glyphicon glyphicon-book"></i>
          Related Data </a></li>
          
           <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Browse <b class="caret"></b></a>
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="browse.html">By Species</a></li>
                    <li><a href="crossspecies.html">By Across Species</a></li> 
                     <li><a href="bylatestpublications.html">By Latest Publications</a></li>     
                     <li><a href="tissuebrowse.html">By Tissue Preferential</a></li>    
                      <li><a href="diffexpbrowse.html">By Differential Expression</a></li>                 
           </ul>
           </li>
           
           <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search"></i>Search <b class="caret"></b></a>
          
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="searchdb.html">Expression Search</a></li>
                    <li><a href="tissuespecific.html"> Tissue preferential expression in species (Foldchange and ROKU)</a></li>   
                    <li><a href="tissuepreferentialde.html"> Tissue preferential expression in species (edgeR)</a></li>                           
          			 <li><a href="diffexpsearch.html">Differential Expression</a></li>
           </ul>
           </li>
           
          
           
         <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search-plus"></i> Custom Search <b class="caret"></b></a>
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="file.html">miRNA Profiling</a></li>
                    <li><a href="checkstatus.html">Search miRNA  profiling results</a></li>  
                    <li><a href="dsupload.jsp">Dataset Profiling</a></li>                
           </ul>
          </li> 
            <li ><a href="downloads.html"><span class="glyphicon glyphicon-download-alt"></span> Downloads    </a></li>
        
         <li ><a href="api.html">PmiRExAt API</a></li>
        
         <li ><a href="contactus.html"><span class="glyphicon glyphicon-earphone"></span> Contact Us    </a></li>

         <li ><a href="support.html"><span class="glyphicon glyphicon-download-alt"></span>   Support </a></li> 
     
      </ul>
 
                 
        
    
            </nav>
        </div>
    </header>







		 <span class="text-capitalize"><strong> Welcome ${sessionScope.Firstname}</strong></span>  <span style="margin-left:80%"> 	<button class="btn btn-sm btn-info" id="logout"  href="#" target="_blank" value="Logout">
									Logout </button> </span>
		 
	
		
			<div class="tabbable-panel" style="margin-top: 20px">
				<div class="tabbable-line ">
				
					<ul class="nav nav-tabs " >
						<li class="active">
							<a href="#tab_default_1" id="submitds" class="ourtab" data-toggle="tab">
							Start Search </a>
						</li>
						<li>
							<a href="#tab_default_2" id="checkresults" class="ourtab" data-toggle="tab">
							Check Results </a>
						</li>
					<c:if test="${sessionScope.role eq 'Admin'}"><li>
							<a href="#tab_default_3" id="approverequests" class="ourtab" data-toggle="tab">
							Approve signup requests </a>
						</li></c:if>
					</ul>
				
					<div class="tab-content">
						<div class="tab-pane active " id="tab_default_1">
							
						 

								 <form id="fileupload" action="Dsupload" method="POST" enctype="multipart/form-data"  style="margin-top: 20px;">
								   
								    <div class="form-group" id="filediv" >  
								    		<label for="file"><strong>Upload your compressed SRA File: </strong></label>
								     		<input type="file" class="form-control"  name="file" id="file"  required accept=".gz,.sra" style="margin-top: 10px"> </input>
								                <p class="help-block">*Note: Your file shoul be in compressed gzip format</p>   
									 </div>
														
										<div class="form-group" id="specisselect">
										
										</div>
								    <br/>
								    
								    <div class="form-group"> 
								        <input type="submit" value="Submit for search"  class="btn btn-primary btn-sm"></input>  
								         <input type="button" id="setdefault" value="Set Default"  class="btn btn-warning btn-sm"></input>    
								    </div>						
							
								
								  </form>
																
		            </div>
						
						
					<div class="tab-pane table-responsive" id="tab_default_2">												
						
						
						<div id="previousresult" style="margin-top: 20px">
						
						</div>
									
					</div>
						
					<div class="tab-pane table-responsive" id="tab_default_3">												
						
						
						<div id="toverify" style="margin-top: 20px">
						
						   This tab will display all entries waiting for authentication
						
						</div>
									
					</div>
						
					</div>
	         	</div>
			</div>
		
			<div class="container">
	
	  <div  id = "messagetext" class="alert alert-success alert-dismissible"  role="alert" style="width: 100%;margin-right: auto;display:none; margin-top: 5px;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> 
 
   </div>
 
 	  <div  id = "messagetext2" class="alert alert-success alert-dismissible"  role="alert" style="width: 100%;margin-right: auto;display:none; margin-top: 5px;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> 
 
   </div>
   
   
<div id="results">

</div>

    <div id="container" style="margin-top: 30px;">
  
  </div>
  
  
  </div>
  	


<div id= "resultsprocessbtn" style="display: none;">
<button class="btn btn-sm btn-success" id="genertateheatmap"  href="#" target="_blank" value="Genrate heatmap">
									Generate Heatmap </button>
								<button class="btn btn-sm btn-warning" id="download"  href="#" target="_blank" value="Genrate heatmap">
									Export Table  Data </button>

</div>

	
</div>	

 

  <pre id="csv" style="overflow: hidden;display: none;">
  
  </pre>
  
	
    <div style="height: 50px"></div>
    
    <div class="navbar navbar-default navbar-fixed-bottom" style="margin-top:3px;height:30px;">
       <div class="container">
       <div class="row">
          <div class="col-md-4 col-xs-7">
                <p class="navbar-text text-center"> Copyright <i class="fa fa-copyright"></i>
 2014  NABI </p> 
          </div>
          <div class = "col-md-6">
              
          </div>
          <div class = "col-md-2 col-xs 5">
             <p class="navbar-text text-right"> Beta Version </p>                  
          </div>        
       </div>      
       </div>
    </div>  <!--  navbar for the footer ends -->
 </div> <!-- container -->
	

</body>
</html>
