<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

 <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
      <!--  scripts to be included -->
      <script src="designs/js/jquery-1.11.1.min.js"></script> <!-- jquery added -->
      
      <script src="Bootstrap/js/bootstrap.min.js"></script>  <!--  bootstrap js added -->
      
      <script src="js/jquery-ui.js"></script>  <!--  jquery ui js added -->
      
      <script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script> <!-- tagit js added-->
      
      <!-- css added-->
      
      <link href="designs/css/style.css" rel="stylesheet">  <!-- Our customized css -->
      
       <link rel="stylesheet" href="css/jquery-ui.css"> <!--  jquery ui compnents css -->
      
      <!--  bootstrap css -->
      
      
      <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
       
      <link href="Bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">


      <!--  tabit css -->

      <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
      <link href="css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
     
 
 
<script>



//jquery function for tagging effect with autocompleter.

$(document).ready(function()
{

	 $('#submit_button').hide();
	 
	 $('#specific_searchspan').hide();
	 
	 $('#catagory_searchspan').hide();
	 
     $("#tosearch").tagit({
     autocomplete:
     {
     source: "rna_autocompleter",
     }
     }); //tosearch tagit method ends
     
     
     $("#select_datasets").tagit(
      {
    	  autocomplete:
    	     {
    	     source: "Datasets_autocompleter_controller",
    	     }
      }); //select_datasets tag-it function ends
       
     $("#specific_search").click(function(){
    	  $("#specific_searchspan").show();
    	  $("#catagory_searchspan").hide();
    	  $('#submit_button').css('margin-top', '10px');
    	  $('#submit_button').show();   	  
    	});
      
     $("#catagory_search").click(function(){
   	    $("#catagory_searchspan").show();
    	$("#specific_searchspan").hide();
    	 $('#submit_button').css('margin-top', '50px');
    	 $('#submit_button').show();
   	});
   
      
}); //document.ready ends



</script>

<style>
.squaredThree {
	width: 20px;	
	margin: 20px auto;
	position: relative;
}

.squaredThree label {
	cursor: pointer;
	position: relative;
	width: 20px;
	height: 20px;
	top: 0;
	border-radius: 4px;

	-webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,.4);
	-moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,.4);
	box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,.4);

	background: -webkit-linear-gradient(top, #222 0%, #45484d 100%);
	background: -moz-linear-gradient(top, #222 0%, #45484d 100%);
	background: -o-linear-gradient(top, #222 0%, #45484d 100%);
	background: -ms-linear-gradient(top, #222 0%, #45484d 100%);
	background: linear-gradient(top, #222 0%, #45484d 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#222', endColorstr='#45484d',GradientType=0 );
}

.squaredThree label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	filter: alpha(opacity=0);
	opacity: 0;
	content: '';
	position: absolute;
	width: 9px;
	height: 5px;
	background: transparent;
	top: 4px;
	left: 4px;
	border: 3px solid #fcfff4;
	border-top: none;
	border-right: none;

	-webkit-transform: rotate(-45deg);
	-moz-transform: rotate(-45deg);
	-o-transform: rotate(-45deg);
	-ms-transform: rotate(-45deg);
	transform: rotate(-45deg);
}

.squaredThree label:hover::after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	filter: alpha(opacity=30);
	opacity: 0.3;
}

.squaredThree input[type=checkbox]:checked + label:after {
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
	filter: alpha(opacity=100);
	opacity: 1;
}

</style>

</head>
<body>
<div class="container" id="main">
 <nav class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
  
      <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.jsp">  <span class="glyphicon glyphicon-home"></span> Portal Name </a>
    </div>
  
  
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
   
       
    <!-- Brand and toggle get grouped for better mobile display -->
    
      <ul class="nav navbar-nav ">
       
         <li><a href="#"> About Us </a></li>
        <li><a href="searchdb.jsp"> <span class="glyphicon glyphicon-search"></span>   Start Search  </a></li>
               
        <li>
        
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a> 
          
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Search for results</a></li>
           
            <li><a href="#">Script2 Check</a></li>
          </ul>  <!--  drop down list content ends -->
          
        </li>
        
         <li ><a href="#"><span class="glyphicon glyphicon-earphone"></span> Contact Us    </a></li>
      </ul>
      
      
     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  
 <div class="btn-group" style="margin-bottom:2%;margin-top:0">
  <button type="button" class="btn   btn-success" id="specific_search"> Search Specific Dataset</button>
  <button type="button" class="btn  btn-danger" id="catagory_search">  Catagory wise Search</button>
</div>
 
  
   <form action="getrecords"  method="post" id="testtab">

<h4> Please Enter Search Parameters: </h4>   <input type="text" id="tosearch" name="tosearch">




<span id="specific_searchspan">

<input type="text" id="select_datasets" name="select_datasets" >

</span>

 <div class="row" style="height:0px">
  

 <span class="squaredThree" id="catagory_searchspan">         
   
      &nbsp; &nbsp;&nbsp; Search wheat database: &nbsp;&nbsp;&nbsp;

	  <input type="checkbox" name="Catagory1" value="wheat" id="squaredThree1" name="check" style="display:none"/>
	  &nbsp;&nbsp;&nbsp; <label for="squaredThree1">&nbsp;&nbsp;&nbsp;</label>
	            
 &nbsp;&nbsp;&nbsp;	  <input type="checkbox" name="Catagory2" value="maize" id="squaredThree2" name="check" style="display:none"/>
  Search maize database:	&nbsp;&nbsp;&nbsp;<label for="squaredThree2">&nbsp;&nbsp;&nbsp; </label>
	
	
	 &nbsp;&nbsp;&nbsp; <input type="checkbox" name="Catagory3"  value="rice" id="squaredThree3" name="check" style="display:none"/>
	Search rice database:&nbsp;&nbsp;&nbsp;   <label for="squaredThree3">&nbsp;&nbsp;&nbsp;</label>
</span>
<br>  <br>  <br>

</div>  <!--  Row div ends -->
<input type="submit" class="btn btn-primary" id="submit_button" style="width:30%;"> 



</form>



</div> <!--  Container Ends -->
  <div class="navbar navbar-default navbar-fixed-bottom">
       <div class="container">
       <div class="row">
          <div class="col-sm-12">
                <p class="navbar-text text-center"> Copyright � 2014 Nabi </p> 
                
          </div>        
       </div>      
       </div>
    </div>  <!--  navbar for the footer ends -->
 


</body>
</html>