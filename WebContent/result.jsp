
<%@page import="java.util.ArrayList"%>
<%@page import="com.pmirexat.nabi.dao.DbCommands"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

                           
       <c:choose>
    <c:when test="${not empty requestScope.result_list}">
        
         <div id = "recordsuccess" class="alert alert-success alert-dismissible" role="alert" style="width: 95%;margin-left: auto;margin-right: auto;"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <i class="fa fa-check-circle-o"></i>&nbsp; Search completed, Following are the expression counts record of input miRs against selected tissue dataset/s
         
   
    </div>
         
         <div class="container">
         
         <%
           
             if(request.getAttribute("tableid") == null)
             {
            	 out.println("<table class=\"table  table-striped table-bordered table-hover table-condensed\" id=\"resultable\" style=\"max-height:270px;overflow:auto;width:100%;display:block\">");
             }
         
             else
             {
            	 out.println("<table class=\"table  table-striped table-bordered table-hover table-condensed\" id="+"\""+request.getAttribute("tableid").toString()+"\" "+"style=\"max-height:450px;overflow:auto;width:95%;display:block\">");
             }
         %>
         
      
           
             <tr  style="width: 100%">
             
             <% 
             
             System.out.println(" ---------------------------- Now starts result.jsp ------------------------ ");
             
                   ArrayList<String> rnas = (ArrayList<String>)request.getAttribute("searched_micro_rnas");
                
             System.out.println("Searched Rnas : "+rnas);
             
             if(request.getAttribute("tableid") == null)
        	 {           
                 for(String st : rnas)
                 {
          	 
                	 String tochklink = st;
                		 
                	 if(st.contains("("))
                	 {
                		 tochklink = st.substring(0,st.indexOf("(")).trim();	 
                	 }
            	 
                	 String tolink = new DbCommands().getaccessurlname(tochklink.trim());
                	 
                	 if(st.equals("Searched Dataset Name"))
                	 {
                		 String chk = "<td  class='header' style='text-align:center'> "+"Dataset"+"</td>";
             	         out.print(chk);
                	 }
                	 
                	 else
                	 {
                		
                	    String chk ="<td class='header'  style='text-align:center;'><a href='"+tolink+"' target=_blank"+"> <strong>"+st+"</strong> </a> </td>";
                	  
                	    out.print(chk);
                	 }
                	 
                   }	 
                 }
             
             
             else
             {
            	 for(String st : rnas)
                 {
          	 
                	 
                	 if(st.equals("Searched Dataset Name"))
                	 {
                		 String chk = "<td  class='header' style='text-align:center'> "+"Dataset"+"</td>";	 
                 	     out.print(chk);
                	 }
                	 
                	 else
                	 {                		
                	    String chk ="<td class='header'  style='text-align:center;'>"+st+" </td>";	  
                	    out.print(chk);
                	 }
                	 
                   }	 
             }
                 
                 System.out.println("For ended in jsp page");
               
             %>
             
             </tr>        
            
              
              <c:forEach var="result" items="${requestScope.result_list}">
               
               <tr style="width:100%">
                 <c:forEach var="intval" items="${result}">
                       <td class="col-xs-1">
                         <c:out value="${intval}"></c:out>
                         </td>
                    
               
                 </c:forEach>
               </tr>
             </c:forEach>
             
          </table>   
    </c:when>
   
    <c:otherwise>
  
  </div>        
          
  <div class="alert alert-danger alert-dismissible" role="alert" style="width: 90%;margin-left: auto;margin-right: auto;">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4><strong><i class="fa fa-times"></i> &nbsp;  No records found!</strong></h4> No search results were found as per your search criteria.
</div>
          
       
</c:otherwise>
</c:choose>
       
       
<c:out value="${morphesus_file}"/>

   
          
     
                 

        
    


 
 

 

             
