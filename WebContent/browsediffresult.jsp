
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList"%>
<%@page import="com.pmirexat.nabi.dao.DbCommands"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PmiRExAt@NABI</title>

  <script src="designs/js/jquery-1.11.1.min.js"></script> <!-- jquery added -->
     
      <script src="Bootstrap/js/bootstrap.min.js"></script>  <!--  bootstrap js added -->

  <link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
       
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
   
   <script>
   
   jQuery(function ($) 
   		{
   	
   	//  -----------------------  for search request Chart  ----------------------------- // 
   	
   	var formrequest = $('#Request_Search');
   	
  	$('.modal').on('hidden.bs.modal', function()
			{
		
		
	    /* $(this).find('form')[0].reset(); */
	
	    var formtoreset = $('#Request_Search');''
	    
	  formtoreset.find('input[type="text"],input[type="email"],textarea,select').val('');
	    
	   $("#alertmessage").hide();
	      $("#querysuccess").hide();
	    
	});
	
   	
   	function validate_requestsearch()
   	{
   		
   		
   		 //Code for checking  that atleast one of the first 2 value is selected
   		 
   		 //  --------------------- My code  check ---------------- /
   		 
   		  /*  var sequence = $("#miRSequence").val(); */
   		  
   		   var miRname = $("#enteredmiR").val();
   		  
   		   var sequence = $("#miRSequence").val();
   		   
   		   var requiredflag = true;
   		   
   		  
   		   
   		   if (!miRname.trim() && !sequence.trim()) {
   			    // is empty or whitespace
   			     requiredflag =false;
   			}
   		   
   		   var species = $("#species").val();
   		   
   		   if(!species.trim())
   		   {
   			 
   			   requiredflag =false;
   		   }
   		   
   		   var email = $("#emailid").val();
   		   
   		   if(!email.trim())
   			   {
   			      requiredflag =false;
   			   }
   		   
   		   if(requiredflag == false)
   			   {
   			      $("#alertmessage").show();
   			      $("#querysuccess").hide();	
   			      
   			   }
   		   
   		   
   		   // ----------------------------------------------- /
   		 
   		 // ---------------------- My code check 2 ----------------//
   		 
   		 return requiredflag;
   			
   	}
   	
   	
   	formrequest.submit(function ()
   			{
   		
   		   
   		
   			 var validationstatus = validate_requestsearch();		
   			 
   			
   			 
   			 if(validationstatus == false)
   				 {
   				   return false;
   				 }
   			 
   			 $.ajax({
   					type: formrequest.attr('method'),
   					url: formrequest.attr('action'),
   					data: formrequest.serialize(),
   					success:  function (data)
   					          {		
   						        
   								 $("#querysuccess").show(); 
   							  $("#querysuccess").hide();
   			                  }
   			       });
   			 
   			  return false;
   			});
   	
   	
   	
   	
   	// ---------------------------------------------------------------- //
   	
   });
   </script>
   
   <style>
       .navbar a:hover { color: #428bca !important; }
       
       .nav>li>a
		{
			padding-left: 10px;
			}
    
   </style>
   

</head>
<body>


<div class="container">

   <header class="navbar navbar-default " role="banner">
        <div class="container ">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                 <a class="navbar-brand" href="index.html">  <span class="glyphicon glyphicon-home"></span> PmiRExAt </a>
            </div>
            <nav class="collapse navbar-collapse  bs-navbar-collapse" role="navigation">
       <ul class="nav navbar-nav ">
       
         <li><a href="about.html"> <i class="fa fa-users"></i>
          About </a></li>
           <li><a href="datashow.html"> <i class="glyphicon glyphicon-book"></i>
          Related Data </a></li>
          
           <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Browse <b class="caret"></b></a>
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="browse.html">By Species</a></li>
                    <li><a href="crossspecies.html">By Across Species</a></li> 
                     <li><a href="bylatestpublications.html">By Latest Publications</a></li>     
                     <li><a href="tissuebrowse.html">By Tissue Preferential</a></li>    
                      <li><a href="diffexpbrowse.html">By Differential Expression</a></li>                 
           </ul>
           </li>
           
           <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search"></i>Search <b class="caret"></b></a>
          
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="searchdb.html">Expression Search</a></li>
                    <li><a href="tissuespecific.html"> Tissue preferential expression in species (Foldchange and ROKU)</a></li>   
                    <li><a href="tissuepreferentialde.html"> Tissue preferential expression in species (edgeR)</a></li>                           
          			 <li><a href="diffexpsearch.html">Differential Expression</a></li>
           </ul>
           </li>
           
          
           
         <li class = "dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search-plus"></i> Custom Search <b class="caret"></b></a>
           <ul class="dropdown-menu" style="background-color: white;">
                    <li><a href="file.html">miRNA Profiling</a></li>
                    <li><a href="checkstatus.html">Search miRNA  profiling results</a></li>  
                    <li><a href="dsupload.jsp">Dataset Profiling</a></li>                
           </ul>
          </li> 
            <li ><a href="downloads.html"><span class="glyphicon glyphicon-download-alt"></span> Downloads    </a></li>
        
         <li ><a href="api.html">PmiRExAt API</a></li>
        
         <li ><a href="contactus.html"><span class="glyphicon glyphicon-earphone"></span> Contact Us    </a></li>

         <li ><a href="support.html"><span class="glyphicon glyphicon-download-alt"></span>   Support </a></li> 
     
      </ul>
 
                 
        
    
            </nav>
        </div>
    </header>

 <p>  Differential Expression" on the basis of edgeR calculation between <c:out value="${Dataset1}"/> and <c:out value="${Dataset2}"/> .
 </p>

 <table id="table1" class="table table-striped table-bordered table-hover">
             <c:forEach var="result" items="${requestScope.results}">

               <tr style="width:100%">

                 <c:forEach var="intval" items="${result}">
                       <td class="col-xs-1">
                         <c:out value="${intval}"></c:out>
                         </td>
                 </c:forEach>
                  </tr>
                 </c:forEach>


</table>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Enter your search parameters</h4>
      </div>
      <div class="modal-body">
      
    <form id="Request_Search" action="customsearch" method="post" class="col-md-12">
    
    
    <div class="form-group">
        <input type="text" class="form-control" name="miRSequence" id="miRSequence" placeholder="Enter miR Sequence">
    </div>
    
    <div class="form-group">
        <input type="text" id="enteredmiR" class="form-control"  name="enteredmiR"  placeholder="Enter miR Name">
    </div>
    
    <div class="form-group">
        <input type="text" class="form-control" name="dataset1" id="dataset1" placeholder="Enter dataset">
    </div>
    
       <div class="form-group">
        <input type="text" class="form-control" name="species" id="species" placeholder="Enter Species">
    </div>
    
    
    
     <div class="form-group">
        <textarea class="form-control" name="details"  placeholder="Enter other details regarding search"></textarea>
    </div>
    
       <div class="form-group">
        <input type="text" class="form-control" name="emailid" id="emailid" placeholder="Enter emailid so we can send you outputs">
    </div>
        
    <div class="form-group">
        <input type="submit" value="Submit for Search" class="btn btn-primary btn-lg btn-block"></input>
        
    </div>
    
           <div id = "querysuccess" class="alert alert-success alert-dismissible" role="alert" style="width: 100%;margin-right: auto;display:none"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong> <i class="fa fa-check-circle-o"></i>&nbsp; Your Search Request Sent</strong> 
         <p> &nbsp;  We will process your request and will get back to you.
  
    </p> </div>
    
     <div id="alertmessage" class="alert alert-danger alert-dismissible" role="alert" style="width: 100%;margin-left: auto;margin-right: auto;display:none">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4><strong><i class="fa fa-times"></i> You missed something</strong></h4>Either miRsequence or miRname should be entered and  Species,emailid's are compulsory field and 
</div>
    
     </form>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

Quick Start Guide for PmiRExAt: Coming Soon.


</div> <!-- container -->
<div style="height: 50px"></div>
      
<div style="height: 50px"></div>
      
    <div class="navbar navbar-default navbar-fixed-bottom" style="margin-top:3px;height:30px;">
       <div class="container">
       <div class="row">
          <div class="col-md-4 col-xs-7">
                <p class="navbar-text text-center"> Copyright <i class="fa fa-copyright"></i>
 2014  NABI </p> 
          </div>
          <div class = "col-md-6">
              
          </div>
          <div class = "col-md-2 col-xs 5">
             <p class="navbar-text text-right"> Beta Version </p>                  
          </div>        
       </div>      
       </div>
    </div>  <!--  navbar for the footer ends -->
</body>
</html>
